package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtilTest {

    @Test
    public void normalizeDate() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();

        DateUtil.normalizeDate(cal);
        Assertions.assertEquals(0, cal.get(Calendar.HOUR_OF_DAY));
        Assertions.assertEquals(0, cal.get(Calendar.MINUTE));
        Assertions.assertEquals(0, cal.get(Calendar.SECOND));
        Assertions.assertEquals(0, cal.get(Calendar.MILLISECOND));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.normalizeDate(null));

        DateUtil.normalizeDateDayEnd(cal);
        Assertions.assertEquals(23, cal.get(Calendar.HOUR_OF_DAY));
        Assertions.assertEquals(59, cal.get(Calendar.MINUTE));
        Assertions.assertEquals(59, cal.get(Calendar.SECOND));
        Assertions.assertEquals(999, cal.get(Calendar.MILLISECOND));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.normalizeDateDayEnd((Calendar) null));

        Date dateDayEnd = DateUtil.normalizeDateDayEnd(date);
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.normalizeDateDayEnd((Date) null));
        Assertions.assertEquals(cal.getTime(), dateDayEnd);
    }

    @Test
    public void addDate() {
        Date today = new Date();
        Date dateThirtyDaysFromNow = DateUtil.addDays(today, 30);
        long diffInMillies = Math.abs(today.getTime() - dateThirtyDaysFromNow.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        Assertions.assertEquals(30, diff);
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.addDays(null, 30));

        Date dateSixMonthsFromNow = DateUtil.addMonths(today, 6);
        long monthsBetween = ChronoUnit.MONTHS.between(
                YearMonth.from(today.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()),
                YearMonth.from(dateSixMonthsFromNow.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
        Assertions.assertEquals(6, Math.abs(monthsBetween));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.addMonths(null, 6));
    }

    @Test
    public void diffInDays() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        ZonedDateTime honoluluTime = ZonedDateTime.parse("2021-03-14 00:00:00 Pacific/Honolulu", formatter);
        ZonedDateTime kritimatiTime = ZonedDateTime.parse("2021-03-16 23:59:59 Pacific/Kiritimati", formatter);

        Period diffPeriod = Period.between(honoluluTime.toLocalDate(), kritimatiTime.toLocalDate());
        Assertions.assertEquals(diffPeriod.getDays(), DateUtil.calculateDaysDiff(new Date(honoluluTime.toInstant()
                .toEpochMilli()), new Date(kritimatiTime.toInstant().toEpochMilli())));
    }

    @Test
    public void parseDate() {
        String format = "dd/MM/yyyy HH:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        String date = "24/02/2018 20:14:57";

        final Date nonGmtParsedDate2 = DateUtil.parseDate(date, format);
        ZonedDateTime zonedDateTime = LocalDateTime.parse(date, formatter).atZone(ZoneId.systemDefault());
        Assertions.assertEquals(nonGmtParsedDate2.getTime(), zonedDateTime.toInstant().toEpochMilli());
        Assertions.assertEquals(DateUtil.formatDate(nonGmtParsedDate2, format), date);
        Assertions.assertNull(DateUtil.parseDate(null, format));
        Assertions.assertNull(DateUtil.formatDate(null, format));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.parseDate(date, null));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.formatDate(nonGmtParsedDate2, null));

        final Date nonGmtParsedDate = DateUtil.parseDate(date, format, false);
        Assertions.assertEquals(nonGmtParsedDate.getTime(), zonedDateTime.toInstant().toEpochMilli());
        Assertions.assertEquals(DateUtil.formatDate(nonGmtParsedDate, format, false), date);
        Assertions.assertNull(DateUtil.parseDate(null, format, false));
        Assertions.assertNull(DateUtil.formatDate(null, format, false));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.parseDate(date, null, false));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.formatDate(nonGmtParsedDate, null, false));

        final Date gmtParsedDate = DateUtil.parseDate(date, format, true);
        zonedDateTime = LocalDateTime.parse(date, formatter).atZone(ZoneId.of("GMT"));
        Assertions.assertEquals(gmtParsedDate.getTime(), zonedDateTime.toInstant().toEpochMilli());
        Assertions.assertEquals(DateUtil.formatDate(gmtParsedDate, format, true), date);
        Assertions.assertNull(DateUtil.parseDate(null, format, true));
        Assertions.assertNull(DateUtil.formatDate(null, format, true));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.parseDate(date, null, true));
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.formatDate(gmtParsedDate, null, true));

        final ZoneId manilaZone = ZoneId.of("Asia/Manila");
        final TimeZone manilaTimeZone = TimeZone.getTimeZone(manilaZone);
        final Date manilaParsedDate = DateUtil.parseDate(date, format, manilaTimeZone);
        zonedDateTime = LocalDateTime.parse(date, formatter).atZone(manilaZone);
        Assertions.assertEquals(manilaParsedDate.getTime(), zonedDateTime.toInstant().toEpochMilli());
        Assertions.assertEquals(DateUtil.formatDate(manilaParsedDate, format, manilaTimeZone), date);
        Assertions.assertNull(DateUtil.parseDate(null, format, manilaTimeZone));
        Assertions.assertEquals(DateUtil.parseDate(date, format, null), DateUtil.parseDate(date, format));
        Assertions.assertNull(DateUtil.formatDate(null, format, manilaTimeZone));
        Assertions.assertEquals(DateUtil.formatDate(manilaParsedDate, format, null),
                DateUtil.formatDate(manilaParsedDate, format));
        Assertions.assertThrows(NullPointerException.class, () ->
                DateUtil.parseDate(date, null, manilaTimeZone));
        Assertions.assertThrows(NullPointerException.class, () ->
                DateUtil.formatDate(manilaParsedDate, null, manilaTimeZone));
    }

    @Test
    public void parseAndFormatXmlDate() {
        Date now = new Date();

        String date = new SimpleDateFormat("yyyy-MM-ddXXX").format(now);
        String time = new SimpleDateFormat("HH:mm:ss.SSSXXX").format(now);
        String datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(now);

        Date parsedDateTime = DateUtil.parseXMLDateTime(datetime);
        Assertions.assertEquals(date, DateUtil.formatXMLDate(parsedDateTime));
        Assertions.assertEquals(time, DateUtil.formatXMLTime(parsedDateTime));
        Assertions.assertEquals(datetime, DateUtil.formatXMLDateTime(parsedDateTime));
        Assertions.assertNull(DateUtil.formatXMLDate(null));
        Assertions.assertNull(DateUtil.formatXMLTime(null));
        Assertions.assertNull(DateUtil.formatXMLDateTime(null));

        Assertions.assertEquals(now, parsedDateTime);
        Assertions.assertNull(DateUtil.parseXMLDateTime(null));
        String invalidDatetime = "2021-13-16T16:11:61.126Z";
        Assertions.assertThrows(IllegalArgumentException.class, () -> DateUtil.parseXMLDateTime(invalidDatetime));

        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(now);
        dateCal.set(Calendar.HOUR_OF_DAY, 0);
        dateCal.set(Calendar.MINUTE, 0);
        dateCal.set(Calendar.SECOND, 0);
        dateCal.set(Calendar.MILLISECOND, 0);
        Assertions.assertEquals(dateCal.getTime(), DateUtil.parseXMLDate(date));
        Assertions.assertNull(DateUtil.parseXMLDate(null));
        String invalidDate = "2021-13-16Z";
        Assertions.assertThrows(IllegalArgumentException.class, () -> DateUtil.parseXMLDate(invalidDate));

        Calendar timeCal = Calendar.getInstance();
        timeCal.setTimeZone(TimeZone.getTimeZone("GMT"));
        timeCal.setTime(now);
        timeCal.set(Calendar.YEAR, 1970);
        timeCal.set(Calendar.MONTH, Calendar.JANUARY);
        timeCal.set(Calendar.DAY_OF_MONTH, 1);

        Assertions.assertEquals(timeCal.getTime(), DateUtil.parseXMLTime(time));
        Assertions.assertNull(DateUtil.parseXMLTime(null));
        String invalidTime = "16:11:61.126Z";
        Assertions.assertThrows(IllegalArgumentException.class, () -> DateUtil.parseXMLTime(invalidTime));
    }

}
