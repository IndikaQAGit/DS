package com.modirum.ds.utils;

import com.modirum.ds.utils.model.Dog;
import com.modirum.ds.utils.model.Person;
import com.modirum.ds.utils.model.Employee;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ObjectDiffTest {

    @Test
    public void objectDiff() {
        List<ObjectDiff> diffList = null;

        // Not applicable to different classes
        diffList = ObjectDiff.diff(Person.builder().name("John").age(25).build(), new Dog("Odie", 5));
        Assertions.assertTrue(diffList.isEmpty());

        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).build(),
                Person.builder().name("Jane").age(22).build());
        Assertions.assertFalse(diffList.isEmpty());

        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).aliases(Arrays.asList("J", "Johnson")).build(),
                Person.builder().name("John").age(25).aliases(Arrays.asList("J", "Johnson")).build());
        Assertions.assertTrue(diffList.isEmpty());

        // TODO: is this expected behavior for lists containing the same strings but on different order
        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).aliases(Arrays.asList("J", "Johnson")).build(),
                Person.builder().name("John").age(25).aliases(Arrays.asList("Johnson", "J")).build());
        Assertions.assertFalse(diffList.isEmpty());

        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("food", "sinigang");
                    put("flower", "sampaguita");
                }}).build(),
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("flower", "sampaguita");
                    put("food", "sinigang");
                }}).build());
        Assertions.assertTrue(diffList.isEmpty());

        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("food", "sinigang");
                    put("flower", "sampaguita");
                }}).build(),
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("food", "sinigang");
                    put("flower", "sampaguita");
                }}).build());
        Assertions.assertTrue(diffList.isEmpty());

        diffList = ObjectDiff.diff(null, null);
        Assertions.assertTrue(diffList.isEmpty());

        // null parameters are treated as same class as non-null object
        diffList = ObjectDiff.diff(null, Person.builder().name("John").build());
        Assertions.assertFalse(diffList.isEmpty());
        diffList = ObjectDiff.diff(Person.builder().name("John").build(), null);
        Assertions.assertFalse(diffList.isEmpty());

        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).build(),
                Employee.builder().name("John").age(22).build());
        Assertions.assertFalse(diffList.isEmpty());
    }

    private void checkMapContainsKeyAndValue(Map<String, Object> map, String key, Object value) {
        Assertions.assertNotNull(map);
        Assertions.assertTrue(map.containsKey(key));
        Assertions.assertEquals(value, map.get(key));
    }

    private void checkMapNotContainsKey(Map<String, Object> map, String key) {
        Assertions.assertNotNull(map);
        Assertions.assertFalse(map.containsKey(key));
    }

    @Test
    public void getProperties() {
        Person person = Person.builder().name("John").age(25).build();
        Map<String, Object> personProperties = ObjectDiff.properties(person, false);
        checkMapContainsKeyAndValue(personProperties, "name", "John");
        checkMapContainsKeyAndValue(personProperties, "age", 25);
        checkMapContainsKeyAndValue(personProperties, "aliases", null);
        checkMapContainsKeyAndValue(personProperties, "favorites", null);

        personProperties = ObjectDiff.properties(person, true);
        checkMapContainsKeyAndValue(personProperties, "name", "John");
        checkMapContainsKeyAndValue(personProperties, "age", 25);
        checkMapNotContainsKey(personProperties, "aliases");
        checkMapNotContainsKey(personProperties, "favorites");

        personProperties = ObjectDiff.properties(null, false);
        Assertions.assertTrue(personProperties.isEmpty());
        personProperties = ObjectDiff.properties(null, true);
        Assertions.assertTrue(personProperties.isEmpty());
    }

    @Test
    public void createSimpleDiffString() {
        List<ObjectDiff> diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("food", "sinigang");
                    put("flower", "sampaguita");
                }}).build(),
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("flower", "sampaguita");
                    put("food", "sinigang");
                }}).build());
        Assertions.assertEquals("", ObjectDiff.createSimpleDiffString(diffList));

        diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("food", "sinigang");
                    put("flower", "sampaguita");
                }}).build(),
                Person.builder().name("John").age(25).favorites(new HashMap<String, String>() {{
                    put("flower", "sampaguita");
                    put("food", "adobo");
                }}).build());
        String diffString = ObjectDiff.createSimpleDiffString(diffList);
        Assertions.assertNotEquals("", diffString);

        diffString = ObjectDiff.createSimpleDiffString(diffList, new HashSet<String>() {{
            add("favorites");
        }});
        Assertions.assertTrue(diffString.contains("#"));

        Assertions.assertThrows(NullPointerException.class, () -> ObjectDiff.createSimpleDiffString(null));
        Assertions.assertThrows(NullPointerException.class, () -> ObjectDiff.createSimpleDiffString(null,
                new HashSet<String>()));
    }

    @Test
    public void removeDiff() {
        List<ObjectDiff> diffList = ObjectDiff.diff(
                Person.builder().name("John").age(25).build(),
                Person.builder().name("Jane").age(22).build());
        Assertions.assertNotNull(ObjectDiff.removeDiffByProperty(diffList, "name"));
        Assertions.assertNull(ObjectDiff.removeDiffByProperty(diffList, "name"));
        Assertions.assertNull(ObjectDiff.removeDiffByProperty(null, "name"));
    }

}
