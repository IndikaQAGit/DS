/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 24.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils.db;

import com.modirum.ds.utils.EqualHashUtil;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class PersistableClass implements Persistable, Cloneable, java.io.Serializable {
    public int hashCode() {
        return EqualHashUtil.hashCode(this);
    }

    public boolean equals(Object o) {
        return EqualHashUtil.equals(this, o);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
