/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 23.05.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils.sms;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Setting;
import com.modirum.ds.utils.SettingService;

/**
 * @author andri
 * Implementation of CardBoardFish
 * Settings applicable
 * cardboardfish.smsurl - required service endpoint url
 * cardboardfish.username - required
 * cardboardfish.password - required
 * cardboardfish.senderid - optional default source
 * cardboardfish.st - optional
 * cardboardfish.dc - optional
 * cardboardfish.v - validity optional (minutes)
 */
public class SMSServiceCardBoardFish implements SMSService {
    Charset msgEnc = StandardCharsets.ISO_8859_1;

    protected static Logger log = LoggerFactory.getLogger(SMSServiceCardBoardFish.class);


    String getSetting(String key, SettingService se) {
        Setting s = se.getSettingByKey(key);

        return s != null ? s.getValue() : null;
    }

    /* (non-Javadoc)
     * @see com.modirum.ds.utils.sms.SMSService#sendSMS(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, SettingService)
     */
    public boolean sendSMS(String dest, String source, String content, Integer validityMins, SettingService se)
            throws Exception {

        HttpsJSSEClient httpClient = new HttpsJSSEClient();
        httpClient.init(null, "X.509", "TLS", "true", null, null, null, 15000, 30000, true);
        StringBuilder sendContent = new StringBuilder(300);
        sendContent.append("_charset_=" + java.net.URLEncoder.encode(msgEnc.name(), msgEnc.name()) + "&");
        sendContent.append("S=H");
        sendContent.append("&UN=" + java.net.URLEncoder.encode(getSetting("cardboardfish.username", se), msgEnc.name()));

        String encPa = java.net.URLEncoder.encode(getSetting("cardboardfish.password", se), msgEnc.name());
        sendContent.append("&P=" + encPa);
        dest = Misc.replace(dest, "+", "");
        dest = Misc.replace(dest, " ", "");
        sendContent.append("&DA=" + java.net.URLEncoder.encode(dest, msgEnc.name()));
        if (source == null) {
            source = getSetting("cardboardfish.senderid", se);
        }
        if (Misc.isNullOrEmpty(source)) {
            source = "CBF";
        }

        sendContent.append("&SA=" + java.net.URLEncoder.encode(source, msgEnc.name()));
        sendContent.append("&M=" + java.net.URLEncoder.encode(content, msgEnc.name()) + "");
        String soureAddrTon = getSetting("cardboardfish.st", se);
        if (Misc.isInt(soureAddrTon)) {
            sendContent.append("&ST=" + java.net.URLEncoder.encode(soureAddrTon, msgEnc.name()) + "");
        }

        String coding = getSetting("cardboardfish.dc", se);
        if (Misc.isNotNullOrEmpty(coding)) {
            sendContent.append("&DC=" + java.net.URLEncoder.encode(coding, msgEnc.name()) + "");
        }

        if (validityMins != null && validityMins > 0) {
            sendContent.append("&V=" + validityMins);
        } else {
            String validity = getSetting("cardboardfish.v", se);
            if (Misc.isInt(validity)) {
                sendContent.append("&V=" + java.net.URLEncoder.encode(validity, msgEnc.name()) + "");
            }
        }
        String sendStr = sendContent.toString();
        byte[] sendBytes = sendStr.getBytes(msgEnc);

        if (log.isDebugEnabled()) {
            log.debug("SMS send " + Misc.replace(sendStr, encPa, Misc.mask(encPa, 1)));
        }

        byte[] result = httpClient.post(getSetting("cardboardfish.smsurl", se), "application/x-www-form-urlencoded", sendBytes);
        String rsContent = new String(result, msgEnc);
        if (log.isDebugEnabled()) {
            log.debug("SMS response " + rsContent);
        }

        String[] parts = Misc.split(rsContent, " ");
        if (parts != null && parts.length >= 2) {
            String p2 = parts[1];
            if (p2 != null) {
                p2 = p2.trim();
            }
            if ("OK".equals(parts[0]) && Misc.isInt(p2) && Misc.parseInt(p2) > 0) {
                return true;
            }

            if (Misc.isInt(p2) && Misc.parseInt(p2) == -15) {
                throw new RuntimeException("SMS Destination invalid or not covered");
            }
            if (Misc.isInt(p2) && Misc.parseInt(p2) == -20) {
                throw new RuntimeException("SMS System error, retry");
            } else {
                throw new RuntimeException("Unrecognized gateway response: " + rsContent);
            }
        }

        return false;
    }

}
