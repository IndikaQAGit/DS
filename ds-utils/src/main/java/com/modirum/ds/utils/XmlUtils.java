/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.01.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.validation.Schema;

public class XmlUtils {

    static XMLInputFactory xif = XMLInputFactory.newFactory();

    static {
        xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
    }

    public static DocumentBuilderFactory getXESafeDocumentBuilderFactory() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // XE prevention
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        dbf.setExpandEntityReferences(false);
        dbf.setXIncludeAware(false);
        dbf.setNamespaceAware(true);

        return dbf;
    }

    public static XMLStreamReader makeJaxbXESafeReader(InputStream is) throws Exception {
        XMLStreamReader xsr = xif.createXMLStreamReader(is);
        return xsr;
    }

    public static class JaxbHelp<T> {
        JAXBContext context = null;
        DocumentBuilder docBuilder = null;
        GenericPool<Marshaller> marshallers = new GenericPool<Marshaller>();
        GenericPool<Unmarshaller> unmarshallers = new GenericPool<Unmarshaller>();
        Class<T> type;
        boolean formatted = false;
        String encoding = "UTF-8";
        DocumentBuilderFactory dbf;
        Schema schema;

        public JaxbHelp(Class<T> type) throws Exception {
            this.type = type;
            context = JAXBContext.newInstance(type);
            dbf = XmlUtils.getXESafeDocumentBuilderFactory();
            docBuilder = dbf.newDocumentBuilder();
        }

        public byte[] marshal(T pojo) throws Exception {
            Marshaller marshaller = createMarshaller();
            ByteArrayOutputStream bos = new ByteArrayOutputStream(4096);
            marshaller.marshal(pojo, bos);
            marshallers.recycle(marshaller);
            return bos.toByteArray();
        }

        public T unmarshal(InputStream is) throws Exception {
            Unmarshaller umarshaller = createUnmarshaller();
            @SuppressWarnings("unchecked")
            T jaxbPojo = (T) umarshaller.unmarshal(makeJaxbXESafeReader(is));
            unmarshallers.recycle(umarshaller);
            return jaxbPojo;
        }

        public Marshaller createMarshaller() throws Exception {
            Marshaller m = marshallers.reuse();
            if (m == null) {
                m = context.createMarshaller();
                m.setProperty(Marshaller.JAXB_ENCODING, encoding);
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatted);
                if (schema != null) {
                    m.setSchema(schema);
                }
            }

            return m;
        }

        public Unmarshaller createUnmarshaller() throws Exception {
            Unmarshaller m = unmarshallers.reuse();
            if (m == null) {
                m = context.createUnmarshaller();
                if (schema != null) {
                    m.setSchema(schema);
                }
                //m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            }

            return m;
        }

        public boolean isFormatted() {
            return formatted;
        }

        public void setFormatted(boolean formatted) {
            this.formatted = formatted;
        }

        public String getEncoding() {
            return encoding;
        }

        public void setEncoding(String encoding) {
            this.encoding = encoding;
        }

        public Schema getSchema() {
            return schema;
        }

        public void setSchema(Schema schema) {
            this.schema = schema;
            if (schema != null) {
                dbf.setSchema(schema);
                try {
                    docBuilder = dbf.newDocumentBuilder();
                } catch (Exception dc) {
                    throw new RuntimeException(dc);
                }
            }
        }
    }

}
