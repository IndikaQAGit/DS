package com.modirum.ds.utils.db;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.SQLServer2012Dialect;
import org.hibernate.engine.jdbc.dialect.internal.StandardDialectResolver;
import org.hibernate.engine.jdbc.dialect.spi.DialectResolutionInfo;

/**
 * A workaround class that helps hibernate identify SQL Server version 12 correctly
 * Starting from hibernate version 5.2.0 this issue was fixed, however it requires java 1.8
 * Currently this project uses java 1.7, therefore will wait until project is switched to java 1.8
 * then hibernate can be updated to >= 5.2.0 and this class deprecated.
 */
public class CustomDialectResolver extends StandardDialectResolver {

    @Override
    public Dialect resolveDialect(DialectResolutionInfo info) {
        final String databaseName = info.getDatabaseName();
        if (databaseName.startsWith("Microsoft SQL Server")) {
            final int majorVersion = info.getDatabaseMajorVersion();
            switch (majorVersion) {
                case 11:
                case 12:
                    return new SQLServer2012Dialect();
            }
        }
        return super.resolveDialect(info);
    }
}
