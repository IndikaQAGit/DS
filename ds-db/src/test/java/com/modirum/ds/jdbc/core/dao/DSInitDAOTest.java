package com.modirum.ds.jdbc.core.dao;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.jdbc.core.model.DSInit;
import com.modirum.ds.jdbc.test.support.RunWithDB;
import com.modirum.ds.jdbc.test.support.TestEmbeddedDbUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;

@RunWithDB
public class DSInitDAOTest {

    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    private static DSInitDAO dsInitDAO;

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        dsInitDAO = new DSInitDAO();
        dsInitDAO.setDataSource(dataSource);
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_init;"})
    public void getByInitKey_success() {
        DSInit expectedDsInit = new DSInit();
        expectedDsInit.setInitKey("key");
        expectedDsInit.setInitValue("value");
        expectedDsInit.setLastModifiedBy("admin");
        expectedDsInit.setLastModifiedDate(new Date());
        dsInitDAO.insert(expectedDsInit);

        DSInit actual = dsInitDAO.getByInitKey("key");
        Assertions.assertNotNull(actual);
        Assertions.assertEquals("value", actual.getInitValue());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_init;"})
    public void insert_success() {
        DSInit expectedDsInit = new DSInit();
        expectedDsInit.setInitKey("insertKey");
        expectedDsInit.setInitValue("insertValue");
        expectedDsInit.setLastModifiedBy("admin");
        expectedDsInit.setLastModifiedDate(new Date());

        dsInitDAO.insert(expectedDsInit);

        DSInit actual = dsInitDAO.getByInitKey("insertKey");
        Assertions.assertNotNull(actual);
        Assertions.assertEquals("insertValue", actual.getInitValue());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_init;"})
    public void update_success() {
        DSInit expectedDsInit = new DSInit();
        expectedDsInit.setInitKey("insertKey");
        expectedDsInit.setInitValue("insertValue");
        expectedDsInit.setLastModifiedBy("admin");
        expectedDsInit.setLastModifiedDate(new Date());
        dsInitDAO.insert(expectedDsInit);

        expectedDsInit.setInitValue("updatedValue");
        dsInitDAO.update(expectedDsInit);

        DSInit actual = dsInitDAO.getByInitKey("insertKey");
        Assertions.assertNotNull(actual);
        Assertions.assertEquals("updatedValue", actual.getInitValue());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_init;"})
    public void exists_success() {
        DSInit expectedDsInit = new DSInit();
        expectedDsInit.setInitKey("insertKey");
        expectedDsInit.setInitValue("insertValue");
        expectedDsInit.setLastModifiedBy("admin");
        expectedDsInit.setLastModifiedDate(new Date());
        dsInitDAO.insert(expectedDsInit);

        boolean isExists = dsInitDAO.exists("insertKey");
        Assertions.assertTrue(isExists);
    }
}
