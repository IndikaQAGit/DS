package com.modirum.ds.jdbc.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HsmDevice {

    private Long id;
    private String name;
    private String className;
    private String status;

}
