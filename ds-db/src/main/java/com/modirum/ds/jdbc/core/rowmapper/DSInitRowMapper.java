package com.modirum.ds.jdbc.core.rowmapper;

import com.modirum.ds.jdbc.core.model.DSInit;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DSInitRowMapper implements RowMapper<DSInit> {
    @Override
    public DSInit mapRow(ResultSet rs, int i) throws SQLException {
        DSInit dsInit = new DSInit();
        dsInit.setInitKey(rs.getString("initKey"));
        dsInit.setInitValue(rs.getString("initValue"));
        dsInit.setLastModifiedBy(rs.getString("lastModifiedBy"));
        dsInit.setLastModifiedDate(rs.getTimestamp("lastModifiedDate"));
        return dsInit;
    }
}
