package com.modirum.ds.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;

/**
 *
 * Base class for Data Access implementations. Implementing subclasses can be initialized either manually,
 * or by a Dependency Injection framework.
 * <p>
 *     For DI initialization a <code>dsDataSource</code> bean of type {@link DataSource} has to be available
 *     in the DI context.
 * </p>
 * <p>
 *     For manual initialization {@link BaseDAO#setDataSource(DataSource)} should be used after constructor.
 * </p>
 *
 * This class contains only DAO initialization (dataSource) / DI functionality.
 */
public abstract class BaseDAO extends NamedParameterJdbcDaoSupport {
    
    @Autowired
    @Qualifier("dsDataSource")
    private DataSource dsDataSource;

    @PostConstruct
    private void init() {
        // set injected datasource to superclass
        setDataSource(dsDataSource);
    }
}
