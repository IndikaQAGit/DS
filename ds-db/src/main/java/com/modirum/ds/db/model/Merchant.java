package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlType(name = "Merchant", propOrder = {"acquirerId", "country", "email", "identifier", "name", "phone", "requestorID", "status", "tdsServerProfileId", "URL"})
public class Merchant extends PersistableClass implements PaymentSystemResource {

    private Long id;
    private Integer acquirerId;
    private Long tdsServerProfileId;
    private String identifier;
    private String name;
    private String URL;
    private String phone;
    private String email;
    private String country;
    private String requestorID;
    private String status;
    private String options;
    private Integer paymentSystemId;

    @XmlAttribute()
    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    @XmlAttribute()
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAcquirerId() {
        return acquirerId;
    }

    public void setAcquirerId(Integer acquirerId) {
        this.acquirerId = acquirerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Long getTdsServerProfileId() {
        return tdsServerProfileId;
    }

    public void setTdsServerProfileId(Long tdsServerProfileId) {
        this.tdsServerProfileId = tdsServerProfileId;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String uRL) {
        URL = uRL;
    }

    public String getRequestorID() {
        return requestorID;
    }

    public void setRequestorID(String requestorID) {
        this.requestorID = requestorID;
    }

    @Override
    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
