package com.modirum.ds.db.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuditLog extends PersistableClass implements Serializable {
    private static final long serialVersionUID = 1L;
    protected Long id;
    protected Long objectId;
    protected String objectClass;
    protected Date when;
    protected String by;
    protected Long byId;
    protected String action;
    protected String details;
    protected Integer paymentSystemId;
}
