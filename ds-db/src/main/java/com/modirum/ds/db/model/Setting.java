/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 22.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import com.modirum.ds.utils.EqualHashUtil;

import java.util.Date;

public class Setting extends com.modirum.ds.utils.Setting implements Persistable {

    private static final long serialVersionUID = 1L;
    private Date lastModified;
    private String lastModifiedBy;

    public String getId() {
        return getKey();
    }

    @Override
    public boolean equals(Object o) {
        return EqualHashUtil.equals(this, o);
    }

    @Override
    public int hashCode() {
        return EqualHashUtil.hashCode(this);

    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
}
