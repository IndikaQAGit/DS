package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = " TDSServerProfile", propOrder = {"acquirerId", "name", "URL", "status", "requestorID", "operatorID", "outClientCert", "inClientCert", "SSLProto", "tdsReferenceNumberList"})
public class TDSServerProfile extends PersistableClass implements PaymentSystemResource {

    private Long id;
    //#1438 Should not be in used anymore in 3DS Messaging flow, leaving it here for ImportService compatibility.
    //Removing or updating ImportService is deferred to a separate task.
    @Deprecated
    private Integer acquirerId;
    private String name;
    private String requestorID;
    private String operatorID;
    private String URL;
    private Long outClientCert;
    private Long inClientCert;
    private String status;
    private String SSLProto;
    private String tdsReferenceNumberList;
    private Integer paymentSystemId;

    @XmlAttribute
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Should not be in used anymore in 3DS Messaging flow, leaving it here for ImportService compatibility.
     * Removing or updating ImportService is deferred to a separate task.
     *
     * @return
     */
    @Deprecated
    public Integer getAcquirerId() {
        return acquirerId;
    }

    /**
     * Should not be in used anymore in 3DS Messaging flow, leaving it here for ImportService compatibility.
     * Removing or updating ImportService is deferred to a separate task.
     *
     * @return
     */
    @Deprecated
    public void setAcquirerId(Integer acquirerId) {
        this.acquirerId = acquirerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String uRL) {
        URL = uRL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSSLProto() {
        return SSLProto;
    }

    public void setSSLProto(String sSLProto) {
        SSLProto = sSLProto;
    }

    public Long getOutClientCert() {
        return outClientCert;
    }

    public void setOutClientCert(Long outClientCert) {
        this.outClientCert = outClientCert;
    }

    public Long getInClientCert() {
        return inClientCert;
    }

    public void setInClientCert(Long inClientCert) {
        this.inClientCert = inClientCert;
    }

    public String getRequestorID() {
        return requestorID;
    }

    public void setRequestorID(String requestorID) {
        this.requestorID = requestorID;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public String getTdsReferenceNumberList() {
        return tdsReferenceNumberList;
    }

    public void setTdsReferenceNumberList(String tdsReferenceNumberList) {
        this.tdsReferenceNumberList = tdsReferenceNumberList;
    }

    @Override
    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
