package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HsmDeviceConf implements Persistable, Serializable {
    private Long hsmDeviceId;
    private Integer dsId;
    private String config;

    @Override
    public Serializable getId() {
        return new HsmDeviceConfCompositeKey(hsmDeviceId, dsId);
    }

    public static final class HsmDeviceConfCompositeKey implements Serializable {
        private Long hsmDeviceId;
        private int dsId;

        public HsmDeviceConfCompositeKey(Long hsmDeviceId, Integer dsId) {
            this.hsmDeviceId = hsmDeviceId;
            this.dsId = dsId;
        }
    }
}
