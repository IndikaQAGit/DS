package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.enums.ReportConfigKeys;

import java.util.Properties;

/**
 * Service that handles filtering fields in csv output.
 */
public class CsvFilteringService {
    private boolean isProhibitedCharacterFilteringEnabled = true;
    private String prohibitedFieldCharactersRegex = "(\\t|\\n|\\r)";
    private boolean isCsvSanitizingEnabled = true;
    private String unsafeStartingCharacters = "=+-@";
    private String csvInjectionSanitizer = "'";

    /**
     * Initializes csv settings from the given Properties.
     *
     * @param props
     */
    public void initializeFromProperties(Properties props) {
        if (props.containsKey(ReportConfigKeys.CSV_FILTERING_ENABLED)) {
            setProhibitedCharacterFilteringEnabled("true".equals(props.getProperty(ReportConfigKeys.CSV_FILTERING_ENABLED)));
        }

        if (isProhibitedCharacterFilteringEnabled()) {
            if (props.containsKey(ReportConfigKeys.CSV_PROHIBITED_CHARACTERS_REGEX)) {
                setProhibitedFieldCharactersRegex(props.getProperty(ReportConfigKeys.CSV_PROHIBITED_CHARACTERS_REGEX));
            }
        }

        if (props.containsKey(ReportConfigKeys.CSV_SANITIZING_ENABLED)) {
            setCsvSanitizingEnabled("true".equals(props.getProperty(ReportConfigKeys.CSV_SANITIZING_ENABLED)));
        }

        if (isCsvSanitizingEnabled()) {
            if (props.containsKey(ReportConfigKeys.CSV_UNSAFE_STARTING_CHARACTERS)) {
                setUnsafeStartingCharacters(props.getProperty(ReportConfigKeys.CSV_UNSAFE_STARTING_CHARACTERS));
            }
            if (props.containsKey(ReportConfigKeys.CSV_INJECTION_SANITIZER)) {
                setCsvInjectionSanitizer(props.getProperty(ReportConfigKeys.CSV_INJECTION_SANITIZER));
            }
        }
    }

    public boolean isProhibitedCharacterFilteringEnabled() {
        return isProhibitedCharacterFilteringEnabled;
    }

    private synchronized void setProhibitedCharacterFilteringEnabled(boolean isProhibitedCharacterFilteringEnabled) {
        this.isProhibitedCharacterFilteringEnabled = isProhibitedCharacterFilteringEnabled;
    }

    private synchronized void setProhibitedFieldCharactersRegex(String prohibitedFieldCharactersRegex) {
        this.prohibitedFieldCharactersRegex = prohibitedFieldCharactersRegex;
    }

    public String replaceProhibitedCharsWithSpace(String str) {
        return isProhibitedCharacterFilteringEnabled ? nullSafe(str).replaceAll(prohibitedFieldCharactersRegex, " ").replaceAll("^ +| +$|( )+", "$1") : str;
    }

    public String nullSafe(String value) {
        return value != null ? value : "";
    }

    public Object nullSafe(Object value) {
        return value != null ? value : "";
    }

    public Object[] nullSafeAndSanitizeArrayValues(Object[] arrayValues) {
        for (int i = 0; i < arrayValues.length; i++) {
            arrayValues[i] = nullSafe(arrayValues[i]);
            if (arrayValues[i] instanceof String) {
                arrayValues[i] = sanitize((String) arrayValues[i]);
            }
        }
        return arrayValues;
    }

    public boolean isCsvSanitizingEnabled() {
        return isCsvSanitizingEnabled;
    }

    private void setCsvSanitizingEnabled(boolean isCsvSanitizingEnabled) {
        this.isCsvSanitizingEnabled = isCsvSanitizingEnabled;
    }

    private void setUnsafeStartingCharacters(String unsafeStartingCharacters) {
        this.unsafeStartingCharacters = unsafeStartingCharacters;
    }

    private void setCsvInjectionSanitizer(String csvInjectionSanitizer) {
        this.csvInjectionSanitizer = csvInjectionSanitizer;
    }

    public String sanitize(String str) {
        if (isCsvSanitizingEnabled) {
            StringBuilder sb = new StringBuilder();
            if (unsafeStartingCharacters.indexOf(extractFirstCharacter(str)) != -1) {
                sb.append(csvInjectionSanitizer);
            }
            sb.append(str);
            return sb.toString();
        } else {
            return str;
        }
    }

    private char extractFirstCharacter(String field) {
        return (field != null && field.length() > 0 ? field.charAt(0) : '\0');
    }

}
