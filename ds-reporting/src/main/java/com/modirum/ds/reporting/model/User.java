package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends PersistableClass implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static User SYSTEM_USER = new User(0L, "System");

    private Long id;
    private String loginName;
    private transient String password;
    private transient String oldpasswords;
    private Date lastPassChangeDate;
    private int loginAttempts;
    private Date lockedDate;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String status;
    private String locale;
    private Set<String> roles;

    private transient String newPassword;
    private transient String newPasswordRepeat;
    private boolean forcedPasswordChange;
    private String factor2Authmethod;
    private String factor2AuthDeviceId;
    private String factor2Data1;
    private String factor2Data2;
    private String factor2Data3;
    private String factor2Data4;
    private Integer paymentSystemId;
    private boolean factor2Passed;
    private String paymentSystemName; // view model representation.

    public User(Long id, String login) {
        this.id = id;
        this.loginName = login;
    }
}
