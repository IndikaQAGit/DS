package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.KeyData;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * ResultSetMapper for KeyData entity
 */
public class KeyDataResultSetMapper implements ResultSetMapper<KeyData> {
    @Override
    public KeyData map(ResultSet resultSet) throws SQLException {
        return KeyData
                .builder()
                .id(JdbcUtils.getColumnValue(resultSet, "id", Long.class))
                .keyAlias(JdbcUtils.getColumnValue(resultSet, "keyAlias", String.class))
                .keyCheckValue(JdbcUtils.getColumnValue(resultSet, "keyCheckValue", String.class))
                .keyData(JdbcUtils.getColumnValue(resultSet, "keyData", String.class))
                .hsmDeviceId(JdbcUtils.getColumnValue(resultSet, "hsmdevice_id", Long.class))
                .wrapkeyId(JdbcUtils.getColumnValue(resultSet, "wrapkey_id", Long.class))
                .keyDate(JdbcUtils.getColumnValue(resultSet, "keyDate", Timestamp.class))
                .cryptoPeriodDays(JdbcUtils.getColumnValue(resultSet, "cryptoPeriodDays", Integer.class))
                .status(JdbcUtils.getColumnValue(resultSet, "status", String.class))
                .signedBy1(JdbcUtils.getColumnValue(resultSet, "signedby1", String.class))
                .signature1(JdbcUtils.getColumnValue(resultSet, "signedby2", String.class))
                .signedBy2(JdbcUtils.getColumnValue(resultSet, "signature1", String.class))
                .signature2(JdbcUtils.getColumnValue(resultSet, "signature2", String.class))
                .build();
    }
}
