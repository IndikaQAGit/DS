package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.TDSRecord;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * ResultSetMapper for TDSRecord
 */
public class TDSRecordResultSetMapper implements ResultSetMapper<TDSRecord> {
    @Override
    public TDSRecord map(ResultSet resultSet) throws SQLException {
        return TDSRecord
                .builder()
                .id(JdbcUtils.getColumnValue(resultSet, "id", Long.class))
                .acctNumber(JdbcUtils.getColumnValue(resultSet, "acctNumber", String.class))
                .acctNumberEnc(JdbcUtils.getColumnValue(resultSet, "acctNumberEnc", String.class))
                .acctNumberHMAC(JdbcUtils.getColumnValue(resultSet, "acctNumberHMAC", String.class))
                .acctNumberHMKid(JdbcUtils.getColumnValue(resultSet, "acctNumberHMKid", Long.class))
                .acctNumberKid(JdbcUtils.getColumnValue(resultSet, "acctNumberKid", Long.class))
                .acquirerBIN(JdbcUtils.getColumnValue(resultSet, "acquirerBIN", String.class))
                .acquirerMerchantID(JdbcUtils.getColumnValue(resultSet, "acquirerMerchantID", String.class))
                .acquirerOptions(JdbcUtils.getColumnValue(resultSet, "acquirerOptions", String.class))
                .ACSOperatorID(JdbcUtils.getColumnValue(resultSet, "ACSOperatorID", String.class))
                .ACSRefNo(JdbcUtils.getColumnValue(resultSet, "ACSRef", String.class))
                .ACSTransID(JdbcUtils.getColumnValue(resultSet, "ACSTransID", String.class))
                .ACSURL(JdbcUtils.getColumnValue(resultSet, "ACSURL", String.class))
                .authenticationMethod(JdbcUtils.getColumnValue(resultSet, "authenticationMethod", String.class))
                .authenticationType(JdbcUtils.getColumnValue(resultSet, "authenticationType", String.class))
                .authenticationValue(JdbcUtils.getColumnValue(resultSet, "authenticationValue", String.class))
                .browserIP(JdbcUtils.getColumnValue(resultSet, "browserIP", String.class))
                .browserJavaEnabled(JdbcUtils.getColumnValue(resultSet, "browserJavaEnabled", String.class))
                .browserLanguage(JdbcUtils.getColumnValue(resultSet, "browserLanguage", String.class))
                .browserUserAgent(JdbcUtils.getColumnValue(resultSet, "browserUserAgent", String.class))
                .cardExpiryDate(JdbcUtils.getColumnValue(resultSet, "cardExpiryDate", String.class))
                .deviceChannel(JdbcUtils.getColumnValue(resultSet, "deviceChannel", String.class))
                .dsRef(JdbcUtils.getColumnValue(resultSet, "dsRef", String.class))
                .errorCode(JdbcUtils.getColumnValue(resultSet, "errorCode", String.class))
                .DSTransID(JdbcUtils.getColumnValue(resultSet, "DSTransID", String.class))
                .errorDetail(JdbcUtils.getColumnValue(resultSet, "errorDetail", String.class))
                .ECI(JdbcUtils.getColumnValue(resultSet, "ECI", String.class))
                .issuerBIN(JdbcUtils.getColumnValue(resultSet, "issuerBIN", String.class))
                .issuerName(JdbcUtils.getColumnValue(resultSet, "issuerName", String.class))
                .issuerOptions(JdbcUtils.getColumnValue(resultSet, "issuerOptions", String.class))
                .localAcquirerId(JdbcUtils.getColumnValue(resultSet, "localAcquirerId", Integer.class))
                .localDateEnd(JdbcUtils.getColumnValue(resultSet, "localDateEnd", Timestamp.class))
                .localIssuerId(JdbcUtils.getColumnValue(resultSet, "localIssuerId", Integer.class))
                .localStatus(JdbcUtils.getColumnValue(resultSet, "localStatus", Short.class))
                .localDateStart(JdbcUtils.getColumnValue(resultSet, "localDateStart", Timestamp.class))
                .MCC(JdbcUtils.getColumnValue(resultSet, "MCC", String.class))
                .merchantCountry(JdbcUtils.getColumnValue(resultSet, "merchantCountry", Short.class))
                .merchantName(JdbcUtils.getColumnValue(resultSet, "merchantName", String.class))
                .merchantRiskIndicator(JdbcUtils.getColumnValue(resultSet, "merchantRiskIndicator", String.class))
                .messageCategory(JdbcUtils.getColumnValue(resultSet, "messageCategory", String.class))
                .MIProfileId(JdbcUtils.getColumnValue(resultSet, "MIProfileId", Long.class))
                .MIReferenceNumber(JdbcUtils.getColumnValue(resultSet, "MIReferenceNumber", String.class))
                .tdsTransID(JdbcUtils.getColumnValue(resultSet, "MITransID", String.class))
                .tdsServerURL(JdbcUtils.getColumnValue(resultSet, "tdsServerURL", String.class))
                .paymentSystemId(JdbcUtils.getColumnValue(resultSet, "paymentSystemId", Integer.class))
                .protocol(JdbcUtils.getColumnValue(resultSet, "protocol", String.class))
                .purchaseAmount(JdbcUtils.getColumnValue(resultSet, "purchaseAmount", Long.class))
                .purchaseCurrency(JdbcUtils.getColumnValue(resultSet, "purchaseCurrency", Short.class))
                .purchaseDate(JdbcUtils.getColumnValue(resultSet, "purchaseDate", Timestamp.class))
                .purchaseExponent(JdbcUtils.getColumnValue(resultSet, "purchaseExponent", Short.class))
                .purchaseInstalData(JdbcUtils.getColumnValue(resultSet, "purchaseInstalData", Short.class))
                .recurringExpiry(JdbcUtils.getColumnValue(resultSet, "recurringExpiry", String.class))
                .recurringFrequency(JdbcUtils.getColumnValue(resultSet, "recurringFrequency", Short.class))
                .requestorID(JdbcUtils.getColumnValue(resultSet, "requestorID", String.class))
                .requestorName(JdbcUtils.getColumnValue(resultSet, "requestorName", String.class))
                .requestorURL(JdbcUtils.getColumnValue(resultSet, "requestorURL", String.class))
                .resultsStatus(JdbcUtils.getColumnValue(resultSet, "resultsStatus", String.class))
                .SDKAppID(JdbcUtils.getColumnValue(resultSet, "SDKAppID", String.class))
                .SDKReferenceNumber(JdbcUtils.getColumnValue(resultSet, "SDKReferenceNumber", String.class))
                .SDKTransID(JdbcUtils.getColumnValue(resultSet, "SDKTransID", String.class))
                .transStatus(JdbcUtils.getColumnValue(resultSet, "transStatus", String.class))
                .transStatusReason(JdbcUtils.getColumnValue(resultSet, "transStatusReason", String.class))
                .transType(JdbcUtils.getColumnValue(resultSet, "transType", String.class))
                .build();
    }
}
