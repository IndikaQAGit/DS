package com.modirum.ds.reporting.model.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper API from ResultSet to an Object
 * @param <T>
 */
public interface ResultSetMapper<T> {
    T map(ResultSet resultSet) throws SQLException;
}
