package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.TDSMessageData;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * ResultSetMapper for TDSMessageData
 */
public class TDSMessageDataResultSetMapper implements ResultSetMapper<TDSMessageData> {

    @Override
    public TDSMessageData map(ResultSet resultSet) throws SQLException {
        return TDSMessageData
                .builder()
                .id(JdbcUtils.getColumnValue(resultSet, "id", Long.class))
                .contents(JdbcUtils.getColumnValue(resultSet, "contents", String.class))
                .tdsrId(JdbcUtils.getColumnValue(resultSet, "tdsrId", Long.class))
                .destIP(JdbcUtils.getColumnValue(resultSet, "destIP", String.class))
                .sourceIP(JdbcUtils.getColumnValue(resultSet, "sourceIP", String.class))
                .messageType(JdbcUtils.getColumnValue(resultSet, "messageType", String.class))
                .messageVersion(JdbcUtils.getColumnValue(resultSet, "messageVersion", String.class))
                .messageDate(JdbcUtils.getColumnValue(resultSet, "messageDate", Timestamp.class))
                .build();
    }
}
