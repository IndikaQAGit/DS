package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.model.Id;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.util.JdbcUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Jdbc Service for entity User
 */
public class UserJdbcService {
    public static final String USER_ID_SEQUENCE_NAME = "userId";
    private final static Logger log = LoggerFactory.getLogger(UserJdbcService.class);
    private final DataSource dataSource;

    UserJdbcService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Saves a new User to the database.
     *
     * @param user
     */
    public void save(User user) throws SQLException {
        IdService idService = ServiceLocator.getInstance().getIdService();
        Id id = idService.getNextId(USER_ID_SEQUENCE_NAME);
        user.setId(id.getSeed());
        try (Connection connection = JdbcUtils.startTransaction(dataSource)) {
            String insertUserSql =
                    "INSERT INTO ds_users (id,loginname,password,lastPassChangeDate,firstName,lastName,status,email,paymentSystemId,phone,locale)" +
                    " VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            JdbcUtils.execute(connection,
                              insertUserSql,
                              new Object[]{
                                      user.getId(),
                                      user.getLoginName(),
                                      user.getPassword(),
                                      user.getLastPassChangeDate(),
                                      user.getFirstName(),
                                      user.getLastName(),
                                      user.getStatus(),
                                      user.getEmail(),
                                      user.getPaymentSystemId(),
                                      user.getPhone(),
                                      user.getLocale()
                              });

            if (CollectionUtils.isNotEmpty(user.getRoles())) {
                for(String role : user.getRoles()) {
                    String insertRoleSql = "INSERT INTO ds_userroles(user_id,role) VALUES(?,?)";
                    JdbcUtils.execute(connection, insertRoleSql, new Object[]{user.getId(), role});
                }
            }
            JdbcUtils.txCommitAndClose(connection);
        }  catch (SQLException e) {
            log.error("Saving User encounter an error", e);
            throw e;
        }
        log.debug("Successfully saved user with username={}", user.getLoginName());
    }
}
