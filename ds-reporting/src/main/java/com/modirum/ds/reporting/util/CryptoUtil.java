package com.modirum.ds.reporting.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.util.zip.InflaterInputStream;

/**
 * Utility class for common crypto related operations.
 */
public final class CryptoUtil {
    private CryptoUtil() {
    }

    /**
     * GZIP inflates input bytes.
     * @param bytes Input data to decompress.
     * @return Decompressed data.
     */
    public static byte[] gunzip(final byte[] bytes) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            InflaterInputStream inflaterInput = new InflaterInputStream(bis);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = inflaterInput.read(buffer);
                if (read == -1) {
                    break;
                }
                bos.write(buffer, 0, read);
            }
            bos.flush();
            return bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Produces sha-512 of the input <code>value</code>.
     * @param value Original value to calculate sha-512 for.
     * @return sha-512 of the input.
     */
    public static byte[] SHA512(byte[] value) {
        try {
            MessageDigest mdigest = MessageDigest.getInstance("SHA-512");
            byte[] digestResult = mdigest.digest(value);
            return digestResult;
        } catch (Exception e) {
            throw new RuntimeException("SHA512 error", e);
        }
    }
}
