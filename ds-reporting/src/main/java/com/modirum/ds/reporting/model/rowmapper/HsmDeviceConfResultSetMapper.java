package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.HsmDeviceConf;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ResultSetMapper for HsmDeviceConf entity
 */
public class HsmDeviceConfResultSetMapper implements ResultSetMapper<HsmDeviceConf> {
    @Override
    public HsmDeviceConf map(ResultSet resultSet) throws SQLException {
        return HsmDeviceConf
                .builder()
                .dsId(JdbcUtils.getColumnValue(resultSet, "dsId", Integer.class))
                .hsmDeviceId(JdbcUtils.getColumnValue(resultSet, "hsmdevice_id", Long.class))
                .config(JdbcUtils.getColumnValue(resultSet, "config", String.class))
                .build();
    }
}
