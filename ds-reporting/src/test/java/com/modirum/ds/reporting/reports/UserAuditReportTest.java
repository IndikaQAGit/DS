package com.modirum.ds.reporting.reports;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.enums.AuditLogAction;
import com.modirum.ds.reporting.enums.Roles;
import com.modirum.ds.reporting.enums.UserStatus;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.services.ServiceLocator;
import com.modirum.ds.reporting.services.UserJdbcService;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import com.modirum.ds.reporting.testutil.TestReportPropertiesUtil;
import com.modirum.ds.reporting.util.JdbcUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

@RunWithDB
public class UserAuditReportTest {
    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        ServiceLocator.initInstance(dataSource);
        ServiceLocator.getInstance()
                      .getCsvFilteringService()
                      .initializeFromProperties(TestReportPropertiesUtil.reportProperties());

        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/mngr-texts.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_users;", "TRUNCATE TABLE ds_userroles;",
                                       "TRUNCATE TABLE ds_auditlog;"})
    public void generateReport() throws Exception {
        UserJdbcService userJdbcService = ServiceLocator.getInstance().getUserJdbcService();

        User johnSmith = User.builder()
                             .loginName("john.smith")
                             .firstName("John")
                             .lastName("Smith")
                             .email("john.smith@modirum.com")
                             .status(UserStatus.ACTIVE.getValue())
                             .roles(new HashSet<>(Arrays.asList(Roles.userEdit, Roles.userView)))
                             .paymentSystemId(2)
                             .build();
        userJdbcService.save(johnSmith);
        String sql = "INSERT INTO ds_auditlog (id,whendate,byuser,byuserId,action,objectId,objectClass,details) " +
                     "VALUES(?,?,?,?,?,?,?,?)";
        JdbcUtils.execute(dataSource, sql,
                          new Object[]{1l, "2020-12-01 08:00:00.000000", johnSmith.getLoginName(), 0L, AuditLogAction.INSERT,
                                       johnSmith.getId(), "User", johnSmith.getLoginName() + " created, roles: "});
        JdbcUtils.execute(dataSource, sql,
                          new Object[]{2l, "2020-12-12 08:00:00.000000", johnSmith.getLoginName(), 0L, AuditLogAction.UPDATE,
                                       johnSmith.getId(), "User", johnSmith.getLoginName() + " status: 'B' -> 'A';"});
        JdbcUtils.execute(dataSource, sql,
                          new Object[]{3l, "2020-12-06 08:00:00.000000", johnSmith.getLoginName(), johnSmith.getId(), AuditLogAction.LOGIN,
                                       johnSmith.getId(), "User",
                                       "Login success user '" + johnSmith.getLoginName() + "' details: "});

        User arthurCool = User.builder()
                              .loginName("arthur.cool")
                              .firstName("Arthur")
                              .lastName("Cool")
                              .email("arthur.cool@modirum.com")
                              .status(UserStatus.DISABLED.getValue())
                              .roles(new HashSet<>(Arrays.asList(Roles.issuerEdit, Roles.issuerView)))
                              .paymentSystemId(2)
                              .build();
        userJdbcService.save(arthurCool);

        User admin = User.builder()
                         .loginName("admin")
                         .firstName("Admin")
                         .lastName("Admin")
                         .email("admin@modirum.com")
                         .status(UserStatus.TEMP_LOCKED.getValue())
                         .roles(new HashSet<>(Arrays.asList(Roles.issuerEdit, Roles.issuerView, Roles.userView,
                                                            Roles.userEdit, Roles.acquirerEdit, Roles.acquirerView,
                                                            Roles.auditLogView, Roles.caEdit, Roles.caView,
                                                            Roles.merchantsEdit, Roles.merchantsView, Roles.panView,
                                                            Roles.recordsView, Roles.adminSetup, Roles.textEdit,
                                                            Roles.textView, Roles.keyManage, Roles.reports)))
                         .paymentSystemId(2)
                         .build();
        userJdbcService.save(admin);

        User otherPaymentSystemUser = User.builder()
                         .loginName("otherUser")
                         .firstName("Other")
                         .lastName("Other")
                         .email("otherUser@modirum.com")
                         .status(UserStatus.ACTIVE.getValue())
                         .roles(new HashSet<>(Arrays.asList(Roles.issuerEdit, Roles.issuerView, Roles.userView,
                                                            Roles.userEdit, Roles.acquirerEdit, Roles.acquirerView,
                                                            Roles.auditLogView, Roles.caEdit, Roles.caView,
                                                            Roles.merchantsEdit, Roles.merchantsView, Roles.panView,
                                                            Roles.recordsView, Roles.adminSetup, Roles.textEdit,
                                                            Roles.textView)))
                         .paymentSystemId(1)
                         .build();
        userJdbcService.save(otherPaymentSystemUser);

        ReportParameters reportParameters = new ReportParameters();
        reportParameters.setReportingDataSource(dataSource);
        reportParameters.setTimeZone(TimeZone.getTimeZone("UTC"));
        reportParameters.setLocale(Locale.ENGLISH);
        Properties criteriaProperties = new Properties();
        criteriaProperties.setProperty(UserAuditReport.PAYMENT_SYSTEM_ID_KEY, "2");
        reportParameters.setCriteriaProperties(criteriaProperties);
        StringWriter writer = new StringWriter();

        new UserAuditReport().writeReport(writer, reportParameters);

        String expectedCsv = String.join(",", UserAuditReport.HEADERS) + System.lineSeparator() +
                             "john.smith,John,Smith,john.smith@modirum.com,YES,YES,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,Active,2020-12-12,2020-12-01,2020-12-06 08:00:00" +
                             System.lineSeparator() +
                             "arthur.cool,Arthur,Cool,arthur.cool@modirum.com,NO,NO,YES,YES,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,Blocked,,," +
                             System.lineSeparator() +
                             "admin,Admin,Admin,admin@modirum.com,YES,YES,YES,YES,YES,YES,YES,YES,YES,YES,YES,YES,YES,YES,Temp. Locked,,," +
                             System.lineSeparator();
        Assertions.assertEquals(expectedCsv, writer.toString());
    }
}