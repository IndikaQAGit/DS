package com.modirum.ds.reporting.services;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.model.AuditLog;
import com.modirum.ds.reporting.model.PaymentSystemConfig;
import com.modirum.ds.reporting.model.rowmapper.AuditLogResultSetMapper;
import com.modirum.ds.reporting.model.rowmapper.PaymentSystemConfigResultSetMapper;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import com.modirum.ds.reporting.util.JdbcUtils;

@RunWithDB
public class PaymentSystemConfigJdbcServiceTest {
    
    private static DataSource dataSource= TestEmbeddedDbUtil.dataSource();
    private static PaymentSystemConfigService paymentSystemConfigService = ServiceLocator.initInstance(dataSource).getPaymentSystemConfigService();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }
    
    @Test
    @DataSet(executeScriptsBefore = {"/services/paymentsystemconfig-service.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_paymentsystem_conf;"})
    public void testGetPaymentSystemConfig() throws Exception {
        PaymentSystemConfig allowAllExceptPsPsc = paymentSystemConfigService.getPaymentSystemConfig(new Long(1), "allowAllOtherPSUsers", 1);
        Assertions.assertEquals("allowAllOtherPSUsers", allowAllExceptPsPsc.getKey());
        Assertions.assertEquals("true", allowAllExceptPsPsc.getValue());
        Assertions.assertEquals("commentOnThisSide", allowAllExceptPsPsc.getComment());
    }
    
    @Test
    @DataSet(executeScriptsBefore = {"/services/paymentsystemconfig-service.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_paymentsystem_conf;"})
    public void testDeletePaymentSystemConfig() throws Exception {
        paymentSystemConfigService.deletePaymentSystemConfig(new Long(2), "allowDeleteEntries", 1);
        PaymentSystemConfig pscToCheck = paymentSystemConfigService.getPaymentSystemConfig(new Long(2), "allowDeleteEntries", 1);
        Assertions.assertNull(pscToCheck);
    }
    
    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_paymentsystem_conf;"})
    public void testAddPaymentSystemConfig() throws Exception {
        PaymentSystemConfig pscToAdd = PaymentSystemConfig.builder()
                .paymentSystemId(new Long(1))
                .dsId(1)
                .key("allowAllOtherPSUsers")
                .value("true")
                .comment("commentOnThisSide")
                .createdDate(new Date())
                .createdUser("DS")
                .lastModifiedDate(new Date())
                .lastModifiedBy("DS")
                .build();
        
        paymentSystemConfigService.addPaymentSystemConfig(pscToAdd);
        PaymentSystemConfig pscToCheck = JdbcUtils.findFirst(dataSource, "SELECT * from ds_paymentsystem_conf", null, new PaymentSystemConfigResultSetMapper());
        Assertions.assertEquals("true", pscToCheck.getValue());
        Assertions.assertEquals("allowAllOtherPSUsers", pscToCheck.getKey());
        Assertions.assertEquals("commentOnThisSide", pscToCheck.getComment());
        Assertions.assertEquals("DS", pscToCheck.getCreatedUser());
    }
    
    @Test
    @DataSet(executeScriptsBefore = {"/services/paymentsystemconfig-service.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_paymentsystem_conf;"})
    public void testEditPaymentSystemConfig() throws Exception {
        PaymentSystemConfig pscToModify = PaymentSystemConfig.builder()
                .paymentSystemId(new Long(3))
                .dsId(1)
                .key("allowEditEntries")
                .value("true")
                .comment("edited by test")
                .createdDate(new Date())
                .createdUser("test user")
                .lastModifiedDate(new Date())
                .lastModifiedBy("test user")
                .build(); 
        
        paymentSystemConfigService.editPaymentSystemConfig(pscToModify);
        PaymentSystemConfig pscToCheck = paymentSystemConfigService.getPaymentSystemConfig(new Long(3), "allowEditEntries", 1);
        Assertions.assertEquals("allowEditEntries", pscToCheck.getKey());
        Assertions.assertEquals("edited by test", pscToCheck.getComment());
        Assertions.assertEquals("test user", pscToCheck.getLastModifiedBy());
    }
    
    
}
