INSERT INTO ds_paymentsystems (id, name, port) VALUES (1, 'Default PS', 0);

-- ds_tdsrecords.id = 464000
INSERT INTO ds_tdsrecords (`id`,`acquirerBIN`,`acquirerMerchantID`,`ACSOperatorID`,`ACSTransID`,`ACSRef`,`ACSURL`,`acctNumber`,`acctNumberEnc`,`acctNumberKid`,`acctNumberHMAC`,`acctNumberHMKid`,`authenticationMethod`,`authenticationType`,`authenticationValue`,`browserIP`,`browserJavaEnabled`,`browserLanguage`,`browserUserAgent`,`cardExpiryDate`,`deviceChannel`,`dsRef`,`errorCode`,`DSTransID`,`errorDetail`,`ECI`,`issuerBIN`,`localDateStart`,`localDateEnd`,`localStatus`,`localIssuerId`,`localAcquirerId`,`requestorID`,`requestorName`,`MCC`,`merchantCountry`,`merchantName`,`merchantRiskIndicator`,`requestorURL`,`messageCategory`,`MIReferenceNumber`,`MITransID`,`MIProfileId`,`protocol`,`purchaseAmount`,`purchaseCurrency`,`purchaseExponent`,`purchaseDate`,`purchaseInstalData`,`recurringExpiry`,`recurringFrequency`,`resultsStatus`,`SDKAppID`,`SDKReferenceNumber`,`SDKTransID`,`transType`,`transStatus`,`transStatusReason`,`acquirerOptions`,`issuerOptions`,`paymentSystemId`) VALUES (464000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'EMVCo1234567','303','875041cd-7aea-58c6-8000-000000071480','threeDSServerOperatorID invalid value',NULL,NULL,'2020-07-10 14:33:53.504000','2020-07-10 14:33:54.317000',112,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ACS-EMULATOR-REF-NUMBER','2d0e3d66-1eaa-4c45-94df-526a6c3c36b0',NULL,'2.1.0',NULL,NULL,NULL,'2020-07-10 14:33:53.645000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

-- ds_tdsmessages.tdsrId = 464000
INSERT INTO ds_tdsmessages (`id`,`tdsrId`,`messageType`,`messageVersion`,`sourceIP`,`destIP`,`messageDate`,`contents`) VALUES (889000,464000,'PReq','2.1.0','3DS:127.0.0.1','DS:192.168.99.1','2020-07-10 14:33:53.504000','{"messageVersion":"2.1.0","messageType":"PReq","threeDSServerRefNumber":"ACS-EMULATOR-REF-NUMBER","threeDSServerOperatorID":"ACS-EMULATOR-OPER-ID","threeDSServerTransID":"2d0e3d66-1eaa-4c45-94df-526a6c3c36b0"}');
INSERT INTO ds_tdsmessages (`id`,`tdsrId`,`messageType`,`messageVersion`,`sourceIP`,`destIP`,`messageDate`,`contents`) VALUES (889010,464000,'Erro','2.1.0','DS:192.168.99.1','3DS:127.0.0.1','2020-07-10 14:33:54.302000','MDE04_1_UP35C81WhDcCeE69NVTuNJ0vsqeaiLC4GdBG74TIyBzWf4NjMOCO7l4S+plxZHintGy4B9aPo2uyi5QLMJ0CpL9r6DiP6W7yHdRL0S/6EomIExs99j/2ZZI0/0DwF8CtVf8ynIZpA+APWY5zN0+wkDxTzHj87x9Mw90eHJ3z1LC8Mdkm2IYr70ktMA/ZvuI/46kQ0kIZ3tuCvY6c2z62BNaaJ2Q/sB0iu13ZbkH0ZzI8PeS5K9U7fFhakTjh0gl5x0SbwHjqmH93O/Uas9LRREl+bbX1ytEu/jgivRICQz47egpZLNMvlBqokrzueClAibbBup8HJRCv7ys+8StqWg==');

INSERT INTO ds_hsmdevice(id, name, className, status) VALUES (1, 'Software engine', 'com.modirum.ds.hsm.SoftwareJCEService', 'A');
INSERT INTO ds_hsmdevice_conf (hsmdevice_id, dsId, config) VALUES (1, 0, 'timeout=30');

-- ds_keydata
INSERT INTO ds_keydata (`id`,`keyAlias`,`status`,`keyData`,`keyCheckValue`,`keyDate`,`cryptoPeriodDays`,`signedby1`,`signedby2`,`signature1`,`signature2`,`hsmdevice_id`,`wrapkey_id`)
VALUES (1,'dataKey','working','k/bL4vTLJjT+MNnT2zquP/r7w23T5Ts1Qy6kg1KMyIU=','6E8C5C','2020-07-10 12:30:21.996000',1460,NULL,NULL,NULL,NULL,1,NULL);

INSERT INTO ds_tdsrecord_attributes (`id`, `tdsRecordId`, `attr`, `value`, `asciiValue`, `createdDate`, `createdUser`) VALUES ('2e15e3eb-72b9-4a59-882e-cc869885d4c5', 464000, 'threeDSServerURL', null, 'https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp', '2021-01-18 15:57:10.594000', 'DS');


