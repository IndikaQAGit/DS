package com.modirum.ds.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateFormatUtilsTest {

    @Test
    public void validDateFormat() {
        Assertions.assertTrue(DateFormatUtils.isDateFormatted("2404", "yyMM"));
    }

    @Test
    public void invalidDateFormat() {
        Assertions.assertFalse(DateFormatUtils.isDateFormatted("2413", "yyMM"));
    }
}
