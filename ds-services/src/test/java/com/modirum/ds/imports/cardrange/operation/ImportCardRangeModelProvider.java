package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;

public class ImportCardRangeModelProvider {

    public BatchCardRangeModel getInvalidModel() {
        return BatchCardRangeModel.builder()
            .alias("alias")
            .binEnd("0000")
            .binStart("1111")
            .cardType("10")
            .issuerName("Bank")
            .status("Participating")
            .paymentSystemId("1")
            .build();
    }

    public BatchCardRangeModel getValidModel() {
        return BatchCardRangeModel.builder()
            .alias("alias")
            .binEnd("2222")
            .binStart("1111")
            .cardType("01")
            .issuerName("Bank")
            .status("participating")
            .paymentSystemId("1")
            .build();
    }
}
