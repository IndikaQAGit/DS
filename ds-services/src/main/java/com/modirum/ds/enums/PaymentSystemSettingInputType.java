package com.modirum.ds.enums;

public class PaymentSystemSettingInputType {
    public static final String TEXTAREA = "textarea";
    public static final String INPUT = "input";
    public static final String DISPLAY_ONLY = "display_only";
    public static final String CHECKBOX = "checkbox";
}