package com.modirum.ds.enums;

public class FieldMaxLength {
    public static final int ERROR_CODE = 3;
    public static final int ERROR_DESCRIPTION = 2048;
    public static final int ERROR_DETAIL = 2048;
}