package com.modirum.ds.enums;

import com.modirum.ds.db.model.PaymentSystem;

public class PaymentSystemConstants {
    public static final Integer UNLIMITED = PaymentSystem.UNLIMITED;

    public static final Integer ALL = PaymentSystem.ALL;
}
