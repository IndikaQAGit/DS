package com.modirum.ds.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ReportType {
    TRANSACTION("TransactionReport", "text.report.transaction"),
    USER_AUDIT("UserAuditReport", "text.report.useraudit");

    private String label;
    private String description;

    private ReportType(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public static final List<ReportType> TRANSACTION_REPORTS = Collections.unmodifiableList(Arrays.asList(
            TRANSACTION));

    public static final List<ReportType> USER_REPORTS = Collections.unmodifiableList(Arrays.asList(
            USER_AUDIT));

    private static final Map<String, ReportType> REPORT_TYPE_MAP;

    static {
        HashMap<String, ReportType> reportMap = new HashMap<>();
        for (ReportType reportType : ReportType.values()) {
            reportMap.put(reportType.label, reportType);
        }
        REPORT_TYPE_MAP = Collections.unmodifiableMap(reportMap);
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public static ReportType getByReportLabel(String key) {
        return REPORT_TYPE_MAP.get(key);
    }

    public static Map<String, ReportType> getMap() {
        return REPORT_TYPE_MAP;
    }

}
