package com.modirum.ds.util;

import com.modirum.ds.utils.Misc;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509ExtensionUtils;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Utility class for common certificate related operations.
 */
public class CertUtil {

    private CertUtil() {
        // util
    }

    /**
     * @autor andri
     * Create subject string in same order as BC V3 library encodes it on V3 certificates
     * if not the client certificates are useless most ssl authentications with unknown_ca as the strings do not match
     * Original: Issuer: C=EE, ST=HM, L=Tallinn, O=Scheme Inc, OU=3DS2, CN=Scheme Inc 3DS2 CA 2016-3
     * BCV3 Rendered to signed: Issuer: CN=Scheme Inc 3DS2 CA 2016-3, OU=3DS2, O=Scheme Inc, L=Tallinn,ST=HM, C=EE
     *
     * If crated this way then root
     * Subject: CN=Scheme Inc 3DS2 CA 2016-4, OU=3DS2 CA, O=Scheme Inc, L=Tallinn, ST=HM, C=EE
     * Issuer: CN=Scheme Inc 3DS2 CA 2016-4, OU=3DS2 CA, O=Scheme Inc, L=Tallinn, ST=HM, C=EE
     * and issued cert from root exacly opposite! need to dig where dog is buried.
     * Issuer: C=EE, ST=HM, L=Tallinn, O=Scheme Inc, OU=3DS2 CA, CN=Scheme Inc
     * Subject: C=EE, ST=HM, L=Tallinn, O=MI3, OU=412345, CN=771133
     *
     * RFC 2253               LADPv3 Distinguished Names          December 1997
     * 				String  X.500 AttributeType
     * 				------------------------------
     * 					CN      commonName
     * 					L       localityName
     * 					ST      stateOrProvinceName
     * 					O       organizationName
     * 					OU      organizationalUnitName
     * 					C       countryName
     * 					STREET  streetAddress
     * 					DC      domainComponent
     * 					UID     userid
     *
     * @param country Certificate country
     * @param state Certificate state
     * @param locality Certificate locality
     * @param org Certificate organization
     * @param ou Certificate org unit
     * @param cn Certificate common name
     * @return SDN concatenated value
     */
    public static StringBuilder createX509SubjectV3BC(String country, String state, String locality, String org, String ou, String cn) {
        StringBuilder subjValue = new StringBuilder(128);
        if (Misc.isNotNullOrEmpty(cn)) {
            subjValue.append((subjValue.length() > 0 ? "," : "") + "CN=" + cn);
        }
        if (Misc.isNotNullOrEmpty(ou)) {
            subjValue.append((subjValue.length() > 0 ? "," : "") + "OU=" + ou);
        }
        if (Misc.isNotNullOrEmpty(org)) {
            subjValue.append((subjValue.length() > 0 ? "," : "") + "O=" + org);
        }
        if (Misc.isNotNullOrEmpty(locality)) {
            subjValue.append((subjValue.length() > 0 ? "," : "") + "L=" + locality);
        }
        if (Misc.isNotNullOrEmpty(state)) {
            subjValue.append((subjValue.length() > 0 ? "," : "") + "ST=" + state);
        }
        if (Misc.isNotNullOrEmpty(country)) {
            subjValue.append((subjValue.length() > 0 ? "," : "") + "C=" + country);
        }
        return subjValue;
    }

    public static List<String> getSubjectAltNames(org.bouncycastle.pkcs.PKCS10CertificationRequest certReq) {
        java.util.List<String> anl = new java.util.ArrayList<>(4);

        org.bouncycastle.asn1.pkcs.Attribute[] certAttributes = certReq.getAttributes();
        for (org.bouncycastle.asn1.pkcs.Attribute attribute : certAttributes) {
            if (attribute.getAttrType().equals(
                    org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers.pkcs_9_at_extensionRequest)) {
                org.bouncycastle.asn1.x509.Extensions extensions = org.bouncycastle.asn1.x509.Extensions.getInstance(
                        attribute.getAttrValues().getObjectAt(0));
                GeneralNames gns = GeneralNames.fromExtensions(extensions, Extension.subjectAlternativeName);
                if (gns != null) {
                    GeneralName[] names = gns.getNames();
                    for (int k = 0; k < names.length; k++) {
                        if (names[k].getTagNo() == GeneralName.dNSName) {
                            anl.add(names[k].getName().toString());
                        } else if (names[k].getTagNo() == GeneralName.iPAddress) {
                            try {
                                //	org.bouncycastle.util.IPAddress.
                                byte[] ip = Misc.lastBytes(names[k].getEncoded(), 2);
                                java.net.InetAddress ia = java.net.InetAddress.getByAddress(ip);
                                anl.add(ia.getHostAddress());
                            } catch (Exception dc) {
                                throw new RuntimeException(dc);
                            }
                        }
                    }
                }
            }
        }

        return anl;
    }

    /**
     * Initializes and populates X509v3CertificateBuilder for generating root certificates.
     * Adds root cert specific information e.g. key usages.
     *
     * @param rootPublicKey Public key to issue the root certificate for.
     * @param subjectDN Subject Distinguished Name for the root certificate.
     * @param daysUntilExpire Root certificate validity period in days.
     * @return Prepared certificate builder.
     */
    public static X509v3CertificateBuilder prepareRootCAcertBuilder(PublicKey rootPublicKey,
                                                                    String subjectDN,
                                                                    int daysUntilExpire) throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, daysUntilExpire);
        Date expiryDate = cal.getTime();

        X500Principal issuer = new X500Principal(subjectDN);
        X500Principal subject = new X500Principal(subjectDN);
        X500Name xissuer = X500Name.getInstance(issuer.getEncoded());
        X500Name xsubject = X500Name.getInstance(subject.getEncoded());

        SubjectPublicKeyInfo subjectPublicKeyInfo =
                SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(rootPublicKey.getEncoded()));

        BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis() / 10);
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(xissuer, serialNumber, new Date(), expiryDate,
                xsubject, subjectPublicKeyInfo);

        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));

        X509ExtensionUtils x509ExtensionUtils = new X509ExtensionUtils(digCalc);
        certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(4));

        Integer keyUsage = KeyUsage.nonRepudiation | KeyUsage.cRLSign | KeyUsage.keyCertSign | KeyUsage.keyEncipherment
                | KeyUsage.dataEncipherment | KeyUsage.digitalSignature;
        certBuilder.addExtension(Extension.keyUsage, true, new KeyUsage(keyUsage));

        certBuilder.addExtension(Extension.subjectKeyIdentifier, false,
                x509ExtensionUtils.createSubjectKeyIdentifier(subjectPublicKeyInfo));
        certBuilder.addExtension(Extension.authorityKeyIdentifier, false,
                x509ExtensionUtils.createAuthorityKeyIdentifier(subjectPublicKeyInfo));

        return certBuilder;
    }

    /**
     * Initializes and populates X509v3CertificateBuilder for generating intermediate certificates. Adds
     * intermediate cert specific information e.g. crlURL, key usages.
     *
     * @param subjectPublicKey Public key to issue the intermediate certificate for.
     * @param subjectDN Subject Distinguished Name for the intermediate certificate.
     * @param daysUntilExpire Intermediate certificate validity period in days.
     * @param issuerCertificate Issuing certificate (parent of intermediate)
     * @param crlUrl Certificate revocation list URL.
     * @return Prepared certificate builder.
     */
    public static X509v3CertificateBuilder prepareIntermediateCAcertBuilder(PublicKey subjectPublicKey,
                                                                            String subjectDN,
                                                                            int daysUntilExpire,
                                                                            X509Certificate issuerCertificate,
                                                                            String crlUrl) throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, daysUntilExpire);
        Date expiryDate = cal.getTime();

        if (expiryDate.after(issuerCertificate.getNotAfter())) {
            throw new CertIOException("New certificate expiration data cannot exceed issuer certificate expiration.");
        }

        X500Principal issuer = issuerCertificate.getSubjectX500Principal();
        X500Principal subject = new X500Principal(subjectDN);
        X500Name xissuer = X500Name.getInstance(issuer.getEncoded());
        X500Name xsubject = X500Name.getInstance(subject.getEncoded());

        SubjectPublicKeyInfo subjectPublicKeyInfo =
                SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(subjectPublicKey.getEncoded()));

        BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis() / 10);
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(xissuer, serialNumber, new Date(),
                expiryDate, xsubject, subjectPublicKeyInfo);

        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));
        X509ExtensionUtils x509ExtensionUtils = new X509ExtensionUtils(digCalc);
        certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(0));

        Integer keyUsage = KeyUsage.keyCertSign | KeyUsage.cRLSign;
        certBuilder.addExtension(Extension.keyUsage, false, new KeyUsage(keyUsage));

        certBuilder.addExtension(Extension.subjectKeyIdentifier, false,
                x509ExtensionUtils.createSubjectKeyIdentifier(subjectPublicKeyInfo));
        SubjectPublicKeyInfo issuerPublicKeyInfo = SubjectPublicKeyInfo.getInstance(
                ASN1Sequence.getInstance(issuerCertificate.getPublicKey().getEncoded()));
        certBuilder.addExtension(Extension.authorityKeyIdentifier, false,
                x509ExtensionUtils.createAuthorityKeyIdentifier(issuerPublicKeyInfo));

        // CRL support
        if (Misc.isNotNullOrEmpty(crlUrl)) {
            GeneralName generalName = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(crlUrl));
            GeneralNames generalNames = new GeneralNames(generalName);
            DistributionPointName distributionPointName = new DistributionPointName(generalNames);
            DistributionPoint distributionPoint = new DistributionPoint(distributionPointName, null, null);
            certBuilder.addExtension(Extension.cRLDistributionPoints, false, new DERSequence(distributionPoint));
        }

        return certBuilder;
    }

    /**
     * Initializes and populates X509v3CertificateBuilder for generating leaf Client/Server TLS certificates.
     * Adds client/server TLS cert specific information e.g. crlURL, key usages, SANs...
     *
     * @param subject Subject for the certificate
     * @param subjectPublicKeyInfo Public key to issue the certificate for
     * @param issuerCertificate Issuer certificate (parent of this created cert)
     * @param expiryDate Validity period
     * @param crlPoint certificate revocation list url
     * @param authorityInfoAccess Authority endpoint
     * @param extendedKeyUsageList extended key usages (e.g. client/server)
     * @param subjAltNames Certificate server SANs
     * @return Prepared certificate builder
     */
    public static X509v3CertificateBuilder prepareTLSCertBuilder(X500Principal subject,
                                                          SubjectPublicKeyInfo subjectPublicKeyInfo,
                                                          X509Certificate issuerCertificate,
                                                          Date expiryDate,
                                                          String crlPoint,
                                                          String authorityInfoAccess,
                                                          List<KeyPurposeId> extendedKeyUsageList,
                                                          List<String> subjAltNames) throws Exception {

        X500Principal issuer = issuerCertificate.getSubjectX500Principal();
        X500Name xissuer = X500Name.getInstance(issuer.getEncoded());
        X500Name xsubject = X500Name.getInstance(subject.getEncoded());

        BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis() / 10);
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(xissuer, serialNumber, new Date(), expiryDate,
                xsubject, subjectPublicKeyInfo);

        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));

        X509ExtensionUtils x509ExtensionUtils = new X509ExtensionUtils(digCalc);
        certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(false));

        Integer keyUsage = KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.digitalSignature;
        certBuilder.addExtension(Extension.keyUsage, false, new KeyUsage(keyUsage));

        if (extendedKeyUsageList != null && extendedKeyUsageList.size() > 0) {
            certBuilder.addExtension(Extension.extendedKeyUsage, false,
                    new ExtendedKeyUsage(extendedKeyUsageList.toArray(new KeyPurposeId[extendedKeyUsageList.size()])));
        }

        // subject alternative names
        if (subjAltNames != null && subjAltNames.size() > 0) {
            GeneralName[] nms = new GeneralName[subjAltNames.size()];
            for (int i = 0; subjAltNames != null && i < subjAltNames.size(); i++) {
                String s = subjAltNames.get(i);
                if (Misc.isIpV4Address(s)) {
                    nms[i] = new GeneralName(GeneralName.iPAddress, s);
                } else {
                    nms[i] = new GeneralName(GeneralName.dNSName, s);
                }
            }
            GeneralNames subjectAltNameSet = new GeneralNames(nms);
            certBuilder.addExtension(Extension.subjectAlternativeName, false, subjectAltNameSet);
        }
        //Subject Key Identifier
        certBuilder.addExtension(Extension.subjectKeyIdentifier, false,
                x509ExtensionUtils.createSubjectKeyIdentifier(subjectPublicKeyInfo));
        SubjectPublicKeyInfo issuerPublicKeyInfo = SubjectPublicKeyInfo.getInstance(
                ASN1Sequence.getInstance(issuerCertificate.getPublicKey().getEncoded()));
        certBuilder.addExtension(Extension.authorityKeyIdentifier, false,
                x509ExtensionUtils.createAuthorityKeyIdentifier(issuerPublicKeyInfo));

        // CRL support
        if (Misc.isNotNullOrEmpty(crlPoint)) {
            GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(crlPoint));
            GeneralNames gns = new GeneralNames(gn);
            DistributionPointName dpn = new DistributionPointName(gns);
            DistributionPoint distp = new DistributionPoint(dpn, null, null);
            certBuilder.addExtension(Extension.cRLDistributionPoints, false, new DERSequence(distp));
        }
        if (Misc.isNotNullOrEmpty(authorityInfoAccess)) {
            GeneralName aiaLocation = new GeneralName(GeneralName.uniformResourceIdentifier,
                    new DERIA5String(authorityInfoAccess));
            AuthorityInformationAccess aia = new AuthorityInformationAccess(AccessDescription.id_ad_caIssuers,
                    aiaLocation);
            certBuilder.addExtension(Extension.authorityInfoAccess, false, aia);
        }

        return certBuilder;
    }

    /**
     * Initializes and populates X509v3CertificateBuilder for generating leaf Client/Server TLS certificates.
     * Adds client/server TLS cert specific information e.g. crlURL, key usages.
     *
     * @param subjectPublicKey Public key to issue the client TLS certificate for.
     * @param subjectDN Subject Distinguished Name for the client TLS certificate.
     * @param daysUntilExpire Client TLS certificate validity period in days.
     * @param issuerCertificate Issuing certificate (parent of this Client TLS cert)
     * @param crlUrl Certificate revocation list URL.
     * @param extendedKeyUsageList Extended key usage (purpose) list.
     *                             May contain KeyPurposeId.id_kp_clientAuth / id_kp_serverAuth to identify the purpose.
     * @return Prepared certificate builder.
     */
    public static X509v3CertificateBuilder prepareTLSCertBuilder(PublicKey subjectPublicKey,
                                                                 String subjectDN,
                                                                 int daysUntilExpire,
                                                                 X509Certificate issuerCertificate,
                                                                 String crlUrl,
                                                                 List<KeyPurposeId> extendedKeyUsageList) throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, daysUntilExpire);
        Date expiryDate = cal.getTime();

        if (expiryDate.after(issuerCertificate.getNotAfter())) {
            throw new CertIOException("New certificate expiration data cannot exceed issuer certificate expiration.");
        }

        X500Principal subject = new X500Principal(subjectDN);
        SubjectPublicKeyInfo subjectPublicKeyInfo =
                SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(subjectPublicKey.getEncoded()));

        return prepareTLSCertBuilder(subject, subjectPublicKeyInfo, issuerCertificate, expiryDate, crlUrl, null,
                extendedKeyUsageList, null);
    }

    /**
     * Initializes and populates X509v3CertificateBuilder for generating SDK RSA decryption key certificates. Adds
     * subject certificate specific information e.g. crlURL, key usages.
     *
     * @param subjectPublicKey Public key to issue the SDK RSA decryption certificate for.
     * @param subjectDN Subject Distinguished Name for the SDK RSA decryption certificate.
     * @param daysUntilExpire SDK RSA decryption certificate validity period in days.
     * @param issuerCertificate Issuing certificate.
     * @param crlUrl Certificate revocation list URL.
     */
    public static X509v3CertificateBuilder prepareSdkRsaDecryptionKeyCertBuilder(PublicKey subjectPublicKey,
                                                                                 String subjectDN,
                                                                                 int daysUntilExpire,
                                                                                 X509Certificate issuerCertificate,
                                                                                 String crlUrl) throws Exception {


        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, daysUntilExpire);
        Date expiryDate = cal.getTime();

        if (expiryDate.after(issuerCertificate.getNotAfter())) {
            throw new CertIOException("New certificate expiration data cannot exceed issuer certificate expiration.");
        }

        X500Principal issuer = issuerCertificate.getSubjectX500Principal();
        X500Principal subject = new X500Principal(subjectDN);
        X500Name xissuer = X500Name.getInstance(issuer.getEncoded());
        X500Name xsubject = X500Name.getInstance(subject.getEncoded());

        SubjectPublicKeyInfo subjectPublicKeyInfo =
                SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(subjectPublicKey.getEncoded()));

        BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis() / 10);
        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(xissuer, serialNumber, new Date(),
                expiryDate, xsubject, subjectPublicKeyInfo);

        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));
        X509ExtensionUtils x509ExtensionUtils = new X509ExtensionUtils(digCalc);
        certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(false));

        Integer keyUsage = KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.digitalSignature;
        certBuilder.addExtension(Extension.keyUsage, false, new KeyUsage(keyUsage));

        certBuilder.addExtension(Extension.subjectKeyIdentifier, false,
                x509ExtensionUtils.createSubjectKeyIdentifier(subjectPublicKeyInfo));
        SubjectPublicKeyInfo issuerPublicKeyInfo = SubjectPublicKeyInfo.getInstance(
                ASN1Sequence.getInstance(issuerCertificate.getPublicKey().getEncoded()));
        certBuilder.addExtension(Extension.authorityKeyIdentifier, false,
                x509ExtensionUtils.createAuthorityKeyIdentifier(issuerPublicKeyInfo));

        // CRL support
        if (Misc.isNotNullOrEmpty(crlUrl)) {
            GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(crlUrl));
            GeneralNames gns = new GeneralNames(gn);
            DistributionPointName dpn = new DistributionPointName(gns);
            DistributionPoint distp = new DistributionPoint(dpn, null, null);
            certBuilder.addExtension(Extension.cRLDistributionPoints, false, new DERSequence(distp));
        }

        return certBuilder;
    }

    /**
     * Recreates the CSR builder from original CSR. Adds extra useful information (e.g. CRL URL, client/server usages..)
     * Used primarily for signing CSRs provided by exernal parties.
     *
     * @param originalCSR Original initial CSR.
     * @param expiryDate Expiry date to set to certificate.
     * @param issuerCertificate CA certificate (will be parent for this signed cert)
     * @param crlUrl Certificate revocation list URL.
     * @return Prepared cert builder
     */
    public static X509v3CertificateBuilder prepareUpdatedCSRbuilder(PKCS10CertificationRequest originalCSR,
                                                                    Date expiryDate,
                                                                    X509Certificate issuerCertificate,
                                                                    String crlUrl) throws Exception {


        if (expiryDate.after(issuerCertificate.getNotAfter())) {
            throw new CertIOException("New certificate expiration data cannot exceed issuer certificate expiration.");
        }

        SubjectPublicKeyInfo subjectPublicKeyInfo = originalCSR.getSubjectPublicKeyInfo();

        X500Principal issuer = issuerCertificate.getSubjectX500Principal();
        X500Name xissuer = X500Name.getInstance(issuer.getEncoded());
        X500Principal subject = new X500Principal(originalCSR.getSubject().getEncoded());
        X500Name xsubject = X500Name.getInstance(subject.getEncoded());
        BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis() / 10);
        X509v3CertificateBuilder cb = new X509v3CertificateBuilder(xissuer, serialNumber, new Date(), expiryDate,
                xsubject, subjectPublicKeyInfo);

        DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));

        X509ExtensionUtils x509ExtensionUtils = new X509ExtensionUtils(digCalc);
        cb.addExtension(Extension.basicConstraints, true, new BasicConstraints(false));

        Integer keyUsage = KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.digitalSignature;
        cb.addExtension(Extension.keyUsage, false, new KeyUsage(keyUsage));

        List<KeyPurposeId> eku = new ArrayList<>(1);
        eku.add(KeyPurposeId.id_kp_clientAuth);
        cb.addExtension(Extension.extendedKeyUsage, false, new ExtendedKeyUsage(eku.toArray(new KeyPurposeId[eku.size()])));

        // subject alternative names
        List<String> subjAltNames = CertUtil.getSubjectAltNames(originalCSR);
        if (subjAltNames != null && subjAltNames.size() > 0) {
            GeneralName[] nms = new GeneralName[subjAltNames.size()];
            for (int i = 0; subjAltNames != null && i < subjAltNames.size(); i++) {
                String s = subjAltNames.get(i);
                if (Misc.isIpV4Address(s)) {
                    nms[i] = new GeneralName(GeneralName.iPAddress, s);
                } else {
                    nms[i] = new GeneralName(GeneralName.dNSName, s);
                }
            }
            GeneralNames subjectAltNameSet = new GeneralNames(nms);
            cb.addExtension(Extension.subjectAlternativeName, false, subjectAltNameSet);
        }

        //Subject Key Identifier
        cb.addExtension(Extension.subjectKeyIdentifier, false,
                x509ExtensionUtils.createSubjectKeyIdentifier(subjectPublicKeyInfo));
        SubjectPublicKeyInfo issuerPublicKeyInfo = SubjectPublicKeyInfo.getInstance(
                ASN1Sequence.getInstance(issuerCertificate.getPublicKey().getEncoded()));
        cb.addExtension(Extension.authorityKeyIdentifier, false,
                x509ExtensionUtils.createAuthorityKeyIdentifier(issuerPublicKeyInfo));

        // CRL support
        if (Misc.isNotNullOrEmpty(crlUrl)) {
            GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(crlUrl));
            GeneralNames gns = new GeneralNames(gn);
            DistributionPointName dpn = new DistributionPointName(gns);
            DistributionPoint distp = new DistributionPoint(dpn, null, null);
            cb.addExtension(Extension.cRLDistributionPoints, false, new DERSequence(distp));
        }

        return cb;
    }
}
