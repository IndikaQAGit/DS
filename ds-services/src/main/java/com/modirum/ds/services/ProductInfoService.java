package com.modirum.ds.services;

import java.util.Date;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.db.model.License;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.utils.DateUtil;

public class ProductInfoService {
    private final PersistenceService persistenceService;

    public ProductInfoService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public ProductInfo loadProductInfo() throws Exception {
        License license = persistenceService.getLicense();
        String product = license.getName();
        String productVer = license.getVersion();
        String productLce = license.getLicensee();
        String productMaxMer = license.getMaxMerchants();
        String productLiExp = license.getExpiration();
        String licenseKey = license.getLicenseKey();
        String productLiIssued = DateUtil.formatDate(license.getIssuedDate(), "yyyy-MM-dd");
        String productExtensions = license.getExtension();

        ProductInfo prodInfo = new ProductInfo();
        prodInfo.setExpiration(productLiExp);
        prodInfo.setLicensee(productLce);
        prodInfo.setLicenseKey(licenseKey);
        prodInfo.setMaxMerchants(productMaxMer);
        prodInfo.setProduct(product);
        prodInfo.setVersion(productVer);
        prodInfo.setIssued(productLiIssued);
        prodInfo.setExtensions(productExtensions);

        return prodInfo;
    }

    public int updateProductInfo(ProductInfo pi) throws Exception {
        License license = persistenceService.getLicense();
        license.setName(pi.getProduct());
        license.setVersion(pi.getVersion());
        license.setLicensee(pi.getLicensee());
        license.setMaxMerchants(pi.getMaxMerchants());
        license.setExpiration(pi.getExpiration());
        license.setIssuedDate(DateUtil.parseDate(pi.getIssued(), "yyyy-MM-dd"));
        license.setLicenseKey(pi.getLicenseKey());
        license.setExtension(pi.getExtensions());

        persistenceService.saveOrUpdate(license);

        return 1;
    }
}
