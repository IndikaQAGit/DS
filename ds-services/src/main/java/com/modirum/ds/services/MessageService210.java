/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 7. jaan 2016
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XJWE;
import com.modirum.ds.tds21msgs.XJWEprotected;
import com.modirum.ds.tds21msgs.XJWSCompact;
import com.modirum.ds.tds21msgs.XacsSignedContentPayload;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author andri Service for transforming ThereeDSecure v2 messages (JSON)
 */
public class MessageService210<T extends TDSMessageBase> extends MessageService<T> {

    protected static Logger log = LoggerFactory.getLogger(MessageService210.class);

    private volatile static MessageService210<TDSMessage> instance = null;

    public static MessageService210<TDSMessage> getInstance() {
        if (instance == null) {
            instance = new MessageService210<>();
        }
        return instance;
    }

    protected MessageService210() {
        super("/tds210.xsd");
        log.info("MessageService210 initialized");
    }

    protected MessageService210(String schemaPath) {
        super(schemaPath);
    }

    public String getSupportedVersion() {
        return TDSModel.MessageVersion.V2_1_0.value();
    }

    public boolean isErrorMessage(TDSMessage tdsMsg) {
        return TDSModel.XmessageType.ERRO.value().equals(tdsMsg.getMessageType());
    }

    /**
     * Validate first duplicates and if passed next field syntax to match as set in XSD
     *
     * @param msg
     * @return
     */
    public void validateFieldSyntax(TDSMessage msg, List<Error> errors) {
        log.info("peos2:{}, {}", errors.size(), errors.size() > 0 ? errors.get(0) : "");
        if (msg.getDuplicateCounts() != null && msg.getDuplicateCounts().size() > 0) {
            Error e = new Error();
            errors.add(e);
            e.setCode(ErrorCode.cDuplicateDataElem204);

            StringBuilder details = new StringBuilder();
            for (Map.Entry<String, AtomicInteger> en : msg.getDuplicateCounts().entrySet()) {
                if (details.length() > 0) {
                    details.append(',');
                }
                details.append(en.getKey() + "(" + en.getValue() + ")");
            }
            e.setFieldName(details.toString());
            return;
        }

        if (schema != null) {
            try {
                JAXBSource source = new JAXBSource(tdsJsonCtx2, msg);
                Validator validator = this.getValidator();
                FieldErrorHandler feh = new FieldErrorHandler(errors);
                log.info("peos3:{}, {}, treat as missing: {}", errors.size(), errors.size() > 0 ? errors.get(0) : "",
                         treatEmptyValueAsMissing);
                feh.treatEmptyValueAsMissing = treatEmptyValueAsMissing;
                validator.setErrorHandler(feh);
                validator.validate(source);
                this.returnValidator(validator);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }

        if (msg.getAcsUiType() != null) {
            if (!TDSModel.ACSUIType2_1_0.contains(msg.getAcsUiType())) {
                Error e = new Error();
                errors.add(e);
                e.setCode(ErrorCode.cInvalidTxData305);
                e.setFieldName("acsUiType");
                e.setDescription("Invalid undefined value");
            }
        }

    }

    /**
     * Validate commons: messageVersion, messageCategory
     *
     * @param aMsg
     * @param errors
     */
    public void validateCommons(TDSMessage aMsg, List<Error> errors) {
        if (Misc.isNullOrEmpty(aMsg.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion",
                                 (aMsg.getMessageVersion() == null ? "is missing" : "is empty")));
        } else {
            boolean isValidMessageVersion = false;
            StringBuilder supportedValues = new StringBuilder();
            for (TDSModel.MessageVersion messageVersion : TDSModel.MessageVersion.values()) {
                if (messageVersion.value().equals(aMsg.getMessageVersion()) && messageVersion.isSupported()) {
                    isValidMessageVersion = true;
                    break;
                }

                if (messageVersion.isSupported()) {
                    if (supportedValues.length() > 0) {
                        supportedValues.append(",");
                    }
                    supportedValues.append(messageVersion.value());
                }
            }
            if (!isValidMessageVersion) {
                errors.add(0, new Error(ErrorCode.cMessageVersionNotSupported102, "messageVersion",
                                        "invalid value '" + aMsg.getMessageVersion() + "'" + " supported versions: " +
                                        supportedValues));
            }
        }

        StringBuilder ireq05Details = new StringBuilder();

        if (Misc.isNullOrEmpty(aMsg.getMessageType())) {
            errors.add(0, new Error(ErrorCode.cRequiredFieldMissing201, "messageType",
                                    (aMsg.getMessageType() == null ? "is missing" : "is empty")));
        } else if (TDSModel.XmessageType.A_REQ.value.equals(aMsg.getMessageType()) ||
                   TDSModel.XmessageType.R_REQ.value.equals(aMsg.getMessageType())) {
            if (Misc.isNullOrEmpty(aMsg.getMessageCategory())) {
                errors.add(0, new Error(ErrorCode.cRequiredFieldMissing201, "messageCategory",
                                        (aMsg.getMessageCategory() == null ? "is missing" : "is empty")));
            } else {
                if (!TDSModel.XMessageCategory.contains(aMsg.getMessageCategory())) {
                    ireq05Details.append("messageCategory invalid value '" + aMsg.getMessageCategory() + "'");
                }
            }
        } else if (aMsg.getMessageCategory() != null) {
            ireq05Details.append("messageCategory not expected in " + aMsg.getMessageType());
        }

        if (ireq05Details.length() > 0) {
            errors.add(0, new Error(ErrorCode.cInvalidFieldFormat203, null, ireq05Details.toString()));
        }
    }

    public String toCompactJWE(com.modirum.ds.tds21msgs.XJWE jwe) throws Exception {
        StringBuilder sb = new StringBuilder(1024);
        StringBuilder ph = new StringBuilder();
        ph.append("{");
        ph.append("\"" + "alg" + "\":\"" + jwe.getProtected().getAlg() + "\",");
        ph.append("\"" + "enc" + "\":\"" + jwe.getProtected().getEnc() + "\"");
        ph.append("}");
        sb.append(Base64.encodeURL(Misc.toUtf8Bytes(ph), 0));

        //sb.append(Base64.encodeURL(toJSON(jwe.getProtected()),0));
        sb.append('.');
        if (jwe.getEncryptedKey() != null) {
            sb.append(Misc.replace(Misc.replace(jwe.getEncryptedKey(), "\n", ""), "\r", ""));
        }
        sb.append('.');
        if (jwe.getIv() != null) {
            sb.append(jwe.getIv());
        }
        sb.append('.');
        sb.append(Misc.replace(Misc.replace(jwe.getCiphertext(), "\n", ""), "\r", ""));
        sb.append('.');
        sb.append(jwe.getTag());

        return sb.toString();
    }

    public XJWE fromCompactJWE(byte[] compactJWE) throws Exception {
        XJWE jwe = new XJWE();
        String compactJWES = new String(compactJWE, StandardCharsets.ISO_8859_1);
        int dot1 = compactJWES.indexOf('.');
        String protStr = new String(compactJWE, 0, dot1, StandardCharsets.ISO_8859_1);
        XJWEprotected prot = ProtectedfromJSON(Base64.decodeURL(protStr));
        int dot2 = compactJWES.indexOf('.', dot1 + 1);

        jwe.setProtected(prot);
        jwe.setEncryptedKey(new String(compactJWE, dot1 + 1, dot2 - dot1 - 1, StandardCharsets.ISO_8859_1));

        jwe.setAad(protStr);
        int dot3 = compactJWES.indexOf('.', dot2 + 1);
        if (dot3 - dot2 > 1) {
            jwe.setIv(new String(compactJWE, dot2 + 1, dot3 - dot2 - 1, StandardCharsets.ISO_8859_1));
        }
        int dot4 = compactJWES.indexOf('.', dot3 + 1);
        jwe.setCiphertext(new String(compactJWE, dot3 + 1, dot4 - dot3 - 1, StandardCharsets.ISO_8859_1));
        jwe.setTag(new String(compactJWE, dot4 + 1, compactJWE.length - dot4 - 1, StandardCharsets.ISO_8859_1));

        return jwe;
    }

    public XJWEprotected ProtectedfromJSON(byte[] json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = new java.util.HashMap<>(4);
        com.modirum.ds.json.JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);

        XJWEprotected o = u.unmarshal(new javax.xml.transform.stream.StreamSource(new ByteArrayInputStream(json)),
                                      XJWEprotected.class).getValue();
        returnUnmarshaller(u);
        o.setDuplicateCounts(dups);
        return o;
    }

    @Override
    public T createNewMessage() {
        return (T) new TDSMessage();
    }

    @Override
    public List getCardRangeData(T m) {
        return ((TDSMessage) m).getCardRangeData();
    }

    public XacsSignedContentPayload getPayloadFromACSSignedContent(String acsSignedContent) throws Exception {
        XJWSCompact jwsc = fromCompactJWS(acsSignedContent);
        byte[] json = Base64.decodeURL(jwsc.getPayload());
        Unmarshaller u = getJSONUnmarshaller();
        XacsSignedContentPayload xpl = u.unmarshal(
                new javax.xml.transform.stream.StreamSource(new ByteArrayInputStream(json)),
                XacsSignedContentPayload.class).getValue();
        returnUnmarshaller(u);

        return xpl;
    }
}
