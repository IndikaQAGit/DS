package com.modirum.ds.services;

import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.dao.PersistenceService;

public class ThreeDSProfileService {

    private final PersistenceService persistenceService;

    public ThreeDSProfileService(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public TDSServerProfile getTdsServerProfileById(final Long threeDSProfileId) {
        return persistenceService.getPersistableByIdUnchecked(threeDSProfileId, TDSServerProfile.class);
    }

    public boolean isExistingDuplicateTdsRequestorId(String requestorId, Integer paymentSystemId, Long tdsProfileId) {
        return persistenceService.getTdsServerProfile(requestorId, paymentSystemId, tdsProfileId) != null;
    }

    public TDSServerProfile getActiveTdsServerProfile(String tdsOperatorId, Integer paymentSystemId) {
        return persistenceService.getActiveTdsServerProfileByOperatorId(paymentSystemId, tdsOperatorId);
    }
}
