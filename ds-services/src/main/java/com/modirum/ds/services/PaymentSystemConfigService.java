package com.modirum.ds.services;

import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import com.modirum.ds.db.model.User;
import com.modirum.ds.jdbc.core.dao.PaymentSystemConfigDAO;
import com.modirum.ds.services.db.jdbc.audit.AuditLogJdbcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PaymentSystemConfigService {
    @Autowired
    private AuditLogJdbcService auditLogJdbcService;
    @Autowired
    private PaymentSystemConfigDAO paymentSystemConfigDAO;

    public PaymentSystemConfig getPaymentSystemConfig(Integer dsId, Integer paymentSystemId, String key) {
        return paymentSystemConfigDAO.getPaymentSystemConfig(dsId, paymentSystemId, key);
    }

    public Map<String, PaymentSystemConfig> getPaymentSystemConfigs(Integer dsId, Integer paymentSystemId) {
        List<PaymentSystemConfig> paymentSystemConfigs = paymentSystemConfigDAO.getPaymentSystemConfigs(dsId,
                                                                                                        paymentSystemId);
        Map<String, PaymentSystemConfig> paymentSystemConfigMap = paymentSystemConfigs.stream().collect(
                Collectors.toMap(PaymentSystemConfig::getKey, paymentSystemConfig -> paymentSystemConfig));
        return paymentSystemConfigMap;
    }

    public void saveOrUpdatePaymentSystemConfig(PaymentSystemConfig updatedPaymentSystemConfig, User user) {
        if (isPaymentSystemConfigExisting(updatedPaymentSystemConfig.getDsId(),
                                          updatedPaymentSystemConfig.getPaymentSystemId(),
                                          updatedPaymentSystemConfig.getKey())) {
            updatePaymentSystemConfig(updatedPaymentSystemConfig, user);
        } else {
            insertPaymentSystemConfig(updatedPaymentSystemConfig, user);
        }
    }

    private boolean isPaymentSystemConfigExisting(Integer dsId, Integer paymentSystemId, String key) {
        return getPaymentSystemConfig(dsId, paymentSystemId, key) != null;
    }

    private void updatePaymentSystemConfig(PaymentSystemConfig updatedPaymentSystemConfig, User user) {
        PaymentSystemConfig paymentSystemConfig = getPaymentSystemConfig(updatedPaymentSystemConfig.getDsId(),
                                                                         updatedPaymentSystemConfig.getPaymentSystemId(),
                                                                         updatedPaymentSystemConfig.getKey());
        paymentSystemConfig.setComment(updatedPaymentSystemConfig.getComment());
        paymentSystemConfig.setValue(updatedPaymentSystemConfig.getValue());
        paymentSystemConfig.setLastModifiedBy(user.getLoginname());
        paymentSystemConfig.setLastModifiedDate(LocalDateTime.now());
        paymentSystemConfigDAO.update(paymentSystemConfig);
        auditLogJdbcService.logUpdate(paymentSystemConfig, user);
    }

    private void insertPaymentSystemConfig(PaymentSystemConfig newPaymentSystemConfig, User user) {
        newPaymentSystemConfig.setCreatedUser(user.getLoginname());
        newPaymentSystemConfig.setCreatedDate(LocalDateTime.now());
        newPaymentSystemConfig.setLastModifiedBy(user.getLoginname());
        newPaymentSystemConfig.setLastModifiedDate(LocalDateTime.now());
        paymentSystemConfigDAO.insert(newPaymentSystemConfig);
        auditLogJdbcService.logUpdate(newPaymentSystemConfig, user);
    }
}