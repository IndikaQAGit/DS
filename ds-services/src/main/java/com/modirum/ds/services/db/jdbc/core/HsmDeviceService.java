package com.modirum.ds.services.db.jdbc.core;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.jdbc.core.dao.HsmDeviceDAO;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.utils.Misc;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("hsmDeviceService")
public class HsmDeviceService {

    @Resource
    private HsmDeviceDAO hsmDeviceDAO;

    @Resource
    private HsmDeviceConfService hsmDeviceConfService;

    private HashMap<Long, HSMDevice> hsmDevices;

    /**
     * Retrieves a HSM Device given an ID
     * @param id
     * @return
     */
    public HsmDevice get(Long id) {
        return hsmDeviceDAO.get(id);
    }

    /**
     * Returns a list of all active HSM Devices
     * @return
     */
    public List<HsmDevice> getActiveHSMDevices() {
        return hsmDeviceDAO.getByStatus(DSModel.HsmDevice.Status.ACTIVE);
    }

    /**
     * Returns a list of all HSM Devices
     * @return
     */
    public List<HsmDevice> getAll() {
        return hsmDeviceDAO.getAll();
    }

    /**
     * Inserts or updates an HSM Device
     * @param hsmDevice
     */
    public void saveOrUpdate(HsmDevice hsmDevice) {
        if (hsmDeviceDAO.exists(hsmDevice.getId())) {
            update(hsmDevice);
        } else {
            insert(hsmDevice);
        }
    }

    /**
     * Updates an HSM Device
     * @param hsmDevice
     */
    public void update(HsmDevice hsmDevice) {
        hsmDeviceDAO.update(hsmDevice);
    }

    /**
     * Inserts an HSM Device
     * @param hsmDevice
     */
    public void insert(HsmDevice hsmDevice) {
        hsmDeviceDAO.insert(hsmDevice);
    }

    /**
     * Checks if an active HSM Device exists given a name and ID
     * @param name
     * @param id
     * @return
     */
    public boolean isExistingActiveHSMDeviceName(String name, Long id) {
        List<HsmDevice> hsmDevices = hsmDeviceDAO.getByName(name);
        if (Misc.isNotNullOrEmpty(hsmDevices)) {
            return hsmDevices.stream()
                .filter(hsmDevice -> (id == null || !hsmDevice.getId().equals(id)) &&
                        hsmDevice.getStatus().equals(DSModel.HsmDevice.Status.ACTIVE))
                .findAny().isPresent();
        }
        return false;
    }

    /**
     * Looks up, initializes and returns all HSM devices registered in DS. Device lookup from DB is performed once at
     * first call to this method. Subsequent calls return cached values
     *
     * @return Map of hsmID - HSM device instances.
     */
    public Map<Long, HSMDevice> getInitializedHSMDevices() {
        if (hsmDevices != null) {
            return hsmDevices;
        }

        hsmDevices = new HashMap<>();
        try {
            List<HsmDevice> dbHsmDevices = getActiveHSMDevices();
            if (dbHsmDevices == null || dbHsmDevices.isEmpty()) {
                return null;
            }

            for (HsmDevice dbHsm : dbHsmDevices) {
                HSMDevice hsmDevice = (HSMDevice) Class.forName(dbHsm.getClassName()).newInstance();
                HsmDeviceConf hsmConf = hsmDeviceConfService.get(dbHsm.getId());
                String cnfStr = hsmConf != null ? hsmConf.getConfig() : null;
                hsmDevice.initializeHSM(cnfStr);
                hsmDevices.put(dbHsm.getId(), hsmDevice);
            }
        } catch (Exception e) {
            throw new RuntimeException("Error initializing HSM devices", e);
        }
        return hsmDevices;
    }

    /**
     * Return an initialized HSM Device given its ID
     * @param hsmDeviceId
     * @return
     */
    public HSMDevice getInitializedHSMDevice(Long hsmDeviceId) {
        return getInitializedHSMDevices().get(hsmDeviceId);
    }

}
