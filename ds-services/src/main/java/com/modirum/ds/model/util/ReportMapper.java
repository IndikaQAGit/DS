package com.modirum.ds.model.util;

import com.modirum.ds.db.model.filter.ReportListItem;
import com.modirum.ds.model.ReportSearcher;

public class ReportMapper {

    public static ReportSearcher mapFromDbModel(ReportListItem filter) {
        return new ReportSearcher(filter.getPaymentSystemId(), filter.getTotal(), filter.getStart(), filter.getLimit());
    }

    public static ReportListItem mapToDbModel(ReportSearcher searcher) {
        return new ReportListItem(searcher.getPaymentSystemId(), searcher.getTotal(), searcher.getStart(), searcher.getLimit());
    }

}
