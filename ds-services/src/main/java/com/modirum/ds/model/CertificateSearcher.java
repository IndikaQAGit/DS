/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.01.2016
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.db.model.CertificateData;

import java.util.Date;

public class CertificateSearcher extends CertificateData implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
    private String keyword;
    Date expiresFrom;
    Date expiresTo;
    Date issuedFrom;
    Date issuedTo;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public java.util.Date getExpiresFrom() {
        return expiresFrom;
    }

    public void setExpiresFrom(java.util.Date expiresFrom) {
        this.expiresFrom = expiresFrom;
    }

    public java.util.Date getExpiresTo() {
        return expiresTo;
    }

    public void setExpiresTo(java.util.Date expiresTo) {
        this.expiresTo = expiresTo;
    }

    public java.util.Date getIssuedFrom() {
        return issuedFrom;
    }

    public void setIssuedFrom(java.util.Date issuedFrom) {
        this.issuedFrom = issuedFrom;
    }

    public java.util.Date getIssuedTo() {
        return issuedTo;
    }

    public void setIssuedTo(java.util.Date issuedTo) {
        this.issuedTo = issuedTo;
    }


}
