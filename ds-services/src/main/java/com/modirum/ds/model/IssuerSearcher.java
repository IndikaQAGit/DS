/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.01.2016
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.db.model.Issuer;

import java.io.Serializable;

public class IssuerSearcher extends Issuer implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
    private String keyword;
    private String binMatch;
    private String pan;

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getBinMatch() {
        return binMatch;
    }

    public void setBinMatch(String binMatch) {
        this.binMatch = binMatch;
    }
}
