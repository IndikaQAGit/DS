package com.modirum.ds.imports.issuer.parser;

import com.modirum.ds.imports.AbstractCsvParser;
import com.modirum.ds.imports.CSVParserException;
import com.modirum.ds.imports.DataBatchBundle;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.util.DSUtils;
import org.apache.commons.csv.CSVRecord;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IssuerCSVParser extends AbstractCsvParser<BatchIssuerModel> {

    private static final String CSV_HEADER_BIN     = "bin";
    private static final String CSV_HEADER_NAME    = "name";
    private static final String CSV_HEADER_PHONE   = "phone";
    private static final String CSV_HEADER_EMAIL   = "email";
    private static final String CSV_HEADER_ADDRESS = "address";
    private static final String CSV_HEADER_CITY    = "city";
    private static final String CSV_HEADER_COUNTRY = "country";
    private static final String CSV_HEADER_STATUS  = "status";
    private static final String CSV_HEADER_ACTION  = "action";
    private static final String CSV_HEADER_PAYMENT_SYSTEM_ID  = "paymentSystemId";

    @Override
    protected Map<CSVHeaderFormat, HeaderFormat> getHeaderFormats() {
        Map<CSVHeaderFormat, HeaderFormat> headerFormats = new HashMap<>();
        headerFormats.put(CSVHeaderFormat.CHANGE_ISSUER_STATE, new HeaderFormat(Arrays.asList(CSV_HEADER_PAYMENT_SYSTEM_ID, CSV_HEADER_NAME, CSV_HEADER_ACTION), false));
        headerFormats.put(CSVHeaderFormat.ADD_ISSUER, new HeaderFormat(Arrays.asList(CSV_HEADER_BIN, CSV_HEADER_NAME, CSV_HEADER_STATUS, CSV_HEADER_PAYMENT_SYSTEM_ID), false));
        return headerFormats;
    }

    @Override
    protected DataBatchBundle<BatchIssuerModel> handleHeaderFormat(final CSVHeaderFormat headerDataFormat,
                                                                   final List<String> headerNames,
                                                                   final List<CSVRecord> records) throws CSVParserException {
        List<BatchIssuerModel> importModels;
        switch (headerDataFormat) {
            case ADD_ISSUER:
                importModels = DSUtils.toList(records.stream().map(record -> mapRecordToIssuerImportModel(headerNames, record)));
                break;
            case CHANGE_ISSUER_STATE:
                importModels = DSUtils.toList(records.stream().map(this::mapRecordToIssuerChangeStateModel));
                break;
            default:
                throw new CSVParserException("Unsupported header data format: " + headerDataFormat);
        }
        return new DataBatchBundle<>(headerDataFormat, importModels);
    }

    private BatchIssuerModel mapRecordToIssuerChangeStateModel(final CSVRecord record) {
        final BatchIssuerModel.BatchIssuerModelBuilder builder = BatchIssuerModel.builder();
        builder.name(record.get(CSV_HEADER_NAME));
        builder.paymentSystemId(record.get(CSV_HEADER_PAYMENT_SYSTEM_ID));
        DataChangeStateOperation operation = DataChangeStateOperation.valueOf(record.get(CSV_HEADER_ACTION).toUpperCase());
        if (DataChangeStateOperation.ADD.equals(operation)) {
            throw new IllegalStateException("'Add' is not valid action for this CSV file format");
        }
        builder.operation(operation);
        return builder.build();
    }

    private BatchIssuerModel mapRecordToIssuerImportModel(final List<String> headerNames, final CSVRecord record) {
        final BatchIssuerModel.BatchIssuerModelBuilder builder = BatchIssuerModel.builder();
        builder.operation(DataChangeStateOperation.ADD);

        // Required fields
        setField(headerNames, CSV_HEADER_BIN, record, builder::bin);
        setField(headerNames, CSV_HEADER_NAME, record, builder::name);
        setField(headerNames, CSV_HEADER_STATUS, record, builder::status);
        setField(headerNames, CSV_HEADER_PAYMENT_SYSTEM_ID, record, builder::paymentSystemId);

        // Optional fields
        setField(headerNames, CSV_HEADER_PHONE, record, builder::phone);
        setField(headerNames, CSV_HEADER_ADDRESS, record, builder::address);
        setField(headerNames, CSV_HEADER_EMAIL, record, builder::email);
        setField(headerNames, CSV_HEADER_COUNTRY, record, builder::country);
        setField(headerNames, CSV_HEADER_CITY, record, builder::city);
        return builder.build();
    }
}
