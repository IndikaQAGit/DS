package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.services.MerchantService;

import java.util.List;

public class ActivateMerchantOperationProcessor implements IDataOperationProcessor<BatchMerchantModel> {

    private final MerchantService merchantService;

    private int processedCount;

    public ActivateMerchantOperationProcessor(final MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @Override
    public void process(final List<BatchMerchantModel> merchants) {
        merchants.stream()
                 .peek(e -> processedCount++)
                 .forEach(model -> merchantService.setMerchantActive(model.getName(), Integer.parseInt(model.getPaymentSystemId())));
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }
}
