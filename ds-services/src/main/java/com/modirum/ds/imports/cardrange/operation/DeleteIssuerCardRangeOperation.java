package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.services.CardRangeService;

import java.util.List;

public class DeleteIssuerCardRangeOperation implements IDataOperationProcessor<BatchCardRangeModel> {

    private final CardRangeService cardRangeService;

    private int processedCount;

    public DeleteIssuerCardRangeOperation(final CardRangeService cardRangeService) {
        this.cardRangeService = cardRangeService;
    }

    @Override
    public void process(List<BatchCardRangeModel> dataList) {
        for (BatchCardRangeModel cardRangeModel : dataList) {
            boolean removed = cardRangeService.deleteCardRange(cardRangeModel.getBinStart(), cardRangeModel.getBinEnd(), cardRangeModel.getPaymentSystemId());
            if (removed) {
                processedCount++;
            }
        }
    }

    @Override
    public int processedCount() {
        return processedCount;
    }
}
