package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.BaseBatchModel;
import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.AbstractOperationProcessorProvider;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.services.MerchantService;
import com.modirum.ds.db.dao.PersistenceService;

import java.util.HashMap;
import java.util.Map;

public final class MerchantOperationProcessorsProvider extends AbstractOperationProcessorProvider {

    private final MerchantService merchantService;

    public MerchantOperationProcessorsProvider(final PersistenceService persistenceService,
                                               final MerchantService merchantService) {
        super(persistenceService);
        this.merchantService = merchantService;
    }

    @Override
    public Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> initProviders() {
        Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> processors = new HashMap<>();
        processors.put(DataChangeStateOperation.ADD, new AddMerchantOperationProcessor(this.persistenceService));
        processors.put(DataChangeStateOperation.DELETE, new DeleteMerchantOperationProcessor(this.persistenceService));
        processors.put(DataChangeStateOperation.DISABLE, new DisableMerchantOperationProcessor(this.merchantService));
        processors.put(DataChangeStateOperation.ACTIVATE, new ActivateMerchantOperationProcessor(this.merchantService));
        return processors;
    }
}
