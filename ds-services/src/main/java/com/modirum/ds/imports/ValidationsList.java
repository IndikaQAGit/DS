package com.modirum.ds.imports;

import com.modirum.ds.imports.AbstractBatchDataValidator.FilterParams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ValidationsList<D> {

    private final List<Function<FilterParams<D>, Boolean>> validationsList;

    public ValidationsList() {
        this.validationsList = new ArrayList<>();
    }

    public void add(final Function<FilterParams<D>, Boolean> param) {
        this.validationsList.add(param);
    }

    public List<Function<FilterParams<D>, Boolean>> getValidationsList() {
        return this.validationsList;
    }
}
