package com.modirum.ds.imports;

import com.modirum.ds.db.model.User;

public class DataBatchHandlerConfiguration {

    private boolean continueOnError;
    private User runningUser;

    public DataBatchHandlerConfiguration(User runningUser) {
       this.runningUser = runningUser;
    }

    public boolean isContinueOnError() {
        return continueOnError;
    }

    public boolean isSuperuser() {
        return runningUser.getPaymentSystemId() == null;
    }

    public Integer getUserPaymentSystemId() {
        return runningUser.getPaymentSystemId();
    }

    public void setContinueOnError(final boolean continueOnError) {
        this.continueOnError = continueOnError;
    }
}
