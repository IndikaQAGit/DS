package com.modirum.ds.imports;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public abstract class BaseBatchModel {

    private DataChangeStateOperation operation;
    private String paymentSystemId;

    public DataChangeStateOperation getOperation() {
        return operation;
    }

    public String getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(String paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
