package com.modirum.ds.imports.issuer;

import com.modirum.ds.imports.AbstractCsvParser;
import com.modirum.ds.imports.AbstractBatchHandlerFacade;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.AbstractBatchDataValidator;
import com.modirum.ds.imports.AbstractOperationProcessorProvider;
import com.modirum.ds.imports.issuer.operation.IssuerOperationProcessorProvider;
import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.imports.issuer.parser.IssuerCSVParser;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;

public class IssuerBatchHandlerFacade extends AbstractBatchHandlerFacade<BatchIssuerModel> {

    public IssuerBatchHandlerFacade(PersistenceService persistenceService, DataBatchHandlerConfiguration configuration) {
        super(persistenceService, configuration);
    }

    @Override
    protected AbstractOperationProcessorProvider createProcessorsProvider() {
        return new IssuerOperationProcessorProvider(this.persistenceService,
                                                    ServiceLocator.getInstance().getIssuerService());
    }

    @Override
    protected AbstractCsvParser<BatchIssuerModel> createParser() {
        return new IssuerCSVParser();
    }

    @Override
    protected AbstractBatchDataValidator<BatchIssuerModel> createDataValidator() {
        return new IssuerBatchModelValidator(this.configuration,
                                             ServiceLocator.getInstance().getPaymentSystemService(),
                                             ServiceLocator.getInstance().getIssuerService());
    }
}