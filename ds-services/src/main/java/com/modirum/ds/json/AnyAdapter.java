/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on Mar 8, 2018
 *
 */
package com.modirum.ds.json;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AnyAdapter extends XmlAdapter<Object, Object> {

    public Object unmarshal(Object value) {
        return value;
    }

    public Object marshal(Object value) {
        return value;
    }
}

