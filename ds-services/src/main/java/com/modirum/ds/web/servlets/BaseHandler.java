/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public abstract class BaseHandler extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected transient static Logger log = LoggerFactory.getLogger(BaseHandler.class);

    protected String sinf = null;

    public String getServletInfo() {
        if (sinf == null) {
            StringBuilder si = new StringBuilder();
            si.append(this.getClass().getName());

            try {
                java.util.Enumeration<java.net.URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
                while (resources.hasMoreElements()) {
                    java.util.jar.Manifest mf = new java.util.jar.Manifest(resources.nextElement().openStream());
                    if (mf.getMainAttributes().getValue("Implementation-Title") != null &&
                            mf.getMainAttributes().getValue("Implementation-Title").startsWith("Modirum")) {
                        si.append("; Implementation-Title: " + mf.getMainAttributes().getValue("Implementation-Title"));
                        si.append("; Implementation-Version: " + mf.getMainAttributes().getValue("Implementation-Version"));
                        break;
                    }
                }
            } catch (Throwable e) {
                log.warn("Error reading manifest", e);
            }

            sinf = si.toString();
        }

        return sinf;
    }

    @SuppressWarnings("rawtypes")
    public void debugRequest(HttpServletRequest req) {
        if (log.isInfoEnabled() && req.getAttribute("requestlogged") == null) {
            log.info("request " + req.getMethod() + " " + req.getRequestURI() + " from remhost=" + req.getRemoteHost() + ", remaddr="
                    + req.getRemoteAddr());
        }

        if (log.isDebugEnabled()) {
            StringBuilder hdrs = new StringBuilder(256);
            for (java.util.Enumeration e = req.getHeaderNames(); e.hasMoreElements(); ) {
                String t = (String) e.nextElement();
                hdrs.append(t + "='" + req.getHeader(t) + "'\n");
            }
            log.debug("Headers:\n" + hdrs);

            StringBuilder params = new StringBuilder(256);
            for (java.util.Enumeration e = req.getParameterNames(); e.hasMoreElements(); ) {
                String t = (String) e.nextElement();
                String[] pvls = req.getParameterValues(t);
                StringBuilder values = new StringBuilder();
                for (int i = 0; pvls != null && i < pvls.length; i++) {
                    if (pvls[i] != null) {
                        values.append("'" + pvls[i] + "'");
                    } else {
                        values.append("Null");
                    }
                    if (i < pvls.length - 1) {
                        values.append(", ");
                    }

                }
                // PCI DSS
                if (t != null && (t.startsWith("card") || t.contains("pan"))) {
                    String val = values.toString().replaceAll("[a-zA-Z0-9]", "*");
                    //log.debug("Parameter: " + t + "=" + val);
                    params.append(t + "=" + val + "\n");
                } else {
                    params.append(t + "=" + values + "\n");
                    //log.debug("Parameter: " + t + "=" + values); //request.getParameter(t));
                }
            }
            log.debug("Params:\n" + params);

        }
    }
}
