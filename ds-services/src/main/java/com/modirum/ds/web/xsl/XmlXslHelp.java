/*
 * Copyright (C) 2010 Modirum  (AZS Services Ltd donated to Modirum.)
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.xsl;

import java.io.OutputStream;
//import org.apache.xml.serialize.OutputFormat;
//import org.apache.xml.serialize.XMLSerializer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.modirum.ds.utils.XmlUtils;
import com.modirum.ds.web.blocks.Page;

public class XmlXslHelp {

    protected transient static Logger log = LoggerFactory.getLogger(XFormer.class);

    JAXBContext context = null;
    DocumentBuilder docBuilder = null;
    List<Marshaller> marshallers = new LinkedList<>();
    List<Class> classesToBeBound = new LinkedList<>();

    public void bindClass(Class<?> clazz) {
        classesToBeBound.add(clazz);
        context = null;
    }

    public void initCtx() throws Exception {
        if (context == null) {
            if (classesToBeBound.size() < 1) {
                classesToBeBound.add(Page.class);
            }

            Class<?>[] bindClasses = classesToBeBound.toArray(new Class[classesToBeBound.size()]);
            context = JAXBContext.newInstance(bindClasses, Collections.<String, Object>emptyMap());

            DocumentBuilderFactory dbf = XmlUtils.getXESafeDocumentBuilderFactory();
            docBuilder = dbf.newDocumentBuilder();
        }
    }

    public Document pageToDom(Page pojo) throws Exception {
        initCtx();
        Document document = docBuilder.newDocument();

        Marshaller marshaller = createMarshaller();
        marshaller.marshal(pojo, document);
        returnMarshaller(marshaller);

        return document;
    }

    public Source pageToSource(Page pojo) throws Exception {
        initCtx();
        return new javax.xml.bind.util.JAXBSource(context, pojo);
    }

    public Marshaller createMarshaller() throws Exception {
        if (marshallers.size() > 0) {
            synchronized (marshallers) {
                if (marshallers.size() > 0) {
                    Marshaller m = marshallers.remove(marshallers.size() - 1);
                    if (m != null) {
                        return m;
                    }
                }
            }
        }

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

        return marshaller;
    }

    public void returnMarshaller(Marshaller marshaller) throws Exception {
        if (marshallers.size() < 10) {
            synchronized (marshallers) {
                marshallers.add(marshaller);
            }
        }
    }

    public static void serializeDOMOut(Document dom, OutputStream os) throws Exception {
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.setOutputProperty(OutputKeys.ENCODING, "utf-8");

        DOMSource source = new DOMSource(dom);
        trans.transform(source, new StreamResult(os));
    }

    public static void serializeDOMOut(Document dom, java.io.Writer os) throws Exception {
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.setOutputProperty(OutputKeys.ENCODING, "utf-8");

        DOMSource source = new DOMSource(dom);
        trans.transform(source, new StreamResult(os));
    }
}
