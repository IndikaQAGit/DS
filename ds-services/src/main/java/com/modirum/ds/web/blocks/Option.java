/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "##default")
public class Option extends Label {

    private static final long serialVersionUID = 8530203709172244266L;

    private String value;

    /**
     *
     */
    public Option() {
    }

    /**
     * @param k
     * @param l
     * @param v
     */
    public Option(String k, String l, String v) {
        super(k, l);
        value = v;
    }

    /**
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }
}
