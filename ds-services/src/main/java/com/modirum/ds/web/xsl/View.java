/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.xsl;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.blocks.Page;
import com.modirum.ds.web.context.WebContext;

public class View {

    protected transient static Logger log = LoggerFactory.getLogger(View.class);

    static XmlXslHelp xmlHelp = new XmlXslHelp();

    public static void view(Page page, HttpServletResponse resp, WebContext ctx) {
        view(page, page.getStyleSheet(), resp, ctx);
    }

    public static void view(Page page, String xsl, HttpServletResponse resp, WebContext ctx) {
        long start = System.currentTimeMillis();
        OutputStream os = null;


        org.w3c.dom.Document xmldoc = null;
        try {
            resp.setContentType("text/html;charset=UTF-8");
            os = resp.getOutputStream();

            XFormer former = XFormer.getRenderer(xsl, ctx);
            xmldoc = xmlHelp.pageToDom(page);

            resp.setBufferSize(1024 * 50);
            former.transformDOM2HTMLOut(xmldoc, "UTF-8", os);

        } catch (Throwable fatal) {
            String errId = WebContext.getUnique();
            log.error("Fatal error id " + errId + " " + fatal, fatal);
            try {
                resp.resetBuffer();
            } catch (Exception e) {
                log.error("Buf Reset error: " + e, e);

            }

            if (os != null) {
                StringBuffer sp = new StringBuffer();
                sp.append("<html>");
                sp.append("<head><title>Error</title></head>");

                sp.append("<body>General XSL transformation error, id " + errId + "</body>");
                sp.append("</html>");

                try {
                    os.write(sp.toString().getBytes("UTF-8"));
                } catch (java.io.IOException ioe) {
                    log.error("IO error:" + ioe, ioe);
                }
            }
        } finally {
            if (log.isDebugEnabled() && xmldoc != null) {
                if (log.isDebugEnabled() && xmldoc != null) {
                    debugXML(xmldoc, xsl);
                }
            }
        }

        long end = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("View processed in" + (end - start) + "ms");
        }
    }


    public static void view(Page page, String xsl, OutputStream os, WebContext ctx) {
        long start = System.currentTimeMillis();

        org.w3c.dom.Document xmldoc = null;
        try {

            XFormer former = XFormer.getRenderer(xsl, ctx);
            xmldoc = xmlHelp.pageToDom(page);
            former.transformDOM2HTMLOut(xmldoc, "UTF-8", os);

        } catch (Throwable fatal) {
            log.error("Fatal error: " + fatal, fatal);

            if (os != null) {
                StringBuffer sp = new StringBuffer();
                sp.append("<html>");
                sp.append("<head><title>Error</title></head>");

                sp.append("<body>General XSL transformation error</body>");
                sp.append("</html>");

                try {
                    os.write(sp.toString().getBytes("UTF-8"));
                } catch (java.io.IOException ioe) {
                    log.error("IO error:" + ioe, ioe);
                }
            }
        } finally {

            if (log.isDebugEnabled() && xmldoc != null) {
                if (log.isDebugEnabled() && xmldoc != null) {
                    debugXML(xmldoc, xsl);
                }
            }
        }

        long end = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("View processed in" + (end - start) + "ms");
        }
    }


    public static void view(Page page, String xsl, HttpServletResponse resp, XFormer former) {
        long start = System.currentTimeMillis();
        java.io.OutputStream os = null;
        org.w3c.dom.Document xmldoc = null;
        try {
            resp.setContentType("text/html;charset=UTF-8");
            os = resp.getOutputStream();

            xmldoc = xmlHelp.pageToDom(page);

            resp.setBufferSize(64000);
            former.transformDOM2HTMLOut(xmldoc, "UTF-8", os);

        } catch (Throwable fatal) {
            resp.resetBuffer();
            log.error("Fatal error: " + fatal, fatal);

            if (os != null) {
                StringBuilder sp = new StringBuilder();
                sp.append("<html>");
                sp.append("<head><title>Error</title></head>");

                sp.append("<body>General XSL transformation error</body>");
                sp.append("</html>");

                try {
                    os.write(Misc.toUtf8Bytes(sp));
                } catch (java.io.IOException ioe) {
                    log.error("IO error:" + ioe, ioe);
                }
            }
        } finally {
            if (log.isDebugEnabled() && xmldoc != null) {
                debugXML(xmldoc, xsl);
            }
        }

        long end = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("View processed in" + (end - start) + "ms");
        }
    }


    public static org.w3c.dom.Element getPathElement(org.w3c.dom.Element xmldoc, String path) {
        int psi = path.indexOf("/");
        String tag = null;
        String remainingpath = null;

        if (psi > 0) {
            tag = path.substring(0, psi);
        } else {
            tag = path;
        }
        if (tag == null || tag.length() < 1) {
            return null;
        }
        if (psi > 0 && path.length() > psi) {
            remainingpath = path.substring(psi + 1);
        }
        org.w3c.dom.NodeList nl = xmldoc.getElementsByTagName(tag);

        if (nl == null) {
            return null;
        }
        org.w3c.dom.Node nd = nl.item(0);
        if (nd == null) {
            return null;
        }
        if (remainingpath != null && nd instanceof org.w3c.dom.Element) {

            return getPathElement((org.w3c.dom.Element) nd, remainingpath);
        } else if (remainingpath == null && nd instanceof org.w3c.dom.Element) {
            return (org.w3c.dom.Element) nd;
        }

        return null;
    }


    /**
     * removes pan and cvv data from xml and debugs the xml to log then
     *
     * @param xmldoc
     * @param xsl
     */
    protected static void debugXML(org.w3c.dom.Document xmldoc, String xsl) {
        try {

            // hide sensitive card data
            org.w3c.dom.Element panEl = getPathElement(xmldoc.getDocumentElement(), "actionObject/card/pan");
            if (panEl != null && panEl.getFirstChild() != null) {
                panEl.getFirstChild().setNodeValue("Removed/PA-DSS");
            }

            org.w3c.dom.Element binEl = getPathElement(xmldoc.getDocumentElement(), "actionObject/card/bin");
            if (binEl != null && binEl.getFirstChild() != null) {
                binEl.getFirstChild().setNodeValue("Removed/PA-DSS");
            }


            org.w3c.dom.Element cvvEl = getPathElement(xmldoc.getDocumentElement(), "actionObject/card/cvv");
            if (cvvEl != null && cvvEl.getFirstChild() != null) {
                cvvEl.getFirstChild().setNodeValue("Removed/PA-DSS");
            }

            org.w3c.dom.Element maskedPan6Plus4El = getPathElement(xmldoc.getDocumentElement(), "actionObject/card/maskedPan6Plus4");
            if (maskedPan6Plus4El != null && maskedPan6Plus4El.getFirstChild() != null) {
                maskedPan6Plus4El.getFirstChild().setNodeValue("Removed/PA-DSS");
            }

            org.w3c.dom.Element sdEl = getPathElement(xmldoc.getDocumentElement(), "actionObject/sensitiveData");
            if (sdEl != null) {
                org.w3c.dom.NodeList childs = sdEl.getChildNodes();
                for (int i = 0; childs != null && i < childs.getLength(); i++) {
                    sdEl.replaceChild(xmldoc.createTextNode("PADSS"), childs.item(i));
                }
            }

            org.w3c.dom.Element paEl = getPathElement(xmldoc.getDocumentElement(), "actionObject/paymentAttrs");
            if (paEl != null) {
                org.w3c.dom.NodeList childs = paEl.getChildNodes();
                for (int i = 0; childs != null && i < childs.getLength(); i++) {
                    paEl.replaceChild(xmldoc.createTextNode("PADSS"), childs.item(i));
                }
            }

            org.w3c.dom.Element fldsEl = getPathElement(xmldoc.getDocumentElement(), "actionObject/fields");
            if (fldsEl != null) {
                org.w3c.dom.NodeList childs = fldsEl.getChildNodes();
                for (int i = 0; childs != null && i < childs.getLength(); i++) {

                    org.w3c.dom.Node cld = childs.item(i);
                    if ("value".equals(cld.getLocalName()) && cld.getFirstChild() != null &&
                            cld.getFirstChild().getNodeType() == org.w3c.dom.Node.TEXT_NODE &&
                            cld.getFirstChild().getNodeValue() != null && cld.getFirstChild().getNodeValue().length() > 0) {
                        cld.getFirstChild().setNodeValue("PA-DSS");
                    } else {
                        org.w3c.dom.NodeList childs2 = cld.getChildNodes();
                        for (int j = 0; childs2 != null && j < childs2.getLength(); j++) {
                            org.w3c.dom.Node cld2 = childs2.item(j);
                            if ("value".equals(cld2.getLocalName()) & cld2.getFirstChild() != null &&
                                    cld2.getFirstChild().getNodeType() == org.w3c.dom.Node.TEXT_NODE &&
                                    cld2.getFirstChild().getNodeValue() != null && cld2.getFirstChild().getNodeValue().length() > 0) {
                                cld2.getFirstChild().setNodeValue("PA-DSS");
                            }
                        }
                    }
                }
            }

            java.io.StringWriter sw = new java.io.StringWriter(2048);
            XmlXslHelp.serializeDOMOut(xmldoc, sw);
            log.debug("XML for " + xsl + ":\n" + sw);

        } catch (Throwable ee) {
            log.error("Error debug xml", ee);
        }
    }
}
