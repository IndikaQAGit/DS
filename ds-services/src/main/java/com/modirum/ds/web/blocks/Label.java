/*
 * Copyright (C) 2002 - 2005 AZS Services Ltd.
 * All rights reserved.
 *
 * @author Andri Kruus, AZS Services
 * Estonia Tallinn, http://www.a2zss.com
 * andri@a2zss.com
 *
 * Created on 10.10.2005
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "##default")
public class Label implements java.io.Serializable {

    private static final long serialVersionUID = -3289874281706221766L;
    static java.util.List<Label> lc = new java.util.ArrayList<Label>(500);
    static int lcMaxSize = 500;
    static int lcMinSize = 10;

    String key = null;
    String label = null;

    public static Label newLabel(String k, String l) {
        Label label = null;
        if (lc.size() > lcMinSize) {
            synchronized (lc) {
                label = lc.remove(lc.size() - 1);
            }
        } else
            label = new Label();

        label.key = k;
        label.label = l;

        return label;
    }

    protected void finalize() {

        synchronized (lc) {
            if (lc.size() < lcMaxSize) {
                lc.add(this);
            }
        }
    }


    public Label() {
    }

    public Label(String k, String l) {
        key = k;
        label = l;
    }

    /**
     * Method getKey
     *
     * @return Returns the key.
     */
    public String getKey() {
        return key;
    }

    /**
     * Method setKey
     *
     * @param key The key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Method getLabel
     *
     * @return Returns the label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Method setLabel
     *
     * @param label The label to set.
     */
    public void setLabel(String label) {
        this.label = label;
    }


}
