package com.modirum.ds.paymentsystem.eftpos.messageprocessor;

import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.JsonMessageService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonMessageBuilder {

    private JsonMessageService jsonMessageService = new JsonMessageService();
    private Map<String, Object> elements = new HashMap<>();

    public static JsonMessageBuilder newBuilder() {
        return new JsonMessageBuilder();
    }

    public JsonMessageBuilder add(String key, int value) {
        elements.put(key, value);
        return this;
    }

    public JsonMessageBuilder add(String key, String value) {
        elements.put(key, value);
        return this;
    }

    public JsonMessage build() throws IOException {
        List<String> list = new ArrayList<>();
        for (String key : elements.keySet()) {
            list.add("\""+key+"\":\""+elements.get(key)+"\"");
        }
        return jsonMessageService.parse("{" + String.join(",", list) + "}");
    }
}
