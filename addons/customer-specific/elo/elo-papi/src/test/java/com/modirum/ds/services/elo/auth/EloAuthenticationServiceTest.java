package com.modirum.ds.services.elo.auth;

import com.modirum.ds.db.model.User;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import com.modirum.ds.services.PaymentSystemConfigService;
import com.modirum.ds.services.elo.EloHttpService;
import com.modirum.ds.services.server.PaymentSystemSettingsCacheService;
import elo.api.bcrypter.EloBcrypter;
import mockit.Expectations;
import mockit.Verifications;
import mockit.MockUp;
import mockit.Injectable;
import mockit.Mock;
import mockit.Mocked;
import mockit.Tested;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.HttpURLConnection;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class EloAuthenticationServiceTest {

    @Injectable
    private PaymentSystemConfigService paymentSystemConfigService;
    @Injectable
    private PaymentSystemSettingsCacheService psSettingsCacheService;
    @Injectable
    private EloHttpService eloHttpService;
    @Tested
    private EloAuthenticationService eloAuthenticationService;

    @Test
    public void testCreateUser_Success(@Mocked HttpURLConnection httpURLConnection) throws Exception {
        String username = "md-elo-user11";
        String graphqlEndpoint = "https://hml-api.elo.com.br/graphql";
        String password = "1234567890";
        String bcryptPassword = "bcryptPass";
        new MockUp<EloBcrypter>() {
            @Mock
            public String encryptPassword(String uname, String pass) {
                return username.equals(uname) && password.equals(pass) ? bcryptPassword : "otherEncryptedPassword";
            }
        };
        String clientId = "8455a143-2e96-3956-b889-dcdf7c416bd6";
        String clientSecret = "324834ac-2aad-3a3f-9b63-0ac9b10f3b9b";
        int eloPaymentSystemId = 1;
        String cpf = "828.463.041-90";
        String responseData = "{\"data\":{\"createUser\":{\"clientMutationId\":\"1\",\"id\":\"f41790b8-cb8d-3f4a-99fa-3219dd94eb39\",\"name\":\"Modirum DS\",\"oauthToken\":{\"accessToken\":\"f41790b8-cb8d-3f4a-99fa-3219dd94eb39\",\"refreshToken\":\"957b12e4-fa6e-3427-bdcf-9a69db89b873\"}}}}";

        new Expectations() {{
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_AUTH_URL);
            result = graphqlEndpoint;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            result = clientId;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            result = clientSecret;
            eloHttpService.getPostConnection(eloPaymentSystemId, graphqlEndpoint);
            result = httpURLConnection;
            eloHttpService.sendHttpRequest(anyString, httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(responseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
        }};

        Boolean success = eloAuthenticationService.createUser(eloPaymentSystemId, username, password, cpf);
        Assertions.assertTrue(success);

        new Verifications() {{
            String query;
            eloHttpService.sendHttpRequest(query = withCapture(), httpURLConnection);
            Assertions.assertTrue(StringUtils.isNotEmpty(query));
            Assertions.assertTrue(query.contains("username: \\\"" + username + "\\\""));
            Assertions.assertTrue(query.contains("cpf: \\\"" + cpf + "\\\""));
            Assertions.assertTrue(query.contains("bcryptPassword: \\\"" + bcryptPassword + "\\\""));

            paymentSystemConfigService.saveOrUpdatePaymentSystemConfig(withInstanceOf(PaymentSystemConfig.class), withInstanceOf(User.class));
            times = 2;
        }};
    }

    @Test
    public void testCreateUser_ExistingCPF(@Mocked HttpURLConnection httpURLConnection) throws Exception {
        String username = "md-elo-user11";
        String graphqlEndpoint = "https://hml-api.elo.com.br/graphql";
        String password = "1234567890";
        String bcryptPassword = "bcryptPass";
        new MockUp<EloBcrypter>() {
            @Mock
            public String encryptPassword(String uname, String pass) {
                return username.equals(uname) && password.equals(pass) ? bcryptPassword : "otherEncryptedPassword";
            }
        };
        String clientId = "8455a143-2e96-3956-b889-dcdf7c416bd6";
        String clientSecret = "324834ac-2aad-3a3f-9b63-0ac9b10f3b9b";
        int eloPaymentSystemId = 1;
        String cpf = "828.463.041-90";
        String responseData = "{\"errors\":[{\"message\":\"[{\\\"code\\\":\\\"412.016\\\",\\\"description\\\":\\\"CPF, RG or CNPJ existing.\\\"}]\",\"locations\":[{\"line\":2,\"column\":3}],\"path\":[\"createUser\"],\"extensions\":{\"code\":\"412\"}}],\"data\":null}";

        new Expectations() {{
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_AUTH_URL);
            result = graphqlEndpoint;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            result = clientId;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            result = clientSecret;
            eloHttpService.getPostConnection(eloPaymentSystemId, graphqlEndpoint);
            result = httpURLConnection;
            eloHttpService.sendHttpRequest(anyString, httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(responseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
        }};

        Boolean success = eloAuthenticationService.createUser(eloPaymentSystemId, username, password, cpf);
        Assertions.assertFalse(success);

        new Verifications() {{
            String query;
            eloHttpService.sendHttpRequest(query = withCapture(), httpURLConnection);
            Assertions.assertTrue(StringUtils.isNotEmpty(query));
            Assertions.assertTrue(query.contains("username: \\\"" + username + "\\\""));
            Assertions.assertTrue(query.contains("cpf: \\\"" + cpf + "\\\""));
            Assertions.assertTrue(query.contains("bcryptPassword: \\\"" + bcryptPassword + "\\\""));
        }};
    }

    @Test
    public void testCreateUser_ExistingUsername(@Mocked HttpURLConnection httpURLConnection) throws Exception {
        String username = "md-elo-user11";
        String graphqlEndpoint = "https://hml-api.elo.com.br/graphql";
        String password = "1234567890";
        String bcryptPassword = "bcryptPass";
        new MockUp<EloBcrypter>() {
            @Mock
            public String encryptPassword(String uname, String pass) {
                return username.equals(uname) && password.equals(pass) ? bcryptPassword : "otherEncryptedPassword";
            }
        };
        String clientId = "8455a143-2e96-3956-b889-dcdf7c416bd6";
        String clientSecret = "324834ac-2aad-3a3f-9b63-0ac9b10f3b9b";
        int eloPaymentSystemId = 1;
        String cpf = "828.463.041-90";
        String responseData = "{\"errors\":[{\"message\":\"[{\\\"code\\\":\\\"422.005\\\",\\\"description\\\":\\\"Error process login: User name already exists for application.\\\"}]\",\"locations\":[{\"line\":2,\"column\":3}],\"path\":[\"createUser\"],\"extensions\":{\"code\":\"422\"}}],\"data\":null}";

        new Expectations() {{
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_AUTH_URL);
            result = graphqlEndpoint;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            result = clientId;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            result = clientSecret;
            eloHttpService.getPostConnection(eloPaymentSystemId, graphqlEndpoint);
            result = httpURLConnection;
            eloHttpService.sendHttpRequest(anyString, httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(responseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
        }};

        Boolean success = eloAuthenticationService.createUser(eloPaymentSystemId, username, password, cpf);
        Assertions.assertFalse(success);

        new Verifications() {{
            String query;
            eloHttpService.sendHttpRequest(query = withCapture(), httpURLConnection);
            Assertions.assertTrue(StringUtils.isNotEmpty(query));
            Assertions.assertTrue(query.contains("username: \\\"" + username + "\\\""));
            Assertions.assertTrue(query.contains("cpf: \\\"" + cpf + "\\\""));
            Assertions.assertTrue(query.contains("bcryptPassword: \\\"" + bcryptPassword + "\\\""));
        }};
    }

    @Test
    public void testLogin_Success(@Mocked HttpURLConnection httpURLConnection) throws Exception {
        String username = "dsUser";
        String graphqlEndpoint = "https://hml-api.elo.com.br/graphql";
        String password = "password";
        String bcryptPassword = "bcryptPass";
        String challenge = "challenge";
        String loginSalt = "loginSalt";
        new MockUp<EloBcrypter>() {
            @Mock
            public String encryptPassword(String uname, String pass) {
                return username.equals(uname) && password.equals(pass) ? bcryptPassword : "otherEncryptedPassword";
            }
            @Mock
            public String createLoginChallenge(String bPassword, String salt) {
                return bcryptPassword.equals(bPassword) && loginSalt.equals(salt) ? challenge : "otherChallenge";
            }
        };
        String clientId = "8455a143-2e96-3956-b889-dcdf7c416bd6";
        String clientSecret = "324834ac-2aad-3a3f-9b63-0ac9b10f3b9b";
        int eloPaymentSystemId = 1;
        String loginSaltResponseData = "{\"data\": { \"createLoginSalt\": { \"username\": \"dsUser\", \"salt\": \"" + loginSalt + "\"}}}";
        String loginResponseData = "{ \"data\": {  \"login\": { \"clientMutationId\": \"1\",\"oauthToken\": { \"accessToken\": \"dsAccessToken\", \"refreshToken\": \"dsRefreshToken\"}}}}";
        new Expectations() {{
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_USERNAME);
            result = username;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_PASSWORD);
            result = password;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_AUTH_URL);
            result = graphqlEndpoint;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            result = clientId;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            result = clientSecret;
            eloHttpService.getPostConnection(eloPaymentSystemId, graphqlEndpoint);
            result = httpURLConnection;
            eloHttpService.sendHttpRequest(withSubstring("createLoginSalt"), httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(loginSaltResponseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
            eloHttpService.sendHttpRequest(withSubstring("login"), httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(loginResponseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
        }};

        String accessToken = eloAuthenticationService.getAccessToken(eloPaymentSystemId);
        Assertions.assertEquals("dsAccessToken", accessToken);

        new Verifications() {{
            List<String> queryList = new ArrayList<>();
            eloHttpService.sendHttpRequest(withCapture(queryList), httpURLConnection);

            String loginSaltQuery = queryList.get(0);
            Assertions.assertTrue(StringUtils.isNotEmpty(loginSaltQuery));
            Assertions.assertTrue(loginSaltQuery.contains("username: \\\"" + username + "\\\""));

            String loginQuery = queryList.get(1);
            Assertions.assertTrue(StringUtils.isNotEmpty(loginQuery));
            Assertions.assertTrue(loginQuery.contains("username: \\\"" + username + "\\\""));
            Assertions.assertTrue(loginQuery.contains("challenge: \\\"" + challenge + "\\\""));
        }};
    }

    @Test
    public void testLogin_ExistingValidAccessToken(
            @Injectable EloAuthToken eloAuthToken,
            @Tested EloAuthenticationService eloAuthenticationService) throws Exception {
        String accessTokenStub = "dsAccessToken";
        new Expectations() {{
            eloAuthToken.getTimestamp();
            result = LocalDateTime.now().plus(EloAuthenticationService.accessTokenMaxLifeMinutes, ChronoUnit.MINUTES);
            eloAuthToken.getAccessToken();
            result = accessTokenStub;
        }};

        String accessToken = eloAuthenticationService.getAccessToken(1);
        Assertions.assertEquals(accessTokenStub, accessToken);

        new Verifications() {{
            eloAuthToken.getTimestamp();
            eloAuthToken.getAccessToken();
        }};
    }

    @Test
    public void testRefreshToken_Success(
            @Mocked HttpURLConnection httpURLConnection,
            @Injectable EloAuthToken eloAuthToken,
            @Tested EloAuthenticationService eloAuthenticationService) throws Exception {
        String graphqlEndpoint = "https://hml-api.elo.com.br/graphql";
        String refreshToken = "dsRefreshToken";
        String clientId = "8455a143-2e96-3956-b889-dcdf7c416bd6";
        String clientSecret = "324834ac-2aad-3a3f-9b63-0ac9b10f3b9b";
        int eloPaymentSystemId = 1;
        String refreshAccessTokenResponseData = "{ \"data\": {  \"refreshAccessToken\": { \"clientMutationId\": \"1\",\"oauthToken\": { \"accessToken\": \"dsAccessToken\", \"refreshToken\": \"dsRefreshToken\"}}}}";
        new Expectations() {{

            eloAuthToken.getTimestamp();
            result = LocalDateTime.now().minus(EloAuthenticationService.accessTokenMaxLifeMinutes, ChronoUnit.MINUTES);
            eloAuthToken.getRefreshToken();
            result = refreshToken;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_AUTH_URL);
            result = graphqlEndpoint;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            result = clientId;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            result = clientSecret;
            eloHttpService.getPostConnection(eloPaymentSystemId, graphqlEndpoint);
            result = httpURLConnection;
            eloHttpService.sendHttpRequest(withSubstring("refreshAccessToken"), httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(refreshAccessTokenResponseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
        }};

        String accessToken = eloAuthenticationService.getAccessToken(eloPaymentSystemId);
        Assertions.assertEquals("dsAccessToken", accessToken);

        new Verifications() {{
            String query;
            eloHttpService.sendHttpRequest(query = withCapture(), httpURLConnection);

            Assertions.assertTrue(StringUtils.isNotEmpty(query));
            Assertions.assertTrue(query.contains("refreshAccessToken"));
            Assertions.assertTrue(query.contains("refreshToken: \\\"" + refreshToken + "\\\""));
        }};
    }

    @Test
    public void testRefreshToken_Error(
            @Mocked HttpURLConnection httpURLConnection,
            @Injectable EloAuthToken eloAuthToken,
            @Tested EloAuthenticationService eloAuthenticationService) throws Exception {
        String username = "dsUser";
        String graphqlEndpoint = "https://hml-api.elo.com.br/graphql";
        String password = "password";
        String bcryptPassword = "bcryptPass";
        String challenge = "challenge";
        String loginSalt = "loginSalt";
        new MockUp<EloBcrypter>() {
            @Mock
            public String encryptPassword(String uname, String pass) {
                return username.equals(uname) && password.equals(pass) ? bcryptPassword : "otherEncryptedPassword";
            }
            @Mock
            public String createLoginChallenge(String bPassword, String salt) {
                return bcryptPassword.equals(bPassword) && loginSalt.equals(salt) ? challenge : "otherChallenge";
            }
        };
        String refreshToken = "dsRefreshToken";
        String clientId = "8455a143-2e96-3956-b889-dcdf7c416bd6";
        String clientSecret = "324834ac-2aad-3a3f-9b63-0ac9b10f3b9b";
        int eloPaymentSystemId = 1;
        String refreshAccessTokenResponseData = "{ \"error\": []}";
        String loginSaltResponseData = "{\"data\": { \"createLoginSalt\": { \"username\": \"dsUser\", \"salt\": \"" + loginSalt + "\"}}}";
        String loginResponseData = "{ \"data\": {  \"login\": { \"clientMutationId\": \"1\",\"oauthToken\": { \"accessToken\": \"dsAccessToken\", \"refreshToken\": \"dsRefreshToken\"}}}}";
        new Expectations() {{

            eloAuthToken.getTimestamp();
            result = LocalDateTime.now().minus(EloAuthenticationService.accessTokenMaxLifeMinutes, ChronoUnit.MINUTES);
            eloAuthToken.getRefreshToken();
            result = refreshToken;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_USERNAME);
            result = username;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_PASSWORD);
            result = password;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_AUTH_URL);
            result = graphqlEndpoint;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            result = clientId;
            psSettingsCacheService.getPaymentSystemSetting(eloPaymentSystemId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            result = clientSecret;
            eloHttpService.getPostConnection(eloPaymentSystemId, graphqlEndpoint);
            result = httpURLConnection;
            eloHttpService.sendHttpRequest(withSubstring("refreshAccessToken"), httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(refreshAccessTokenResponseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();

            eloHttpService.sendHttpRequest(withSubstring("createLoginSalt"), httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(loginSaltResponseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
            eloHttpService.sendHttpRequest(withSubstring("login"), httpURLConnection);
            result = EloHttpService.Response
                    .builder()
                    .body(loginResponseData)
                    .statusCode(HttpURLConnection.HTTP_OK)
                    .build();
        }};

        String accessToken = eloAuthenticationService.getAccessToken(eloPaymentSystemId);
        Assertions.assertEquals("dsAccessToken", accessToken);

        new Verifications() {{

            List<String> queryList = new ArrayList<>();
            eloHttpService.sendHttpRequest(withCapture(queryList), httpURLConnection);

            String refreshTokenQuery = queryList.get(0);
            Assertions.assertTrue(StringUtils.isNotEmpty(refreshTokenQuery));
            Assertions.assertTrue(refreshTokenQuery.contains("refreshAccessToken"));
            Assertions.assertTrue(refreshTokenQuery.contains("refreshToken: \\\"" + refreshToken + "\\\""));

            String loginSaltQuery = queryList.get(1);
            Assertions.assertTrue(StringUtils.isNotEmpty(loginSaltQuery));
            Assertions.assertTrue(loginSaltQuery.contains("username: \\\"" + username + "\\\""));

            String loginQuery = queryList.get(2);
            Assertions.assertTrue(StringUtils.isNotEmpty(loginQuery));
            Assertions.assertTrue(loginQuery.contains("username: \\\"" + username + "\\\""));
            Assertions.assertTrue(loginQuery.contains("challenge: \\\"" + challenge + "\\\""));
        }};
    }
}
