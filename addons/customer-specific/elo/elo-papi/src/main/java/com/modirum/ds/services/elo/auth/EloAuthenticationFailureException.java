package com.modirum.ds.services.elo.auth;

/**
 * Exception thrown when ELO authentication fails.
 */
public class EloAuthenticationFailureException extends Exception {
    public EloAuthenticationFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
