package com.modirum.ds.services.elo.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class EloAuthToken {
    private String accessToken;
    private String refreshToken;
    //timestamp when accessToken is received
    //needed to determine if access token has to be refreshed
    private LocalDateTime timestamp;
}
