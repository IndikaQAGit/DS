<%@ page import="java.io.BufferedReader, org.json.JSONObject, org.json.JSONException, java.io.StringWriter, java.io.PrintWriter, java.util.Map, java.util.UUID" %><%
    // parse request body into a json object
    StringBuffer sb = new StringBuffer();
    BufferedReader br = request.getReader();
    String line;
    while ((line = br.readLine()) != null) {
        sb.append(line);
        sb.append("\n");
    }
    String areqJsonString = sb.toString();

    JSONObject areqObject;
    try {
        areqObject = new JSONObject(areqJsonString);
    } catch (JSONException jsonEx) {
        StringWriter sw = new StringWriter();
        jsonEx.printStackTrace(new PrintWriter(sw));
        out.print(sw.toString());
        return;
    }

    // retrive essential data fields
    String threeDSServerTransID = (String) areqObject.get("threeDSServerTransID");
    String dsTransID = (String) areqObject.get("dsTransID");
    String dsReferenceNumber = (String) areqObject.get("dsReferenceNumber");


    Map<String, String> aresCache = null;
    if (application.getAttribute("aresCache") != null) { // from cacheAcsAres.jsp
        aresCache = (Map<String, String>) application.getAttribute("aresCache");
    }

    // check cache for prepared ARes by threeDSServerTransID from AReq, if present then use it and remove from cache
    if (aresCache.containsKey(threeDSServerTransID)) {
        String cachedAresString = aresCache.remove(threeDSServerTransID);
        JSONObject cachedAresObject;
        try {
            cachedAresObject = new JSONObject(cachedAresString);
        } catch (JSONException jsonEx) {
            StringWriter sw = new StringWriter();
            jsonEx.printStackTrace(new PrintWriter(sw));
            out.print(sw.toString());
            return;
        }
        // those are added by DS
        cachedAresObject.put("dsTransID", dsTransID);
        cachedAresObject.put("dsReferenceNumber", dsReferenceNumber);

        // Don't use pretty-printing, it's the responsibility of DS to pretty-print
        out.print(cachedAresObject.toString());
        return;

    } else {
        // It's not really useful to return a random-generated ARes, but for the sake of sane defaults let's generate
        // a working one
        String acsTransId = UUID.randomUUID().toString();
        String messageVersion = (String) areqObject.get("messageVersion");

        // use auto-generated successful ARes
        // try to reassemble parameters into successful ARes, based on received AReq.
        String generatedAresString = "{\n" +
                "  \"acsOperatorID\" : \"ACS-EMULATOR-OPER-ID\",\n" +
                "  \"acsReferenceNumber\" : \"ACS-EMULATOR-REF-NO\",\n" +
                "  \"acsTransID\" : \"" + acsTransId + "\",\n" +
                "  \"authenticationValue\" : \"AJkBB4YJgQAAAACElgmBAAAAAAA=\",\n" +
                "  \"dsReferenceNumber\" : \"" + dsReferenceNumber + "\",\n" +
                "  \"dsTransID\" : \"" + dsTransID + "\",\n" +
                "  \"eci\" : \"05\",\n" +
                "  \"messageType\" : \"ARes\",\n" +
                "  \"messageVersion\" : \"" + messageVersion + "\",\n" +
                "  \"threeDSServerTransID\" : \"" + threeDSServerTransID + "\",\n" +
                "  \"transStatus\" : \"Y\"\n" +
                "}";

        out.print(generatedAresString);
        return;
    }
%>