<%@ page import="java.util.Map" %><%

    if (request.getParameter("clear") != null) {
        application.removeAttribute("rresCache");
    }

    StringBuilder sb = new StringBuilder();
    sb.append("Cached items: ");

    if (application.getAttribute("rresCache") != null) {
        Map<String, String> rresCache = (Map<String, String>) application.getAttribute("rresCache");

        if (rresCache.isEmpty()) {
            sb.append("empty cache<br/>");
        } else {
            sb.append(rresCache.size() + " <br/>");
        }

        for (Map.Entry entry : rresCache.entrySet()) {
            sb.append("key: " + entry.getKey() + " value: " + entry.getValue()).append("<br/>");
        }
    } else {
        sb.append("no cache");
    }

    out.print(sb.toString());
%>