package com.modirum.ds.tests;

import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.model.TDSModel.ErrorComponent;
import com.modirum.ds.model.TDSModel.XtransStatusReason;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService.PublicBOS;
import com.modirum.ds.services.MessageService210;
import com.modirum.ds.services.MessageService220;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.http.SSLSocketHelper;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.modirum.ds.util.DSUtils.createUUID;

/**
 * @author andri<br />
 * ACS Emulator for simple DS 2.0 testing scenarious
 * Does not support SDK communications.<br/>
 * <p>
 * Context params format<br/>
 * in web.xml
 * <pre>
 * {@code
 * <context-param>
 * <param-name>ACSEmu.acsRef</param-name>
 * <param-value>1213232232</param-value>
 * </context-param>
 * }</pre>
 * <p>
 * same in context.xml files:
 * <pre>{@code
 * <Parameter name="ACSEmu.acsRef" value="1213232232" override="false"/>
 * }</pre>
 * <p>
 * <p>
 * Usage:<br/>
 * Deploy dstets.war to suitable location..<br/>
 * <p>
 * Set init parameters in servlet context files:<br/>
 * Test cases examples:
 * Param values
 * PAN:CASE
 * or
 * PAN:CASE:CAVV
 *
 * <pre>
 * {@code
 * <context-param>
 * <param-name>ACSEmu.case1</param-name>
 * <param-value>4016000000002:Y</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case2</param-name>
 * <param-value>4016000000010:N</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case3</param-name>
 * <param-value>4016000000028:A</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case4</param-name>
 * <param-value>4016000000036:U</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case5</param-name>
 * <param-value>4016000000044:R</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case6</param-name>
 * <param-value>4016000000051:C</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case7</param-name>
 * <param-value>4016010000067:IR56</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case8</param-name>
 * <param-value>4016000000101:CAUTO</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case9</param-name>
 * <param-value>4016000000119:ERROR</param-value>
 * </context-param>
 * <context-param>
 * <param-name>ACSEmu.case10</param-name>
 * <param-value>4016000000127:U13</param-value>
 * </context-param>
 * }</pre>
 * <br/>
 * Parameter format PAN : Case<br/>
 * Possible cases:
 * <ul>
 * Y - auhtenticated<br/>
 * A - attempt<br/>
 * U - U authentication could not be performed<br/>
 * U13 - U authentication could not be performed with statusreason 13<br/>
 * R - rejected<br/>
 * C - challenge flow<br/>
 * CAUTO - with auto RReq response in 10 seconds<br/>
 * ERROR - an error is returned <br/>
 * No case match will return  U with statusreason 08<br/>
 * </ul>
 * <p>
 * ACS Refrence number:
 * <pre>
 * {@code
 * <context-param>
 * <param-name>ACSEmu.acsRef</param-name>
 * <param-value>1213232232</param-value>
 * </context-param>
 * }
 * </pre>
 * Defaults to : ACSEmu2<br/>
 * Correct challenge/paasword in challenge flow is: correct<br/>
 * <p>
 * SSL Keys and certificates the funny part :) for many <br/>
 * <p>
 * DS.SSLProto - SSL protocol to connect DS, defaults to TLSv1.2 if missing<br/>
 * <p>
 * SSL Trust when connection to DS:<br/>
 * DS.ssl.client.trustany - (true then no truststore is needed)<br/>
 * DS.ssl.roots - SSL truststore keystore file JKS format (can be created with java keytool)<br/>
 * <br/><br/>
 * <p>
 * SSL Client Certificates connecting to DS:<br/>
 * <ul>
 * DS.keystore - SSL client key/certificate keystore JKS file to connect to DS<br/>
 * DS.keystorePass - keystore password<br/>
 * DS.keyalias - key/certicate entry alias in keystore<br/>
 * </ul>
 * or alternatively
 * <ul>
 * DS.ssl.client.certs - client certificate chain dicelcty as parameter value with BEGIN and END certifcate tags<br/>
 * DS.ssl.client.pk - client private key PKCS8 base 64 encoded without any headers<br/>
 * </ul>
 */
public class ACSEmu extends HttpServlet {

    private static final long serialVersionUID = 1L;
    protected transient static Logger log = LoggerFactory.getLogger(ACSEmu.class);

    MessageService<?> mse210;
    MessageService<?> mse220;
    static String acsRef = "ACSEmu2";
    Map<String, String> cases;
    Map<String, String> caseCAVVs;
    Map<String, ChallengeAuto> callengeCases;
    private AtomicLong requestPerMinCounter = new AtomicLong(0);
    private long requestPerMinCounterStart = 0;

    public void init(javax.servlet.ServletConfig config) throws ServletException {
        log.info("ACSEmu Servlet init.., logging started");
        super.init(config);

        mse210 = MessageService210.getInstance();
        mse220 = MessageService220.getInstance();
        cases = new java.util.TreeMap<String, String>();
        caseCAVVs = new java.util.TreeMap<String, String>();
        callengeCases = new java.util.TreeMap<String, ChallengeAuto>();

        int ecc = 0;
        for (int i = 1; i < 300; i++) {
            String casex = this.getServletContext().getInitParameter("ACSEmu.case" + i);
            if (Misc.isNotNullOrEmpty(casex)) {
                ecc = 0;
                String[] panAndCase = Misc.split(casex, ":");
                if (panAndCase.length > 1) {
                    cases.put(panAndCase[0], panAndCase[1]);
                }
                if (panAndCase.length > 2) {
                    caseCAVVs.put(panAndCase[0], panAndCase[2]);
                }

            } else if (casex == null) {
                ecc++;
                if (ecc > 90) {
                    break;
                }
            }
        }
        log.info("ACSEmu " + cases.size() + " cases loaded, with " + caseCAVVs.size() + " cavvs");

        String acsRefSet = this.getServletContext().getInitParameter("ACSEmu.acsRef");
        if (Misc.isNotNullOrEmpty(acsRefSet)) {
            acsRef = acsRefSet;
        }

        log.info("Servlet init completed");
    }

    public void destroy() {
        log.info("Servlet stopping.. (" + getServletInfo() + ")");
        if (sexec != null) {
            sexec.shutdownNow();
        }
        super.destroy();
        log.info("Stopping done, logging stoped");
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
        doPost(req, resp);
    }

    public void debugRequest(HttpServletRequest req) {
        if (log.isDebugEnabled()) {
            StringBuilder headers = new StringBuilder(128);
            for (java.util.Enumeration e = req.getHeaderNames(); e.hasMoreElements(); ) {
                String t = (String) e.nextElement();
                headers.append("\t" + t + "=" + req.getHeader(t) + "\n");
            }
            log.debug("Headers:\n{}", headers);

            StringBuilder params = new StringBuilder(64);
            for (java.util.Enumeration e = req.getParameterNames(); e.hasMoreElements(); ) {
                String t = (String) e.nextElement();
                String[] pvls = req.getParameterValues(t);
                StringBuilder values = new StringBuilder(32);

                // PCI DSS more optimized debugging without creating new sensible strings
                if (t != null && (t.contains("card.") || t.toLowerCase().equals("data") || t.startsWith("payer") ||
                                  t.toLowerCase().startsWith("customer") || t.toLowerCase().contains("ccn") ||
                                  t.toLowerCase().contains("cvc") || t.toLowerCase().contains("cvv"))) {
                    values.append("PCIDSS: ");
                    for (int i = 0; pvls != null && i < pvls.length; i++) {
                        // log first 3 digits of pan
                        StringBuilder startsWith = new StringBuilder();
                        int max = t.contains("card.pan") ? 5 : 3;
                        if (t.contains("card.pan") || t.startsWith("payer") || t.toLowerCase().startsWith("customer")) {
                            for (int j = 0; j < max && pvls[i] != null && j < pvls[i].length(); j++) {
                                startsWith.append(pvls[i].charAt(j));
                            }


                        }
                        if (i > 0) {
                            values.append(", ");
                        }

                        values.append("v" + i + " " + (pvls[i] != null ?
                                (startsWith.length() > 0 ? "" + startsWith + ".." : "") + " (" + pvls[i].length() +
                                ")" : "Null"));
                    }
                } else {

                    for (int i = 0; pvls != null && i < pvls.length; i++) {
                        if (pvls[i] != null) {
                            values.append("'" + pvls[i] + "'");
                        } else {
                            values.append("Null");
                        }
                        if (i < pvls.length - 1) {
                            values.append(", ");
                        }

                    }
                }
                params.append("\t" + t + "=" + values + "\n"); // request.getParameter(t));
            }
            log.debug("Parameters:\n{}", params);

        } // debug
    }

    protected TDSMessageBase createErrorMessage(ErrorCode ecode, TDSMessageBase req, String descr, CharSequence detail, TDSMessageBase m) {
        m.setErrorCode(ecode.value);
        if (descr == null && ecode != null) {
            descr = ecode.desc;
        }

        m.setErrorDescription(descr);
        m.setErrorDetail(detail != null ? detail.toString() : null);
        m.setMessageVersion(req.getMessageVersion());
        m.setMessageType(TDSModel.XmessageType.ERRO.value());
        m.setErrorComponent(ErrorComponent.cACS.value);
        m.setErrorMessageType(req != null && req.getMessageType() != null ? req.getMessageType() : "");
        m.setSdkTransID(req != null ? req.getSdkTransID() : null);
        m.setTdsServerTransID(req != null ? req.getTdsServerTransID() : null);
        m.setDsTransID(req != null ? req.getDsTransID() : null);

        return m;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
        long start = System.currentTimeMillis();
        String charSet = this.getInitParameter("requestCharSet");
        if (Misc.isNullOrEmpty(charSet)) {
            charSet = "UTF-8";
        }
        req.setCharacterEncoding(charSet);
        MessageService xmse = mse210;

        String ct = req.getHeader("Content-type");
        int ctLen = req.getContentLength();
        log.info("New " + req.getMethod() + ": " + req.getRequestURI() + " contenttype: " + ct + " " + ctLen +
                 " bytes from " + req.getRemoteAddr());
        this.debugRequest(req);

        // AReq
        if (ct != null && ct.contains(MessageService.CT_JSON)) {
            OutputStream os = resp.getOutputStream();
            try {
                byte[] jsonContent = null;
                if (ctLen <= 0) {
                    log.error("Processing failed no json content");
                    resp.setContentType(MessageService.CT_JSON_UTF8);
                    TDSMessageBase error = createErrorMessage(ErrorCode.cInvalidMessageType101, null,
                                                              "No message content", "No message content",
                                                              xmse.createNewMessage());
                    resp.getOutputStream().write(xmse.toJSON(error));
                    return;
                }

                jsonContent = new byte[ctLen];
                ServletInputStream sis = req.getInputStream();
                int read = 0;
                int lastRead = 0;
                do {
                    lastRead = sis.read(jsonContent, read, ctLen - read);
                    if (lastRead < 0) {
                        break;
                    }

                    read += lastRead;

                } while (read < ctLen);

                if (read != ctLen) {
                    log.warn("Content length was " + ctLen + " actual content read " + read +
                             " bytes, may be partial content!!!");
                }

                if (log.isDebugEnabled()) {
                    log.debug("Content read in " + read + " bytes");
                }

                String[] jsc = new String[]{new String(jsonContent, StandardCharsets.UTF_8)};

                String mv = MessageService.getMessageVersion(jsc);
                if (mv != null && TDSModel.MessageVersion.V2_1_0.value().equals(mv)) {
                    xmse = mse210;
                } else if (mv != null && TDSModel.MessageVersion.V2_2_0.value().equals(mv)) {
                    xmse = mse220;
                }
                log.info("Msg ver=" + mv + " service sel: " + xmse.getClass().getSimpleName());

                JsonObjectBuilder tdsResBuilder = null;
                log.info("Got content: " + jsc[0]);
                TDSMessageBase tdsReq = xmse.fromJSON(new StringReader(jsc[0]));
                log.info("Got msgtype " + tdsReq.getMessageType());

                TDSMessageBase tdsRes = xmse.createNewMessage();

                if (req.getRequestURI().contains("DSDummy")) {
                    tdsRes = createAResMessage(tdsReq, null, null, null, TDSModel.XtransStatus.Y, null,
                                               xmse.createNewMessage());
                    tdsRes.setDsTransID(createUUID(System.currentTimeMillis(), System.currentTimeMillis()));
                    tdsRes.setDsReferenceNumber("DSDummy");
                    resp.setContentType(MessageService.CT_JSON_UTF8);
                    if (tdsResBuilder == null) {
                        tdsResBuilder = xmse.toJSONOB(tdsRes);
                    }
                    if (tdsReq.getPurchaseAmount() != null && tdsReq.getPurchaseAmount().contains("1919")) {
                        Thread.sleep(9000);
                    } else if (tdsReq.getPurchaseAmount() != null && tdsReq.getPurchaseAmount().contains("1515")) {
                        Thread.sleep(7000);
                    }

                    PublicBOS res = xmse.toJSONBos(tdsResBuilder.build());
                    os.write(res.getBuf(), 0, res.getCount());
                    long e = System.currentTimeMillis();

                    String rpm = "";
                    requestPerMinCounter.incrementAndGet();
                    if (requestPerMinCounterStart < e - 20000) {
                        if (requestPerMinCounterStart > e - 22000) {
                            rpm = "current rate " + (requestPerMinCounter.get() * 3) + " rpm, ";
                        }
                        requestPerMinCounterStart = e;
                        requestPerMinCounter.set(0);
                    }
                    log.info("DSDummy " + rpm + " Response in " + (e - start) + "ms :" +
                             new String(res.getBuf(), 0, res.getCount(), StandardCharsets.UTF_8));


                    return;
                }

                String xcase = cases.get(tdsReq.getAcctNumber());

                String acsUrl = req.getRequestURL().toString();
                String publicPort = getServletContext().getInitParameter("ACS.publicPort");
                if (Misc.isNullOrEmpty(publicPort)) {
                    publicPort = "443";
                }
                String sport = "" + req.getServerPort();
                if (!sport.equals(publicPort)) {
                    acsUrl = Misc.replace(acsUrl, ":" + sport + "/", ":" + publicPort + "/");
                }

                com.modirum.ds.tds21msgs.TDSMessage tdsReq21 = null;
                if (TDSModel.MessageVersion.V2_2_0.value().equals(mv) ||
                    TDSModel.MessageVersion.V2_1_0.value().equals(mv)) {
                    tdsReq21 = ((com.modirum.ds.tds21msgs.TDSMessage) tdsReq);
                }

                if (TDSModel.XmessageType.A_REQ.value().equals(tdsReq.getMessageType())) {
                    if (xcase == null) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.U,
                                                   XtransStatusReason.C08NoCardrecord.value, xmse.createNewMessage());
                    } else if (TDSModel.MessageVersion.V2_2_0.value().equals(mv) && xcase != null &&
                               xcase.startsWith("D") && "Y".equals(tdsReq21.getThreeDSRequestorDecReqInd())) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.D, null,
                                                   xmse.createNewMessage());
                        String stRe = null;
                        TDSModel.XtransStatus st = TDSModel.XtransStatus.Y;
                        if (xcase.endsWith("A")) {
                            st = TDSModel.XtransStatus.A;
                        } else if (xcase.endsWith("N")) {
                            st = TDSModel.XtransStatus.N;
                            stRe = XtransStatusReason.C01AuthFailed.value;
                        } else if (xcase.endsWith("R")) {
                            st = TDSModel.XtransStatus.R;
                            stRe = XtransStatusReason.C10StolenCard.value;
                        } else if (xcase.endsWith("U")) {
                            st = TDSModel.XtransStatus.U;
                            stRe = XtransStatusReason.C03UnsupDevice.value;
                        }

                        //threeDSRequestorDecMaxTime
                        int maxTimeInt = Misc.parseInt(tdsReq21.getThreeDSRequestorDecMaxTime());
                        // this is similar to challenge auto, we really not going to implement decupled plugin here for test tool
                        // instead depending on case def automatically return results
                        ChallengeAuto ca = new ChallengeAuto(tdsReq, tdsRes, st, xmse, null);
                        ca.str = stRe;
                        this.getScheduledExecutorService().schedule(ca, maxTimeInt > 1 ? 65 : 15, TimeUnit.SECONDS);
                    } else if ("Y".equals(xcase) || xcase != null && xcase.endsWith("Y")) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.Y, null,
                                                   xmse.createNewMessage());
                    } else if ("A".equals(xcase) || xcase != null && xcase.endsWith("A")) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.A, null,
                                                   xmse.createNewMessage());
                    } else if ("U".equals(xcase) || xcase != null && xcase.endsWith("U")) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.U,
                                                   XtransStatusReason.C03UnsupDevice.value, xmse.createNewMessage());
                    } else if ("U08".equals(xcase)) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.U,
                                                   XtransStatusReason.C08NoCardrecord.value, xmse.createNewMessage());
                    } else if ("U13".equals(xcase)) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.U,
                                                   XtransStatusReason.C13CardNotEnrolled.value,
                                                   xmse.createNewMessage());
                    } else if ("N".equals(xcase) || xcase != null && xcase.endsWith("N")) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.N,
                                                   XtransStatusReason.C01AuthFailed.value, xmse.createNewMessage());
                    } else if ("N90".equals(xcase)) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.N, "90",
                                                   xmse.createNewMessage());
                    } else if ("R".equals(xcase) || xcase != null && xcase.endsWith("R") && !xcase.endsWith("OR")) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.R,
                                                   XtransStatusReason.C01AuthFailed.value, xmse.createNewMessage());

                    } else if ("C".equals(xcase) || xcase != null && xcase.endsWith("C")) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.C, null,
                                                   xmse.createNewMessage());
                        //tdsRes.setACSCertificate("Certificate TBD");
                        callengeCases.put(tdsRes.getAcsTransID(), new ChallengeAuto(tdsReq, tdsRes, null, xmse, null));
                    } else if ("CAUTO".equals(xcase)) {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.C, null,
                                                   xmse.createNewMessage());
                        //tdsRes.setACSCertificate("Certificate TBD");
                        this.getScheduledExecutorService().schedule(
                                new ChallengeAuto(tdsReq, tdsRes, TDSModel.XtransStatus.Y, xmse, null), 10,
                                TimeUnit.SECONDS);
                    } else if ("ERROR".equals(xcase)) {
                        tdsRes = this.createErrorMessage(ErrorCode.cTransientSysFailure403, tdsReq,
                                                         "Temporary ACS failure", "Internal ACS error",
                                                         xmse.createNewMessage());
                    } else if ("ERROR654".equals(xcase)) {
                        tdsRes = this.createErrorMessage(ErrorCode.cTransientSysFailure403, tdsReq,
                                                         "Invalid error code 654", "Invalid error code 654",
                                                         xmse.createNewMessage());
                        tdsRes.setErrorCode("654");
                    } else if ("CB-EXTENSIONS-ARes".equals(xcase) || "CB-EXTENSIONS-RReq".equals(xcase)) {
                        JsonObjectBuilder data = Json.createObjectBuilder();
                        data.add("text", "test");
                        JsonObjectBuilder extensionBuilder = Json.createObjectBuilder();
                        extensionBuilder.add("name", "name");
                        extensionBuilder.add("id", "CB-AVALGO");
                        extensionBuilder.add("criticalityIndicator", false);
                        extensionBuilder.add("data", data);

                        JsonObjectBuilder data2 = Json.createObjectBuilder();
                        data2.add("text", "test2");
                        JsonObjectBuilder extensionBuilder2 = Json.createObjectBuilder();
                        extensionBuilder2.add("name", "name");
                        extensionBuilder2.add("id", "should-pass");
                        extensionBuilder2.add("criticalityIndicator", false);
                        extensionBuilder2.add("data", data2);

                        JsonArrayBuilder messageExtensions = Json.createArrayBuilder();
                        messageExtensions.add(extensionBuilder);
                        messageExtensions.add(extensionBuilder2);

                        if ("CB-EXTENSIONS-ARes".equals(xcase)) {
                            tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.Y, null,
                                                       xmse.createNewMessage());
                            tdsResBuilder = xmse.toJSONOB(tdsRes);
                            tdsResBuilder.add("messageExtension", messageExtensions);
                        }

                        if ("CB-EXTENSIONS-RReq".equals(xcase)) {
                            tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.C, null,
                                                       xmse.createNewMessage());
                            callengeCases.put(tdsRes.getAcsTransID(),
                                              new ChallengeAuto(tdsReq, tdsRes, null, xmse, messageExtensions, null));
                        }
                    } else if ("YMCE".equals(xcase)) {
                        //{ "name": "ACS Data", "id": "A000000004-acsData", "criticalityIndicator": false,
                        //"data": { "A000000004-acsData": { "whitelistStatus": "Y" } } } ] }
                        JsonObjectBuilder datadata = Json.createObjectBuilder();
                        datadata.add("whitelistStatus", "Y");
                        JsonObjectBuilder data = Json.createObjectBuilder();
                        data.add("A000000004-acsData", datadata);
                        JsonObjectBuilder extensionBuilder = Json.createObjectBuilder();
                        extensionBuilder.add("name", "ACS Data");
                        extensionBuilder.add("id", "A000000004-acsData");
                        extensionBuilder.add("criticalityIndicator", false);
                        extensionBuilder.add("data", data);

                        JsonArrayBuilder messageExtensions = Json.createArrayBuilder();
                        messageExtensions.add(extensionBuilder);

                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.Y, null,
                                                   xmse.createNewMessage());
                        tdsResBuilder = xmse.toJSONOB(tdsRes);
                        tdsResBuilder.add("messageExtension", messageExtensions);
                    } else {
                        tdsRes = createAResMessage(tdsReq, acsUrl, null, null, TDSModel.XtransStatus.U,
                                                   XtransStatusReason.C08NoCardrecord.value, xmse.createNewMessage());
                    }
                } else {
                    tdsRes = this.createErrorMessage(ErrorCode.cInvalidMessageType101, tdsReq, "Unknown message",
                                                     "Unknown message", xmse.createNewMessage());
                }

                resp.setContentType(MessageService.CT_JSON_UTF8);


                if (tdsResBuilder == null) {
                    tdsResBuilder = xmse.toJSONOB(tdsRes);
                }
                PublicBOS res = xmse.toJSONBos(tdsResBuilder.build());
                os.write(res.getBuf(), 0, res.getCount());
                long e = System.currentTimeMillis();
                log.info("Response in " + (e - start) + "ms :" +
                         new String(res.getBuf(), 0, res.getCount(), StandardCharsets.UTF_8));
            } catch (Throwable e) {
                String eId = "" + System.currentTimeMillis();
                log.error("Processing failed eid=" + eId + "", e);

                //resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Errors");
                TDSMessageBase tdsRese = this.createErrorMessage(ErrorCode.cTransientSysFailure403, null,
                                                                 "Temporary ACS failure",
                                                                 "Internal ACS error error ref " + eId,
                                                                 xmse.createNewMessage());
                try {
                    byte[] res = xmse.toJSON(tdsRese);
                    log.info("Error Response " + new String(res, StandardCharsets.UTF_8));
                    os.write(res);
                } catch (Exception dc) {
                    log.error("Processing error response failed..", dc);
                }

            }
        } else // browser authentication or handshake
        {
            if (req.getParameter("handshake") != null) {
                try {
                    log.info("Got handshake 3dmethod request ");
                    String jsonB64 = req.getParameter("threeDSMethodData");
                    if (Misc.isNullOrEmpty(jsonB64)) // fallback older specs
                    {
                        jsonB64 = req.getParameter("3DSMethodData");
                    }

                    //jsonObject.append("{ 3DSServerTransID: \"" + td.tID + "\", 3DSMethodNotificationURL: \"" + tsdFormMethodClbUrl + "\" }");
                    StringBuilder rf = new StringBuilder();
                    if (jsonB64 != null) {
                        byte[] json = Base64.decodeURL(jsonB64);
                        String jsonStr = new String(json, StandardCharsets.UTF_8);
                        log.info("Got 3D Method data " + jsonStr);

                        String txId = null;
                        String nUrl = null;

                        javax.json.JsonReader jr = javax.json.Json.createReader(new StringReader(jsonStr));
                        JsonObject jso = jr.readObject();
                        if (jso.containsKey("threeDSServerTransID")) {
                            txId = jso.getString("threeDSServerTransID");
                        }
                        if (jso.containsKey("threeDSMethodNotificationURL")) {
                            nUrl = jso.getString("threeDSMethodNotificationURL");
                        }

                        if (txId != null && nUrl != null) {
                            resp.setContentType("text/html");
                            rf.append("<html><body>\n");
                            rf.append("<form id=\"3dsmethodrecall\" name=\"3dsmethodrecall\" action=\"" + nUrl +
                                      "\" method=\"post\"/>");
                            rf.append("<input type=\"hidden\" name=\"threeDSServerTransID\" value=\"" + txId + "\"/>");
                            String tdsmd = "{\"threeDSServerTransID\":\"" + txId + "\"}";
                            tdsmd = Base64.encodeURL(tdsmd.getBytes(StandardCharsets.ISO_8859_1));
                            rf.append("<input type=\"hidden\" name=\"threeDSMethodData\" value=\"" + tdsmd + "\"/>");
                            rf.append("</form>\n");
                            rf.append("<script type=\"text/javascript\" language=\"javascript\">\n" +
                                      "var timer=setTimeout(\"document.getElementById('3dsmethodrecall').submit();\",1250);\n" +
                                      "</script>\n");
                            rf.append("</body></html>\n");
                        } else {
                            resp.setContentType("text/plain");
                            rf.append("No proper json data found");
                        }
                    } else {
                        resp.setContentType("text/plain");
                        rf.append("No json data found");

                    }

                    Thread.sleep(500);

                    log.info("" + rf);
                    resp.getWriter().print(rf.toString());
                    return;
                } catch (Exception e) {
                    log.error("Error procesisng handshake data ", e);
                    resp.setContentType("text/plain");
                    resp.getWriter().print("Procesisng failed".getBytes(StandardCharsets.UTF_8));
                    return;
                }
            }

            try {
                String creqBase64 = req.getParameter("creq");
                if (creqBase64 == null) {
                    creqBase64 = req.getParameter("CReq");
                }
                String threeDSSessionData = req.getParameter("threeDSSessionData");
                req.setAttribute("threeDSSessionData", threeDSSessionData);

                req.setAttribute("cReq", creqBase64);

                String challenge = req.getParameter("challenge");
                ChallengeAuto ca = null;
                TDSMessageBase cReq = null;
                int counter = Misc.parseInt(req.getParameter("counter"));
                if (creqBase64 != null) {
                    //cReq = mse.fromJSON(Helper.inflateMessage(creqDeflatedBase64.getBytes(StandardCharsets.UTF_8)));
                    byte[] json = null;
                    try {
                        json = Base64.decodeURL(creqBase64);
                    } catch (Exception eb) {
                        log.error("Error creq decodeURL", eb);
                        try {
                            json = Base64.decode(creqBase64);
                        } catch (Exception eb2) {
                            String errId = Context.getUnique();
                            log.error("Error creq decode errId " + errId, eb2);
                            resp.setContentType("text/plain");
                            resp.getWriter().write("Bad CReq unable to decode, errid " + errId);
                            return;
                        }
                    }

                    cReq = xmse.fromJSON(json);
                    log.info("Got msgtype " + cReq.getMessageType() + ": " + new String(json, "UTF-8") +
                             " set to wiaitng challenge acs id " + cReq.getAcsTransID());
                    ca = callengeCases.get(cReq.getAcsTransID());
                }

                if (cReq != null && ca != null &&
                    Misc.in(challenge, new String[]{"correct", "correctad", "delayed", "attempt"})) {
                    ca.st = TDSModel.XtransStatus.Y;
                    if ("attempt".equals(challenge)) {
                        ca.st = TDSModel.XtransStatus.A;
                    } else if ("correctad".equals(challenge)) {
                        JsonObjectBuilder acsData = Json.createObjectBuilder();
                        acsData.add("whitelistStatus", "Y");
                        JsonObjectBuilder data2 = Json.createObjectBuilder();
                        data2.add("A000000004-acsData", acsData);
                        JsonObjectBuilder extensionBuilder2 = Json.createObjectBuilder();
                        extensionBuilder2.add("name", "ACS Data");
                        extensionBuilder2.add("id", "A000000004-acsData");
                        extensionBuilder2.add("criticalityIndicator", false);
                        extensionBuilder2.add("data", data2);
                        JsonArrayBuilder messageExtensions = Json.createArrayBuilder();
                        messageExtensions.add(extensionBuilder2);
                        ca.rreqMessageExtensionsBuilder = messageExtensions;
                    }

                    if (!ca.result) {
                        if ("delayed".equals(challenge)) {
                            this.getScheduledExecutorService().schedule(ca, 10, TimeUnit.SECONDS);
                        } else {
                            ca.run();
                        }
                    }

                    TDSMessageBase cRes = createCResMessage(cReq, null, null, TDSModel.XtransStatus.Y, null, null,
                                                            xmse.createNewMessage());
                    byte[] cResData = xmse.toJSON(cRes);
                    cResData = EmbedTestFilter.applyTransformations(cResData, 0, cResData.length);
                    String cResDataEnc = Base64.encodeURL(
                            cResData); ////new String(Helper.deflateMessage(cResData), StandardCharsets.UTF_8);
                    req.setAttribute("cRes", cResDataEnc);

                    String tu = null;
                    if (cReq.getNotificationURL() != null) {
                        tu = cReq.getNotificationURL(); // 2.0.1
                    } else if (ca.aReq != null && ca.aReq.getNotificationURL() != null) // 2.1.0
                    {
                        tu = ca.aReq.getNotificationURL();
                    }

                    req.setAttribute("termUrl", tu);
                    log.info("Sending OK redirect " + tu + " CRes " + new String(cResData, StandardCharsets.UTF_8));
                } else if (cReq != null && ca != null) {
                    if (counter < 2 && !Misc.in(challenge, new String[]{"cancel", "error", "unable", "reject"})) {
                        if (challenge != null) {
                            counter++;
                            req.setAttribute("error", "Authentication failed invalid password");
                        }
                        req.setAttribute("counter", "" + counter);
                    } else {
                        ca.str = "01";
                        ca.st = TDSModel.XtransStatus.N;
                        if (Misc.in(challenge, new String[]{"cancel", "error", "reject", "unable"})) {
                            if ("cancel".equals(challenge)) {
                                ca.cci = "01";
                            }
                            if ("error".equals(challenge)) {
                                ca.cci = "06";
                            }
                            if ("reject".equals(challenge)) {
                                ca.str = "12";
                                ca.cci = "07";
                                ca.st = TDSModel.XtransStatus.R;
                            }
                            if ("unable".equals(challenge)) {
                                ca.str = "11";
                                ca.cci = "07";
                                ca.st = TDSModel.XtransStatus.U;
                            }
                        }

                        // send RReq to DS
                        ca.run();
                        if (!ca.result) {
                            ca.run();
                        }

                        TDSMessageBase cRes = createCResMessage(cReq, null, null, TDSModel.XtransStatus.N, null,
                                                                TDSModel.XtransStatusReason.C01AuthFailed.value,
                                                                xmse.createNewMessage());
                        byte[] cResData = xmse.toJSON(cRes);
                        cResData = EmbedTestFilter.applyTransformations(cResData, 0, cResData.length);
                        //String cResDataEnc=new String(Helper.deflateMessage(cResData), StandardCharsets.UTF_8);
                        String cResDataEnc = Base64.encode(cResData);
                        req.setAttribute("cRes", cResDataEnc);
                        String tu = null;
                        if (cReq.getNotificationURL() != null) {
                            tu = cReq.getNotificationURL(); // 2.0.1
                        } else if (ca.aReq != null && ca.aReq.getNotificationURL() != null) // 2.1.0
                        {
                            tu = ca.aReq.getNotificationURL();
                        }

                        req.setAttribute("termUrl", tu);
                        log.info("Sending redirect " + tu + " CRes " + new String(cResData, StandardCharsets.UTF_8));
                    }
                } else {
                    req.setAttribute("error", "Invalid request or session");
                    req.setAttribute("counter", "" + counter);
                }

                req.getRequestDispatcher("WEB-INF/acs.jsp").include(req, resp);

            } catch (Exception dc) {
                log.error("Processing non json request..", dc);
                resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            }
        }

    } // doPost


    String getAAV(String pan) {
        String aavb64 = this.caseCAVVs.get(pan);
        if (!(aavb64 != null && aavb64.length() == 28)) {
            byte[] aav = new byte[20];
            aav[0] = 'A';
            aav[1] = 'C';
            aav[2] = 'S';
            aav[3] = 'E';
            aav[4] = 'M';
            aav[5] = 'U';
            for (int i = 6; i < aav.length; i++) {
                aav[i] = (byte) ((Math.random() * 96) + 32);
            }
            aavb64 = Base64.encode(aav);
        }
        return aavb64;
    }

    public TDSMessageBase createAResMessage(TDSMessageBase aReq, String acsUrl, String channel, String ireqDetail, TDSModel.XtransStatus txStatus, String statusReason, TDSMessageBase newMsg) {

        newMsg.setMessageVersion(aReq.getMessageVersion());
        //m.setProtocol(MessageService.cPaymentProtocolTDSV2);
        newMsg.setMessageType(TDSModel.XmessageType.A_RES.value());

        newMsg.setDsReferenceNumber(aReq.getDsReferenceNumber());
        newMsg.setDsTransID(aReq.getDsTransID());
        newMsg.setAcsReferenceNumber(acsRef);

        // from areq
        newMsg.setSdkTransID(aReq.getSdkTransID());
        newMsg.setTdsServerTransID(aReq.getTdsServerTransID());
        newMsg.setTransStatus(txStatus.value());
        newMsg.setTransStatusReason(statusReason);

        if (TDSModel.XtransStatus.Y == txStatus || TDSModel.XtransStatus.A == txStatus) {
            newMsg.setAuthenticationType("01");
            newMsg.setAuthenticationValue(getAAV(aReq.getAcctNumber()));
            newMsg.setEci("05");

        }

        if (TDSModel.XtransStatus.N == txStatus && newMsg instanceof com.modirum.ds.tds21msgs.TDSMessage) {
            ((com.modirum.ds.tds21msgs.TDSMessage) newMsg).setCardholderInfo("Call 1-800-Trimspa");
        }

        if (TDSModel.XtransStatus.C == txStatus) {
            newMsg.setAuthenticationType("02");

            if (newMsg instanceof com.modirum.ds.tds20msgs.TDSMessage) {
                com.modirum.ds.tds20msgs.TDSMessage m = (com.modirum.ds.tds20msgs.TDSMessage) newMsg;
                m.setAcschallengeMandated("Y");
                m.setAcsChallengeMandated("Y");

                if (TDSModel.DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel())) {
                    com.modirum.ds.tds20msgs.XrenderObj2 ro = new com.modirum.ds.tds20msgs.XrenderObj2();
                    ro.setInterface("02");
                    ro.setUiType("01");
                    //ro.getUiType().add("05");
                    m.setAcsRenderingType(ro);

                    if (aReq instanceof com.modirum.ds.tds20msgs.TDSMessage &&
                        ((com.modirum.ds.tds20msgs.TDSMessage) aReq).getSdkEphemPubKey() != null) {
                        String header = "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZvWERURTRNREl4TXpFeE16Y3lNVm93RHpFTk1Bc0dBMVVFQXd3RWRHVnpkRENDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFMWFdjbitkbUtWcXJPdUl5WWpFS2Q1K3NKdGViTUhFXC96TEw3QXRcL2JtWGJCWHhHV0VDOEVjSkdnUjByT3JtVUtuazZtSWFsSmY1RW1uQ0JJZEdNMHFXYXljcTBwdmNZY0dXcis4RlBzaVQrYW9FMmZiWmpsbytYZ1Nib1wvS3hzNjlkQ09yZW5QTnZhaXkrZlNTaG5VVndqNmVVMzk4OFZlOXNwUWZHWThPeW9hZGhlMUhZUXhsd2hMZnowUCtIeG05UGRiMTNxMVgzeTlcLzhVZWhEc3c4WGNId2t2TVFjWTU0K0NxTjFma1RIXC82d29OVVwvMzRaS3ZEUnNnMlpTM0pqdDZiVXBsOHhYMGlyTklRcUFjRGtXM2NnQ2NpdGk2Q2Y4ZE9DUjlyRmx2aXNjUnlHbE05STBhRlowa05aTmxKTU85dGNyakhQSWlMbDZsQXNYSTFmQjBDQXdFQUFUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFFZ0h1dGg1ZFNFc0NtbXVBckQ3bE1VU3FJcEpcL0RcLzh5b2FweHFLcWtPY3Znc2UyR3dKcjQzb29lXC9UWE9IWE05OU41b1M4Rzl2cmwydWNLXC9rcUE2VFFJcmRZUEJUYXAyN3ZKU0NjMVdSMmdxa1ZtK1wvRnNGeE5Sb2w0YWZ6WFpLdmVEVE1RaGkrNkpaWmYrc2N6cnkxWmFYSDdVcW1yQW9PdU1reFAxYjRtTzRZYTZUVlBcL1FETGJoU1wvV0RBSlRUejAzKzFDNDZKSXV5XC9PTjhpM090YnoyaXhkdXNjM2puZ0d5Rk1nV1BiRGVGdVUrdXJWVzF5OTdrRTFpUmQ5S3FodHExMzRlZWpDcnAyaWJNWHBTRnkrXC91ZmFnK00xRzM3YzZzNTU2d1FlVngrOVwva0p2NFNGVE9ETEtsdXppbGRwVlVGZTBnUndtY211V1Y4dXRKVWh3PT0iXSwiYWxnIjoiUlMyNTYifQ";
                        String signature = "aKW-siPDLKH4f84CqWiyVhSfqqxxkjEURdAS6q3-gQx9J9jaM-GYk4eLsjAImYRvt4ybJdUVF1PxYGT8V_HrfTSfLX2_vyBl23GrL5ntJhzKiUFJNlEfjN3XTXSNWO4jWd464wi44dpIqKkcZTCkrAFUZpGU3hRVfxORFQSkMq5pvNsXS4db0v3GxVSJGdWckoX0HAdzPTlF3FvMCvbe1Q1Ae2BMJvokqIyGKvNEkBpQxMIUMT36yX-NzLk4rtB0pznxkcYxJEFTsyfMnsvjMOmgOSLTHfg798FaJ2EFkw4A7fFaLkY63B2AwH1PT6IZ7aR0Nbas5Z-iRil4mx2EOw";
                        String payload = "{\"acsURL\":\"" + acsUrl + "\", " +
                                         "\"acsEphemPubKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEb6VDHMWXeMgUSOhEF1VyernTqpNqSkOW31TckuzSHNhTIr8iDpMxyHVB5Jx8lALkkBAvQjNT147FY4grnxM6HQ==\"," +
                                         "\"sdkEphemPubKey\":\"" +
                                         ((com.modirum.ds.tds20msgs.TDSMessage) aReq).getSdkEphemPubKey() + "\"}";

                        payload = Base64.encodeURL(payload.getBytes(StandardCharsets.UTF_8));
                        m.setAcsSignedContent(header + "." + payload + "." + signature);
                    } else if (aReq instanceof com.modirum.ds.tds21msgs.TDSMessage &&
                               ((com.modirum.ds.tds21msgs.TDSMessage) aReq).getSdkEphemPubKey() != null) {
                        // TODO: fix
                        String epk = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEb6VDHMWXeMgUSOhEF1VyernTqpNqSkOW31TckuzSHNhTIr8iDpMxyHVB5Jx8lALkkBAvQjNT147FY4grnxM6HQ==";
                        //((com.modirum.ds.tds21msgs.TDSMessage)aReq).getSdkEphemPubKey();
                        String header = "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZvWERURTRNREl4TXpFeE16Y3lNVm93RHpFTk1Bc0dBMVVFQXd3RWRHVnpkRENDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFMWFdjbitkbUtWcXJPdUl5WWpFS2Q1K3NKdGViTUhFXC96TEw3QXRcL2JtWGJCWHhHV0VDOEVjSkdnUjByT3JtVUtuazZtSWFsSmY1RW1uQ0JJZEdNMHFXYXljcTBwdmNZY0dXcis4RlBzaVQrYW9FMmZiWmpsbytYZ1Nib1wvS3hzNjlkQ09yZW5QTnZhaXkrZlNTaG5VVndqNmVVMzk4OFZlOXNwUWZHWThPeW9hZGhlMUhZUXhsd2hMZnowUCtIeG05UGRiMTNxMVgzeTlcLzhVZWhEc3c4WGNId2t2TVFjWTU0K0NxTjFma1RIXC82d29OVVwvMzRaS3ZEUnNnMlpTM0pqdDZiVXBsOHhYMGlyTklRcUFjRGtXM2NnQ2NpdGk2Q2Y4ZE9DUjlyRmx2aXNjUnlHbE05STBhRlowa05aTmxKTU85dGNyakhQSWlMbDZsQXNYSTFmQjBDQXdFQUFUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFFZ0h1dGg1ZFNFc0NtbXVBckQ3bE1VU3FJcEpcL0RcLzh5b2FweHFLcWtPY3Znc2UyR3dKcjQzb29lXC9UWE9IWE05OU41b1M4Rzl2cmwydWNLXC9rcUE2VFFJcmRZUEJUYXAyN3ZKU0NjMVdSMmdxa1ZtK1wvRnNGeE5Sb2w0YWZ6WFpLdmVEVE1RaGkrNkpaWmYrc2N6cnkxWmFYSDdVcW1yQW9PdU1reFAxYjRtTzRZYTZUVlBcL1FETGJoU1wvV0RBSlRUejAzKzFDNDZKSXV5XC9PTjhpM090YnoyaXhkdXNjM2puZ0d5Rk1nV1BiRGVGdVUrdXJWVzF5OTdrRTFpUmQ5S3FodHExMzRlZWpDcnAyaWJNWHBTRnkrXC91ZmFnK00xRzM3YzZzNTU2d1FlVngrOVwva0p2NFNGVE9ETEtsdXppbGRwVlVGZTBnUndtY211V1Y4dXRKVWh3PT0iXSwiYWxnIjoiUlMyNTYifQ";
                        String signature = "aKW-siPDLKH4f84CqWiyVhSfqqxxkjEURdAS6q3-gQx9J9jaM-GYk4eLsjAImYRvt4ybJdUVF1PxYGT8V_HrfTSfLX2_vyBl23GrL5ntJhzKiUFJNlEfjN3XTXSNWO4jWd464wi44dpIqKkcZTCkrAFUZpGU3hRVfxORFQSkMq5pvNsXS4db0v3GxVSJGdWckoX0HAdzPTlF3FvMCvbe1Q1Ae2BMJvokqIyGKvNEkBpQxMIUMT36yX-NzLk4rtB0pznxkcYxJEFTsyfMnsvjMOmgOSLTHfg798FaJ2EFkw4A7fFaLkY63B2AwH1PT6IZ7aR0Nbas5Z-iRil4mx2EOw";
                        String payload = "{\"acsURL\":\"" + acsUrl + "\", " +
                                         "\"acsEphemPubKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEb6VDHMWXeMgUSOhEF1VyernTqpNqSkOW31TckuzSHNhTIr8iDpMxyHVB5Jx8lALkkBAvQjNT147FY4grnxM6HQ==\"," +
                                         "\"sdkEphemPubKey\":\"" + epk + "\"}";

                        payload = Base64.encodeURL(payload.getBytes(StandardCharsets.UTF_8));
                        m.setAcsSignedContent(header + "." + payload + "." + signature);
                    }

                } else {
                    m.setAcsURL(acsUrl);
                }

            } else if (newMsg instanceof com.modirum.ds.tds21msgs.TDSMessage) {
                com.modirum.ds.tds21msgs.TDSMessage m = (com.modirum.ds.tds21msgs.TDSMessage) newMsg;

                m.setAcsChallengeMandated("Y");

                if (TDSModel.DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel())) {
                    com.modirum.ds.tds21msgs.XrenderObj2 ro = new com.modirum.ds.tds21msgs.XrenderObj2();
                    ro.setAcsInterface("02");
                    ro.setAcsUiTemplate("05");
                    //ro.getUiType().add("05");
                    m.setAcsRenderingType(ro);

                    if (aReq instanceof com.modirum.ds.tds20msgs.TDSMessage &&
                        ((com.modirum.ds.tds20msgs.TDSMessage) aReq).getSdkEphemPubKey() != null) {
                        String header = "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZvWERURTRNREl4TXpFeE16Y3lNVm93RHpFTk1Bc0dBMVVFQXd3RWRHVnpkRENDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFMWFdjbitkbUtWcXJPdUl5WWpFS2Q1K3NKdGViTUhFXC96TEw3QXRcL2JtWGJCWHhHV0VDOEVjSkdnUjByT3JtVUtuazZtSWFsSmY1RW1uQ0JJZEdNMHFXYXljcTBwdmNZY0dXcis4RlBzaVQrYW9FMmZiWmpsbytYZ1Nib1wvS3hzNjlkQ09yZW5QTnZhaXkrZlNTaG5VVndqNmVVMzk4OFZlOXNwUWZHWThPeW9hZGhlMUhZUXhsd2hMZnowUCtIeG05UGRiMTNxMVgzeTlcLzhVZWhEc3c4WGNId2t2TVFjWTU0K0NxTjFma1RIXC82d29OVVwvMzRaS3ZEUnNnMlpTM0pqdDZiVXBsOHhYMGlyTklRcUFjRGtXM2NnQ2NpdGk2Q2Y4ZE9DUjlyRmx2aXNjUnlHbE05STBhRlowa05aTmxKTU85dGNyakhQSWlMbDZsQXNYSTFmQjBDQXdFQUFUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFFZ0h1dGg1ZFNFc0NtbXVBckQ3bE1VU3FJcEpcL0RcLzh5b2FweHFLcWtPY3Znc2UyR3dKcjQzb29lXC9UWE9IWE05OU41b1M4Rzl2cmwydWNLXC9rcUE2VFFJcmRZUEJUYXAyN3ZKU0NjMVdSMmdxa1ZtK1wvRnNGeE5Sb2w0YWZ6WFpLdmVEVE1RaGkrNkpaWmYrc2N6cnkxWmFYSDdVcW1yQW9PdU1reFAxYjRtTzRZYTZUVlBcL1FETGJoU1wvV0RBSlRUejAzKzFDNDZKSXV5XC9PTjhpM090YnoyaXhkdXNjM2puZ0d5Rk1nV1BiRGVGdVUrdXJWVzF5OTdrRTFpUmQ5S3FodHExMzRlZWpDcnAyaWJNWHBTRnkrXC91ZmFnK00xRzM3YzZzNTU2d1FlVngrOVwva0p2NFNGVE9ETEtsdXppbGRwVlVGZTBnUndtY211V1Y4dXRKVWh3PT0iXSwiYWxnIjoiUlMyNTYifQ";
                        String signature = "aKW-siPDLKH4f84CqWiyVhSfqqxxkjEURdAS6q3-gQx9J9jaM-GYk4eLsjAImYRvt4ybJdUVF1PxYGT8V_HrfTSfLX2_vyBl23GrL5ntJhzKiUFJNlEfjN3XTXSNWO4jWd464wi44dpIqKkcZTCkrAFUZpGU3hRVfxORFQSkMq5pvNsXS4db0v3GxVSJGdWckoX0HAdzPTlF3FvMCvbe1Q1Ae2BMJvokqIyGKvNEkBpQxMIUMT36yX-NzLk4rtB0pznxkcYxJEFTsyfMnsvjMOmgOSLTHfg798FaJ2EFkw4A7fFaLkY63B2AwH1PT6IZ7aR0Nbas5Z-iRil4mx2EOw";
                        String payload = "{\"acsURL\":\"" + acsUrl + "\", " +
                                         "\"acsEphemPubKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEb6VDHMWXeMgUSOhEF1VyernTqpNqSkOW31TckuzSHNhTIr8iDpMxyHVB5Jx8lALkkBAvQjNT147FY4grnxM6HQ==\"," +
                                         "\"sdkEphemPubKey\":\"" +
                                         ((com.modirum.ds.tds20msgs.TDSMessage) aReq).getSdkEphemPubKey() + "\"}";

                        payload = Base64.encodeURL(payload.getBytes(StandardCharsets.UTF_8));
                        m.setAcsSignedContent(header + "." + payload + "." + signature);
                    } else if (aReq instanceof com.modirum.ds.tds21msgs.TDSMessage &&
                               ((com.modirum.ds.tds21msgs.TDSMessage) aReq).getSdkEphemPubKey() != null) {
                        // TODO: fix
                        String epk = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEb6VDHMWXeMgUSOhEF1VyernTqpNqSkOW31TckuzSHNhTIr8iDpMxyHVB5Jx8lALkkBAvQjNT147FY4grnxM6HQ==";
                        //((com.modirum.ds.tds21msgs.TDSMessage)aReq).getSdkEphemPubKey();
                        String header = "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZvWERURTRNREl4TXpFeE16Y3lNVm93RHpFTk1Bc0dBMVVFQXd3RWRHVnpkRENDQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFMWFdjbitkbUtWcXJPdUl5WWpFS2Q1K3NKdGViTUhFXC96TEw3QXRcL2JtWGJCWHhHV0VDOEVjSkdnUjByT3JtVUtuazZtSWFsSmY1RW1uQ0JJZEdNMHFXYXljcTBwdmNZY0dXcis4RlBzaVQrYW9FMmZiWmpsbytYZ1Nib1wvS3hzNjlkQ09yZW5QTnZhaXkrZlNTaG5VVndqNmVVMzk4OFZlOXNwUWZHWThPeW9hZGhlMUhZUXhsd2hMZnowUCtIeG05UGRiMTNxMVgzeTlcLzhVZWhEc3c4WGNId2t2TVFjWTU0K0NxTjFma1RIXC82d29OVVwvMzRaS3ZEUnNnMlpTM0pqdDZiVXBsOHhYMGlyTklRcUFjRGtXM2NnQ2NpdGk2Q2Y4ZE9DUjlyRmx2aXNjUnlHbE05STBhRlowa05aTmxKTU85dGNyakhQSWlMbDZsQXNYSTFmQjBDQXdFQUFUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFFZ0h1dGg1ZFNFc0NtbXVBckQ3bE1VU3FJcEpcL0RcLzh5b2FweHFLcWtPY3Znc2UyR3dKcjQzb29lXC9UWE9IWE05OU41b1M4Rzl2cmwydWNLXC9rcUE2VFFJcmRZUEJUYXAyN3ZKU0NjMVdSMmdxa1ZtK1wvRnNGeE5Sb2w0YWZ6WFpLdmVEVE1RaGkrNkpaWmYrc2N6cnkxWmFYSDdVcW1yQW9PdU1reFAxYjRtTzRZYTZUVlBcL1FETGJoU1wvV0RBSlRUejAzKzFDNDZKSXV5XC9PTjhpM090YnoyaXhkdXNjM2puZ0d5Rk1nV1BiRGVGdVUrdXJWVzF5OTdrRTFpUmQ5S3FodHExMzRlZWpDcnAyaWJNWHBTRnkrXC91ZmFnK00xRzM3YzZzNTU2d1FlVngrOVwva0p2NFNGVE9ETEtsdXppbGRwVlVGZTBnUndtY211V1Y4dXRKVWh3PT0iXSwiYWxnIjoiUlMyNTYifQ";
                        String signature = "aKW-siPDLKH4f84CqWiyVhSfqqxxkjEURdAS6q3-gQx9J9jaM-GYk4eLsjAImYRvt4ybJdUVF1PxYGT8V_HrfTSfLX2_vyBl23GrL5ntJhzKiUFJNlEfjN3XTXSNWO4jWd464wi44dpIqKkcZTCkrAFUZpGU3hRVfxORFQSkMq5pvNsXS4db0v3GxVSJGdWckoX0HAdzPTlF3FvMCvbe1Q1Ae2BMJvokqIyGKvNEkBpQxMIUMT36yX-NzLk4rtB0pznxkcYxJEFTsyfMnsvjMOmgOSLTHfg798FaJ2EFkw4A7fFaLkY63B2AwH1PT6IZ7aR0Nbas5Z-iRil4mx2EOw";
                        String payload = "{\"acsURL\":\"" + acsUrl + "\", " +
                                         "\"acsEphemPubKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEb6VDHMWXeMgUSOhEF1VyernTqpNqSkOW31TckuzSHNhTIr8iDpMxyHVB5Jx8lALkkBAvQjNT147FY4grnxM6HQ==\"," +
                                         "\"sdkEphemPubKey\":\"" + epk + "\"}";

                        payload = Base64.encodeURL(payload.getBytes(StandardCharsets.UTF_8));
                        m.setAcsSignedContent(header + "." + payload + "." + signature);
                    }
                } else {
                    m.setAcsURL(acsUrl);
                }

            }

        } // C

        // not available
        newMsg.setAcsTransID(createUUID(330330, (System.currentTimeMillis() + (int) (Math.random() * 100))));

        return newMsg;
    }

    public TDSMessageBase createRReqMessage(TDSMessageBase aReq, TDSMessageBase aRes, String ireqCode, String ireqDetail, TDSModel.XtransStatus txStatus, String statusReason, String chellengeCancel, TDSMessageBase m) {

        m.setMessageVersion(aReq.getMessageVersion());
        //m.setProtocol(MessageService.cPaymentProtocolTDSV2);
        m.setMessageType(TDSModel.XmessageType.R_REQ.value());

        m.setDsReferenceNumber(aReq.getDsReferenceNumber());
        m.setDsTransID(aReq.getDsTransID());
        //m.setAcsReferenceNumber(acsRef);

        // from areq
        m.setSdkTransID(aReq.getSdkTransID());
        //m.setMITransID(aReq.getMITransID());
        m.setTdsServerTransID(aReq.getTdsServerTransID());
        m.setTransStatus(txStatus.value());
        m.setTransStatusReason(statusReason);
        m.setMessageCategory(aReq.getMessageCategory());

        // from ares
        m.setAcsTransID(aRes.getAcsTransID());

        if (m instanceof com.modirum.ds.tds20msgs.TDSMessage) {
            com.modirum.ds.tds20msgs.TDSMessage mx = (com.modirum.ds.tds20msgs.TDSMessage) m;
            if (TDSModel.DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel())) {
                com.modirum.ds.tds20msgs.XrenderObj2 ro = new com.modirum.ds.tds20msgs.XrenderObj2();
                ro.setInterface("02");
                ro.setUiType("01");
                //ro.getUiType().add("05");
                mx.setAcsRenderingType(ro);
            }
            mx.setChallengeCancel(chellengeCancel);
        } else if (m instanceof com.modirum.ds.tds21msgs.TDSMessage) {
            com.modirum.ds.tds21msgs.TDSMessage mx = (com.modirum.ds.tds21msgs.TDSMessage) m;

            if (TDSModel.DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel())) {
                com.modirum.ds.tds21msgs.XrenderObj2 ro = new com.modirum.ds.tds21msgs.XrenderObj2();
                ro.setAcsInterface("02");
                ro.setAcsUiTemplate("05");
                //ro.getUiType().add("05");
                mx.setAcsRenderingType(ro);
            }
            mx.setChallengeCancel(chellengeCancel);
        }
        // MUST BE U
        if (TDSModel.XtransStatus.Y == txStatus || TDSModel.XtransStatus.A == txStatus) {
            m.setAuthenticationType("01");
            m.setAuthenticationValue(getAAV(aReq.getAcctNumber()));
            m.setEci("05");
            if (m instanceof com.modirum.ds.tds21msgs.TDSMessage) {
                ((com.modirum.ds.tds21msgs.TDSMessage) m).setAuthenticationMethod("01");
            }
            if (m instanceof com.modirum.ds.tds20msgs.TDSMessage) {
                ((com.modirum.ds.tds20msgs.TDSMessage) m).setAuthenticationMethod("02");
            }
        } else {
            if (m instanceof com.modirum.ds.tds21msgs.TDSMessage) {
                ((com.modirum.ds.tds21msgs.TDSMessage) m).setAuthenticationMethod("01");
                ((com.modirum.ds.tds21msgs.TDSMessage) m).setAuthenticationType("01");
            }
            if (m instanceof com.modirum.ds.tds20msgs.TDSMessage) {
                ((com.modirum.ds.tds20msgs.TDSMessage) m).setAuthenticationMethod("02");
                ((com.modirum.ds.tds21msgs.TDSMessage) m).setAuthenticationType("02");
            }

        }

        return m;
    }

    public class ChallengeAuto implements Runnable {
        TDSMessageBase aReq, aRes = null;
        TDSModel.XtransStatus st = null;
        boolean result = false;
        String str = null;
        MessageService xmse;
        JsonArrayBuilder rreqMessageExtensionsBuilder;
        String cci;

        public ChallengeAuto(TDSMessageBase aReq, TDSMessageBase aRes, TDSModel.XtransStatus s, MessageService mse, String cci) {
            this(aReq, aRes, s, mse, null, cci);
        }

        public ChallengeAuto(TDSMessageBase aReq, TDSMessageBase aRes, TDSModel.XtransStatus s, MessageService mse, JsonArrayBuilder rreqMessageExtensionsBuilder, String cci) {
            this.aReq = aReq;
            this.aRes = aRes;
            st = s;
            this.cci = cci;
            xmse = mse;
            this.rreqMessageExtensionsBuilder = rreqMessageExtensionsBuilder;
        }

        @Override
        public void run() {
            try {
                HttpsJSSEClient client = new HttpsJSSEClient();
                String sslContext = getServletContext().getInitParameter("DS.SSLProto");
                if (Misc.isNullOrEmpty(sslContext)) {
                    sslContext = SSLSocketHelper.SSLProto.TLSv12;
                }
                // for testing only!!!
                String sslTrustAny = getServletContext().getInitParameter("DS.ssl.client.trustany");

                PrivateKey pk = null;
                X509Certificate[] chain = null;
                KeyStore trustStore = null;
                String dsUrl = aReq.getDsURL();
                TDSMessageBase rReq = createRReqMessage(aReq, aRes, null, null, st, str, cci, xmse.createNewMessage());
                rReq.setInteractionCounter("01");

                if (dsUrl.toLowerCase().startsWith("https://")) {
                    trustStore = getSchemeRootsAsKeyStore();
                    Object o[] = getPkAndCerticiateChainById();
                    if (o != null) {
                        pk = (PrivateKey) o[0];
                        chain = (X509Certificate[]) o[1];
                    }
                }

                int dsTimeout = 5000;
                String kalg = javax.net.ssl.KeyManagerFactory.getDefaultAlgorithm();
                client.init(null, kalg, sslContext, "true".equals(sslTrustAny) ? "true" : "false", pk, chain,
                            trustStore, 2000, dsTimeout, false);
                client.setExecService(getScheduledExecutorService());

                JsonObjectBuilder tdsResBuilder = xmse.toJSONOB(rReq);
                if (rreqMessageExtensionsBuilder != null) {
                    tdsResBuilder.add("messageExtension", rreqMessageExtensionsBuilder);
                }

                PublicBOS raw = xmse.toJSONBos(tdsResBuilder.build());
                if (!EmbedTestFilter.factorySet) {
                    byte[] raw1 = EmbedTestFilter.applyTransformations(raw.getBuf(), 0, raw.getCount());
                    raw = new PublicBOS(raw1);
                }
                log.info("Sending " + rReq.getMessageType() + " to DS " + dsUrl + "\n" +
                         new String(raw.getBuf(), 0, raw.getCount(), StandardCharsets.UTF_8));

                byte[] resp = client.post(dsUrl, MessageService.CT_JSON, raw.getBuf(), 0, raw.getCount(), null, null);
                String respContent = new String(resp, StandardCharsets.UTF_8);
                log.info("RRes: " + respContent);
                result = true;
            } catch (Exception e) {
                log.error("Error with sending RReq to ds", e);
                result = false;
            }

        }

    }

    ScheduledThreadPoolExecutor sexec = null;

    public ScheduledThreadPoolExecutor getScheduledExecutorService() {
        if (sexec == null) {
            sexec = new ScheduledThreadPoolExecutor(2, new com.modirum.ds.utils.DaemonThreadFactory("ACSEmuServiceExec"));
            sexec.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
            sexec.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
            sexec.setMaximumPoolSize(3);
        }

        return sexec;
    }

    KeyStore schemeRoots = null;

    public KeyStore getSchemeRootsAsKeyStore() throws Exception {
        String dsSchemeRoots = getServletContext().getInitParameter("DS.ssl.roots");
        if (schemeRoots == null && Misc.isNotNullOrEmpty(dsSchemeRoots)) {
            KeyStore ks = java.security.KeyStore.getInstance("JKS");
            ks.load(null, null);

            byte[] certData = null;
            if (dsSchemeRoots.length() > 255) {
                certData = dsSchemeRoots.getBytes(StandardCharsets.ISO_8859_1);
            } else // for some reason shumik wants to load from file
            {
                Path path = Paths.get(dsSchemeRoots);
                if (!path.isAbsolute()) {
                    path = Paths.get(this.getServletContext().getRealPath(dsSchemeRoots));
                }
                certData = Files.readAllBytes(path);
            }

            X509Certificate[] roots = KeyService.parseX509PemHeadersSafeCertificates(certData, null);
            for (X509Certificate rx : roots) {
                ks.setCertificateEntry(rx.getSubjectDN().getName(), rx);
            }
            schemeRoots = ks;
        }
        return schemeRoots;
    }

    KeyStore ksc = null;

    public Object[] getPkAndCerticiateChainById() throws Exception {
        String keyalias = this.getServletContext().getInitParameter("DS.keyalias");
        String keystore = this.getServletContext().getInitParameter("DS.keystore");
        String keystoreType = this.getServletContext().getInitParameter("DS.keystoreType");
        String keystorePs = this.getServletContext().getInitParameter("DS.keystorePass");
        if (Misc.isNotNullOrEmpty(keyalias) && Misc.isNotNullOrEmpty(keystore)) {
            if (Misc.isNullOrEmpty(keystoreType)) {
                keystoreType = "JKS";
            }
            if (ksc == null && Paths.get(keystore).isAbsolute()) {
                ksc = Helper.loadKeyStore(keystore, keystorePs, keystoreType);
            }
            if (ksc == null) {
                ksc = Helper.loadKeyStore(this.getServletContext().getRealPath(keystore), keystorePs, keystoreType);
            }
            Object[] o = new Object[2];
            o[0] = ksc.getKey(keyalias, keystorePs != null ? keystorePs.toCharArray() : new char[0]);

            Certificate[] xcert = ksc.getCertificateChain(keyalias);
            X509Certificate[] certs = new X509Certificate[xcert != null ? xcert.length : 0];
            for (int i = 0; i < certs.length; i++) {
                certs[i] = (X509Certificate) xcert[i];
            }
            o[1] = certs;

            return o;
        }

        String dsClientCert = getServletContext().getInitParameter("DS.ssl.client.certs");
        String dsClientPk = getServletContext().getInitParameter("DS.ssl.client.pk");

        if (Misc.isNotNullOrEmpty(dsClientCert)) {
            X509Certificate[] certs = KeyService.parseX509Certificates(
                    dsClientCert.getBytes(StandardCharsets.ISO_8859_1), null);
            // verify that chain in legitime and not altered..
            byte[] keyPlain = Base64.decode(dsClientPk);
            PKCS8EncodedKeySpec priKeySpec = new PKCS8EncodedKeySpec(keyPlain);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey pk = keyFactory.generatePrivate(priKeySpec);
            Object[] o = new Object[2];
            o[0] = pk;
            o[1] = certs;
            return o;
        }

        return null;
    }

    public TDSMessageBase createCResMessage(TDSMessageBase cReq, String ireqCode, String ireqDetail, TDSModel.XtransStatus txStatus, String statusReason, String md, TDSMessageBase m) {
        m.setMessageVersion(cReq.getMessageVersion());
        //m.setProtocol(MessageService.cPaymentProtocolTDSV2);
        m.setMessageType(TDSModel.XmessageType.C_RES.value());

        m.setDsReferenceNumber(cReq.getDsReferenceNumber());
        m.setDsTransID(cReq.getDsTransID());
        m.setAcsReferenceNumber(acsRef);

        // from creq
        m.setSdkTransID(cReq.getSdkTransID());
        m.setTdsServerTransID(cReq.getTdsServerTransID());
        m.setTransStatus(txStatus.value());
        m.setTransStatusReason(statusReason);

        m.setAcsTransID(cReq.getAcsTransID());

        return m;
    }


}
