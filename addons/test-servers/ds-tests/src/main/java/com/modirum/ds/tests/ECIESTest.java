/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 6. jaan 2016
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.services.KeyService;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import org.bouncycastle.jce.ECNamedCurveTable;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;

public class ECIESTest {


    public static void main(String[] args) throws Exception {

        //KeyFactory fact = KeyFactory.getInstance("ECDH", "SC");
        java.security.SecureRandom sr = java.security.SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(("sejrhui4h" + new java.util.Date() + "s5NNSG((Jssdjskdj" + Runtime.getRuntime().totalMemory() +
                    "Zsld9Bsl" + Math.random() + System.currentTimeMillis()).getBytes());

        getBouncyCastleProvider();
        java.security.KeyPairGenerator kpg = java.security.KeyPairGenerator.getInstance("ECDSA", "BC"); //"ECDH"

        //kpg.initialize(256, sr);
        AlgorithmParameterSpec params = ECNamedCurveTable.getParameterSpec("prime256v1");
        //AlgorithmParameterSpec params=new ECNamedCurveSpec(ECNamedCurveTable.getParameterSpec("prime256v1"));

        kpg.initialize(params, sr);


        java.security.KeyPair keyPair = kpg.genKeyPair();
        String data = "Some data 1234567890 this needs to be long so well see if the stuff is as advertized or not exactly";
        System.out.println("Data: " + data);
        Cipher cipher = Cipher.getInstance("ECIES"); //, "SC");
        System.out.println("Pubk: " + Base64.encode(keyPair.getPublic().getEncoded()));
        System.out.println("Pubk: " + HexUtils.toString(keyPair.getPublic().getEncoded()));
        System.out.println("Prik: " + Base64.encode(keyPair.getPrivate().getEncoded()));
        System.out.println("Prik: " + HexUtils.toString(keyPair.getPrivate().getEncoded()));

        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
        byte[] enc = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
        System.out.println("Enc: " + HexUtils.toString(enc));

        Cipher cipher2 = Cipher.getInstance("ECIES");//, ECIES "SC");
        cipher2.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());

        byte[] plain = cipher2.doFinal(enc);

        System.out.println("Back: " + new String(plain, StandardCharsets.UTF_8));

        String subjValue = "C=EE, L=Tallinn, ST=HM, O=Org, OU=Unit, CN=Common name";

        String sigAlg = KeyService.sigAlgECDSASHA256;
        javax.security.auth.x500.X500Principal subject = new javax.security.auth.x500.X500Principal(subjValue);
        org.bouncycastle.asn1.ASN1Set attrs = new org.bouncycastle.asn1.DERSet();
        // this is the only thing that really works
        //org.bouncycastle.jce.PKCS10CertificationRequest csr = new org.bouncycastle.jce.PKCS10CertificationRequest(sigAlg,
        //		subject, keyPair.getPublic(), attrs, keyPair.getPrivate(), KeyService.getSignatureProvider(sigAlg));

        X509Certificate[] cert = null;
        org.bouncycastle.cert.jcajce.JcaX509v1CertificateBuilder certBuilder = new org.bouncycastle.cert.jcajce.JcaX509v1CertificateBuilder(
                subject, java.math.BigInteger.valueOf(System.currentTimeMillis() / 1000), new java.util.Date(),
                new java.util.Date(System.currentTimeMillis() + 24L * 3600L * 1000L * 365), subject,
                keyPair.getPublic());

        org.bouncycastle.operator.ContentSigner coSigner = new org.bouncycastle.operator.jcajce.JcaContentSignerBuilder(
                sigAlg).build(keyPair.getPrivate());

        org.bouncycastle.cert.X509CertificateHolder certHolder = certBuilder.build(coSigner);
        cert = KeyService.parseX509Certificates(certHolder.getEncoded(), "X509");

        System.out.println("Cert: " + Base64.encode(certHolder.getEncoded()));


    }


    public static java.security.Provider getBouncyCastleProvider() {
        java.security.Provider bcpe = java.security.Security.getProvider(
                org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME);
        if (bcpe == null) {
            bcpe = new org.bouncycastle.jce.provider.BouncyCastleProvider();
            java.security.Security.addProvider(bcpe);
        }

        return bcpe;
    }

}
