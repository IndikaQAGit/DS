<%@page import="java.nio.charset.StandardCharsets, javax.json.JsonObject" %>
<%@page import="com.modirum.ds.utils.DateUtil" %>
<%@page import="com.modirum.ds.model.TDSModel" %>
<%@page import="java.security.PrivateKey" %>
<%@page import="com.modirum.ds.tests.Helper" %>
<%@page import="java.security.cert.X509Certificate" %>
<%@page session="true" import="com.modirum.ds.utils.Base64,com.modirum.ds.utils.Misc" %>
<%@ page import="com.modirum.ds.services.MessageService" %>
<%!
    public static String getValueOrDefault(String value, String defaultValue) {
        return Misc.isNullOrEmpty(value) ? defaultValue : value;
    }
%>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("com.modirum.mdpay.ds.web.dstest");
// RRES

    String clientCertSDN = "";
    java.util.Date now = new java.util.Date();
    String puDate = DateUtil.formatDate(now, "yyyyMMdd HH:mm:ss", true);
    String puDate21 = DateUtil.formatDate(now, "yyyyMMddHHmmss", true);
    com.modirum.ds.services.MessageService mse = com.modirum.ds.services.MessageService210.getInstance();

    if (request.getContentType() != null && request.getContentType().contains(com.modirum.ds.services.MessageService.CT_JSON)) {
        log.info("GOT json request, parsing..");

        int ctLen = request.getContentLength();

        com.modirum.ds.model.TDSMessageBase req = null;

        if (ctLen > 0 && ctLen < 100000) {
            byte[] js = new byte[ctLen];
            ServletInputStream sis = request.getInputStream();
            int read = 0;
            int lastRead = 0;
            do {
                lastRead = sis.read(js, read, ctLen - read);
                if (lastRead < 0) {
                    break;
                }

                read += lastRead;

            } while (read < ctLen);

            String[] json = new String[]{new String(js, StandardCharsets.UTF_8)};
            com.modirum.ds.services.MessageService.JsonAndDucplicates jsd = MessageService.parseJsonAndDups(json);
            JsonObject jsonObj = jsd.json;
            //String mv=MessageService.getMessageVersion(json);
            String mv = jsonObj != null && jsonObj.containsKey("messageVersion") ? jsonObj.getString("messageVersion") : null;

            req = mse.fromJSON(js);

            this.getServletContext().setAttribute("rreq" + System.currentTimeMillis(), js);
        } else {
            req = mse.fromJSON(request.getReader());
        }

        log.info("GOT " + req.getMessageType());
        if (TDSModel.XmessageType.R_REQ.value().equals(req.getMessageType())) {
            com.modirum.ds.tds21msgs.TDSMessage rres = new com.modirum.ds.tds21msgs.TDSMessage();
            rres.setAcsTransID(req.getAcsTransID());
            rres.setDsTransID(req.getDsTransID());
            rres.setMessageType(TDSModel.XmessageType.R_RES.value());
            rres.setMessageVersion(req.getMessageVersion());
            rres.setResultsStatus(TDSModel.XResultsStatus.C01RRes.value);

            rres.setTdsServerTransID(req.getTdsServerTransID());
            byte[] json = mse.toJSON(rres);
            response.setContentType(request.getContentType());
            response.setContentLength(json.length);
            response.getOutputStream().write(json);
            //response.flushBuffer();
            log.info("Sent " + rres.getMessageVersion() + " response " + rres.getMessageType());
            return;
        }
    }
    if ("getrreq".equals(request.getParameter("cmd"))) {
        java.util.Enumeration<String> an = this.getServletContext().getAttributeNames();
        StringBuilder sb = new StringBuilder(256);
        while (an.hasMoreElements()) {
            String anx = (String) an.nextElement();
            if (anx.startsWith("rreq")) {
                byte[] mc = (byte[]) this.getServletContext().getAttribute(anx);
                this.getServletContext().removeAttribute(anx);
                sb.append(new String(mc, "UTF-8"));
                sb.append("\n");
            }
        }

        response.setContentType(MessageService.CT_JSON_UTF8);
        response.getWriter().write(sb.toString());
        log.info("Sent rreqs to browser");
        return;
    }

%>
<html>
<head>
    <title>DS JSON test</title>
    <script type="text/javascript">

    </script>
</head>
<body>
<%

%>

<h2>DS test tool</h2>
<b>This tool is developed for Modirum internal testing purposes only</b>,
<br/>
<h2>Make test Request</h2>
<div id="serverMsg">
</div>
<a href="dstest.jsp">Start over</a>
<form name="dummy" action="dstest.jsp" method="post">
    <%
        String keyStore = this.getServletContext().getInitParameter("keystore");
        String keyStoreType = getValueOrDefault(this.getServletContext().getInitParameter("keystoreType"), "JKS");
        String keyStorePass = this.getServletContext().getInitParameter("keystorePass");
        String trustStore = this.getServletContext().getInitParameter("truststore");
        String trustStorePass = this.getServletContext().getInitParameter("truststorePass");
        String keyalias = this.getServletContext().getInitParameter("keyalias");
        String skdalias = this.getServletContext().getInitParameter("sdkalias");
        String skdecalias = this.getServletContext().getInitParameter("sdkecalias");
        String rootCertURI = this.getServletContext().getInitParameter("RootCertURI");
        if (rootCertURI != null && rootCertURI.startsWith("/")) {
            StringBuffer sb = request.getRequestURL();
            Misc.replace(sb, request.getRequestURI(), rootCertURI);
            rootCertURI = sb.toString();
        }

        String sdkCertUrl = this.getServletContext().getInitParameter("sdkcerturl");
        if (sdkCertUrl != null && sdkCertUrl.startsWith("/")) {
            StringBuffer sb = request.getRequestURL();
            Misc.replace(sb, request.getRequestURI(), sdkCertUrl);
            sdkCertUrl = sb.toString();
        }

        String sdkECCertUrl = this.getServletContext().getInitParameter("sdkECcerturl");
        if (sdkECCertUrl != null && sdkECCertUrl.startsWith("/")) {
            StringBuffer sb = request.getRequestURL();
            Misc.replace(sb, request.getRequestURI(), sdkECCertUrl);
            sdkECCertUrl = sb.toString();
        }

        String defaultAcqBin = getValueOrDefault(this.getServletContext().getInitParameter("defaultAcqBin"), "412345");
        String defaultMerchantId = getValueOrDefault(this.getServletContext().getInitParameter("defaultMerchantId"), "771133");

		/*String devda= ("{"+
				"\"DeviceData\" : {"+
				"\"Android_ID\" : \"12c123e12321f3a12d\","+
				"\"Build_Release\" : \"5.1\","+
				"\"Build_Serial\" : \"123e12ab\","+
				"\"Build_SDK\" : \"LOLLIPOP\","+
				"\"Wifi_MAC\" : \"01:02:03:04:05:06\","+
				"\"TeleMan_GetID\" : \"123456789123456789\","+
				"\"SIM_Serial\" : \"123456789123456789\""+
				"}"+
				"}");
		*/

        String devda = "{\"DV\":\"1.0\",\n" +
                "\"DD\":{\"C001\":\"Android\",\"C002\":\"HTC One_RSA\",\"C004\":\"5.0.1\",\"C005\":\"en_US\",\"C006\":\"Eastern Standard Time\",\n" +
                "\"C007\":\"06797903-fb61-41ed-94c2-4d2b74e27d18\",\"C009\":\"John's Android Device\"},\"DPNA\":{\"C010\":\"RE01\",\"C011\":\"RE03\"},\"SW\":[\"SW01\",\"SW04\"]}";
        String devdaec = "{\"DV\":\"1.0\",\n" +
                "\"DD\":{\"C001\":\"Android\",\"C002\":\"HTC One_EC\",\"C004\":\"5.0.1\",\"C005\":\"en_US\",\"C006\":\"Eastern Standard Time\",\n" +
                "\"C007\":\"06797903-fb61-41ed-94c2-4d2b74e27d18\",\"C009\":\"John's Android Device\"},\"DPNA\":{\"C010\":\"RE01\",\"C011\":\"RE03\"},\"SW\":[\"SW01\",\"SW04\"]}";

        String sdkData = (String) request.getSession().getAttribute("SDKDATA");
        String sdkDataEC = (String) request.getSession().getAttribute("SDKDATAEC");
        String serviceUrlSDKKey = request.getParameter("serviceUrlSDKKey");
        if (Misc.isNullOrEmpty(serviceUrlSDKKey)) {
            serviceUrlSDKKey = (String) session.getAttribute("serviceUrlSDKKey");
        } else {
            session.setAttribute("serviceUrlSDKKey", serviceUrlSDKKey);
        }
        if (Misc.isNullOrEmpty(serviceUrlSDKKey)) {
            serviceUrlSDKKey = sdkCertUrl;
        }

        String serviceUrlSDKECKey = request.getParameter("serviceUrlSDKECKey");
        if (Misc.isNullOrEmpty(serviceUrlSDKECKey)) {
            serviceUrlSDKECKey = (String) session.getAttribute("serviceUrlSDKECKey");
        } else {
            session.setAttribute("serviceUrlSDKECKey", serviceUrlSDKECKey);
        }
        if (Misc.isNullOrEmpty(serviceUrlSDKECKey)) {
            serviceUrlSDKECKey = sdkECCertUrl;
        }


        String prev = (String) session.getAttribute("serviceUrlSDKKeyPrev");
        if (prev != null && !prev.equals(serviceUrlSDKKey)) {
            sdkData = null;
        }

        String prevEC = (String) session.getAttribute("serviceUrlSDKECKeyPrev");
        if (prevEC != null && !prevEC.equals(serviceUrlSDKECKey)) {
            sdkDataEC = null;
        }

        if (sdkData == null) {
            if (serviceUrlSDKKey != null) {
                try {
                    com.modirum.ds.utils.http.HttpsJSSEClient client = new com.modirum.ds.utils.http.HttpsJSSEClient();
                    client.init(null, "X509", "TLSv1.2", "true", null, null, null, 2000, 6000, false);
                    byte[] resp = client.get(serviceUrlSDKKey, null, null, null, null);
                    X509Certificate sdkCert = com.modirum.ds.services.KeyService.parseX509Certificates(resp, null)[0];
                    com.modirum.ds.services.CryptoService crs = new com.modirum.ds.services.CryptoService();
                    sdkData = crs.encryptJWEComp(sdkCert.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A256GCM, "RSA/None/OAEPWithSHA1AndMGF1Padding", null,
                            devda.getBytes(java.nio.charset.StandardCharsets.UTF_8), false);
                    session.setAttribute("SDKDATA", sdkData);
                    session.setAttribute("serviceUrlSDKKeyPrev", serviceUrlSDKKey);
                } catch (Exception e) {
                    log.error("Error loading RSA sdk key ", e);
    %>Error loading RSA SDK key <%=e %><br/><%
            }
        } else if (keyStore != null) {
            java.security.KeyStore ks1 = Helper.loadKeyStore(this.getServletContext().getRealPath(keyStore), keyStorePass, keyStoreType);
            java.security.cert.Certificate s1 = ks1.getCertificate(skdalias != null ? skdalias : "sdkcertrsa");
            if (s1 != null) {
                com.modirum.ds.services.CryptoService crs = new com.modirum.ds.services.CryptoService();
                //				com.modirum.ds.tds20msgs.XJWE jwe=crs.encryptJWE(s1.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A256GCM,  "RSA/None/OAEPWithSHA1AndMGF1Padding",
                //						devda.getBytes(java.nio.charset.StandardCharsets.UTF_8));

                //sdkData="\""+com.modirum.ds.services.MessageService.getInstance().toCompactJWE(jwe)+"\"";
                sdkData = crs.encryptJWEComp(s1.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A256GCM, "RSA/None/OAEPWithSHA1AndMGF1Padding", null,
                        devda.getBytes(java.nio.charset.StandardCharsets.UTF_8), false);

                session.setAttribute("SDKDATA", sdkData);
            }
        }
    }

    if (sdkDataEC == null) {
        if (serviceUrlSDKECKey != null) {
            try {
                com.modirum.ds.utils.http.HttpsJSSEClient client = new com.modirum.ds.utils.http.HttpsJSSEClient();
                client.init(null, "X509", "TLSv1.2", "true", null, null, null, 2000, 6000, false);
                byte[] resp = client.get(serviceUrlSDKECKey, null, null, null, null);
                X509Certificate sdkCert = com.modirum.ds.services.KeyService.parseX509Certificates(resp, null)[0];
                com.modirum.ds.services.CryptoService crs = new com.modirum.ds.services.CryptoService();
                //com.modirum.ds.tds20msgs.XJWE jwe=crs.encryptJWE(sdkCert.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A256GCM, "ECIES",
                //		devdaec.getBytes(java.nio.charset.StandardCharsets.UTF_8));
                //sdkDataEC="\""+com.modirum.ds.services.MessageService.getInstance().toCompactJWE(jwe)+"\"";
                sdkDataEC = crs.encryptJWEComp(sdkCert.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A128GCM, "ECIES", "DS1",
                        devda.getBytes(java.nio.charset.StandardCharsets.UTF_8), false);

                session.setAttribute("SDKDATAEC", sdkDataEC);
                session.setAttribute("serviceUrlSDKECKeyPrev", serviceUrlSDKKey);
            } catch (Exception e) {
                log.error("Error loading EC sdk key ", e);
%>Error loading EC SDK key <%=e %><br/><%
            }
        } else if (keyStore != null) {
            java.security.KeyStore ks1 = Helper.loadKeyStore(this.getServletContext().getRealPath(keyStore), keyStorePass, keyStoreType);
            java.security.cert.Certificate s1 = ks1.getCertificate(skdecalias != null ? skdecalias : "sdkcertec");
            if (s1 != null) {
                com.modirum.ds.services.CryptoService crs = new com.modirum.ds.services.CryptoService();
                //com.modirum.ds.tds20msgs.XJWE jwe=crs.encryptJWE(s1.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A256GCM, "ECIES",
                //		devdaec.getBytes(java.nio.charset.StandardCharsets.UTF_8));
                //sdkDataEC="\""+com.modirum.ds.services.MessageService.getInstance().toCompactJWE(jwe)+"\"";
                sdkDataEC = crs.encryptJWEComp(s1.getPublicKey(), com.modirum.ds.services.CryptoService.JWE.enc.A128GCM, "ECIES", "DS1",
                        devda.getBytes(java.nio.charset.StandardCharsets.UTF_8), false);

                session.setAttribute("SDKDATAEC", sdkDataEC);
            }
        }
    }


    String reqUrl = request.getRequestURL().toString();

    String respUrl = this.getServletContext().getInitParameter("3DS.serverUrl");
    if (Misc.isNullOrEmpty(respUrl)) {
        respUrl = request.getRequestURL().toString();
    }

    String dsUrl = request.getParameter("dsUrl");
    if (Misc.isNotNullOrEmpty(dsUrl))
        session.setAttribute("dsUrl", dsUrl);

    dsUrl = (String) session.getAttribute("dsUrl");

    if (Misc.isNullOrEmpty(dsUrl))
        dsUrl = this.getServletContext().getInitParameter("ds.url");

    if (Misc.isNullOrEmpty(dsUrl)) {
        StringBuffer sb = request.getRequestURL();
        Misc.replace(sb, request.getRequestURI(), "/ds/DServer");
        dsUrl = sb.toString();
    }

    String serviceUrlServerSide = request.getParameter("serviceUrl");
    if (Misc.isNullOrEmpty(serviceUrlServerSide))
        serviceUrlServerSide = (String) session.getAttribute("serviceUrlPost");


    String json = "";
    if (request.getParameter("json") != null) {
        json = request.getParameter("json");
    }

    String tdsTransId = com.modirum.ds.services.MessageService.createUUID(1022929, System.currentTimeMillis());
    String sdkTransId = com.modirum.ds.services.MessageService.createUUID(888333222, System.currentTimeMillis());

    if (Misc.isNotNullOrEmpty(rootCertURI)) { %>
    Get DS root certs <a href="<%=rootCertURI %>"><%=rootCertURI %>
</a>
    <%
        }


        // cards dynamics
        String case1y = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case1"));
        String case2n = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case2"));
        String case3a = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case3"));
        String case4u = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case4"));
        String case5r = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case5"));
        String case6c = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case6"));
        String case7e = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case7"));
        String case8ca = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case8"));
        String case9e = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case9"));
        String case10u = getCardFromCase(this.getServletContext().getInitParameter("ACSEmu.case10"));


    %>
    Test DS Service URL <input type="text" name="serviceUrl" id="serviceUrl"
                               value="<%=(serviceUrlServerSide!=null && serviceUrlServerSide.length()>0 ? serviceUrlServerSide : dsUrl)%>"
                               size="80"/><br/>
    Test DS SDK RSA key URL <input type="text" name="serviceUrlSDKKey" id="serviceUrlSDKKey"
                                   value="<%=(serviceUrlSDKKey!=null && serviceUrlSDKKey.length()>0 ? serviceUrlSDKKey : sdkCertUrl)%>"
                                   size="80"/><br/>
    Test DS SDK EC key URL <input type="text" name="serviceUrlSDKECKey" id="serviceUrlSDKECKey"
                                  value="<%=(serviceUrlSDKECKey!=null && serviceUrlSDKECKey.length()>0 ? serviceUrlSDKECKey : sdkECCertUrl)%>"
                                  size="80"/><br/>
    <br/>
    Select template:
    <select name="template" id="template" onchange="setTemplate(this.value);">
        <option value=""></option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
 "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case1y %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%=tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case1y %> frictionless Y
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "broadInfo" : {"field1" : "value1", "field2" : "value2", "values" : [1,2,3,4,5], "sub" : { "sub1" :"subval1", "num1" : 22 } },
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
 "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case1y %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSCompInd" : "U",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.1.0",
 "notificationURL" : "<%=reqUrl+"?notify=y" %>",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate21 %>"
}'>2.1.0 - <%=case1y %> frictionless Y
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "broadInfo" : {"field1" : "value1", "field2" : "value2", "values" : [1,2,3,4,5], "sub" : { "sub1" :"subval1", "num1" : 22 } },
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
 "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserJavascriptEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case1y %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSCompInd" : "U",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.2.0",
 "notificationURL" : "<%=reqUrl+"?notify=y" %>",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate21 %>"
}'>2.2.0 - <%=case1y %> frictionless Y
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "broadInfo" : {"field1" : "value1", "field2" : "value2", "values" : [1,2,3,4,5], "sub" : { "sub1" :"subval1", "num1" : 22 } },
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
 "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case1y %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSCompInd" : "U",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.1.0",
 "notificationURL" : "<%=reqUrl+"?notify=y" %>",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "messageExtension" : [ {"criticalityIndicator" : false, "id" : "e1", "name" : "Ext1", "data" : { "foo":"bar", "bar": "foo", "array" : [1,2,3] }} ],
 "purchaseDate" : "<%=puDate21 %>"
}'>2.1.0 - <%=case1y %> frictionless Y + extensions
        </option>

        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case2n %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case2n %> frictionless N
        </option>

        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case3a %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case3a %> frictionless A
        </option>

        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case5r %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case5r %> Not authenticated R
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "broadInfo" : {"field1" : "value1", "field2" : "value2", "values" : [1,2,3,4,5], "sub" : { "sub1" :"subval1", "num1" : 22 } },
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
 "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case5r %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSCompInd" : "U",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.1.0",
 "notificationURL" : "<%=reqUrl+"?notify=y" %>",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "messageExtension" : [ {"criticalityIndicator" : false, "id" : "e1", "name" : "Ext1", "data" : { "foo":"bar", "bar": "foo", "array" : [1,2,3] }} ],
 "purchaseDate" : "<%=puDate21 %>"
}'>2.1.0 - <%=case5r %> Not authenticated R
        </option>

        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case7e %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
  "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case7e %>:ERR
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case9e %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
  "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case9e %>:ERROR
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case8ca %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
  "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case8ca %> browser challenge with auto RReq 10s
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
  "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case8ca %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "mcc" : "5544",
 "threeDSCompInd" : "U",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.1.0",
 "notificationURL" : "<%=reqUrl+"?notify=y" %>",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "purchaseAmount" : "135",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate21 %>"
}'>2.1.0 - <%=case8ca %> browser challenge with auto RReq 10s
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "acctNumber" : "<%=case8ca %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "01",
 "deviceRenderOptions" : {"interface" : "3", "uiType": ["1","2","3","4"]},
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 <%--"messageExtension" : [ "32838293", "true", "some data", "32838294", "true", "some data2" ], commented for now by default until ACS can handle this field --%>
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "sdkTransID" : "<%=sdkTransId %>",
 "sdkAppID" :   "7ffffffd-abf4-1ccc-ffff-000000000100",
 "sdkReferenceNumber" : "SDKEmuV1",
  <%=(sdkData!=null ? "\"sdkEncData\" : \""+sdkData+"\"," : "" ) %>
 "sdkEphemPubKey" : "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZ",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case8ca %> app challenge (RSA SDK DATA) with auto RReq 10s
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "acctNumber" : "<%=case8ca %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "01",
 "deviceRenderOptions" : {"sdkInterface" : "03", "sdkUiType": ["01","02","03","04"]},
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 <%--"messageExtension" : [ "32838293", "true", "some data", "32838294", "true", "some data2" ], commented for now by default until ACS can handle this field --%>
 "messageType" : "AReq",
 "messageVersion" : "2.1.0",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "sdkMaxTimeout" : "06",
 "sdkTransID" : "<%=sdkTransId %>",
 "sdkAppID" :   "7ffffffd-abf4-1ccc-ffff-000000000100",
 "sdkReferenceNumber" : "SDKEmuV1",
  <%=(sdkData!=null ? "\"sdkEncData\" : \""+sdkData+"\"," : "" ) %>
  "sdkEphemPubKey": {
		"kty": "EC",
		"crv": "P-256",
		"x": "b7uAYO80QAbOVRBIQ-gQyfwxTUEhUrI06Ttz_Z_9F4Y",
		"y": "bn0maI8XBaoJjAY3QEEmRK4nSl4oC7eTVxBwS9H8Ptc"
	}, 
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate21 %>"
}'>2.1.0 - <%=case8ca %> app challenge (RSA SDK DATA) with auto RReq 10s
        </option>

        <% if (sdkDataEC != null) {%>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "acctNumber" : "<%=case8ca %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "01",
 "deviceRenderOptions" : {"interface" : "3", "uiType": ["1","2","3","4"]},
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1EC",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 <%--"messageExtension" : [ "32838293", "true", "some data", "32838294", "true", "some data2" ], commented for now by default until ACS can handle this field --%>
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "sdkMaxTimeout" : "06",
 "sdkTransID" : "<%=sdkTransId %>",
 "sdkAppID" :   "7ffffffd-abf4-1ccc-ffff-000000000100",
 "sdkReferenceNumber" : "SDKEmuV1",
  <%=(sdkDataEC!=null ? "\"sdkEncData\" : \""+sdkDataEC+"\"," : "" ) %>
 "sdkEphemPubKey" : "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZ",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case8ca %> app challenge (EC SDK DATA) with auto RReq 10s
        </option>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "acctNumber" : "<%=case8ca %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "01",
 "deviceRenderOptions" : {"sdkInterface" : "03", "sdkUiType": ["01","02","03","04"]},
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "threeDSRequestorAuthenticationInd" : "01", 
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 <%--"messageExtension" : [ "32838293", "true", "some data", "32838294", "true", "some data2" ], commented for now by default until ACS can handle this field --%>
 "messageType" : "AReq",
 "messageVersion" : "2.1.0",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "sdkMaxTimeout" : "06",
 "sdkTransID" : "<%=sdkTransId %>",
 "sdkAppID" :   "7ffffffd-abf4-1ccc-ffff-000000000100",
 "sdkReferenceNumber" : "SDKEmuV1",
  <%=(sdkDataEC!=null ? "\"sdkEncData\" : \""+sdkDataEC+"\"," : "" ) %>
  "sdkEphemPubKey": {
		"kty": "EC",
		"crv": "P-256",
		"x": "b7uAYO80QAbOVRBIQ-gQyfwxTUEhUrI06Ttz_Z_9F4Y",
		"y": "bn0maI8XBaoJjAY3QEEmRK4nSl4oC7eTVxBwS9H8Ptc"
	}, 
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate21 %>"
}'>2.1.0 - <%=case8ca %> app challenge (EC SDK DATA) with auto RReq 10s
        </option>
        <% } %>
        <option value='{
 "acquirerBIN" : "<%=defaultAcqBin%>",
 "acquirerMerchantID" : "<%=defaultMerchantId%>",
 "browserAcceptHeader" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
 "browserLanguage" : "en-US",
 "browserColorDepth" : "32",
 "browserScreenHeight" : "720",
 "browserScreenWidth" : "1280",
 "browserIP" : "<%=request.getRemoteAddr() %>",
 "browserTZ" : "120",
 "browserJavaEnabled" : true,
 "browserUserAgent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36",
 "acctNumber" : "<%=case10u %>",
 "cardExpiryDate" : "1812",
 "deviceChannel" : "02",
 "deviceRenderOptions" : {"interface" : "3", "uiType": ["1","2","3","4"]},
 "mcc" : "5544",
 "threeDSRequestorName" : "3DS Requestor 1",
 "threeDSRequestorID" : "100001",
 "threeDSRequestorURL" : "http://merchant.com",
 "merchantName" : "Merchant 1",
 "merchantCountryCode": "826",
 "messageCategory" : "01",
 "messageType" : "AReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm",
 "threeDSServerURL" : "<%=respUrl %>",
 "sdkMaxTimeout" : "06",
 "sdkTransID" : "<%=sdkTransId %>",
 "sdkAppID" : "7ffffffd-abf4-1ccc-ffff-000000000100",
 "sdkReferenceNumber" : "SDKEmuV1",
 "sdkEphemPubKey" : "eyJ4NWMiOlsiTUlJQ2x6Q0NBWCtnQXdJQkFnSUJBVEFOQmdrcWhraUc5dzBCQVFzRkFEQVBNUTB3Q3dZRFZRUUREQVIwWlhOME1CNFhEVEUzTURJeE16RXhNemN5TVZ",
 "purchaseAmount" : "123",
 "purchaseExponent" : "2",
 "purchaseCurrency" : "978",
 "purchaseDate" : "<%=puDate %>"
}'>2.0.1 - <%=case10u %> U 13 (not enrolled to attempt)
        </option>
        <option value='{
 "messageType" : "PReq",
 "messageVersion" : "2.0.1",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm"
}'>2.0.1 - PReq/PRes load card ranges data
        </option>
        <option value='{
 "messageType" : "PReq",
 "messageVersion" : "2.1.0",
 "threeDSServerTransID" : "<%= tdsTransId %>",
 "threeDSServerRefNumber" : "PosterForm"
}'>2.1.0 - PReq/PRes load card ranges data
        </option>
    </select>
    <br/>
    <script type="text/javascript">

        var loaded = false;
        var loadcount = 0;
        var blinked = 0;

        function setTemplate(template) {
            document.getElementById("json").value = template;
        }

        function getRReqs() {
            blinked = 0;
            if (loadcount < 5 && loaded == false) {
                var url = "<%=reqUrl %>";
                loadServerMsg(url, "serverMsg");
                loadcount++;
                var timer = setTimeout("getRReqs();", 5000);
            }

        }


        function blink() {
            if (blinked < 6) {
                var elem = document.getElementById("serverMsg");
                if (blinked % 2 == 0) {
                    elem.style.color = "red";
                } else {
                    elem.style.color = "black";
                }
                blinked++;
                var timer = setTimeout("blink();", 350);
            }

        }

        function loadServerMsg(url, elid) {
            var elem = document.getElementById(elid);
            loaded = false;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            xmlhttp.onreadystatechange = function () {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var t = new String(xmlhttp.responseText);
                    if (t.trim().length > 5) {
                        elem.innerHTML = "Server GOT " + xmlhttp.status + "<br/><pre>" + t + "</pre>";
                        blink();
                        loaded = true;
                    } else {
                        elem.innerHTML = "";
                    }
                } else if (xmlhttp.readyState == 4) {
                    elem.innerHTML = "Error code " + xmlhttp.status;
                }
            }

            xmlhttp.open("POST", url, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("cmd=getrreq");
        }

        getRReqs();

        function clearres() {
            var elem = document.getElementById("mresult");
            if (elem != null) {
                elem.innerHTML = "Sending new request..";
            }

        }

    </script>

    JSON: <br/>
    <textarea cols="80" rows="15" name="json" id="json"><%=json %></textarea>
    <%
        int count = 0;
        if (request.getParameter("dsrequest") != null) {
            long st = System.currentTimeMillis();
            System.out.println("Service url " + serviceUrlServerSide);
            int repeat = Misc.parseInt(request.getParameter("repeat"));

            try {

                com.modirum.ds.utils.http.HttpsJSSEClient client = new com.modirum.ds.utils.http.HttpsJSSEClient();
                //PrivateKey clientKey, Certificate[] clientCert, KeyStore truststore
                java.security.KeyStore truststore = null;
                java.security.KeyStore keystore = null;
                if (keyStore != null) {
                    String rp = this.getServletContext().getRealPath(keyStore);
                    if (rp == null || keyStore.startsWith("/")) {
                        rp = keyStore;
                    }
                    log.info("SSL init keystore=" + keyStore + " (" + keyStoreType + ")=" + rp);
                    keystore = Helper.loadKeyStore(rp, keyStorePass, keyStoreType);
                }
                if (trustStore != null) {
                    String rp = this.getServletContext().getRealPath(trustStore);
                    if (rp == null || trustStore.startsWith("/")) {
                        rp = trustStore;
                    }
                    log.info("SSL init truststore=" + trustStore + " (" + keyStoreType + ")=" + rp);
                    truststore = Helper.loadKeyStore(rp, trustStorePass, keyStoreType);
                }

                String kalg = javax.net.ssl.KeyManagerFactory.getDefaultAlgorithm();

                boolean preq = (json != null && json.indexOf("\"PReq\"") > 0);
                java.security.cert.Certificate[] clientChain = keystore != null ? keystore.getCertificateChain(keyalias) : null;

                log.info("SSL init key=" + keyalias + " ks=" + keystore + " chain length=" + (clientChain != null ? "" + clientChain.length : "is null"));
                if (clientChain != null && clientChain.length > 0) {

                    clientCertSDN = "" + ((X509Certificate) clientChain[0]).getSubjectDN();
                }

                client.init(null, kalg, "TLSv1.2",
                        "true", keystore != null ? (PrivateKey) keystore.getKey(keyalias, keyStorePass.toCharArray()) : null,
                        clientChain,
                        truststore, 2000, preq ? 15000 : 6000, false);

                StringBuilder sb = new StringBuilder(1024);
                Long respTime = 0L;
                long respTimeTotal = 0L;

                while (count < 1 || count < repeat) {
                    count++;
                    log.info("Sending msg " + count);
                    byte[] req = json.getBytes(StandardCharsets.UTF_8);
                    long s = System.currentTimeMillis();
                    byte[] resp = null;
                    if ("PUT".equals(request.getParameter("httpmethod"))) {
                        resp = client.send("PUT", serviceUrlServerSide, "application/json; charset=\"utf-8\"", req, 0, req.length, null, null);
                    }
                    if ("GET".equals(request.getParameter("httpmethod"))) {
                        resp = client.send("GET", serviceUrlServerSide, "application/json; charset=\"utf-8\"", req, 0, req.length, null, null);
                    } else {
                        resp = client.post(serviceUrlServerSide, "application/json; charset=\"utf-8\"", req);
                    }
                    long e = System.currentTimeMillis();
                    respTime = e - s;
                    respTimeTotal += respTime;
                    log.info("Got resp " + count + " " + (resp != null ? resp.length : 0) + " bytes in " + respTime + "ms");
                    sb.append(resp.length > 100000 ?
                            new String(resp, 0, 5000, "UTF-8") + "..." + resp.length + "... " +
                                    new String(resp, resp.length - 5000, 5000, "UTF-8")
                            : new String(resp, "UTF-8"));
                    sb.append("\n");
    %><%=" " %><%
            }
            request.setAttribute("jsonResponse", sb.toString());
            request.setAttribute("responseTimeAvg", (respTimeTotal / count));

            long et = System.currentTimeMillis();
            request.setAttribute("timeTotal", (et - st));

        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("Error", "Count=" + count + ": " + e + " keystore=" + keyStore + " keyalias=" + keyalias + " SDN=" + clientCertSDN);
        }
    }
%><br/>

    <select name="httpmethod">
        <option value="POST">POST</option>
        <option value="PUT">PUT</option>
    </select>

    <select name="repeat">
        <option value="1">Single</option>
        <option value="10">10 Times</option>
        <option value="100">100 Times</option>
        <option value="250">250 Times</option>
        <option value="500">500 Warn Dont kill DS</option>
    </select>

    <button type="submit" name="dsrequest" onclick="clearres();">Send JSON to DS</button>
    <br/>
    DS result:<span id="mresult"> <%=Misc.defaultString((request.getAttribute("Error"))) %><br/>
	Total time <%=Misc.defaultString((request.getAttribute("timeTotal"))) %>ms<br/>
	Count <%=count %> Avg req to response in <%=Misc.defaultString((request.getAttribute("responseTimeAvg"))) %>ms<br/>
	Reply JSON: <pre><%=Misc.defaultString(((String) request.getAttribute("jsonResponse"))) %></pre>
</span>
    <br/>

    <%
        String casesErrr = null;
        com.modirum.ds.tests.EmbedTestFilter.Cases cases =
                (com.modirum.ds.tests.EmbedTestFilter.Cases) this.getServletContext().getAttribute("EmbedTestFilter.cases");

        String activeCasesXML = "";
        String casesXML = request.getParameter("casesXML");
        if (Misc.isNotNullOrEmpty(casesXML) && request.getParameter("saveCases") != null) {
            try {
                cases = com.modirum.ds.tests.EmbedTestFilter.jaxbHelp.unmarshal(new java.io.ByteArrayInputStream(casesXML.getBytes("UTf-8")));
                this.getServletContext().setAttribute("EmbedTestFilter.cases", cases);
            } catch (Exception ee) {
                log.error("Error parsing XML cases", ee);
                casesErrr = "Error parsing XML cases " + ee;
            }
        }

        if (cases != null) {
            activeCasesXML = new String(com.modirum.ds.tests.EmbedTestFilter.jaxbHelp.marshal(cases), "UTF-8");
        }
        if (Misc.isNullOrEmpty(casesXML)) {
            casesXML = Misc.defaultString(activeCasesXML);
        }


    %>
    <br/>
    ACS or ACSEmulator injected transform cases:<br/>
    Usage and XML Format see <a target="_blank" href="javadocs/com/modirum/ds/tests/EmbedTestFilter.html">here</a><br/>
    <% if (casesErrr != null) { %><span style="color: red;">Error parsing cases xml: <%=casesErrr %></span><br/><% } %>

    <textarea cols="80" rows="25" name="casesXML" id="casesXML"><%=Misc.encodeHTMLForForm(casesXML) %></textarea>
    <br/>
    Current active cases:<br/>
    <pre>
<%=Misc.encodeHTMLForForm(casesXML) %>
</pre>

    <br/>
    <button type="submit" name="saveCases">Save cases</button>
    <br/>
</form>
</body>
</html>
<%! String getCardFromCase(String casex) {
    String[] pair = Misc.splitTwo(casex, ":");
    if (pair != null && pair.length > 0) {
        casex = pair[0];
    }

    return casex;
}


%>
