package com.modirum.generic.scripts.threedsv2;

import com.modirum.generic.utility.ElemFormatsTestCaseGenerator;
import com.modirum.generic.utility.TestSummary;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * TestAReq_ElemFormats_Browser - This class contains element format tests for AReq Json messages in the Browser - Payment flow.
 */
public class TestAReq_ElemFormats_Browser extends DSAReqTestSuite {
    private ElemFormatsTestCaseGenerator m_tcGen;

    protected void initTestClassFields() {
        m_messageType = "AReq";
        m_deviceChannel = "02"; // BRW (02)
        m_messageCategory = "01"; // Payment (01)
    }

    protected void initElemFormatInfo() {
        m_tcGen = new ElemFormatsTestCaseGenerator();
        m_tcGen.initElemFormatInfo();
    }

    protected void initTestCaseSummary() {
        m_testSummary = new TestSummary(
                new String[]{"TEST FORMAT ACTION", "LENGTH < FIXED SIZE", "LENGTH > FIXED SIZE", "LENGTH = MIN", "LENGTH < MIN", "LENGTH = MAX", "LENGTH > MAX", "VALID VALUE (VAL)", "VALID VALUE (RES)", "INVALID VALUE (VAL)", "INVALID VALUE (RES)", "SPACE AT START", "SPACE AT MID", "SPACE AT END", "NUMBERS", "ALPHA", "SYMBOLS", "CYRILLIC", "CHINESE", "GREEK"});
    }

    /***************************************************
     * Start of Test Cases
     ***************************************************/
    @Test(enabled = true, description = "Test element format verification for browserAcceptHeader", dataProvider = "ElemFormats")
    public void testElemFormat_browserAcceptHeader(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserAcceptHeader" + testName, "browserAcceptHeader", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserIP", dataProvider = "ElemFormats")
    public void testElemFormat_browserIP(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserIP" + testName, "browserIP", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserJavaEnabled", dataProvider = "ElemFormats")
    public void testElemFormat_browserJavaEnabled(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserJavaEnabled" + testName, "browserJavaEnabled", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserLanguage", dataProvider = "ElemFormats")
    public void testElemFormat_browserLanguage(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserLanguage" + testName, "browserLanguage", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserColorDepth", dataProvider = "ElemFormats")
    public void testElemFormat_browserColorDepth(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserColorDepth" + testName, "browserColorDepth", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserScreenHeight", dataProvider = "ElemFormats")
    public void testElemFormat_browserScreenHeight(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserScreenHeight" + testName, "browserScreenHeight", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserScreenWidth", dataProvider = "ElemFormats")
    public void testElemFormat_browserScreenWidth(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserScreenWidth" + testName, "browserScreenWidth", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserTZ", dataProvider = "ElemFormats")
    public void testElemFormat_browserTZ(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserTZ" + testName, "browserTZ", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for browserUserAgent", dataProvider = "ElemFormats")
    public void testElemFormat_browserUserAgent(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_browserUserAgent" + testName, "browserUserAgent", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    /***************************************************
     * Data Provider
     ***************************************************/
    @DataProvider(name = "ElemFormats")
    protected Iterator<Object[]> elemFormatsDP(Method m) {
        String[] parseMethodName = m.getName().split("_");
        if (parseMethodName.length == 2 && parseMethodName[0].equals("testElemFormat")) {
            return m_tcGen.getElemFormatDataProvider(parseMethodName[1], getTestGroupJson(parseMethodName[1]));
        }
        return null;
    }
}
