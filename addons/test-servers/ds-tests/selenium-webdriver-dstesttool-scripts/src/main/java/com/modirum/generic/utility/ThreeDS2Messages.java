package com.modirum.generic.utility;

import org.testng.asserts.SoftAssert;

import java.util.List;

public class ThreeDS2Messages {

    /**
     * This function removes the err id code from the input string
     **/
    public static String removeErrIdCode(String origMsg) {
        String newMsg = origMsg;
        String errIdString = "err id=";
        int startIndex = origMsg.indexOf(errIdString);
        startIndex = startIndex + errIdString.length();

        if (startIndex >= errIdString.length() && (origMsg.length() >= (startIndex + 13))) {
            String errId = origMsg.substring(startIndex, startIndex + 13);
            newMsg = origMsg.replace(errId, "");
            System.out.println(" Replaced error ID: " + errId);
        }
        return newMsg;
    }

    public static void verifyErrorDetail(SoftAssert softAssert, String val, List<String> errorDetail) {
        String actualErrDetail = removeErrIdCode(val);
        for (String errField : errorDetail) {
            softAssert.assertTrue(actualErrDetail.contains(errField),
                                  "\nFAILED TEST: incorrect errorDetail. [" + errField + "] not found.");
        }
    }
}
