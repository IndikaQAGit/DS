package com.modirum.generic.utility;

public class XmlData {
    private String xmlMsg;

    public XmlData(String xml) {
        xmlMsg = xml;
    }

    public String getXmlMsg() {
        return xmlMsg;
    }

    public boolean containsField(String field) {
        int start, end;
        start = xmlMsg.indexOf("<" + field + ">");
        end = xmlMsg.indexOf("</" + field + ">");
        if (start >= 0 && end > start) {
            return true;
        }
        if (xmlMsg.indexOf("<" + field + "/>") > 0) {
            return true;
        }
        return false;
    }

    public String getFieldValue(String field) {
        int start, end;
        start = xmlMsg.indexOf("<" + field + ">");
        end = xmlMsg.indexOf("</" + field + ">");
        if (start >= 0 && end > start) {
            return xmlMsg.substring(start + field.length() + 2, end);

        }
        return "";
    }

    public void replaceFieldValue(String field, String value) {
        String temp;
        int start, end;
        start = xmlMsg.indexOf("<" + field + ">");
        end = xmlMsg.indexOf("</" + field + ">");
        if (start >= 0 && end > start) {
            temp = xmlMsg.substring(0, start + field.length() + 2) + value + xmlMsg.substring(end);
            xmlMsg = temp;
        }
    }

    public void removeField(String field) {
        String temp;
        int start, end;
        start = xmlMsg.indexOf("<" + field + ">");
        end = xmlMsg.indexOf("</" + field + ">");
        if (start >= 0 && end > start) {
            temp = xmlMsg.substring(0, start) + xmlMsg.substring(end + field.length() + 3).trim();
            xmlMsg = temp;
        }
    }

    public void addField(String parent, String field, String value) {
        String temp;
        int start, end;
        start = xmlMsg.indexOf("<" + parent + ">");
        end = xmlMsg.indexOf("</" + parent + ">");
        if (start >= 0 && end > start) {
            temp = xmlMsg.substring(0, end) + "<" + field + ">" + value + "</" + field + ">\n" + xmlMsg.substring(end);
            xmlMsg = temp;
        }
    }

    public boolean loadFromFile(String fullpath) {
        boolean bLoaded = false;
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(fullpath));
            StringBuilder readLines = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                readLines.append(line);
            }
            xmlMsg = readLines.toString();
            reader.close();
            bLoaded = true;
        } catch (FileNotFoundException fe) {
            System.out.println("[ERROR] file " + fullpath + " not found.");
            fe.printStackTrace();
        } catch (UnsupportedEncodingException ue) {
            System.out.println("[ERROR] file " + fullpath + " unsupported encoding.");
            ue.printStackTrace();
        } catch (IOException ie) {
            System.out.println("[ERROR] file " + fullpath + " IOException.");
            ie.printStackTrace();
        } finally {
            return bLoaded;
        }
    }

}
