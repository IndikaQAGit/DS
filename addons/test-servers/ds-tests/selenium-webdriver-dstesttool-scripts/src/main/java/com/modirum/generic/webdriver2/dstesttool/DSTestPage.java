package com.modirum.generic.webdriver2.dstesttool;

import com.modirum.generic.utility.JsonData;
import com.modirum.generic.utility.SelDriver;
import com.modirum.generic.webdriver2.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * DSTestPage - Page Object for the DS Test Tool page
 */
public class DSTestPage extends AbstractPage {

    //********************************************************************************
    // DS Test Page Locators
    private static final By LOC_TEMPLATE_SEL = By.id("template");
    private static final By LOC_INPUTJSON_TXT = By.id("json");
    private static final By LOC_SEND_BTN = By.xpath("//button[@name='dsrequest']");
    private static final By LOC_RESULT_LBL = By.xpath("//span[@id='mresult']/pre");
    private static final By LOC_STARTOVER_LNK = By.linkText("Start over");
    //********************************************************************************

    private final String title = "DS JSON test";
    private final By byFindElement = LOC_INPUTJSON_TXT;

    /**
     * Constructor
     *
     * @param d - Selenium Driver instance
     */
    public DSTestPage(SelDriver d) {
        super(d);

        try {
            WebDriverWait wait = new WebDriverWait(driver.getDriver(), 15);
            wait.until(ExpectedConditions.presenceOfElementLocated(byFindElement));
        } catch (TimeoutException e) {
            if (!getTitle().equals(title)) {
                throw new IllegalStateException(
                        "This is not the DS Test Tool Page! This is: [" + getTitle() + "] at " + getUrl());
            }
        }
    }

    /**
     * Select the template for the input Json
     *
     * @param template - Exact string of the template to select
     */
    public void selectTemplate(String template) {
        driver.selectSingleOption(LOC_TEMPLATE_SEL, template);
    }

    /**
     * Get the JsonData in the input text box
     *
     * @return
     */
    public JsonData getInputJsonData() {
        String data = driver.getValue(LOC_INPUTJSON_TXT);
        //System.out.println("[DEBUG] Json data:" + data);
        JsonData json = new JsonData();
        json.convertToJson(data);

        return json;
    }

    /**
     * Set the input Json as a JsonData object
     *
     * @param json - Input as a JsonData object
     */
    public void setInputJsonData(JsonData json) {
        String data = json.convertToString();
        //System.out.println(data);
        driver.setInputField(LOC_INPUTJSON_TXT, data);
    }

    /**
     * Set the input Json as a String
     *
     * @param data - Input as a String
     */
    public void setInputJsonData(String data) {
        driver.setInputField(LOC_INPUTJSON_TXT, data);
    }

    /**
     * Sends the Json in the input text box to DS by clicking the Send button
     */
    public void sendJsonToDS() {
        driver.click(LOC_SEND_BTN);
    }

    /**
     * Get the Json Response of DS
     *
     * @return JsonData object containing the response from DS
     */
    public JsonData getJsonResult() {
        String result = driver.getValue(LOC_RESULT_LBL);
        //System.out.println("[DEBUG] Json data:" + result);
        JsonData json = new JsonData();
        json.convertToJson(result);

        return json;
    }

    /**
     * Reset the DS Test Tool page
     */
    public void resetPage() {
        driver.click(LOC_STARTOVER_LNK);
    }
}
