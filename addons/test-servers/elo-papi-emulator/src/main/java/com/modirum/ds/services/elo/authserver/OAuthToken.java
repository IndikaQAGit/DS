package com.modirum.ds.services.elo.authserver;

public class OAuthToken {
    private String accessToken;
    private String refreshToken;

    public String getAccessToken() {
        return accessToken;
    }

    public OAuthToken setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuthToken setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }
}
