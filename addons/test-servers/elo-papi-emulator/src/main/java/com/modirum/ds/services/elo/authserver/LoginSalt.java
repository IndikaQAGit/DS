package com.modirum.ds.services.elo.authserver;

public class LoginSalt {
    private String username;
    private String salt;

    public String getUsername() {
        return username;
    }

    public LoginSalt setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getSalt() {
        return salt;
    }

    public LoginSalt setSalt(String salt) {
        this.salt = salt;
        return this;
    }
}
