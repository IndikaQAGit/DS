package com.modirum.ds.services.elo.papi;

import com.modirum.ds.services.elo.papi.test.support.RunWithDB;
import com.modirum.ds.services.elo.papi.test.support.TestEmbeddedDbUtil;
import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.util.TimeZone;

@RunWithDB
public class EloPapiToolsSetupTest {
    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../../../db-scripts/ds-schema-mysql-a.sql");
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../../../db-scripts/mngr-texts.sql");
    }

    @AfterAll
    public static void afterAll() throws Exception {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    public void testApplicationContextSetup() {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:test-application-context.xml");
        Assertions.assertNotNull(context.getBean(EloPapiTools.class));
        BasicDataSource dsDataSource = context.getBean(BasicDataSource.class, "dsDataSource");
        Assertions.assertNotNull(dsDataSource);
        Assertions.assertEquals("org.h2.Driver", dsDataSource.getDriverClassName());
        Assertions.assertEquals("jdbc:h2:mem:test;SCHEMA=PUBLIC;MODE=MySQL", dsDataSource.getUrl());
        Assertions.assertEquals("sa", dsDataSource.getUsername());
        Assertions.assertEquals("password", dsDataSource.getPassword());
        Assertions.assertEquals("SELECT 1", dsDataSource.getValidationQuery());
    }

}
