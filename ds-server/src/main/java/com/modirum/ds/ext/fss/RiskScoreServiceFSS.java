/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 6. oct 2018
 *
 */
package com.modirum.ds.ext.fss;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.model.ProductInfo.Extensions;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService.JsonAndDucplicates;
import com.modirum.ds.services.MessageService.PublicBOS;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.web.services.AbstractDirectoryService.Results;
import com.modirum.ds.web.services.DirectoryService210;
import com.modirum.ds.web.services.ExtensionService;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.http.SSLSocketHelper;
import com.modirum.ds.utils.CachedObject;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.net.ssl.KeyManagerFactory;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implementation of DS/FSS connectivity.
 */
public class RiskScoreServiceFSS implements ExtensionService {
    static Logger log = LoggerFactory.getLogger(RiskScoreServiceFSS.class);

    public final static int cDefaultConnectTimeout = 2500; // does not need to be short.. as wait is asynch
    public final static int cDefaultResponseTimeout = 5000; // does not need to be short.. as wait is asynch
    public final static int cDefaultFSSMaxWait = 3000;
    public final static int cMaxTimeoutMultipler = 5;
    public final static int cMaxDbStoreTxTime = 7;
    public final static int cRetryUpdateInterVal = 30;

    enum FSSAcqOptions {
        FSSA, // fss on always
        FSSC, // fss on challenge case only
        FSSN, // fss off
    }

    enum FSSResultsOptions {
        // results
        FSSE,    // fss was on but failed to deliver
        FSSY, // fss was on band delivered
        FSSD,    // fss requested but fss was disabled
    }


    enum FSSIssOptions {
        FSSP, // fss on
        FSSN //fss off
    }

    enum AReqPostState {
        NotPosted(0), InPosting(1), InRetry(2), Posted(3), Failed(4);
        int value;

        AReqPostState(int value) {
            this.value = value;
        }

    }

    Map<Long, CachedObject<AtomicInteger>> stateMap = new java.util.TreeMap<>();

    /**
     * post to data to fss for scoring and create extensions
     */
    @Override
    public void createAReqExtensions(TDSRecord record, Results results, Merchant m, Acquirer acq, CardRange bin, Issuer is, List<ACSProfile> acsList, TDSMessage aReq, DirectoryService210 dss, WebContext wctx) {

        if (TDSModel.DeviceChannel.cChannelBrow02.value().equals(aReq.getDeviceChannel()) &&
            dss.getDirectory().getTdsmRelayService() != null) {
            // 3ds method relay and original acs has not 3ds method url set
            // then fix this here to U in order not to confuse original acs
            if (acsList != null && acsList.size() > 0) {
                ACSProfile acpFirst = acsList.get(0);
                if (Misc.isNullOrEmpty(acpFirst.getThreeDSMethodURL())) {
                    aReq.setThreeDSCompInd(TDSModel.XthreeDSCompInd.U.value());
                }
            }
        }

        boolean enabled = !"false".equals(wctx.getStringSetting(DsSetting.FSS_ENABLED.getKey()));
        if (enabled) // start fss post/calculation regardless options always if fss enabled..
        {
            String mOptions = m != null ? m.getOptions() : wctx.getStringSetting(DsSetting.FSS_DEFAULT_MER_OPTIONS.getKey());
            if (mOptions != null &&
                (mOptions.contains(FSSAcqOptions.FSSA.name()) || mOptions.contains(FSSAcqOptions.FSSC.name()))) {
                results.waitingForAResExtensions = true;
            }
            results.extState = new AtomicInteger(AReqPostState.NotPosted.value);
            CachedObject<AtomicInteger> co = new CachedObject<>();
            co.setLoaded(System.currentTimeMillis());
            co.setObject(results.extState);
            synchronized (stateMap) {
                stateMap.put(record.getId(), co);
            }
            calculateRiskAsync(record, results, m, acq, bin, is, aReq, dss, wctx);
        }

        if (bin.getOptions() != null && bin.getOptions().contains(FSSIssOptions.FSSP.name())) {
            if (!enabled) {
                record.setIssuerOptions(FSSResultsOptions.FSSD.name());
            } else {
                results.waitingForAReqExtensions = true;
                if (results.fssExtensions == null) {
                    int mw = wctx.getIntSetting(DsSetting.FSS_AREQ_MAXWAIT.getKey());
                    synchronized (results) {
                        try {
                            results.wait(mw > 0 && mw < 3 * cDefaultFSSMaxWait ? mw : cDefaultFSSMaxWait);
                        } catch (InterruptedException e) {
                        }
                    }
                }

                if (results.fssExtensions != null) {
                    results.areqExtensions = results.fssExtensions;
                    record.setIssuerOptions(FSSResultsOptions.FSSY.name());
                } else {
                    log.warn("DS TX " + record.getId() + "/" + record.getDSTransID() +
                             " Issuer requested fss for case: " + bin.getOptions() +
                             " but fss failed to deliver (in time)");
                    results.waitingForAReqExtensions = false;
                    record.setIssuerOptions(FSSResultsOptions.FSSE.name());
                }
            }
        }
    }

    /**
     * for update of authentication status and extensions for merchant if needed
     */
    @Override
    public void createAResExtensions(TDSRecord record, Results results, Merchant m, TDSMessage aRes, Long maxWait, DirectoryService210 dss, WebContext wctx) {
        boolean enabled = !"false".equals(wctx.getStringSetting(DsSetting.FSS_ENABLED.getKey()));
        if (enabled && !TDSModel.XtransStatus.C.value().equals(aRes.getTransStatus())) {
            statusUpdate(record, results, aRes, dss, wctx);
        }

        String mOptions = m != null ? m.getOptions() : wctx.getStringSetting(DsSetting.FSS_DEFAULT_MER_OPTIONS.getKey());
        if (mOptions != null && (mOptions.contains(FSSAcqOptions.FSSA.name()) ||
                                 (mOptions.contains(FSSAcqOptions.FSSC.name()) &&
                                  TDSModel.XtransStatus.C.value().equals(aRes.getTransStatus())))) {

            if (!enabled) {
                record.setAcquirerOptions(FSSResultsOptions.FSSD.name());
                return;
            }

            if (results.fssExtensions == null) {
                synchronized (results) {
                    try {
                        results.wait(maxWait != null && maxWait > 0 ? maxWait : cDefaultFSSMaxWait);
                    } catch (InterruptedException e) {
                    }
                }
            }
            if (results.fssExtensions != null) {
                results.aresExtensions = results.fssExtensions;
                record.setAcquirerOptions(FSSResultsOptions.FSSY.name());
            } else {
                log.warn("DS TX " + record.getId() + "/" + record.getDsRef() +
                         " Merchant or default requested fss for case: " + mOptions +
                         " but fss failed to deliver (in time)");
                record.setAcquirerOptions(FSSResultsOptions.FSSE.name());
            }
        }
    }

    /**
     * for update of authentication status, no extensions created at this point
     */
    @Override
    public void createRReqExtensions(TDSRecord record, Results results, Merchant mer, TDSMessage rReq, Long maxWait, DirectoryService210 dss, WebContext wctx) {
        boolean enabled = !"false".equals(wctx.getStringSetting(DsSetting.FSS_ENABLED.getKey()));
        if (enabled) {
            CachedObject<AtomicInteger> co = stateMap.get(record.getId());
            if (co != null && co.getObject() != null) {
                results.extState = co.getObject();
            } else {
                results.extState = new AtomicInteger(AReqPostState.Posted.value);
            }

            statusUpdate(record, results, rReq, dss, wctx);
        }
    }

    void calculateRiskAsync(TDSRecord record, Results results, Merchant mer, Acquirer acq, CardRange bin, Issuer is, TDSMessage aReq, DirectoryService210 dss, WebContext wctx) {
        //CompletableFuture.runAsync(()-> calculateRisk(record, results, acq, bin,  is, aReq, dss,  webContext) );
        log.info("calculateRiskAsync(submit) calculateRisk(" + record.getId() + "," + aReq.getMessageType() + ")");
        dss.getScheduledExecutorService().submit(() -> calculateRisk(record, results, acq, bin, is, aReq, dss, wctx));
    }

    void calculateRisk(TDSRecord record, Results results, Acquirer acq, CardRange bin, Issuer is, TDSMessage aReq, DirectoryService210 dss, WebContext wctx) {
        if (log.isDebugEnabled()) {
            log.debug("calculateRisk, starting..");
        }
        JsonObjectBuilder job = null;
        try {
            job = dss.createFilteredJson(aReq, aReq.getJsonObject(), null, record, null);
            job.add("dsReferenceNumber", dss.getDSReferenceNumber(wctx));
            job.add("dsTransID", record.getDSTransID());

            if (TDSModel.DeviceChannel.cChannelBrow02.value().equals(aReq.getDeviceChannel()) &&
                dss.getDirectory().getTdsmRelayService() != null) {
                CachedObject<ThreeDSMethodRelayService.DeviceData> co = dss.getDirectory().getTdsmRelayService().getThreeDSMethodDataBy3DSId(
                        aReq.getTdsServerTransID());
                if (co != null) {
                    ThreeDSMethodRelayService.DeviceData dd = co.getObject();
                    if ((dd == null || dd.scriptData == null) && co.getLoaded() > System.currentTimeMillis() - 1500) {
                        synchronized (co) {
                            try {
                                co.wait(100);
                            } catch (Exception dc) {
                            }
                        }
                    }
                    dd = co.getObject();
                    if (dd.scriptData == null) {
                        log.warn("DD prepared but script completed, not available for " + aReq.getTdsServerTransID() +
                                 " TDS " + record.getId());
                    } else {
                        if (dd.version != null) {
                            job.add("tdsmScriptVersion", dd.version);
                        }

                        job.add("tdsmScriptData", Json.createValue(dd.scriptData.toString()));
                    }
                    if (dd.cookie != null) {
                        job.add("tdsmCookies", dd.cookie.toString());
                    }
                    if (dd.headers != null) {
                        job.add("tdsmHeaders", dd.headers.toString());
                    }

                    if (Misc.isNotNullOrEmpty(dd.TDS2_Navigator_javaEnabled)) {
                        job.add("browserJavaEnabled3DSM", "true".equals(dd.TDS2_Navigator_javaEnabled));
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Navigator_jsEnabled)) {
                        job.add("browserJSEnabled3DSM", "true".equals(dd.TDS2_Navigator_jsEnabled));
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Navigator_language)) {
                        job.add("browserLanguage3DSM", dd.TDS2_Navigator_language);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Screen_colorDepth)) {
                        job.add("browserColorDepth3DSM", dd.TDS2_Screen_colorDepth);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Screen_height)) {
                        job.add("browserScreenHeight3DSM", dd.TDS2_Screen_height);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Screen_pixelDepth)) {
                        job.add("browserColorDepth3DSM", dd.TDS2_Screen_pixelDepth);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Screen_width)) {
                        job.add("browserScreenWidth3DSM", dd.TDS2_Screen_width);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_TimezoneOffset)) {
                        job.add("browserTZ3DSM", dd.TDS2_TimezoneOffset);
                    }
                    if (Misc.isNotNullOrEmpty(dd.ipAddress)) {
                        job.add("browserIP3DSM", dd.ipAddress);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Navigator_Accept)) {
                        job.add("browserAcceptHeader3DSM", dd.TDS2_Navigator_Accept);
                    }
                    if (Misc.isNotNullOrEmpty(dd.TDS2_Navigator_UA)) {
                        job.add("browserUserAgent3DSM", dd.TDS2_Navigator_UA);
                    }
                } else {
                    log.warn("DD not found at all for " + aReq.getTdsServerTransID() + " TDS " + record.getId());
                }
            } else if (aReq.getDeviceInfo() != null) // mobile device case
            {
                // shall not be present already due filter in createFilteredJson
                //	job.remove("sdkEncData");
                job.add("deviceInfo", aReq.getDeviceInfo());
            }

            if (!"false".equals(wctx.getStringSetting(DsSetting.FSS_PAN_HASH.getKey()))) {
                job.remove("acctNumber");
                try {
// TODO add hmac to FSS using hsm engine
//                    job.add("acctNumberHash", dss.getCryptoService().createPanHMAC(aReq.getAcctNumber(),
//                                                                                   ServiceLocator.getInstance().getKeyService().getHMACKey(
//                                                                                           KeyService.KeyAlias.hmacKeyExt)));
                } catch (Exception e) {
                    log.error("Failed to caclulate panhash of TDS " + record.getId(), e);
                    return;
                }
                job.add("acctNumberBin", Misc.cut(aReq.getAcctNumber(), 6));
                job.add("acctNumberLast", Misc.cutLeft(aReq.getAcctNumber(), 4));
            }

            JsonObject fssReqJson = job.build();
            PublicBOS fssReq = dss.getMessageService().toJSONBos(fssReqJson);
            sendMsg(record, results, fssReq, aReq, 0, dss, wctx);
        } catch (Throwable e) {
            log.error("Unexpected RiskScoreServiceFSS error", e);
            if (job != null) {
                log.info("JsonObjectBuilder=" + job + " " + job.getClass().getName() + " origin: " +
                         Utils.classOrigin(job));
            }
        }
    } // calc


    /**
     * send authentication status update to fss on ARes (if not C) or on RReq
     *
     * @param record
     * @param results
     * @param aRes_rReq
     * @param dss
     * @param wctx
     */
    void statusUpdate(TDSRecord record, Results results, TDSMessage aRes_rReq, DirectoryService210 dss, WebContext wctx) {
        JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("dsReferenceNumber", dss.getDSReferenceNumber(wctx));
        job.add("dsTransID", record.getDSTransID());
        if (Misc.isNotNullOrEmpty(record.getACSTransID())) {
            job.add("acsTransID", record.getACSTransID());
        } else if (Misc.isNotNullOrEmpty(aRes_rReq.getAcsTransID())) {
            job.add("acsTransID", aRes_rReq.getAcsTransID());
        }
        job.add("messageType", aRes_rReq.getMessageType() == null ? "ARes" : aRes_rReq.getMessageType());

        if (Misc.isNotNullOrEmpty(aRes_rReq.getEci())) {
            job.add("eci", aRes_rReq.getEci());
        }
        if (Misc.isNotNullOrEmpty(aRes_rReq.getAuthenticationValue())) {
            job.add("authenticationValue", aRes_rReq.getAuthenticationValue());
        }

        String tt = aRes_rReq.getTransStatus();

        if (tt == null) {
            tt = "E";
        }
        if ("RReq".equals(aRes_rReq.getMessageType())) {
            tt = "C" + tt;
        } else if ("ARes".equals(aRes_rReq.getMessageType())) {
            tt = "Y" + tt;
        }

        job.add("transStatus", tt);
        JsonObject fssReqJson = job.build();
        try {
            PublicBOS fssReq = dss.getMessageService().toJSONBos(fssReqJson);
            // on RReq assume posted
            if (results.extState != null && results.extState.intValue() == AReqPostState.Posted.value) {
                dss.getScheduledExecutorService().submit(
                        () -> sendMsg(record, results, fssReq, aRes_rReq, 0, dss, wctx));
            } else if (results.extState != null && results.extState.intValue() < AReqPostState.Failed.value) {
                int retry = getRetryInterval(wctx);
                log.info("Record " + record.getId() + " AReq post not complete schedule delayed status update");
                dss.getScheduledExecutorService().schedule(
                        () -> sendMsg(record, results, fssReq, aRes_rReq, 0, dss, wctx), retry, TimeUnit.SECONDS);
            } else {
                log.warn("Record " + record.getId() + " AReq post status " +
                         (results.extState != null ? results.extState.intValue() : null) +
                         ", status update post not applicable");
            }

        } catch (Exception e) {
            log.error("Unexpected RiskScoreServiceFS error", e);
        }
    } // calc

    void sendMsg(TDSRecord record, Results results, PublicBOS fssReq, TDSMessage aReq_aRes_rReq, int failCount, DirectoryService210 dss, WebContext wctx) {
        // AReq not yet posted reschedule update as well or if final failed update not applicable
        boolean update = false;
        if (!"AReq".equals(aReq_aRes_rReq.getMessageType())) {
            //"ARes" "RReq" or Erro
            update = true;
            if (results.extState != null && results.extState.intValue() < AReqPostState.Posted.value) {
                int fcf = failCount;
                int retry = getRetryInterval(wctx);
                log.info("Record " + record.getId() + " AReq post not complete reschedule status update");
                dss.getScheduledExecutorService().schedule(
                        () -> sendMsg(record, results, fssReq, aReq_aRes_rReq, fcf, dss, wctx), retry,
                        TimeUnit.SECONDS);
                return;
            } else if (results.extState != null && results.extState.intValue() == AReqPostState.Failed.value) {
                log.warn("Record " + record.getId() + " AReq post status final failed " + results.extState.get() +
                         ", status update post not applicable");
                return;
            }
        }
        if ("AReq".equals(aReq_aRes_rReq.getMessageType())) {
            if (results.extState != null) {
                results.extState.set(AReqPostState.InPosting.value);
            } else {
                results.extState = new AtomicInteger(AReqPostState.InPosting.value);
            }
        }

        long mStart = System.currentTimeMillis();
        boolean wantedAreq = results.waitingForAReqExtensions;
        boolean wantedAres = results.waitingForAResExtensions;

        String fssUrl = wctx.getStringSetting(DsSetting.FSS_SERVICE_URL.getKey());
        String fssAuthCertId = wctx.getStringSetting(DsSetting.FSS_CLIENT_CERTID.getKey());
        Long fssAuthCertIdL = null;
        if (Misc.isLong(fssAuthCertId)) {
            fssAuthCertIdL = Misc.parseLongBoxed(fssAuthCertId);
        }

        final String ckey = "fss kid: " + fssAuthCertIdL + " url: " + fssUrl;
        Map<String, String> resph = new java.util.HashMap<String, String>();
        HttpsJSSEClient client = dss.getDirectory().getHttpsJSSEClient(ckey);
        KeyService keyse = ServiceLocator.getInstance().getKeyService();
        boolean newcl = false;
        byte[] fssresp = null;
        try {

            int fssTimeout = wctx.getIntSetting(DsSetting.FSS_RESPONSE_TIMEOUT.getKey());
            if (fssTimeout < cDefaultResponseTimeout / 2 ||
                fssTimeout > cMaxTimeoutMultipler * cDefaultResponseTimeout) {
                fssTimeout = cDefaultResponseTimeout;
            }

            if (client == null) {
                client = new HttpsJSSEClient();
                newcl = true;
                String sslContext = wctx.getStringSetting(DsSetting.FSS_SSL_CLIENT_PROTO.getKey());
                if (Misc.isNullOrEmpty(sslContext)) {
                    sslContext = SSLSocketHelper.SSLProto.TLSv12;
                }
                String[] sslCiphers = Misc.split(wctx.getStringSetting(DsSetting.FSS_SSL_CLIENT_CIPHERS.getKey()), ",");
                if (Misc.isNotNullOrEmpty(sslCiphers)) {
                    client.setCiphers(sslCiphers);
                }

                // for testing only!!!
                String sslTrustAny = wctx.getStringSetting(DsSetting.FSS_SSL_CLIENT_TRUSTANY.getKey());
                String sslVerifyHostName = wctx.getStringSetting(DsSetting.FSS_SSL_CLIENT_VERIFY_HOSTNAME.getKey());

                PrivateKey pk = null;
                X509Certificate[] chain = null;
                KeyStore trustStore = null;

                if (fssUrl.toLowerCase().startsWith("https://")) {
                    trustStore = keyse.getAllTrustedRootsAsKeyStore();

                    if (fssAuthCertIdL != null) {
                        KeyData clientKeyAndCert = keyse.getPrivateKeyAndCertChainById(fssAuthCertIdL);
                        if (clientKeyAndCert != null) {
                            byte[] privateKeyBytes  = dss.getCryptoService().unwrapPrivateKey(clientKeyAndCert);
                            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(clientKeyAndCert.getHsmDeviceId());
                            pk = hsmDevice.toRSAprivateKey(privateKeyBytes);
                            String certChainPem = KeyService.parsePEMChain(clientKeyAndCert.getKeyData());
                            chain = KeyService.parseX509Certificates(certChainPem.getBytes(StandardCharsets.ISO_8859_1));
                        } else {
                            log.warn("Auth to FSS Client cert/key not found by id " + fssAuthCertIdL);
                        }
                    }
                }

                int connectTimeout = wctx.getIntSetting(DsSetting.FSS_CONNECT_TIMEOUT.getKey());
                if (connectTimeout < cDefaultConnectTimeout / 2 ||
                    connectTimeout > cMaxTimeoutMultipler * cDefaultConnectTimeout) {
                    connectTimeout = cDefaultConnectTimeout;
                }

                String kf = wctx.getStringSetting(DsSetting.SSL_CLIENT_KEYFACTORY.getKey());
                client.init(null, Misc.isNullOrEmpty(kf) ? KeyManagerFactory.getDefaultAlgorithm() : kf, sslContext,
                            "true".equals(sslTrustAny) ? "true" : "false", pk, chain, trustStore, connectTimeout,
                            fssTimeout, !"false".equals(sslVerifyHostName));
                dss.configureHttpClient(client, wctx);
            }


            log.info("Sending " + aReq_aRes_rReq.getMessageType() + " " + aReq_aRes_rReq.getTdsServerTransID() + "/" +
                     aReq_aRes_rReq.getDsTransID() + " to FSS " + ckey + ", client key " +
                     (client.isClientKeySet() ? "yes" : "no"));


            for (int a = 0; a < 2; a++) {
                fssresp = null;
                try {
                    client.setReadTimeout(failCount > 0 ? 2 * fssTimeout : fssTimeout);
                    fssresp = client.post(fssUrl, MessageService.CT_JSON_UTF8, fssReq.getBuf(), 0, fssReq.getCount(),
                                          null, resph);
                    break;
                } catch (Exception cfe) {
                    //immediate connect/ssl failed retry Seq 5.60 [Req 243]
                    if (dss.isConnectionFailedException(cfe) && a < 1) {
                        log.warn("First connection attempt to " + fssUrl + " failed, making next auto attempt");
                        continue;
                    } else {
                        throw cfe;
                    }
                }
            } // for

            if (newcl) {
                dss.getDirectory().putHttpsJSSEClient(ckey, client);
            }

            String rspCode = resph.get("HTTP_RESPONSE");
            String rspMsg = resph.get("HTTP_RESPONSE_MSG");
            String rspCt = resph.get("Content-Type");

            if (fssresp != null && fssresp.length > 0 && rspCt != null && rspCt.contains(MessageService.CT_JSON)) {
                JsonAndDucplicates jd = MessageService.parseJsonAndDups(fssresp);
                if (jd.json != null) {
                    String reqTxStatus = aReq_aRes_rReq.getTransStatus();
                    String mt = MessageService.getStrValue(jd.json, "messageType");
                    boolean hasext = jd.json.containsKey("messageExtension");
                    if ("AReq".equals(aReq_aRes_rReq.getMessageType()) && "FRes".equals(mt)) {
                        if (results.extState != null) {
                            results.extState.set(AReqPostState.Posted.value);
                        } else {
                            results.extState = new AtomicInteger(AReqPostState.Posted.value);
                        }
                    } else if (!"AReq".equals(aReq_aRes_rReq.getMessageType()) && "FRes".equals(mt)) {
                        synchronized (stateMap) {
                            stateMap.remove(record.getId());
                        }
                    }

                    if ("FRes".equals(mt) && (hasext || reqTxStatus != null)) {
                        Boolean inTimeAreq = null;
                        Boolean inTimeAres = null;
                        JsonArray jsa = null;
                        if (hasext) {
                            jsa = jd.json.getJsonArray("messageExtension");
                            if (jsa != null && (results.waitingForAReqExtensions || results.waitingForAResExtensions)) {
                                results.fssExtensions = jsa;
                                synchronized (results) {
                                    if (wantedAreq) {
                                        inTimeAreq = results.waitingForAReqExtensions;
                                    }
                                    if (wantedAres) {
                                        inTimeAres = results.waitingForAResExtensions;
                                    }

                                    results.notifyAll();
                                }

                            }
                        }
                        long mEnd = System.currentTimeMillis();
                        log.info("FRes received and proc in " + (mEnd - mStart) + " ms, for " + record.getId() +
                                 " extensions: " + (jsa != null ? jsa.size() : 0) +
                                 (inTimeAreq != null ? ", in time for AReq=" + inTimeAreq : "") +
                                 (inTimeAres != null ? ", in time for ARes=" + inTimeAres : ""));

                        failCount = 0;
                    } else {
                        // results.extFailCount++; most likely not recoverable
                        String ec = MessageService.getStrValue(jd.json, "errorCode");
                        String ede = MessageService.getStrValue(jd.json, "errorDescription");
                        String ed = MessageService.getStrValue(jd.json, "errorDetail");
                        String st = MessageService.getStrValue(jd.json, "transStatus");
                        String tid = MessageService.getNumValue(jd.json, "tid");
                        if (update && ec == null) {
                            log.info("FSS response " + mt + " " + (st != null ? "st: " + st + " " : "") +
                                     (tid != null ? "tid: " + tid + " " : "") +
                                     (ede != null ? "errorDescription: " + ede + " " : "") +
                                     (ed != null ? "errorDetail: " + ed + " " : "") + (!hasext ? "no extensions" : ""));
                        } else {
                            log.error("FSS response " + mt + " " + (st != null ? "st: " + st + " " : "") +
                                      (tid != null ? "tid: " + tid + " " : "") +
                                      (ec != null ? "errorCode: " + ec + " " : "") +
                                      (ede != null ? "errorDescription: " + ede + " " : "") +
                                      (ed != null ? "errorDetail: " + ed + " " : "") +
                                      (!hasext ? "no extensions" : "") + " is not ok");

                            record.setErrorDetail(Misc.cut(Misc.merge(record.getErrorDetail(), "FSS Data " + mt + " " +
                                                                                               (ec != null ?
                                                                                                       "errorCode: " +
                                                                                                       ec + " " : "") +
                                                                                               (ede != null ?
                                                                                                       "errorDescription: " +
                                                                                                       ede + " " : "") +
                                                                                               (ed != null ?
                                                                                                       "errorDetail: " +
                                                                                                       ed + " " : "") +
                                                                                               (!hasext ? "no extensions" : "")),
                                                           2048));
                            // my be stored already so update prop only
                            if (record.getLocalDateEnd() != null) {
                                try {
                                    ServiceLocator.getInstance().getPersistenceService().updateTDSPropertyOnly(
                                            record.getId(), "errorDetail", record.getErrorDetail());
                                } catch (Exception e) {
                                    log.error("Failed to update of TDS " + record.getId() + " errordetail to: " +
                                              record.getErrorDetail(), e);
                                }
                            }
                        }

                        if (update && "301".equals(ec)) {
                            failCount++; // update too early for some reason keep retying
                        } else // non recoverable error no need to retry
                        {
                            failCount = 0;
                        }
                    }
                } else {
                    failCount++;
                }
            } else {
                failCount++;
                log.error("No valid response content from FSS" + (rspCode != null ? ", HTTP Code " + rspCode : "") +
                          (rspMsg != null ? "tdsMessageBase :" + rspMsg : "") + " ct=" + rspCt + " c={}",
                          (fssresp != null ? Misc.toStringBuilder(fssresp) : null));

                record.setErrorDetail(Misc.cut(Misc.merge(record.getErrorDetail(), "No content from FSS" +
                                                                                   (rspCode != null ? ", HTTP Code " +
                                                                                                      rspCode : "")),
                                               2048));

                // my be stored already so update prop only
                if (record.getLocalDateEnd() != null) {
                    try {
                        ServiceLocator.getInstance().getPersistenceService().updateTDSPropertyOnly(record.getId(),
                                                                                                   "errorDetail",
                                                                                                   record.getErrorDetail());
                    } catch (Exception e) {
                        log.error("Failed to update of TDS " + record.getId() + " errordetail to: " +
                                  record.getErrorDetail(), e);
                    }
                }
            }

        } catch (Exception ge) {
            failCount++;
            log.error("Error with FSS", ge);
            record.setErrorDetail(Misc.cut(Misc.merge(record.getErrorDetail(), "Error with FSS " + ge), 2048));
            try {
                ServiceLocator.getInstance().getPersistenceService().updateTDSPropertyOnly(record.getId(),
                                                                                           "errorDetail",
                                                                                           record.getErrorDetail());
            } catch (Exception e) {
                log.error("Failed to update of TDS " + record.getId() + " errordetail to: " + record.getErrorDetail(),
                          e);
            }
        } finally {
            // one retry in case possibly recoverable
            if (failCount > 0 && failCount < 2) {
                if ("AReq".equals(aReq_aRes_rReq.getMessageType())) {
                    if (results.extState != null) {
                        results.extState.set(AReqPostState.InRetry.value);
                    } else {
                        results.extState = new AtomicInteger(AReqPostState.InRetry.value);
                    }
                }
                int failCountFinal = failCount;
                int retry = getRetryInterval(wctx);
                log.info("Failed (" + failCount + ") FSS posting " + aReq_aRes_rReq.getMessageType() + " for " +
                         record.getId() + " scheduled for retry in next " + retry + " sec");
                dss.getScheduledExecutorService().schedule(
                        () -> sendMsg(record, results, fssReq, aReq_aRes_rReq, failCountFinal, dss, wctx), retry,
                        TimeUnit.SECONDS);
            } else if ("AReq".equals(aReq_aRes_rReq.getMessageType()) && failCount > 1) {
                if (results.extState != null) {
                    results.extState.set(AReqPostState.Failed.value);
                } else {
                    results.extState = new AtomicInteger(AReqPostState.Failed.value);
                }
                if (log.isDebugEnabled()) {
                    log.debug("Failed (" + failCount + ") FSS posting " + aReq_aRes_rReq.getMessageType() + " for " +
                              record.getId() + " no more retyr failure is final");
                }
            }
        }
    } // tdsMessageBase

    int getRetryInterval(WebContext wctx) {
        int retry = wctx.getIntSetting(DsSetting.FSS_RETRY_WAIT.getKey());
        retry = retry > 0 && retry <= 3 * cRetryUpdateInterVal ? retry : cRetryUpdateInterVal;

        return retry;
    }

    public void periodic(WebContext wctx) {
        int maxAge = 3 * getRetryInterval(wctx);
        Long[] keys = this.stateMap.keySet().toArray(new Long[0]);
        long expired = System.currentTimeMillis() - maxAge * 1000L;
        for (Long key : keys) {
            CachedObject<AtomicInteger> co = this.stateMap.get(key);
            if (co != null && co.getLoaded() < expired) {
                synchronized (this.stateMap) {
                    this.stateMap.remove(key);
                }
            }
        }
    }

    @Override
    public Extensions getExtensionId() {
        return Extensions.tdsmrelay;
    }
}
