/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 * Estonia Tallinn, https://www.modirum.com
 *
 * @author Andri Kruus,andri
 * Created on Nov 23, 2018 2:55:51 PM
 */
package com.modirum.ds.ext.fss;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.model.Timings;
import com.modirum.ds.web.services.Directory;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.CachedObject;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ThreeDSMethodRelayService extends DDCollectionService {

    private static final Logger log = LoggerFactory.getLogger(ThreeDSMethodRelayService.class);

    private final Directory dir;

    /**
     * Initialize.
     */
    public ThreeDSMethodRelayService(Directory dir, WebContext wctx) {
        super.setCookieName("TDSMR");
        String cn = wctx.getStringSetting(DsSetting.TDSM_RELAY_COOKIE_NAME.getKey());
        if (Misc.isNotNullOrEmptyTrimmed(cn)) {
            super.setCookieName(cn);
        }

        String cd = wctx.getStringSetting(DsSetting.TDSM_RELAY_COOKIE_DOMAIN.getKey());
        if (Misc.isNotNullOrEmptyTrimmed(cd)) {
            super.setCookieDomain(cd);
        }
        int cma = wctx.getIntSetting(DsSetting.TDSM_RELAY_COOKIE_MAXAGEDAYS.getKey());
        if (cma > 0) {
            super.setCookieAge(cma * 3600 * 24);
        }

        String sp = wctx.getStringSetting(DsSetting.TDSM_RELAY_SCRIPT_JSP.getKey());
        if (Misc.isNotNullOrEmpty(sp)) {
            super.setDdCollectScriptJSPPath(sp);
            log.info("Custom 3DSMRelay.scriptjsp intialized as " + sp);
        }

        String cTDSMURI = wctx.getStringSetting(DsSetting.TDSM_RELAY_URI.getKey());
        if (cTDSMURI != null) {
            super.setForwardURI(cTDSMURI);
        }

        String cfpScriptUrl = wctx.getStringSetting(DsSetting.TDSM_RELAY_FP_SCRIPT_URL.getKey());
        if (Misc.isNotNullOrEmpty(cfpScriptUrl)) {
            super.setFpScriptUrl(cfpScriptUrl);
        }

        String cDdVersion = wctx.getStringSetting(DsSetting.TDSM_RELAY_DD_VERSION.getKey());
        if (Misc.isNotNullOrEmpty(cDdVersion)) {
            super.setDdVersion(cDdVersion);
        }

        String cSkipHeaders = wctx.getStringSetting(DsSetting.TDSM_RELAY_SKIP_CAPTURE_HEADERS.getKey());
        if (Misc.isNotNullOrEmpty(cSkipHeaders)) {
            String[] csh = Misc.split(cSkipHeaders, ",");
            if (csh != null && csh.length > 0) {
                super.setSkipHeaders(csh);
            }
        }

        String cECPath = wctx.getStringSetting(DsSetting.TDSM_RELAY_EVERCOOKIE_URI.getKey());
        if (Misc.isNotNullOrEmpty(cECPath)) {
            super.setEvercookiePath(cECPath);
        }
        super.captureIP = !"false".equals(wctx.getStringSetting(DsSetting.TDSM_RELAY_CAPTURE_IP.getKey()));
        super.captureHeaders = !"false".equals(wctx.getStringSetting(DsSetting.TDSM_RELAY_CAPTURE_HEADERS.getKey()));
        super.captureCookie = !"false".equals(wctx.getStringSetting(DsSetting.TDSM_RELAY_CAPTURE_COOKIE.getKey()));
        super.addCookieIfMissing = !"false".equals(wctx.getStringSetting(DsSetting.TDSM_RELAY_ADD_COOKIE_IF_MISSING.getKey()));
        super.useEverCookie = "true".equals(wctx.getStringSetting(DsSetting.TDSM_RELAY_USE_EVERCOOKIE.getKey()));

        this.dir = dir;
    }

    public void handlePublic(HttpServletRequest req, HttpServletResponse resp, Timings timings) throws ServletException, java.io.IOException {
        String mfn = "threeDSMethodData";
        String tdsmJsonB64 = req.getParameter(mfn);
        resp.setCharacterEncoding("UTF-8");

        if (Misc.isNullOrEmpty(tdsmJsonB64)) {
            mfn = "3DSMethodData";
            tdsmJsonB64 = req.getParameter(mfn);
        }
        String tdsTransId = req.getParameter("tdsTransId");
        if (tdsmJsonB64 != null && !req.getRequestURI().contains(this.collectURI)) {
            byte[] json;
            try {
                json = Base64.decodeURL(tdsmJsonB64);
            } catch (Exception ee) {
                log.warn("Failed to decode threeDSMethodData with b64url trying with b64 {} ex {}",
                         new Object[]{tdsmJsonB64, ee});
                json = Base64.decode(tdsmJsonB64);
                log.info("threeDSMethodData decoding with b64 succesfful repcakging to b64url for acs relay..");
                tdsmJsonB64 = Base64.encodeURL(json);
            }

            if (log.isDebugEnabled()) {
                log.debug("Got 3DS Method data {}={}", new Object[]{mfn, Misc.toStringBuilder(json)});
            }

            String txId = null;
            String nUrl = null;

            JsonObject jso = null;
            try {
                jso = Json.createReader(new ByteArrayInputStream(json)).readObject();
            } catch (Exception e) {
                log.error("Errro parsing 3ds method data json ", e);
            }

            if (jso != null && jso.containsKey("threeDSServerTransID")) {
                txId = jso.getString("threeDSServerTransID");
            }
            if (jso != null && jso.containsKey("threeDSMethodNotificationURL")) {
                nUrl = jso.getString("threeDSMethodNotificationURL");
            }

            String acsId = req.getRequestURI();

            if (acsId != null && acsId.contains("/")) {
                acsId = acsId.substring(acsId.lastIndexOf('/') + 1);
            }
            try {
                ACSProfile acp = null;
                if (Misc.isNumber(acsId)) {
                    acp = dir.getIssuerService().getACSById(Misc.parseIntBoxed(acsId), true);
                }

                if (acp != null) {
                    if (txId == null) {
                        log.warn(
                                "Invalid 3dsmethod relay request txId={}, notUrl={}, acsId={} acsfound={} trying still to relay as is",
                                txId, nUrl, acsId, acp != null);
                    } else {
                        // render 3dsmethod relay and data collection script

                        if (Misc.isNullOrEmptyTrimmed(acp.getThreeDSMethodURL())) {
                            req.setAttribute("3dsmNotiURL", nUrl);
                        } else {
                            req.setAttribute("acs3dsmURL", acp.getThreeDSMethodURL());
                        }
                        req.setAttribute("3DSMethodData", tdsmJsonB64);
                        req.setAttribute("tdsId", txId);
                    }
                    super.forward(req, resp);
                } else {
                    log.warn("Invalid 3dsmethod relay request txId={}, notUrl={}, acsId={} acsfound={}", txId, nUrl,
                             acsId, acp != null);
                    resp.setContentType("text/plain; charset=utf-8");
                    resp.getOutputStream().write(ERR);
                }
            } catch (Exception e) {
                log.error(
                        "Failed to process 3ds method relay for txId=" + txId + ", notUrl=" + nUrl + ", acsId=" + acsId,
                        e);
                resp.setContentType("text/plain; charset=utf-8");
                resp.getOutputStream().write(ERR);
            }
        } else if (tdsTransId != null) {
            processDeviceData(req, resp);
        } else {
            log.warn("Invalid request no 3dsmethod data and no tdsTransId, device/ec data");
            resp.setContentType("text/plain");
            resp.getOutputStream().write(ERR);
        }
    }

    public void processDeviceData(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        collect(req, resp);
    }

    public CachedObject<DeviceData> getThreeDSMethodDataBy3DSId(String tdsId) {
        return deviceDataCache.get(tdsId);
    }
}
