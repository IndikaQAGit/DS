package com.modirum.ds.web.core;

import com.modirum.ds.db.model.Setting;
import com.modirum.ds.model.Timings;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class RequestsTiming {

    private static final Logger log = LoggerFactory.getLogger(RequestsTiming.class);

    private static final int C_MAX_TIMINGS = 10000;

    private final String[] xtimingsnames = {"aReqTimings", "rReqTimings", "pReqTimings"};

    private long timeline;
    private int timingsCounter;

    private final String hostId;
    private final PersistenceService persistenceService;

    protected LinkedList<Timings> aReqTimings;
    protected LinkedList<Timings> pReqTimings;
    protected LinkedList<Timings> rReqTimings;

    public RequestsTiming(PersistenceService persistenceService, String hostId) {
        this.persistenceService = persistenceService;
        this.hostId = hostId;
        aReqTimings = new LinkedList<>();
        pReqTimings = new LinkedList<>();
        rReqTimings = new LinkedList<>();
    }

    public void addAReqTiming(Timings t) {
        synchronized (aReqTimings) {
            aReqTimings.add(t);
            if (aReqTimings.size() > C_MAX_TIMINGS) {
                aReqTimings.removeFirst();
            }
        }
    }

    public void addPReqTiming(Timings t) {
        synchronized (pReqTimings) {
            pReqTimings.add(t);
            if (pReqTimings.size() > C_MAX_TIMINGS) {
                pReqTimings.removeFirst();
            }
        }
    }

    public void addRReqTiming(Timings t) {
        synchronized (rReqTimings) {
            rReqTimings.add(t);
            if (rReqTimings.size() > C_MAX_TIMINGS) {
                rReqTimings.removeFirst();
            }
        }
    }

    public LinkedList<Timings> getaReqTimings() {
        return aReqTimings;
    }

    public LinkedList<Timings> getpReqTimings() {
        return pReqTimings;
    }

    public LinkedList<Timings> getrReqTimings() {
        return rReqTimings;
    }

    public synchronized void storeTimings() {
        try {
            // only older than 10 seconds so they are most likely completed
            long now = System.currentTimeMillis() - 10000;
            @SuppressWarnings("rawtypes") LinkedList[] xtimings = {getaReqTimings(), getrReqTimings(), getpReqTimings()};
            Map<String, LinkedList<Timings>> timingMap = new HashMap<>();

            timingsCounter++;
            if (timingsCounter > 90) // 0,25h
            {
                timingsCounter = 1;
            }

            Setting intancXTs = new Setting();
            intancXTs.setKey("DS.timings." + hostId + "/" + Misc.padCutStringLeft("" + timingsCounter, '0', 2));
            intancXTs.setProcessorId((short) 0);
            intancXTs.setComment("Stats " + hostId);
            intancXTs.setLastModifiedBy("DServer " + hostId);

            for (int i = 0; i < xtimings.length; i++) {
                @SuppressWarnings("unchecked") LinkedList<Timings> xtiming = xtimings[i];
                String xtimingn = xtimingsnames[i];

                LinkedList<Timings> xtimingsTimed = new LinkedList<Timings>();
                timingMap.put(xtimingn, xtimingsTimed);

                // compact sum up timings in 100 ms samples 100 per 10 seconds
                int step = 1000; //ms 10x / sec

                if (timeline == 0) {
                    timeline = now - 10000;
                }

                // ts 100 ms summary
                Timings ts = Timings.getInstance();
                ts.reqStart = timeline;

                if (xtiming.size() < 1) {
                    xtimingsTimed.add(ts);
                } else {
                    while (xtiming.size() > 0) {
                        Timings t = null;
                        //synchronized(xtiming)
                        //{ shall be ok with linkedlist since only one removing thread
                        t = xtiming.removeFirst();

                        if (now < t.reqStart) {
                            // stop and put back
                            synchronized (xtiming) {
                                xtiming.addFirst(t);
                            }

                            break;
                        }
                        // ignore possible incompleted
                        if (t.resEnd == 0) {
                            continue;
                        }
                        // in range
                        if (t.reqStart > 0 && t.reqStart >= ts.reqStart && t.reqStart < ts.reqStart + step) {
                            ts.cnt++;
                            ts.reqMid += t.reqMid;
                            ts.reqEnd += t.reqEnd;
                            ts.resStart += t.resStart;
                            ts.resMid += t.resMid;
                            ts.resEnd += t.resEnd;

                        } // switch next time slot
                        else if (t.reqStart > 0 && t.reqStart >= ts.reqStart + step) {
                            if (ts != null) {
                                xtimingsTimed.add(ts);
                            }

                            ts = Timings.getInstance();
                            ts.reqStart = t.reqStart;
                            ts.cnt++;
                            ts.reqMid += t.reqMid;
                            ts.reqEnd += t.reqEnd;
                            ts.resStart += t.resStart;
                            ts.resMid += t.resMid;
                            ts.resEnd += t.resEnd;
                        }

                        // if reached now break up
                    } // while

                    if (ts != null) {
                        xtimingsTimed.add(ts);
                    }
                }
            } /// for

            intancXTs.setLastModified(new java.util.Date());
            intancXTs.setValue(toJSON(timingMap).toString());
            persistenceService.saveOrUpdate(intancXTs);
            timeline = now;
        } catch (Exception e) {
            log.warn("Unable to save or update setting timings", e);
        }
    }

    StringBuilder toJSON(Map<String, LinkedList<Timings>> timings) {
        StringBuilder sb = new StringBuilder(4 * 1024);
        sb.append("{");
        StringBuilder sbx = new StringBuilder(1 * 1024);
        for (int i = 0; i < xtimingsnames.length; i++) {
            LinkedList<Timings> xtiml = timings.get(xtimingsnames[i]);
            if (xtiml != null) {
                sb.append("\"" + xtimingsnames[i] + "\" : [");
                for (Timings t : xtiml) {
                    if (sbx.length() > 0) {
                        sbx.append(',');
                    }
                    sbx.append(t.reqStart);
                    sbx.append(',');
                    sbx.append(t.cnt);
                    sbx.append(',');
                    sbx.append(t.reqMid);
                    sbx.append(',');
                    sbx.append(t.reqEnd);
                    sbx.append(',');
                    sbx.append(t.resStart);
                    sbx.append(',');
                    sbx.append(t.resMid);
                    sbx.append(',');
                    sbx.append(t.resEnd);
                }

                sb.append(sbx);
                sbx.setLength(0);
                sb.append("]");
            }
            if (i < xtimingsnames.length - 1) {
                sb.append(",");
            }
        }
        sb.append("}");

        return sb;
    }
}
