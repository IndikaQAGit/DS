package com.modirum.ds.web.filters;

import com.modirum.ds.model.TDSModel;

import java.util.ArrayList;
import java.util.List;

public class ExcludeFiltersProvider230 {

    /**
     * For forwarded messsages
     * MSGType + fieldname + Device + category
     * MSGType + fieldname + Device + category + status
     * MSGType + fieldname  (for all)
     */
    public static List<String> getFilters() {
        List<String> filters = new ArrayList<>();

        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "01.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "01.02.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "02.01.C");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationType." + "02.02.C");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "03.02");

        return filters;
    }

}
