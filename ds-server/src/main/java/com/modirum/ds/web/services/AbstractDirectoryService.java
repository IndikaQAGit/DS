package com.modirum.ds.web.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.FieldName;
import com.modirum.ds.json.JsonUtil;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.model.Error;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.model.Timings;
import com.modirum.ds.services.AcquirerService;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService210;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.TdsRecordAttributeService;
import com.modirum.ds.tds20msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XJWSCompact;
import com.modirum.ds.util.CryptoUtil;
import com.modirum.ds.web.core.DSConfigurableSettings;
import com.modirum.ds.web.helper.PurchaseCurrencyHelper;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.ISO3166;
import com.modirum.ds.utils.ISO4217;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import javax.json.JsonArray;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.NoRouteToHostException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.modirum.ds.util.DSUtils.isUUID;
import static com.modirum.ds.web.util.Utils.isDbSafeDate;
import static java.lang.String.format;

public abstract class AbstractDirectoryService {

    public final static int C_MAX_DB_STORE_TX_TIME = 7;
    public final static String DS_REFERENCE_NUMBER = "DSReferenceNumber";
    public static final String TDS_MASK_PREFIX = "3ds.mask.";
    /**
     * To support backward compatibility, the default value for storing encrypted/hashed pan is true.
     * This means null value is consider true and if setting is non-existing in the db, also considered true as well.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractDirectoryService.class);
    private final static String PURCHASE_DATE_FORMAT = "yyyyMMdd HH:mm:ss";
    private final static String PURCHASE_DATE_FORMAT_JULY_2017 = "yyyyMMddHHmmss";
    @Autowired
    protected Directory directory;
    protected ExtensionService extensionService;
    protected PersistenceService persistenceService;
    @Autowired
    protected CryptoService cryptoService;
    protected IssuerService issuerService;
    @Autowired
    protected KeyService keyService;
    protected AcquirerService acquirerService;
    protected DSConfigurableSettings dsSettings;
    @Autowired
    protected TdsRecordAttributeService tdsRecordAttributeService;

    protected VirtualACSService virtualAcsService;

    protected MessageService210<TDSMessage> mse;
    protected Long uuidPfx;
    protected volatile Map<Integer, MessageService.PublicBOS> cachedRangeDataRawMap = new HashMap<>();
    protected final JsonMessageService jsonMessageService;
    private String dsReferenceNumber;

    /**
     * Constructor.
     */
    public AbstractDirectoryService() {
        persistenceService = ServiceLocator.getInstance().getPersistenceService();
        dsSettings = new DSConfigurableSettings(persistenceService);
        issuerService = ServiceLocator.getInstance().getIssuerService();
        acquirerService = ServiceLocator.getInstance().getAcquirerService();
        jsonMessageService = ServiceLocator.getInstance().getJsonMessageService();
        if (dsSettings.isVirtualACSEnabled()) {
            virtualAcsService = new VirtualACSService();
        }

        String esc = null;
        try {
            esc = persistenceService.getStringSetting(DsSetting.EXTENSION_SERVICE.getKey());
            if (Misc.isNotNullOrEmptyTrimmed(esc)) {
                ExtensionService es = (ExtensionService) Class.forName(esc).getDeclaredConstructor().newInstance();
                if (directory.checkExtensionLicensing(es.getExtensionId().name())) {
                    extensionService = es;
                    log.info("Intialized ExtensionService: " + extensionService);
                } else {
                    log.warn("Extension " + es.getExtensionId().name() + " not licensed, service " +
                             es.getClass().getSimpleName() + " not activated");
                }
            }
        } catch (Throwable t) {
            log.error(
                    "Failed to intialize ExtensionService class " + esc + " (specified processing remains unavailable)",
                    t);
        }
    }

    static MessageService.PublicBOS pResSpecial(MessageService.PublicBOS pres) {
        pres.getBuf()[0] = (byte) ',';
        return pres;
    }

    /**
     *
     */
    public abstract void createAndAddRangeData(CardRange cr, WebContext wctx);

    /**
     *
     */
    public abstract void periodic(WebContext wctx);

    /**
     *
     */
    public abstract Results processRequest(TDSMessageBase reqMsgbase, byte[] msgRaw1, String[] msgRaw2, Date now, Timings timings, WebContext wctx) throws Exception;

    public Directory getDirectory() {
        return directory;
    }

    public String getDSID() {
        return directory.getHostId();
    }

    /**
     * Some sort of name based prefix (unique namsecpace)
     *
     * @param wctx
     * @return
     */
    public final Long getUUIDPrefix(WebContext wctx) {
        if (uuidPfx == null) {
            byte[] pfxbuf = CryptoUtil.SHA256(
                    (getDSReferenceNumber(wctx) + System.getProperties().toString() + Utils.getLocalNonLoIp() +
                     System.currentTimeMillis()).getBytes());
            uuidPfx = Misc.bytesToLong(pfxbuf);
        }

        long l = uuidPfx + (long) (Integer.MAX_VALUE * Math.random());
        log.info("UUID prefix: " + l); // Extra log
        return l;
    }

    public void safeUpdate(Persistable obj, int count, Context ctx) {
        try {
            if ((count == -1 || count == -2) && !this.getScheduledExecutorService().isShutdown()) {
                int delay = 1;
                if (count == -2) {
                    delay = 200;
                }
                this.getScheduledExecutorService().schedule(new UpdateFinalStateForTx(obj, 1, ctx),
                                                            delay, TimeUnit.MILLISECONDS);
            } else {
                // primary attempt must be fast not to delay processing
                synchronized (obj) {
                    persistenceService.saveOrUpdate(obj, count ==
                                                         0 ? C_MAX_DB_STORE_TX_TIME : persistenceService.getTransactionTimeOut());
                }
            }
        } catch (Throwable t) {
            if (count < 3 && !this.getScheduledExecutorService().isShutdown()) {
                log.error("Failed to update obj " + obj.getId() + " final state uc " + count + " times, will retry..",
                          t);
                this.getScheduledExecutorService().schedule(new UpdateFinalStateForTx(obj, (count +
                                                                                            1), ctx), 30, TimeUnit.SECONDS);
            } else {
                log.error("Failed to update obj " + obj.getId() + " final state uc " + count +
                          "!!! times, wont try any more, fix your database", t);
                StringBuilder emerd = new StringBuilder(512);
                emerd.append("<EMER objId=\"" + obj.getId() + "\" type=\"" + obj.getClass().getName() + "\">\n");
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream(512);
                    ObjectOutputStream oos = new ObjectOutputStream(bos);
                    oos.writeObject(obj);
                    oos.flush();
                    emerd.append(Base64.encode(bos.toByteArray()));
                    emerd.append("\n</EMER>");
                    log.error("Data for emergency recovery for obj " + obj.getId() + "/" + obj.getClass().getName() +
                              "\n" + emerd);
                    oos.close();
                } catch (Exception e) {
                    log.error("Failed to create emergency data for obj " + obj.getId());
                }
            }
        }
    }

    public StringBuilder createErrorDetails(List<Error> errors) {
        StringBuilder result = new StringBuilder(512);
        for (Error error : errors) {
            String d1 = Misc.cut(Misc.merge(error.getFieldName(), error.getDescription()), 2040);
            if (result.length() == 0 || result.length() + d1.length() + 1 < 2048) {
                if (result.length() > 0) {
                    result.append(",");
                }
                result.append(d1);
            } else {
                break;
            }
        }
        return result;
    }

    public TDSMessageData createNewTDSMessageData(TDSMessageBase msg, TDSRecord record, byte[] msgRawBuf, java.util.Date now, String src, String dst, WebContext wctx) throws Exception {
        String[] msgRaw = new String[]{new String(msgRawBuf, StandardCharsets.UTF_8)};
        return createNewTDSMessageData(msg, record, msgRaw, now, src, dst, wctx);
    }

    public TDSMessageData createNewTDSMessageData(TDSMessageBase msg, TDSRecord record, MessageService.PublicBOS bos, java.util.Date now, String src, String dst, WebContext wctx) throws Exception {
        String[] msgRaw = new String[]{bos.toString(StandardCharsets.UTF_8)};
        return createNewTDSMessageData(msg, record, msgRaw, now, src, dst, wctx);
    }

    protected void savePResTDSMessage(TDSMessageBase msg, TDSRecord record, String pRes, String src, String dst, WebContext webContext) {
        TDSMessageData messageData = new TDSMessageData();
        messageData.setSourceIP(Misc.cut(src, 128));
        messageData.setDestIP(Misc.cut(dst, 128));
        messageData.setContents(pRes); // pRes is not necessarily a valid json here, possible trimmed which fails json parsing
        messageData.setTdsrId(record != null ? record.getId() : 0L);
        messageData.setMessageVersion(
                msg != null && msg.getMessageVersion() != null ? Misc.cut(msg.getMessageVersion(), 12) : "");
        messageData.setMessageType(
                msg != null && msg.getMessageType() != null ? Misc.cut(msg.getMessageType(), 10) : "");
        messageData.setMessageDate(new Date());

        safeUpdate(messageData, -1, webContext);
    }

    public TDSMessageData createNewTDSMessageData(TDSMessageBase msg, TDSRecord record, String[] oneLen, Date now, String src, String dst, WebContext webContext) throws Exception {
        TDSMessageData messageData = new TDSMessageData();
        messageData.setSourceIP(Misc.cut(src, 128));
        messageData.setDestIP(Misc.cut(dst, 128));

        String jsonTdsMessage = oneLen[0];
        if (msg != null && msg.getAcctNumber() != null) {
            jsonTdsMessage = Misc.replace(jsonTdsMessage, msg.getAcctNumber(), Misc.mask(msg.getAcctNumber(), 6, 3));
        }

        if (JsonUtil.isJsonValid(jsonTdsMessage)) {
            jsonTdsMessage = maskedJsonFieldsBasedOnSettings(jsonTdsMessage, webContext);
        }

        String encryptedContent = getEncryptedIfSensitiveData(jsonTdsMessage, webContext);

        messageData.setContents(encryptedContent);
        messageData.setTdsrId(record != null ? record.getId() : 0L);
        messageData.setMessageVersion(
                msg != null && msg.getMessageVersion() != null ? Misc.cut(msg.getMessageVersion(), 12) : "");
        messageData.setMessageType(
                msg != null && msg.getMessageType() != null ? Misc.cut(msg.getMessageType(), 10) : "");
        messageData.setMessageDate(now);

        this.safeUpdate(messageData, -1, webContext);

        return messageData;
    }

    /**
     * Encrypts <code>json</code> with default Data Key, if there is 3DS Sensitive Data in the message.  If key is not
     * found, then the message sensitive data elements are masked (without encryption). Messages with no sensitive data
     * are not encrypted.
     *
     * @param json Original json message
     * @param webContext context with configuration settings.
     * @return Encrypted json, if encryption criteria is met. Otherwise clear-text masked json.
     * @throws Exception in case json message could not be correctly processed.
     */
    private String getEncryptedIfSensitiveData(String json, WebContext webContext) throws Exception {
        // PCI-3DS / 3DS Sensitive Data / (Encrypted) storage requirement
        boolean hasSensitiveData = hasSensitiveData(json, webContext);
        if (!hasSensitiveData) {
            return json;
        }

        try {
            // Encrypting whole message (not separate fields) because symmetric encryption is not expensive,
            // and encrypting separate fields would add network(HSM)/processing overhead and make parsing harder.
            return cryptoService.encryptData(json);
        } catch (Exception e) {
            log.error("Error encrypting TDS message content for storage.", e);
        }

        log.info("Masking sensitive data from TDS message content before storing.");
        // Mask known sensitive data fields. Inefficient de/serialization, but abnormal case in production anyway.
        if (JsonUtil.isJsonValid(json)) {
            JsonMessage jsonMessage = jsonMessageService.parse(json);
            jsonMessageService.hideValue(jsonMessage, FieldName.authenticationValue);
            jsonMessageService.hideValue(jsonMessage, FieldName.deviceInfo);
            jsonMessageService.hideValue(jsonMessage, FieldName.sdkEncData);
            jsonMessageService.hideValue(jsonMessage, FieldName.sdkEphemPubKey);
            hideEphemKeysFromAcsSignedContent(jsonMessage);
            return jsonMessage.toString();
        }

        return json;
    }

    /**
     * Checks whether JSON message contains 3DS Sensitive Data elements, and whether they are flagged to be stored by DS.
     */
    private boolean hasSensitiveData(String json, WebContext webContext) {
        boolean isStoreAuthValue = webContext.isTrueSetting(DsSetting.STORE_AUTHENTICATION_VALUE.getKey());
        boolean isStoreDeviceInfo = webContext.isTrueSetting(DsSetting.STORE_DEVICE_INFO.getKey());
        boolean isStoreSdkEncData = webContext.isTrueSetting(DsSetting.STORE_SDK_ENC_DATA.getKey());
        boolean isStoreEphemPubKeys = webContext.isTrueSetting(DsSetting.STORE_EPHEM_PUB_KEYS.getKey());

        if (isStoreAuthValue && json.contains(FieldName.authenticationValue)) {
            return true;
        }
        if (isStoreDeviceInfo && json.contains(FieldName.deviceInfo)) {
            return true;
        }
        if (isStoreSdkEncData && json.contains(FieldName.sdkEncData)) {
            return true;
        }
        if (isStoreEphemPubKeys && json.contains(FieldName.acsSignedContent)) {
            // acsSignedContent contains internally acsEphemPubKey and sdkEphemPubKey as well
            return true;
        }
        if (isStoreEphemPubKeys && json.contains(FieldName.sdkEphemPubKey)) {
            // sdkEphemPubKey may be in initial AReq from 3DSS as one of root-level elements
            return true;
        }
        return false;
    }

    private String maskedJsonFieldsBasedOnSettings(String json, WebContext webContext) throws Exception {
        JsonMessage jsonMessage = jsonMessageService.parse(json);

        boolean isStoreAuthValue = webContext.isTrueSetting(DsSetting.STORE_AUTHENTICATION_VALUE.getKey());
        boolean isStoreDeviceInfo = webContext.isTrueSetting(DsSetting.STORE_DEVICE_INFO.getKey());
        boolean isStoreSdkEncData = webContext.isTrueSetting(DsSetting.STORE_SDK_ENC_DATA.getKey());
        boolean isStoreEphemPubKeys = webContext.isTrueSetting(DsSetting.STORE_EPHEM_PUB_KEYS.getKey());
        // These elements are defined by PCI-3DS as "3DS Sensitive Data".
        // PCI-3DS-Data-Matrix-v1_1.pdf / Table 1: 3DS Sensitive Data
        // These elements must be either encrypted (if storage permitted) or not stored at all (e.g. full value mask).
        // By safe (and HSM-friendly) default DS will not store these values. Processing before any custom masking.
        if (!isStoreAuthValue) {
            jsonMessageService.hideValue(jsonMessage, FieldName.authenticationValue);
        }
        if (!isStoreDeviceInfo) {
            jsonMessageService.hideValue(jsonMessage, FieldName.deviceInfo);
        }
        if (!isStoreSdkEncData) {
            jsonMessageService.hideValue(jsonMessage, FieldName.sdkEncData);
        }
        if (!isStoreEphemPubKeys) {
            // sdkEphemPubKey is present both as root element(AReq) and as part of acsSignedContent(ARes).
            jsonMessageService.hideValue(jsonMessage, FieldName.sdkEphemPubKey);
            hideEphemKeysFromAcsSignedContent(jsonMessage);
        }

        List<Setting> maskedFieldSettings = persistenceService.getSettingsByIdStart(TDS_MASK_PREFIX + "%", null, null);
        if (Misc.isNotNullOrEmpty(maskedFieldSettings)) {
            maskedFieldSettings.forEach(setting -> {
                String fieldName = setting.getKey().substring(TDS_MASK_PREFIX.length());
                jsonMessageService.maskValue(jsonMessage, fieldName, setting.getValue(), JsonMessageService.DEFAULT_MASK_REPLACEMENT);
            });
        }
        return jsonMessage.toString();
    }

    private void hideEphemKeysFromAcsSignedContent(JsonMessage jsonMessage) throws Exception {
        JsonNode acsSignedContentNode = jsonMessage.getRootNode().get(FieldName.acsSignedContent);
        if (acsSignedContentNode != null) {
            String acsSignedContentAsText = acsSignedContentNode.asText();
            if (!acsSignedContentAsText.isEmpty()) {
                XJWSCompact jwsCompact = MessageService.fromCompactJWS(acsSignedContentAsText);
                byte[] payload = Base64.decodeURL(jwsCompact.getPayload());
                JsonMessage acsSignedContentPayload = jsonMessageService.parse(new String(payload, StandardCharsets.UTF_8));
                jsonMessageService.hideValue(acsSignedContentPayload, FieldName.acsEphemPubKey);
                jsonMessageService.hideValue(acsSignedContentPayload, FieldName.sdkEphemPubKey);
                String acsSignedContentEphemMasked = acsSignedContentPayload.toString();
                jwsCompact.setPayload(Base64.encodeURL(acsSignedContentEphemMasked.getBytes(StandardCharsets.UTF_8)));
                ((ObjectNode) jsonMessage.getRootNode()).put(FieldName.acsSignedContent, MessageService.toCompactJWS(jwsCompact));
            }
        }
    }

    public abstract void initPRes();

    public abstract void createRawCache() throws Exception;

    protected Optional<Integer> getPaymentSystemIdByPort(int serverPort) {
        Short fallbackPort = (short) 0;
        try {
            Short port = (short) serverPort;
            Optional<Integer> paymentSystemId = persistenceService.getPaymentSystemIdByPort(port);
            if (paymentSystemId.isPresent()) {
                return paymentSystemId;
            }
            log.warn("Payment system by port={} not found.", port);

            paymentSystemId = persistenceService.getPaymentSystemIdByPort(fallbackPort);
            if (paymentSystemId.isPresent()) {
                log.info("Fallback to paymentSystemId={} found by port={}", paymentSystemId.get(), fallbackPort);
                return paymentSystemId;
            }
        } catch (Exception e) {
            log.error("Could not retrieve a payment system entry", e);
        }

        log.error("No payment system entries for: request port={} or fallback port={}", serverPort, fallbackPort);
        return Optional.empty();
    }

    /**
     * Creates a transaction entry in the database for the EMV 3DS transaction being processed.
     */
    public TDSRecord createNewTDSRecord(TDSMessageBase tdsMessageBase, Date now, WebContext wctx) throws Exception {
        TDSRecord tdsRecord = new TDSRecord();
        tdsRecord.setPaymentSystemId(wctx.getPaymentSystemId());
        tdsRecord.setLocalDateStart(now);
        tdsRecord.setProtocol(Misc.trunc(tdsMessageBase.getMessageVersion(), 16));

        if (tdsMessageBase.getAcctNumber() != null) {
            tdsRecord.setAcctNumber(Misc.trunc(Misc.mask(tdsMessageBase.getAcctNumber(), 6, 4), 20));

            boolean isStoreEncryptedPan = Boolean.parseBoolean(wctx.getStringSetting(DsSetting.ENABLE_STORING_ENCRYPTED_PAN_KEY.getKey()));
            if (isStoreEncryptedPan) {
                KeyData secretKey = keyService.getSecretKey(KeyService.KeyAlias.dataKey, KeyData.KeyStatus.working, CryptoService.DATAENCALG);

                tdsRecord.setAcctNumberEnc(cryptoService.encrypt(tdsMessageBase.getAcctNumber(), secretKey));
                tdsRecord.setAcctNumberKid(keyService.getKeyId(KeyService.KeyAlias.dataKey, KeyData.KeyStatus.working, CryptoService.DATAENCALG));
            }

            boolean isStoreHashedPan = Boolean.parseBoolean(wctx.getStringSetting(DsSetting.ENABLE_STORING_HASHED_PAN_KEY.getKey()));
            if (isStoreHashedPan) {
                KeyData hmacKeyData = keyService.getHMACKey();
                // todo add back hardware/software hmacSupport
//                tdsRecord.setAcctNumberHMAC(cryptoService.createPanHMAC(tdsMessageBase.getAcctNumber(), hmacKeyData));
//                tdsRecord.setAcctNumberHMKid(keyService.getHMACKeyId());
            }

        }

        tdsRecord.setRequestorID(Misc.trunc(tdsMessageBase.getTdsRequestorID(), 35));
        tdsRecord.setRequestorName(Misc.trunc(tdsMessageBase.getTdsRequestorName(), 40));
        tdsRecord.setAcquirerBIN(Misc.trunc(tdsMessageBase.getAcquirerBIN(), 10));
        tdsRecord.setAcquirerMerchantID(Misc.trunc(tdsMessageBase.getAcquirerMerchantID(), 32));

        tdsRecord.setBrowserLanguage(Misc.trunc(tdsMessageBase.getBrowserLanguage(), 8));
        tdsRecord.setBrowserIP(Misc.trunc(tdsMessageBase.getBrowserIP(), 64));

        Boolean browserJavaEnabled = tdsMessageBase.getBrowserJavaEnabled();
        if (browserJavaEnabled != null) {
            tdsRecord.setBrowserJavaEnabled(browserJavaEnabled ? "T" : "F");
        }
        tdsRecord.setBrowserUserAgent(Misc.trunc(tdsMessageBase.getBrowserUserAgent(), 2048));

        tdsRecord.setDeviceChannel(Misc.trunc(tdsMessageBase.getDeviceChannel(), 2));
        tdsRecord.setCardExpiryDate(Misc.trunc(tdsMessageBase.getCardExpiryDate(), 4));

        if (TDSModel.XmessageType.A_REQ.value().equals(tdsMessageBase.getMessageType())) {
            tdsRecord.setDsRef(getDSReferenceNumber(wctx));
        } else {
            tdsRecord.setDsRef(Misc.trunc(tdsMessageBase.getDsReferenceNumber(), 32));
        }

        Long pid = Misc.parseLongBoxed(tdsMessageBase.getPurchaseInstalData());
        tdsRecord.setPurchaseInstalData(pid != null ? pid.shortValue() : null);
        tdsRecord.setMCC(Misc.trunc(tdsMessageBase.getMcc(), 4));
        Long mcoco = Misc.parseLongBoxed(tdsMessageBase.getMerchantCountryCode());
        tdsRecord.setMerchantCountry(mcoco != null ? mcoco.shortValue() : null);
        tdsRecord.setMerchantName(Misc.trunc(tdsMessageBase.getMerchantName(), 40));
        tdsRecord.setRequestorURL(Misc.trunc(tdsMessageBase.getTdsRequestorURL(), 2048));
        tdsRecord.setMessageCategory(Misc.trunc(tdsMessageBase.getMessageCategory(), 2));
        tdsRecord.setMIReferenceNumber(Misc.trunc(tdsMessageBase.getTdsServerRefNumber(), 32));
        tdsRecord.setTdsTransID(Misc.trunc(tdsMessageBase.getTdsServerTransID(), 40));
        tdsRecord.setTdsServerURL(Misc.trunc(tdsMessageBase.getTdsServerURL(), 2048));
        tdsRecord.setDsRef(getDSReferenceNumber(wctx));
        Long pa = Misc.parseLongBoxed(tdsMessageBase.getPurchaseAmount());
        tdsRecord.setPurchaseAmount(pa);
        Long puc = Misc.parseLongBoxed(tdsMessageBase.getPurchaseCurrency());
        tdsRecord.setPurchaseCurrency(puc != null ? puc.shortValue() : null);
        java.util.Date pd = null;

        if (TDSModel.XmessageType.A_REQ.value().equals(tdsMessageBase.getMessageType())) {
            try {
                String pdv = tdsMessageBase.getPurchaseDate();
                if (pdv != null && pdv.trim().length() == PURCHASE_DATE_FORMAT.length()) {
                    pd = DateUtil.parseDate(tdsMessageBase.getPurchaseDate(), PURCHASE_DATE_FORMAT, true);
                }
                //EMVCo july 2017 spec purchaseDate format is yyyyMMddHHmmss
                else if (pdv != null && pdv.trim().length() == PURCHASE_DATE_FORMAT_JULY_2017.length()) {
                    pd = DateUtil.parseDate(tdsMessageBase.getPurchaseDate(), PURCHASE_DATE_FORMAT_JULY_2017, true);
                } else {
                    log.warn("Unknown purchase date format '" + tdsMessageBase.getPurchaseDate() +
                             "' defaulting to NOW..");
                }
            } catch (Exception e) {
                log.warn("Invalid unparseable purchase date format '" + tdsMessageBase.getPurchaseDate() +
                         "' defaulting to NOW..");
            }
        }

        if (pd != null && tdsMessageBase.getPurchaseDate() != null &&
            Misc.isNotNullOrEmpty(wctx.getStringSetting(DsSetting.PATCH_PURCHASE_DATE_FORMAT.getKey()))) {
            tdsMessageBase.setPurchaseDate(
                    DateUtil.formatDate(pd, wctx.getStringSetting(DsSetting.PATCH_PURCHASE_DATE_FORMAT.getKey()), true));
        }

        // insert now because field is not null
        tdsRecord.setPurchaseDate(pd != null && isDbSafeDate(pd) ? pd : new Date());

        Long pue = Misc.parseLongBoxed(tdsMessageBase.getPurchaseExponent());
        tdsRecord.setPurchaseExponent(pue != null ? pue.shortValue() : null);
        tdsRecord.setRecurringExpiry(Misc.trunc(tdsMessageBase.getRecurringExpiry(), 8));
        Long ref = Misc.parseLongBoxed(tdsMessageBase.getRecurringFrequency());
        tdsRecord.setRecurringFrequency(ref != null ? ref.shortValue() : null);
        tdsRecord.setSDKAppID(Misc.trunc(tdsMessageBase.getSdkAppID(), 40));
        tdsRecord.setSDKReferenceNumber(Misc.trunc(tdsMessageBase.getSdkReferenceNumber(), 32));
        tdsRecord.setSDKTransID(Misc.trunc(tdsMessageBase.getSdkTransID(), 40));
        tdsRecord.setTransType(Misc.trunc(tdsMessageBase.getTransType(), 2));
        tdsRecord.setLocalStatus(DSModel.TSDRecord.Status.INPROCESS);
        persistenceService.save(tdsRecord, C_MAX_DB_STORE_TX_TIME);

        return tdsRecord;
    }

    public boolean isConnectionFailedException(Throwable cfe) {
        return (cfe instanceof SocketException || cfe instanceof SocketTimeoutException && !(cfe.getMessage() != null &&
                                                                                             cfe.getMessage().toLowerCase().contains(
                                                                                                     "read timed out")) ||
                cfe instanceof ConnectException || cfe instanceof NoRouteToHostException ||
                cfe instanceof UnknownHostException || cfe instanceof IOException && cfe.getCause() != null &&
                                                       ("Connection timed out.".equals(cfe.getCause().getMessage()) ||
                                                        cfe.getCause().getMessage() != null &&
                                                        cfe.getCause().getMessage().contains(
                                                                "Server returned HTTP response code") ||
                                                        isConnectionFailedException(cfe.getCause())) ||
                cfe instanceof java.io.FileNotFoundException || // wrong url or not deployed
                // SSL
                cfe instanceof javax.net.ssl.SSLException || cfe instanceof javax.net.ssl.SSLKeyException);
    }

    public boolean isReadTimeoutException(Throwable cfe) {
        return (cfe instanceof SocketTimeoutException && cfe.getMessage() != null &&
                cfe.getMessage().toLowerCase().contains("read timed out") ||
                cfe instanceof IOException && cfe.getCause() != null &&
                "Connection read timed out.".equals(cfe.getCause().getMessage()) ||
                cfe.getCause() != null && isReadTimeoutException(cfe.getCause()));
    }

    public ScheduledThreadPoolExecutor getScheduledExecutorService() {
        return ServiceLocator.getInstance().getScheduledExecutorService();
    }

    public CryptoService getCryptoService() {
        return cryptoService;
    }

    public final String getDSReferenceNumber(WebContext webContext) {
        if (dsReferenceNumber != null) {
            return dsReferenceNumber;
        }

        dsReferenceNumber = webContext.getStringSetting(DsSetting.DS_REFERENCE_NUMBER.getKey());
        if (dsReferenceNumber != null) {
            return dsReferenceNumber;
        }

        // tomcat app context (conf/Catalina/localhost/ds.xml), with default to web.xml
        dsReferenceNumber = webContext.getInitParameter(DS_REFERENCE_NUMBER);
        return dsReferenceNumber;
    }

    // all extra client configs in one place
    public void configureHttpClient(HttpsJSSEClient client, WebContext wctx) {
        log.info("Configure http client"); // Extra log
        // rpoxy if neeeded
        client.setProxy(getOutgoingHTTPProxy(wctx));

        // poole thread for io if available enough
        if (getScheduledExecutorService().getActiveCount() < ServiceLocator.maxSchedulerThreads * 0.9) {
            client.setExecService(getScheduledExecutorService());
        }
        // custom user agent
        String ua = wctx.getStringSetting(DsSetting.HTTP_USERAGENT.getKey());
        if (Misc.isNotNullOrEmpty(ua)) {
            client.setUserAgent(ua);
        }
    }

    public Proxy getOutgoingHTTPProxy(WebContext wctx) {
        log.info("Get outgoing HTTP proxy"); // Extra log
        Proxy proxy = null;
        String ph = wctx.getStringSetting(DsSetting.HTTP_PROXY_HOST.getKey());
        String pp = wctx.getStringSetting(DsSetting.HTTP_PROXY_PORT.getKey());
        if (Misc.isNotNullOrEmpty(ph) && Misc.isInt(pp)) {
            final String pxkey = "com.modirum.ds.httpproxy" + ph + ":" + pp;
            proxy = (Proxy) wctx.getApplicationAttribute(pxkey);

            if (proxy == null) {
                SocketAddress address = new InetSocketAddress(ph, Misc.parseInt(pp));
                proxy = new Proxy(Proxy.Type.HTTP, address);
                wctx.setApplicationAttribute(pxkey, proxy);
            }
        }

        return proxy;
    }

    public List<Error> validateISOCodes(TDSMessageBase tdaMessage, WebContext webContext) {
        List<Error> errors = new ArrayList<>();
        if (tdaMessage != null) {
            // currency code
            String purchaseCurrency = tdaMessage.getPurchaseCurrency();
            if (Misc.isNumber(purchaseCurrency) && purchaseCurrency.length() == 3) {
                PurchaseCurrencyHelper config = new PurchaseCurrencyHelper(webContext.getStringSetting(DsSetting.SUPPORTED_PURCHASE_CURRENCIES.getKey()), directory.cache.getTableA6());
                if (config.isExcluded(purchaseCurrency)) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, "purchaseCurrency", format("prohibited value '%s'", purchaseCurrency)));
                } else if (config.isLimitedNotSupported(purchaseCurrency)) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, "purchaseCurrency", format("Purchase currency must be '%s'", config.getAllowedAsString())));
                } else if (!config.isSupported(purchaseCurrency)) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, "purchaseCurrency", format("unknown value '%s'", purchaseCurrency)));
                } else {
                    ISO4217 isoCurrency = ISO4217.findFromNumeric((short) Misc.parseInt(purchaseCurrency));
                    if (isoCurrency != null &&
                            "true".equals(webContext.getStringSetting(DsSetting.VALIDATE_CURRENCY_EXPONENT.getKey()))) {
                        if (Misc.isInt(tdaMessage.getPurchaseExponent()) &&
                                isoCurrency.getCurrencyExponent() != Misc.parseInt(tdaMessage.getPurchaseExponent())) {
                            errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, "purchaseExponent", format("invalid exponent '%s'", isoCurrency.getCurrencyExponent())));
                        }
                    }
                }
            }

            // country codes
            String[][] msgCountries = new String[][]{{tdaMessage.getBillAddrCountry(), "billAddrCountry", tdaMessage.getBillAddrState(), "billAddrState"}, {tdaMessage.getShipAddrCountry(), "shipAddrCountry", tdaMessage.getShipAddrState(), "shipAddrState"}, {tdaMessage.getMerchantCountryCode(), "merchantCountryCode"}};

            for (String[] cox : msgCountries) {
                if (Misc.isNumber(cox[0]) && cox[0].length() == 3) {
                    short coxs = (short) Misc.parseInt(cox[0]);
                    boolean emvco;
                    if (emvco = (coxs > 900 && coxs < 1000) ||
                            ("," + webContext.getStringSetting(DsSetting.EXTRA_COUNTRIES.getKey()) + ",").contains(
                                    "-" + cox[0])) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, cox[1],
                                (emvco ? "EMVCo " : "") + "prohibited value '" + cox[0] + "'"));
                    } else {
                        ISO3166 icx = ISO3166.findFromNumeric(coxs);
                        if (icx == null &&
                                !("," + webContext.getStringSetting(DsSetting.EXTRA_COUNTRIES.getKey()) + ",").contains(cox[0])) {
                            errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, cox[1],
                                    "unknown value '" + cox[0] + "'"));
                        } else if (icx != null && "USA".equals(icx.a3code) && cox.length > 2 && cox[2] != null) {
                            // state codes US only can not do whole world here
                            if (icx.subDivisions != null && icx.subDivisions.size() > 0 &&
                                    icx.subDivisions.get(cox[2]) == null) {
                                errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304, cox[3],
                                        "unknown value '" + cox[2] + "'"));
                            }
                        }
                    }
                }
            }
        }
        return errors;
    }

    /**
     * Create TDS message with an error.
     */
    public TDSMessage createErrorMessage(String messageVersion, TDSModel.ErrorCode ecode, String descr, CharSequence detail, TDSMessage req) {
        log.info("Create error message"); // Extra log
        return createErrorMessage(messageVersion, ecode, req != null ? req.getMessageType() : null, descr, detail,
                                  req !=
                                  null ? new String[]{req.getSdkTransID(), req.getTdsServerTransID(), req.getDsTransID(), req.getAcsTransID()} : null);
    }

    /**
     * Create TDS message with an error.
     */
    public TDSMessage createErrorMessage(String messageVersion, TDSModel.ErrorCode ecode, String mtsrc, String descr, CharSequence detail, String[] sdkIdTDSIdDSIdSACSId) {
        TDSMessage message = new TDSMessage();
        message.setErrorCode(ecode.value);
        if (descr == null && ecode != null) {
            descr = ecode.desc;
        }

        message.setMessageVersion(messageVersion);
        message.setMessageType(TDSModel.XmessageType.ERRO.value);

        message.setErrorDescription(descr);
        message.setErrorDetail(detail != null ? detail.toString() : null);
        message.setErrorMessageType(mtsrc != null && mtsrc.length() == 4 ? mtsrc : null);
        message.setErrorComponent(TDSModel.ErrorComponent.cDS.value);

        // include specific id if present and is valid
        message.setSdkTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 0 &&
                              isUUID(sdkIdTDSIdDSIdSACSId[0]) ? sdkIdTDSIdDSIdSACSId[0] : null);
        message.setTdsServerTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 1 &&
                                    isUUID(sdkIdTDSIdDSIdSACSId[1]) ? sdkIdTDSIdDSIdSACSId[1] : null);
        message.setDsTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 2 &&
                             isUUID(sdkIdTDSIdDSIdSACSId[2]) ? sdkIdTDSIdDSIdSACSId[2] : null);
        message.setAcsTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 3 &&
                              isUUID(sdkIdTDSIdDSIdSACSId[3]) ? sdkIdTDSIdDSIdSACSId[3] : null);

        return message;
    }

    public Results createErrorResults(String v, TDSModel.ErrorCode ecode, String mtsrc, String descr, CharSequence detail, String[] sdkIdTDSIdDSIdSACSId) throws JAXBException {
        TDSMessage tdsMessage = createErrorMessage(v, ecode, mtsrc, descr, detail, sdkIdTDSIdDSIdSACSId);
        Results results = new Results();
        results.tdsMessageBase = tdsMessage;
        results.jsonRaw = mse.toJSONBos(tdsMessage);
        return results;
    }

    public MessageService<?> getMessageService() {
        return mse;
    }

    // TODO Move me to a separate class
    public static class Results {

        public TDSMessageBase tdsMessageBase;
        public volatile TDSRecord tdsRecord;
        public MessageService.PublicBOS jsonRaw;
        public MessageService.PublicBOS jsonRaw2;
        public volatile JsonArray fssExtensions; // fss created extesnions later to be populated to areq/ares as necessary
        public volatile boolean waitingForAReqExtensions;
        public volatile JsonArray areqExtensions;
        public volatile boolean waitingForAResExtensions;
        public volatile JsonArray aresExtensions;
        public volatile JsonArray rreqExtensions;
        public volatile AtomicInteger extState;
    }

    class UpdateFinalStateForTx implements Runnable {

        Persistable obj;
        Context ctx;
        int count;

        UpdateFinalStateForTx(Persistable obj, int count, Context ctx) {
            this.obj = obj;
            this.ctx = ctx;
            this.count = count;
        }

        @Override
        public void run() {
            MDC.getMDCAdapter().put("tx", "tx" + obj.getId());
            safeUpdate(obj, count, ctx);
            MDC.getMDCAdapter().clear();
        }
    }
}
