package com.modirum.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JsonData {
    private static final Logger LOGGER = LogManager.getLogger(JsonData.class.getName());
    private static ObjectMapper objMapper = new ObjectMapper();

    public static Map<String, Object> readFromFile(String fullFilePath){
        try {
            return objMapper.readValue(new File(fullFilePath), new TypeReference<Map<String, Object>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("[ERROR] unable to read JSON from file: " + fullFilePath);
        }
        return null;
    }

    public static Map<String, Object> readFromString(String jsonString){
        try {
            return objMapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {});
        } catch (IOException e) {
            //e.printStackTrace();
            LOGGER.error("[ERROR] unable to convert to JSON: " + jsonString);
        }
        return new HashMap<>();
    }

    public static String getAsString(Map<String, Object> jsonMap){
        try {
            String jsonString = objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonMap);
            //LOGGER.debug("getAsString: " + jsonString);
            return jsonString;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Map<String, Object> addOrUpdateField(Map<String, Object> jsonMap, String fieldName, Object fieldValue){
        jsonMap.put(fieldName, fieldValue);
        return jsonMap;
    }

    public static Object getField(Map<String, Object> jsonMap, String fieldName){
        return jsonMap.get(fieldName);
    }

    public static Map<String, Object> addOrUpdateChildField(Map<String, Object> jsonMap, String fieldName, String childFieldName, Object childFieldValue){
        if(jsonMap == null) return null;

        Map<String, Object> childMap;
        if(jsonMap.get(fieldName) instanceof Map.Entry){
            childMap = (Map<String, Object>) getField(jsonMap, fieldName);
            addOrUpdateField(childMap, childFieldName, childFieldValue);
            addOrUpdateField(jsonMap, childFieldName, childMap);
        }
        return jsonMap;
    }

    public static Map<String, Object> removeField(Map<String, Object> jsonMap, String fieldName){
        if(jsonMap == null) return null;

        jsonMap.remove(fieldName);
        return jsonMap;
    }

    public static Map<String, Object> removeChildField(Map<String, Object> jsonMap, String fieldName, String childFieldName){
        if(jsonMap == null) return null;

        Map<String, Object> childMap;
        if(jsonMap.get(fieldName) instanceof Map.Entry){
            childMap = (Map<String, Object>) getField(jsonMap, fieldName);
            removeField(childMap, childFieldName);
            addOrUpdateField(jsonMap, childFieldName, childMap);
        }
        return jsonMap;
    }

    public static Object getChildField(Map<String, Object> jsonMap, String fieldName, String childFieldName){
        if(jsonMap == null) return null;

        Map<String, Object> childMap;
        if(jsonMap.get(fieldName) instanceof Map.Entry){
            childMap = (Map<String, Object>) jsonMap.get(fieldName);
            return getField(childMap, childFieldName);
        }
        return null;
    }

    public static String duplicateField(Map<String, Object> jsonMap, String fieldName){
        if(jsonMap == null) return "";

        // force add duplicate field using String. Duplicate fields are not supported by Map class
        String originalString = getAsString(jsonMap);
        String updatedString = "";

        Map<String, Object> field = new HashMap<>();
        addOrUpdateField(field, fieldName, getField(jsonMap, fieldName));
        Object fieldValue = getField(jsonMap, fieldName);

        LOGGER.info("DUPLICATE FIELD: " + fieldName + " : " + fieldValue.toString());

        updatedString = originalString.substring(0, originalString.length()-2)
                .concat(",")
                .concat(getAsString(field).substring(1));

        LOGGER.info("UPDATED STRING: " + updatedString);
        return updatedString;
    }
}
