package com.modirum.utilities;

import java.io.File;

public class FileUtil {

    public static boolean isWindowsOS(){
        if (System.getProperty("os.name").contains("Windows")) {
            return true;
        }
        return false;
    }

    public static boolean isMacOS(){
        if (System.getProperty("os.name").contains("Mac")) {
            return true;
        }
        return false;
    }

    public static boolean isUnixOS(){
        if (System.getProperty("os.name").contains("Unix")) {
            return true;
        }
        return false;
    }

    public static String getTestResourceFilePath(String resourceDir, String fileName){
        return concatStringWithDelimeter(File.separator, System.getProperty("user.dir"), "src", "test", "resources", resourceDir, fileName);
    }

    private static String concatStringWithDelimeter(String delimeter, String ... path){
        String fullString = "";
        for(String s : path){
            fullString = fullString.concat(s);
            fullString = fullString.concat(delimeter);
        }
        return fullString.substring(0, fullString.length() - 1);
    }

    public static String getFullFilePath(String ... path){
        return concatStringWithDelimeter(File.separator, path);
    }

}
