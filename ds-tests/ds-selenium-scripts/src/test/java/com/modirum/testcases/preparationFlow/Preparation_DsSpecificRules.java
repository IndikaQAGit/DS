package com.modirum.testcases.preparationFlow;

import com.modirum.models.DeviceChannel;
import com.modirum.models.MessageCategory;
import com.modirum.models.DsEmulatorType;
import com.modirum.pages.dsemulator.preq.PreqEmulator;
import com.modirum.pages.dsemulator.preq.PreqEmulatorFactory;
import com.modirum.reports.ITestReporter;
import com.modirum.testcases.AbstractProtocolTest;
import com.modirum.testcases.testdata.TestCommon_DataProviders;
import com.modirum.utilities.MessageBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Preparation_DsSpecificRules extends AbstractProtocolTest {
    private static Logger LOGGER;
    private String m_testClassName;

    MessageBuilder defaultPreq;
    private String emulatorUrl;
    private DsEmulatorType emulatorType;

    @Parameters({"targetDS", "testSet", "emulatorType"})
    @BeforeClass(alwaysRun = true)
    public void setUp(ITestContext context, String targetDS, String testSet, String emulatorType){
        String testContextName = context.getName();
        String[] p = context.getAllTestMethods()[0].getInstance().getClass().getSimpleName().split("_");
        m_testClassName = testContextName + "_" + p[p.length-1];

        LOGGER = LogManager.getLogger(m_testClassName);

        switch(emulatorType){
            case "HTTPCLIENT":
                this.emulatorType = DsEmulatorType.HTTPCLIENT;
                break;
            case "WEBPAGE" :
            default:
                this.emulatorType = DsEmulatorType.WEBPAGE;
                break;
        }

        context.setAttribute("targetDS", targetDS);
        context.setAttribute("testSet", testSet);

        initTestFlowFields();
        initDefaultMessages();
        setTestContext(context);
        reporter = (ITestReporter) context.getAttribute("testReporter");
        // Get base URLs
        //emulatorUrl = String.format("%s%s%s", testEnv, configProps.get("url.dsemulator.base"), configProps.get("url.dsemulator.preq"));
        emulatorUrl = testEnv;
    }

    protected void initTestFlowFields(){
        deviceChannel = DeviceChannel.NA;
        messageCategory = MessageCategory.NA;
    }

    protected void initDefaultMessages(){
        defaultPreq = generateMessage("PReq");
    }

    protected void setTestContext(ITestContext context){
        context.setAttribute("requiredPreqFields", defaultPreq.getRequiredFields());
    }

    @Test(enabled = true,
            description = "Test threeDSServerOperatorID",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_PREQ_ThreeDSServerOperatorID"
    )
    public void testThreeDSServerOperatorID(String operatorID, String expectedMessageType){
        PreqEmulator preqEmulator = PreqEmulatorFactory.getPreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder preq = getMessageForNewTransaction(this.defaultPreq);

        preq.addOrUpdateField("threeDSServerOperatorID", operatorID);

        String preqString = preq.toString();
        reporter.logInfo("preq: " + preqString);
        preqEmulator.setPreq(preqString);
        preqEmulator.sendPreq();

        String responseString = preqEmulator.waitForDsPres();

        LOGGER.debug("PRES/ERRO responseString: " + responseString);
        reporter.logInfo("PRES/ERRO responseString: " + responseString);

        // ASSERT that response is a JSON
        Assert.assertTrue(responseString.startsWith("{"), "ACTUAL RESPONSE (not JSON): " + responseString);

        // ASSERTIONS
        if(expectedMessageType.equals("PRes")) {
            assertThatResponseIsPRes(responseString, preq);
        } else {
            assertThatResponseIsErro303(responseString);
        }
    }
}
