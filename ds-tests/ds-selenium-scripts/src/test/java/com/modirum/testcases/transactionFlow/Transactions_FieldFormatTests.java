package com.modirum.testcases.transactionFlow;

import com.modirum.models.DeviceChannel;
import com.modirum.models.DsEmulatorType;
import com.modirum.models.ErrorCode;
import com.modirum.models.MessageCategory;
import com.modirum.pages.dsemulator.areqrreq.AreqRreqEmulator;
import com.modirum.pages.dsemulator.areqrreq.AreqRreqEmulatorFactory;
import com.modirum.reports.ITestReporter;
import com.modirum.testcases.AbstractProtocolTest;
import com.modirum.testcases.testdata.TestFormats_DataProviders;
import com.modirum.utilities.MessageBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Transactions_FieldFormatTests extends AbstractProtocolTest {
    private static Logger LOGGER;
    private String m_testClassName;

    MessageBuilder defaultAreq;
    MessageBuilder defaultAres;
    MessageBuilder defaultRreq;
    MessageBuilder defaultRres;

    private String emulatorUrl;
    private DsEmulatorType emulatorType;

    @Parameters({"deviceChannel", "messageCategory", "targetDS", "emulatorType"})
    @BeforeClass(alwaysRun = true)
    public void setUp(ITestContext context, String deviceChannel, String messageCategory, String targetDS, String emulatorType){
        String testContextName = context.getName();
        String[] p = context.getAllTestMethods()[0].getInstance().getClass().getSimpleName().split("_");
        m_testClassName = testContextName + "_" + p[p.length-1];

        LOGGER = LogManager.getLogger(m_testClassName);

        switch(emulatorType){
            case "HTTPCLIENT":
                this.emulatorType = DsEmulatorType.HTTPCLIENT;
                break;
            case "WEBPAGE" :
            default:
                this.emulatorType = DsEmulatorType.WEBPAGE;
                break;
        }

        this.deviceChannel = DeviceChannel.getDeviceChannelFromValue(deviceChannel);
        this.messageCategory = MessageCategory.getMessageCategoryFromValue(messageCategory);

        initDefaultMessages();
        setTestContext(context);
        reporter = (ITestReporter) context.getAttribute("testReporter");

        // Get base URLs
        //emulatorUrl = String.format("%s%s%s", testEnv, configProps.get("url.dsemulator.base"), configProps.get("url.dsemulator.areqRreq"));
        emulatorUrl = testEnv;
    }

    protected void initDefaultMessages(){
        defaultAreq = generateMessage("AReq");
        defaultAres = generateMessage("ARes");
        defaultRreq = generateMessage("RReq");
        defaultRres = generateMessage("RRes");
    }

    protected void setTestContext(ITestContext context){
        context.setAttribute("deviceChannel",this.deviceChannel);
        context.setAttribute("messageCategory", this.messageCategory);
        context.setAttribute("requiredAreqFields", defaultAreq.getRequiredFields());
        context.setAttribute("conditionalAreqFields", defaultAreq.getConditionalFields());
        context.setAttribute("requiredRreqFields", defaultRreq.getRequiredFields());
        context.setAttribute("requiredRresFields", defaultRres.getRequiredFields());
    }

    private void testAREQ_FieldFormat(String field, String value, ErrorCode errorCode) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);

        areq.addOrUpdateFieldWithDefaultValue("threeDSServerTransID");

        areq.addOrUpdateField(field, value); // add the conditional field

        String areqString = areq.toString();
        reporter.logInfo("areq: " + areqString);
        areqEmulator.set3dssAreq(areqString);

        areqEmulator.generateAcsAresFrictionless();
        areqEmulator.cacheAcsAres();

        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(errorCode == null){
            assertThatResponseIsARes(responseString, "Y", "");
        } else if(errorCode.equals(ErrorCode.INVALID_FORMAT)) {
            assertThatResponseIsErro203(responseString, field);
        }
    }

    @Test(enabled = true,
            description = "TODO: Tests threeDSReqAuthMethodInd field format in AReq",
            dataProviderClass = TestFormats_DataProviders.class,
            dataProvider = "DP_AREQ_CONDITIONAL_FIELDS_threeDSReqAuthMethodInd"
    )
    public void testAREQ_threeDSReqAuthMethodInd(String value, ErrorCode errorCode) {
        testAREQ_FieldFormat("threeDSReqAuthMethodInd", value, errorCode);
    }

    @Test(enabled = true,
            description = "TODO: Tests threeDSReqAuthMethodInd field format in AReq",
            dataProviderClass = TestFormats_DataProviders.class,
            dataProvider = "DP_AREQ_CONDITIONAL_FIELDS_acctType"
    )
    public void testAREQ_acctType(String value, ErrorCode errorCode) {
        testAREQ_FieldFormat("acctType", value, errorCode);
    }

}
