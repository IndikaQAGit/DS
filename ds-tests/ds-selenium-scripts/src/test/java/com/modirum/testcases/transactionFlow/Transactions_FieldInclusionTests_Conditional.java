package com.modirum.testcases.transactionFlow;

import com.modirum.models.DeviceChannel;
import com.modirum.models.DsEmulatorType;
import com.modirum.models.ErrorCode;
import com.modirum.models.MessageCategory;
import com.modirum.pages.dsemulator.areqrreq.AreqRreqEmulator;
import com.modirum.pages.dsemulator.areqrreq.AreqRreqEmulatorFactory;
import com.modirum.reports.ITestReporter;
import com.modirum.testcases.AbstractProtocolTest;
import com.modirum.testcases.testdata.TestConditions_DataProviders;
import com.modirum.utilities.MessageBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Transactions_FieldInclusionTests_Conditional extends AbstractProtocolTest {
    private static Logger LOGGER;
    private String m_testClassName;
    private DsEmulatorType emulatorType;

    MessageBuilder defaultAreq;
    MessageBuilder defaultAres;
    MessageBuilder defaultRreq;
    MessageBuilder defaultRres;

    private String emulatorUrl;

    @Parameters({"deviceChannel", "messageCategory", "targetDS", "testSet", "emulatorType"})
    @BeforeClass(alwaysRun = true)
    public void setUp(ITestContext context, String deviceChannel, String messageCategory, String targetDS, String testSet, String emulatorType){
        String testContextName = context.getName();
        String[] p = context.getAllTestMethods()[0].getInstance().getClass().getSimpleName().split("_");
        m_testClassName = testContextName + "_" + p[p.length-1];

        LOGGER = LogManager.getLogger(m_testClassName);

        switch(emulatorType){
            case "HTTPCLIENT":
                this.emulatorType = DsEmulatorType.HTTPCLIENT;
                break;
            case "WEBPAGE" :
            default:
                this.emulatorType = DsEmulatorType.WEBPAGE;
                break;
        }

        this.deviceChannel = DeviceChannel.getDeviceChannelFromValue(deviceChannel);
        this.messageCategory = MessageCategory.getMessageCategoryFromValue(messageCategory);
        context.setAttribute("targetDS", targetDS);
        context.setAttribute("testSet", testSet);

        initDefaultMessages();
        setTestContext(context);
        reporter = (ITestReporter) context.getAttribute("testReporter");

        // Get base URLs
        //emulatorUrl = String.format("%s%s%s", testEnv, configProps.get("url.dsemulator.base"), configProps.get("url.dsemulator.areqRreq"));
        emulatorUrl = testEnv;
    }

    protected void initDefaultMessages(){
        defaultAreq = generateMessage("AReq");
        defaultAres = generateMessage("ARes");
        defaultRreq = generateMessage("RReq");
        defaultRres = generateMessage("RRes");
    }

    protected void setTestContext(ITestContext context){
        context.setAttribute("deviceChannel",this.deviceChannel);
        context.setAttribute("messageCategory", this.messageCategory);
        context.setAttribute("requiredAreqFields", defaultAreq.getRequiredFields());
        context.setAttribute("optionalAreqFields", defaultAreq.getOptionalFields());
        context.setAttribute("conditionalAreqFields", defaultAreq.getConditionalFields());
        context.setAttribute("requiredAresFields", defaultAres.getRequiredFields());
        context.setAttribute("optionalAresFields", defaultAres.getOptionalFields());
        context.setAttribute("conditionalAresFields", defaultAres.getConditionalFields());
        context.setAttribute("requiredRreqFields", defaultRreq.getRequiredFields());
        context.setAttribute("optionalRreqFields", defaultRreq.getOptionalFields());
        context.setAttribute("conditionalRreqFields", defaultRreq.getConditionalFields());
        context.setAttribute("requiredRresFields", defaultRres.getRequiredFields());
        context.setAttribute("optionalRresFields", defaultRres.getOptionalFields());
        context.setAttribute("conditionalRresFields", defaultRres.getConditionalFields());
    }

    @Override
    @BeforeMethod
    public void BeforeMethod(Method method, Object[] testData, ITestContext ctx) {
        String group = "";
        if(!deviceChannel.equals(DeviceChannel.NA))
            group = "(" + group + deviceChannel.alias();
        if(!messageCategory.equals(MessageCategory.NA))
            group = group + "-" + messageCategory.alias() + ") ";

        if (testData.length > 3) {
            if(testData[1] == null){
                testName.set(group + method.getName() + " [ " + testData[0] + " ] : " + testData[2] + " ");
                ctx.setAttribute("testName", testName.get());
            } else {
                testName.set(group + method.getName() + " [ " + testData[0] + "=" + testData[1] + " ] : " + testData[2] + " ");
                ctx.setAttribute("testName", testName.get());
            }
        } else {
            testName.set(group + method.getName());
            ctx.setAttribute("testName", testName.get());
        }
    }

    /*********************************************************************************
     * AREQ
     *********************************************************************************/
    private void testAREQ_ConditionalField(String conditionField, Object conditionValue, String [] fields, String testField, Object testFieldValue, ErrorCode errorCode) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);

        areq.addOrUpdateFieldWithDefaultValue("threeDSServerTransID");

        // Set up condition
        if(conditionValue != null) {
            areq.addOrUpdateField(conditionField, conditionValue);
        }

        // Set conditional fields
        for (String field : fields){
            areq.addOrUpdateFieldWithDefaultValue(field);
        }

        // Set testField
        if(testFieldValue == null) {
            areq.removeField(testField);
        } else {
            areq.addOrUpdateField(testField, testFieldValue);
        }

        String areqString = areq.toString();
        reporter.logInfo("areq: " + areqString);
        areqEmulator.set3dssAreq(areqString);

        areqEmulator.generateAcsAresFrictionless();
        areqEmulator.cacheAcsAres();

        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(errorCode == null){
            assertThatResponseIsARes(responseString, "Y", "");
        } else if(errorCode.equals(ErrorCode.INVALID_FORMAT)) {
            assertThatResponseIsErro203(responseString, testField);
        } else if(errorCode.equals(ErrorCode.MISSING_REQUIRED_ELEMENT)) {
            assertThatResponseIsErro201(responseString, testField);
        }
    }

    @Test(enabled = true,
            description = "Tests missing conditionally required field inclusion in AReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_AREQ_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testAREQ_MissingConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testAREQ_ConditionalField(field, value, conditionalFields, testField, null, ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally required field inclusion in AReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_AREQ_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testAREQ_EmptyConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testAREQ_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests missing conditionally optional field inclusion in AReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_AREQ_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testAREQ_MissingConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testAREQ_ConditionalField(field, value, conditionalFields, testField, null, null);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally optional field inclusion in AReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_AREQ_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testAREQ_EmptyConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testAREQ_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.INVALID_FORMAT);
    }

    /*********************************************************************************
     * ARES
     *********************************************************************************/
    private void testARES_ConditionalField(String conditionField, Object conditionValue, String [] fields, String testField, Object testFieldValue, ErrorCode errorCode) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);
        areq.addOrUpdateField("threeDSServerTransID", areq.generateAutoValue("UUID"));
        reporter.logInfo("areq: " + areq.toString());
        areqEmulator.set3dssAreq(areq.toString());

        MessageBuilder ares = new MessageBuilder(defaultAres);
        ares.addOrUpdateField("threeDSServerTransID", areq.getField("threeDSServerTransID"));

        // Set up condition
        if(conditionValue != null) {
            ares.addOrUpdateField(conditionField, conditionValue);
        }

        // Set conditional fields
        for (String field : fields){
            ares.addOrUpdateFieldWithDefaultValue(field);
        }

        // Set testField
        if(testFieldValue == null) {
            ares.removeField(testField);
        } else {
            ares.addOrUpdateField(testField, testFieldValue);
        }

        reporter.logInfo("ares: " + ares.toString());
        areqEmulator.setAcsAres(ares.toString());
        areqEmulator.cacheAcsAres();
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(errorCode == null){
            assertThatResponseIsARes(responseString, "Y", "");
        } else if(errorCode.equals(ErrorCode.INVALID_FORMAT)) {
            assertThatResponseIsErro203(responseString, testField);
        } else if(errorCode.equals(ErrorCode.MISSING_REQUIRED_ELEMENT)) {
            assertThatResponseIsErro201(responseString, testField);
        }
    }

    @Test(enabled = true,
            description = "Tests missing conditionally required field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testARES_MissingConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testARES_ConditionalField(field, value, conditionalFields, testField, null, ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally required field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testARES_EmptyConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testARES_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests missing conditionally optional field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testARES_MissingConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testARES_ConditionalField(field, value, conditionalFields, testField, null, null);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally optional field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testARES_EmptyConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testARES_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.INVALID_FORMAT);
    }

    /*********************************************************************************
     * ARES - DECOUPLED
     *********************************************************************************/

    private void testARES_Decoupled_ConditionalField(String conditionField, Object conditionValue, String [] fields, String testField, Object testFieldValue, ErrorCode errorCode) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);
        areq.addOrUpdateField("threeDSServerTransID", areq.generateAutoValue("UUID"));
        reporter.logInfo("areq: " + areq.toString());
        areqEmulator.set3dssAreq(areq.toString());

        MessageBuilder ares = new MessageBuilder(defaultAres);
        ares.addOrUpdateField("threeDSServerTransID", areq.getField("threeDSServerTransID"));

        ares.addOrUpdateField("transStatus", "D");

        // Set up condition
        if(conditionValue != null) {
            ares.addOrUpdateField(conditionField, conditionValue);
        }

        // Set conditional fields
        for (String field : fields){
            ares.addOrUpdateFieldWithDefaultValue(field);
        }

        // Set testField
        if(testFieldValue == null) {
            ares.removeField(testField);
        } else {
            ares.addOrUpdateField(testField, testFieldValue);
        }

        reporter.logInfo("ares: " + ares.toString());
        areqEmulator.setAcsAres(ares.toString());
        areqEmulator.cacheAcsAres();
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(errorCode == null){
            assertThatResponseIsARes(responseString, "Y", "");
        } else if(errorCode.equals(ErrorCode.INVALID_FORMAT)) {
            assertThatResponseIsErro203(responseString, testField);
        } else if(errorCode.equals(ErrorCode.MISSING_REQUIRED_ELEMENT)) {
            assertThatResponseIsErro201(responseString, testField);
        }
    }

    @Test(enabled = true,
            description = "Tests missing conditionally required field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_DECOUPLED_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testARES_Decoupled_MissingConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testARES_Decoupled_ConditionalField(field, value, conditionalFields, testField, null, ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally required field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_DECOUPLED_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testARES_Decoupled_EmptyConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testARES_Decoupled_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests missing conditionally optional field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_DECOUPLED_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testARES_Decoupled_MissingConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testARES_Decoupled_ConditionalField(field, value, conditionalFields, testField, null, null);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally optional field inclusion in ARes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_ARES_DECOUPLED_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testARES_Decoupled_EmptyConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testARES_Decoupled_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.INVALID_FORMAT);
    }

    /*********************************************************************************
     * RREQ
     *********************************************************************************/
    private void testRREQ_ConditionalField(String conditionField, Object conditionValue, String [] fields, String testField, Object testFieldValue, ErrorCode errorCode) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);

        areq.addOrUpdateFieldWithDefaultValue("threeDSServerTransID");
        String areqString = areq.toString();
        reporter.logInfo("areq: " + areqString);
        areqEmulator.set3dssAreq(areqString);

        areqEmulator.generateAcsAresChallenge();
        areqEmulator.cacheAcsAres();

        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        MessageBuilder ares = assertThatResponseIsARes(responseString, "C", "");

        // if OK, cache rres
        areqEmulator.generate3dssRres();
        areqEmulator.cache3dssRres();
        reporter.logInfo("3DSS rres : auto-generated and cached");

        // Create rreq
        MessageBuilder rreq = new MessageBuilder(defaultRreq);
        rreq.addOrUpdateField("threeDSServerTransID", ares.getField("threeDSServerTransID"));
        rreq.addOrUpdateField("acsTransID", ares.getField("acsTransID"));
        rreq.addOrUpdateField("dsTransID", ares.getField("dsTransID"));

        // Set up condition
        if(conditionValue != null) {
            rreq.addOrUpdateField(conditionField, conditionValue);
        }

        // Set conditional fields
        for (String field : fields){
            rreq.addOrUpdateFieldWithDefaultValue(field);
        }

        // Set testField
        if(testFieldValue == null) {
            rreq.removeField(testField);
        } else {
            rreq.addOrUpdateField(testField, testFieldValue);
        }

        reporter.logInfo("rreq: " + rreq.toString());

        areqEmulator.setAcsRreqJson(rreq.toString());
        areqEmulator.sendAcsRreq();

        responseString = areqEmulator.waitForDsRres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(errorCode == null){
            assertThatResponseIsRRes(responseString, "01");
        } else if(errorCode.equals(ErrorCode.INVALID_FORMAT)) {
            assertThatResponseIsErro203(responseString, testField);
        } else if(errorCode.equals(ErrorCode.MISSING_REQUIRED_ELEMENT)) {
            assertThatResponseIsErro201(responseString, testField);
        }
    }

    @Test(enabled = true,
            description = "Tests missing conditionally required field inclusion in RReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RREQ_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testRREQ_MissingConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testRREQ_ConditionalField(field, value, conditionalFields, testField, null, ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally required field inclusion in RReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RREQ_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testRREQ_EmptyConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testRREQ_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests missing conditionally optional field inclusion in RReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RREQ_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testRREQ_MissingConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testRREQ_ConditionalField(field, value, conditionalFields, testField, null, null);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally optional field inclusion in RReq",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RREQ_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testRREQ_EmptyConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testRREQ_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.INVALID_FORMAT);
    }

    /*********************************************************************************
     * RRES
     *********************************************************************************/
    private void testRRES_ConditionalField(String conditionField, Object conditionValue, String [] fields, String testField, Object testFieldValue, ErrorCode errorCode) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);

        areq.addOrUpdateFieldWithDefaultValue("threeDSServerTransID");
        String areqString = areq.toString();
        reporter.logInfo("areq: " + areqString);
        areqEmulator.set3dssAreq(areqString);

        areqEmulator.generateAcsAresChallenge();
        areqEmulator.cacheAcsAres();

        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        MessageBuilder ares = assertThatResponseIsARes(responseString, "C", "");

        // if OK, cache rres
        MessageBuilder rres = new MessageBuilder(defaultRres);
        rres.addOrUpdateField("threeDSServerTransID", ares.getField("threeDSServerTransID"));
        rres.addOrUpdateField("acsTransID", ares.getField("acsTransID"));
        rres.addOrUpdateField("dsTransID", ares.getField("dsTransID"));

        // Set up condition
        if(conditionValue != null) {
            rres.addOrUpdateField(conditionField, conditionValue);
        }

        // Set conditional fields
        for (String field : fields){
            rres.addOrUpdateFieldWithDefaultValue(field);
        }

        // Set testField
        if(testFieldValue == null) {
            rres.removeField(testField);
        } else {
            rres.addOrUpdateField(testField, testFieldValue);
        }

        areqEmulator.setRresJson(rres.toString());
        areqEmulator.cache3dssRres();
        reporter.logInfo("3DSS rres : " + rres.toString() + "cached");

        // Create rreq
        areqEmulator.generateAcsRreq();
        areqEmulator.sendAcsRreq();

        responseString = areqEmulator.waitForDsRres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(errorCode == null){
            assertThatResponseIsRRes(responseString, "01");
        } else if(errorCode.equals(ErrorCode.INVALID_FORMAT)) {
            assertThatResponseIsErro203(responseString, testField);
        } else if(errorCode.equals(ErrorCode.MISSING_REQUIRED_ELEMENT)) {
            assertThatResponseIsErro201(responseString, testField);
        }
    }

    @Test(enabled = false, // no conditionally required fields in RRes
            description = "Tests missing conditionally required field inclusion in RRes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RRES_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testRRES_MissingConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testRRES_ConditionalField(field, value, conditionalFields, testField, null, ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = false, // no conditionally required fields in RRes
            description = "Tests empty conditionally required field inclusion in RRes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RRES_CONDITIONALLY_REQUIRED_FIELDS"
    )
    public void testRRES_EmptyConditionallyRequired(String field, Object value, String testField, String[] conditionalFields){
        testRRES_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.MISSING_REQUIRED_ELEMENT);
    }

    @Test(enabled = true,
            description = "Tests missing conditionally optional field inclusion in RRes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RRES_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testRRES_MissingConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testRRES_ConditionalField(field, value, conditionalFields, testField, null, null);
    }

    @Test(enabled = true,
            description = "Tests empty conditionally optional field inclusion in RRes",
            dataProviderClass = TestConditions_DataProviders.class,
            dataProvider = "DP_RRES_CONDITIONALLY_OPTIONAL_FIELDS"
    )
    public void testRRES_EmptyConditionallyOptional(String field, Object value, String testField, String[] conditionalFields){
        testRRES_ConditionalField(field, value, conditionalFields, testField, "", ErrorCode.INVALID_FORMAT);
    }
}
