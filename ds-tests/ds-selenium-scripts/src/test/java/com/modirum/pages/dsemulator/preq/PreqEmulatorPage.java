package com.modirum.pages.dsemulator.preq;

import com.modirum.utilities.SelDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PreqEmulatorPage implements PreqEmulator {
    private static final Logger LOGGER = LogManager.getLogger(PreqEmulatorPage.class.getName());
    protected SelDriver selDriver;
    private static final String pageUrl = "/ds-3ds-emulator/preq.html";

    @FindBy(id = "dsEntrypointUrl")             private WebElement txtDsEntrypointUrl;
    @FindBy(id = "preq")                        private WebElement txtPreq;
    @FindBy(id = "pres")                        private WebElement txtDsPres;
    @FindBy(id = "genAreq210PaBrwBtn")          private WebElement btnGeneratePreqPaBrw210;
    @FindBy(id = "sendAreqBtn")                 private WebElement btnSendPreq;

    public PreqEmulatorPage(SelDriver selDriver, String baseURL){
        this.selDriver = selDriver;
        selDriver.openUrl(baseURL + pageUrl);
        PageFactory.initElements(selDriver.getDriver(), this);
    }

    public PreqEmulatorPage(SelDriver selDriver){
        this.selDriver = selDriver;
        PageFactory.initElements(selDriver.getDriver(), this);
    }

    /***************************************************
     * Getters
     ***************************************************/
    public String getDsEntrypointUrl(){ return selDriver.getValue(txtDsEntrypointUrl); }
    public String getPreq(){ return selDriver.getValue(txtPreq); }
    public String getDsPres(){ return selDriver.getValue(txtDsPres); }

    /***************************************************
     * Setters / Clickers
     ***************************************************/
    public PreqEmulatorPage setDsEntrypointUrl(String dsEntrypointUrl){ selDriver.setInputField(txtDsEntrypointUrl, dsEntrypointUrl); return this; }
    public PreqEmulatorPage setPreq(String preq){ selDriver.setInputField(txtPreq, preq); return this; }
    public void generatePreqPaBrw210(){ selDriver.click(btnGeneratePreqPaBrw210); }
    public void sendPreq(){ selDriver.click(btnSendPreq); }

    /***************************************************
     * Common Methods
     ***************************************************/
    public boolean waitForDsResponseJson(WebElement el){
        try {
            int timeout = 15;
            WebDriverWait wait = new WebDriverWait(selDriver.getDriver(), timeout);
            wait.until(ExpectedConditions.textToBePresentInElementValue(el, "{"));
        }catch(TimeoutException e){
            LOGGER.warn("Expected condition failed: waiting for condition to not be valid: text ('{') to be present in element");
            LOGGER.warn("ACTUAL VALUE: " + selDriver.getValue(el));
            return false;
        }
        return true;
    }

    public String waitForDsPres(){
        waitForDsResponseJson(txtDsPres);
        return getDsPres();
    }
}
