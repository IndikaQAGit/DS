package com.modirum.models;

public enum DsEmulatorType {
    HTTPCLIENT,
    WEBPAGE
}
