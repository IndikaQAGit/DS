package com.modirum.models;

public enum TransactionFlow {
    FRICTIONLESS,
    CHALLENGE;
}
