package com.modirum.ds.jce;

import com.modirum.ds.utils.Base64;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;

/**
 * Modirum RSA public key implementation for custom JCE support.
 */
public final class MdRSAPublicKey implements RSAPublicKey {

    private static final Logger LOG = LoggerFactory.getLogger(MdRSAPublicKey.class);

    private final BigInteger modulus;
    private final BigInteger exponent;

    public MdRSAPublicKey(final byte[] key) throws IOException {
        try {
            ASN1InputStream is = new ASN1InputStream(key);
            ASN1Sequence seq = (ASN1Sequence) is.readObject();
            ASN1Integer mod = (ASN1Integer) seq.getObjectAt(0);
            ASN1Integer exp = (ASN1Integer) seq.getObjectAt(1);
            modulus = mod.getPositiveValue();
            exponent = exp.getValue();
        } catch (IOException e) {
            LOG.error("Error creating MdRSAPublicKey.", e);
            throw e;
        }
    }

    /**
     * Creates RSA public key from base64-encoded bytes.
     *
     * @param base64key base64 string
     * @return Instance of public key.
     * @throws IOException in case of read errors.
     */
    public static MdRSAPublicKey fromString(String base64key) throws IOException {
        return new MdRSAPublicKey(Base64.decode(base64key));
    }

    @Override
    public BigInteger getModulus() {
        return modulus;
    }

    @Override
    public BigInteger getPublicExponent() {
        return exponent;
    }

    @Override
    public String getAlgorithm() {
        return "RSA";
    }

    @Override
    public String getFormat() {
        return "X.509";
    }

    @Override
    public byte[] getEncoded() {
        ASN1Encodable asn1publicKey =
                new org.bouncycastle.asn1.pkcs.RSAPublicKey(getModulus(), getPublicExponent()).toASN1Primitive();
        AlgorithmIdentifier algorithmIdentifier =
                new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE);

        try {
            SubjectPublicKeyInfo info = new SubjectPublicKeyInfo(algorithmIdentifier, asn1publicKey);
            return info.getEncoded(ASN1Encoding.DER);
        } catch (IOException e) {
            LOG.error("Error in MdRSAPublicKey.getEncoded.", e);
            return null;
        }
    }
}
