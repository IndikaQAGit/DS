package com.modirum.ds.hsm;

import javax.smartcardio.Card;

import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;

public class ToolKit {

    public static void main(String[] args) throws Exception {
        new ToolKit().mainImpl();
    }

    protected boolean doHSMMenu(final HSMDevice hsmDevice) throws Exception {
        System.out.print(generateHsmMenu(hsmDevice));
        String migi = getPrompt("Your choice: ");
        boolean isAcos = hsmDevice instanceof ACOS5Service;
        if ("x".equals(migi)) {
            return false;
        } else if ("p".equals(migi)) {
            encryptConfigPropertyPassword(hsmDevice);
            return true;
        }

        if (isAcos) {
            ACOS5Service acos5Service = (ACOS5Service) hsmDevice;
            if ("M".equals(migi)) {
                installACOS5AESKey(acos5Service);
                return true;
            } else if ("RUP".equals(migi)) {
                resetAcos5PinUsingPuk(acos5Service);
                return true;
            } else if ("W".equals(migi)) {
                wipeOutAcos5MasterKey(acos5Service);
                return true;
            } else if ("CUP".equals(migi)) {
                setAcos5LocalPin(acos5Service);
                return true;
            } else if ("RUP".equals(migi)) {
                resetAcos5PinUsingPuk(acos5Service);
                return true;
            } else if ("CGP".equals(migi)) {
                setAcos5GlobalPinOrPuk(acos5Service);
                return true;
            } else if ("SER".equals(migi)) {
                showAcos5SerialNumber(acos5Service);
                return true;
            } else if ("MCV".equals(migi)) {
                showAcos5MasterKeyCheckValue(acos5Service);
                return true;
            }
        }
        return true;
    }

    protected void encryptConfigPropertyPassword(final HSMDevice hsms) throws Exception {
        String password = getPrompt("Enter the database password to be encrypted: ");

        String encPassword = CryptoServiceBase.encryptConfigPassword(password, hsms);

        System.out.println("Encrypted password is:");
        System.out.println("vep://" + encPassword);
        String decryptedPassword = CryptoServiceBase.decryptConfigPassword(encPassword, hsms);
        if (decryptedPassword.equals(password)) {
            System.out.println("Password encryption successful");
            System.out.println("Decrypted password is: '" + decryptedPassword + "'");
        } else {
            System.out.println("Password encryption failed, do not use this value");
            System.out.println("Decrypted password is not equal to original:");
            System.out.println(decryptedPassword);
        }
    }

    protected HSMDevice initHSM() throws Exception {
        String hsmDeviceClassName = getPrompt("Enter HSM device impl. class " + "(default: " + ACOS5Service.class.getName() + "):");
        String hsmconf = getPrompt("Enter HSM config string (example 'host=127.0.0.1;port=1500' or 'pin=1234'):");
        System.out.println("Intializing HSM device..");

        HSMDevice hsmDevice;
        if (Misc.isNullOrEmpty(hsmDeviceClassName)) {
            hsmDeviceClassName = ACOS5Service.class.getName();
        }
        hsmDevice = (HSMDevice) Class.forName(hsmDeviceClassName).newInstance();

        if (Misc.isNullOrEmpty(hsmDevice)) {
            throw new RuntimeException("Error initializing HSM device, HSM security model cant be established..");
        }

        hsmDevice.initializeHSM(hsmconf);
        return hsmDevice;
    }

    protected boolean installACOS5AESKey(final ACOS5Service acos5Service) throws Exception {
        System.out.println("Start installation of AES master key into ACOS5 device");
        String part1 = null;
        String part2 = null;
        byte[] newKey = null;
        String genOrEnter;

        while (true) {
            genOrEnter = getPrompt("Doy want to intall existing key or generate new key e/n?");
            if (("e".equals(genOrEnter) || "n".equals(genOrEnter))) {
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            getPrompt("Attention Key custodian " + (i + 1) + " ask for privacy, press enter when privacy established");

            while ("e".equals(genOrEnter)) {
                String part = getPrompt("Key custodian " + (i + 1) + " enter your key part 32 hex chars: ");
                part = part.trim();
                try {
                    if (part.length() == 32 && HexUtils.fromString(part).length == 16) {
                        if (i == 0) {
                            part1 = part;
                        }
                        if (i == 1) {
                            part2 = part;
                        }

                        getPrompt("Value ok, press enter to continue..");
                        break;
                    }
                } catch (Exception e) {
                    System.out.println("Invalid data entered");
                    e.printStackTrace();
                }
            }

            if ("n".equals(genOrEnter)) {
                if (newKey == null) {
                    newKey = KeyServiceBase.genKey(32);
                    part1 = HexUtils.toString(newKey).substring(0, 32);
                    part2 = HexUtils.toString(newKey).substring(32);
                }

                System.out.println("Keypart " + (i + 1) + ": " + (i == 0 ? part1 : part2));
                getPrompt("Key custodian " + (i + 1) + " write down your key part, press enter when done");
            }

            for (int ccc = 0; ccc < 10000; ccc++) {
                System.out.println();
            }

        }

        String keyOk = null;
        if ("e".equals(genOrEnter)) {
            System.out.println("Attention Key custodians 1 and 2 verify, the key check value!");
            System.out.println("Key check value of key formed from entered parts is "
                    + KeyServiceBase.calculateAESKeyCheckValue(HexUtils.fromString(part1 + part2)));

            keyOk = getPrompt("Is this correct key check value y/n?");
            if (!"y".equals(keyOk)) {
                return false;
            }
        }
        if ("n".equals(genOrEnter)) {
            System.out.println("Attention Key custodians 1 and 2 write down, the key check value!");
            System.out.println("Key check value of key formed from generated parts is "
                    + KeyServiceBase.calculateAESKeyCheckValue(HexUtils.fromString(part1 + part2)));

        }

        if (part1 != null && part2 != null) {

            byte[] keyData = HexUtils.fromString(part1 + part2);
            String kcv = KeyServiceBase.calculateAESKeyCheckValue(keyData);

            while (true) {

                String cardSerial = HexUtils.toString(acos5Service.getCardSerial());
                getPrompt("Enter the ACOS5 card to cardreader and when ready press enter");
                char[] pin = getPassword("Enter PIN for keyfile:");
                acos5Service.setPin(pin);

                acos5Service.installAESKey(keyData);
                System.out.println("New master key with checkvalue " + kcv + " to ACOS5 card with id " + cardSerial + " was installed ");

                String kcv2 = acos5Service.getMasterKeyCheckValue();
                if (!kcv2.equals(kcv)) {
                    throw new RuntimeException("Device failed to produce correct key check value, expected " + kcv + " got "
                            + kcv2);
                }

                String y = getPrompt(
                        "Do You want to install same key to another card, if yes remove current card and insert new card and enter y/n: ");
                if (!"y".equals(y)) {
                    break;
                }

            }

            return true;
        }

        return false;
    }

    protected String generateHsmMenu(final HSMDevice hsmDevice) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        if (hsmDevice instanceof ACOS5Service) {
            sb.append("M Install master key into ACOS5-64\n");
            sb.append("W Wipe out master key in ACOS5-64\n");
            sb.append("CUP Change ACOS5-64 user/local PIN\n");
            sb.append("RUP Reset ACOS5-64 user/local PIN using PUK\n");
            sb.append("CGP Change ACOS5-64 global PIN/PUK\n");
            sb.append("SER Display ACOS5-64 inserted card serial number\n");
            sb.append("MCV Display ACOS5-64 inserted card master key check value\n");
        }
        sb.append("p To encrypt a config/database password\n");
        sb.append("x Exit\n");
        return sb.toString();
    }

    public static char[] getPassword(String prompt) throws Exception {
        System.out.print(prompt);
        return System.console().readPassword();
    }


    public static String getPrompt(String prompt) throws Exception {
        System.out.print(prompt);
        java.io.BufferedReader br = new java.io.LineNumberReader(new java.io.InputStreamReader(System.in));
        return br.readLine();
    }

    public void mainImpl() throws Exception {
        HSMDevice hsmDevice = initHSM();
        while (doHSMMenu(hsmDevice)) {
        }
    }

    protected void setAcos5GlobalPinOrPuk(final ACOS5Service acos5Service) throws Exception {
        System.out.println("Change global PIN/PUK");
        String curPin = getPrompt("Enter current PUK:");
        String newPin = getPrompt("Enter new PUK:");

        Card card = null;
        try {
            card = acos5Service.initCardConnection();
            acos5Service.selectMF(card);
            acos5Service.selectFile(HexUtils.fromString("4100"), card);
            acos5Service.verify((byte) 0x01, curPin.getBytes("UTF-8"), card);
            acos5Service.changePin((byte) 0x01, newPin.getBytes("UTF-8"), card);
            System.out.println("Change global PIN/PUK success");
        } catch (Exception e) {
            System.out.println("Change global PIN/PUK failed " + e);
        } finally {
            acos5Service.disposeCardConnection(card);
        }
    }

    protected void setAcos5LocalPin(final ACOS5Service acos5Service) throws Exception {
        System.out.println("Change user PIN");
        String curPin = getPrompt("Enter current PIN:");
        String newPin = getPrompt("Enter new PIN:");

        Card card = null;
        try {
            card = acos5Service.initCardConnection();
            acos5Service.selectMF(card);
            acos5Service.selectFile(HexUtils.fromString("4100"), card);
            acos5Service.verify((byte) 0x81, curPin.getBytes("UTF-8"), card);
            acos5Service.changePin((byte) 0x81, newPin.getBytes("UTF-8"), card);
            System.out.println("Change user PIN success");
        } catch (Exception e) {
            System.out.println("Change user PIN failed " + e);
        } finally {
            acos5Service.disposeCardConnection(card);
        }
    }

    protected void showAcos5MasterKeyCheckValue(final ACOS5Service acos5Service) {
        try {
            String cv = acos5Service.getMasterKeyCheckValue();
            System.out.println("Inserted Card masterkey check value is: " + cv);
        } catch (Exception e) {
            System.out.println("Key check value calculation failed (may be card without key loaded) " + e);
        }
    }

    protected void showAcos5SerialNumber(final ACOS5Service acos5Service) {
        try {
            byte[] ser = acos5Service.getCardSerial();
            System.out.println("Inserted Card serial is: " + HexUtils.toString(ser));
        } catch (Exception e) {
            System.out.println("Get ACOS5-64 serial failed " + e);
        }
    }

    protected void resetAcos5PinUsingPuk(final ACOS5Service hsms) throws Exception {
        while (true) {
            System.out.println("Reset user PIN using PUK");
            String puk = getPrompt("Enter PUK:");
            if (puk.length() < 4 || puk.length() > 8) {
                System.out.println("Invalid PUK length, must be 4..8");
                continue;
            }

            String newUsPin = getPrompt("Enter new user PIN:");
            if (newUsPin.length() < 4 || newUsPin.length() > 8) {
                System.out.println("Invalid PIN length, must be 4..8");
                continue;
            }

            Card card = null;
            try {
                card = hsms.initCardConnection();
                hsms.selectMF(card);
                hsms.selectFile(HexUtils.fromString("4100"), card);
                hsms.selectFile(HexUtils.fromString("4101"), card); // EF1
                hsms.verify((byte) 0x01, puk.getBytes("UTF-8"), card);
                hsms.updateRecord(
                        HexUtils.fromString("81" + // pin id
                                "88" + // counters
                                ("0" + newUsPin.length()) + // pin len
                                HexUtils.toString(newUsPin.getBytes("UTF-8")) + // pin
                                "88" + // reset counter
                                ("0" + puk.length()) + // puk len
                                HexUtils.toString(puk.getBytes("UTF-8")) // puk
                        ), true, card);
                System.out.println("Reset user PIN success");
            } catch (Exception e) {
                System.out.println("Reset user PIN failed " + e);
            } finally {
                hsms.disposeCardConnection(card);
            }

            return;
        }
    }

    protected void wipeOutAcos5MasterKey(final ACOS5Service acos5Service) throws Exception {
        String sure = getPrompt("Are You sure want to Wipe out master key y/n, all dependent data becomes useless!");
        if ("y".equals(sure)) {
            String serial = HexUtils.toString(acos5Service.getCardSerial());

            String kcv = null;
            try {
                kcv = acos5Service.getMasterKeyCheckValue();
            } catch (Exception e) {
                System.out.println("Seems no valid master key in that card, but will continue..");
            }

            acos5Service.wipeoutAESKey();
            System.out.println("Master key " + (kcv != null ? "with checkvalue " + kcv : " location ") + " in card " + serial
                    + " wiped out..");

        }
    }
}
