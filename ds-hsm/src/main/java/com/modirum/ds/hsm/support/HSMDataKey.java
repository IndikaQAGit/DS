package com.modirum.ds.hsm.support;

import com.modirum.ds.hsm.HSMDevice;

/**
 * Data holder for HSM key bytes and KCV.
 */
public class HSMDataKey implements HSMDevice.Key {
    byte[] value;
    byte[] checkValue;

    public HSMDataKey(byte[] value, byte[] checkValue) {
        this.value = value;
        this.checkValue = checkValue;
    }

    public byte[] getValue() {
        return this.value;
    }

    public byte[] getCheckValue() {
        return this.checkValue;
    }
}
