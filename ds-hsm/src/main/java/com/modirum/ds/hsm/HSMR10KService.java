package com.modirum.ds.hsm;

import com.modirum.ds.hsm.support.HSMDataKey;
import com.modirum.ds.hsm.support.HsmException;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import com.modirum.ds.utils.HexUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.util.Arrays;

/**
 * Implements Thales payShield 10K HSM interface necessary for DS functionality.
 */
public class HSMR10KService extends HSMR9000Service implements HSMDevice {

    // Logger class for parent classes is HSMR8000Service. Core library needs an interface to set logger correctly.
    protected static transient Logger log = LoggerFactory.getLogger(HSMR10KService.class);

    public HSMR10KService(String host, int port) {
        super(host, port);
    }

    public HSMR10KService() {
        super();
    }

    /**
     * Generate 256-bit AES key.
     * @return Generated key and key check value.
     * @throws Exception in case of any errors during key generation.
     */
    @Override
    public Key generateHSMDataEncryptionKey() throws Exception {
        log.info("Generating HSM Data Encryption Key");
        byte[][] keyAndKCV = generateAESKey("A3");
        byte[] kcvUnhexedBytes = HexUtils.fromString(new String(keyAndKCV[1], StandardCharsets.ISO_8859_1));
        return new HSMDataKey(keyAndKCV[0], kcvUnhexedBytes);
    }

    /**
     * Encrypts <code>data</code> using data encryption key <code>hsmDataEncryptionKey</code>.
     *
     * @param hsmDataEncryptionKey Data encryption key under LMK.
     * @param data cleartext data to be encrypted.
     * @return encrypted data.
     * @throws Exception in case of any errors during encryption.
     */
    @Override
    public byte[] encryptData(final byte[] hsmDataEncryptionKey, final byte[] data) throws Exception {
        log.info("Encrypting data with Data Encryption Key");
        byte[] paddedData = data;
        if (isUsePad()) {
            paddedData = pkcs5pad(data, BLOCK_SIZE_AES);
        }
        return aesEncrypt(hsmDataEncryptionKey, paddedData, false);
    }

    /**
     * Decrypts <coda>data</coda> using provided data encryption key <code>hsmDataEncryptionKey</code>
     *
     * @param hsmDataEncryptionKey Data encryption key under LMK
     * @param data encrypted data to be decrypted.
     * @return decrypted data.
     * @throws Exception in case of any errors during decryption.
     */
    @Override
    public byte[] decryptData(final byte[] hsmDataEncryptionKey, final byte[] data) throws Exception {
        log.info("Decrypting data with Data Encryption Key");
        byte[] decryptedData = aesDecrypt(hsmDataEncryptionKey, data, false);
        if (isUsePad()) {
            return pkcs5unpad(decryptedData, BLOCK_SIZE_AES);
        }

        return decryptedData;
    }

    @Override
    public String getMasterKeyCheckValue() throws Exception {
        log.info("Retrieveing Master Key KCV");
        // no difference with thales 9000 interface.
        return super.getMasterKeyCheckValue();
    }

    @Override
    public String getHardwareSerial() throws Exception {
        log.info("Retrieving HSM hardware serial number");
        // no difference with thales 9000 interface.
        return super.getHardwareSerial();
    }

    @Override
    public byte[] encryptDataInHSMWithMaster(byte[] data, boolean encrypt) throws Exception {
        // Thales doesn't expose LMK encryption. Should throw unsupported exception. Forwarding impl to parent.
        return super.encryptDataInHSMWithMaster(data, encrypt);
    }

    @Override
    public KeyPair generateRSAdecryptAndSign(int keysize) throws HsmException {
        // no change from 9k interface
        return super.generateRSAdecryptAndSign(keysize);
    }

    @Override
    public byte[] rsaPrivateDecrypt(byte rsaPrivateKey[], byte encryptedData[], RSADecryptPadMode padMode)
            throws Exception {
        // no change from 9k interface
        return super.rsaPrivateDecrypt(rsaPrivateKey, encryptedData, padMode);
    }

    /**
     * Can be used as a manually-triggered integration test. Automated integration would put useless load on HSM.
     */
    public static void main(String args[]) {
        try {
            String host = args != null && args.length > 0 ? args[0] : "127.0.0.1";
            String port = args != null && args.length > 1 ? args[1] : "1500";
            HSMR10KService r10k = new HSMR10KService(host, Integer.parseInt(port));
//            r10k.initializeHSM("tcp=true;lmkva=00;lmkkb=01");
            r10k.initializeHSM("tcp=true");
            r10k.alive();

            Key aesDataEncryptionKey = r10k.generateHSMDataEncryptionKey();
            System.out.println("HSM Key'" + new String(aesDataEncryptionKey.getValue()) + "' kcv " + HexUtils.toString(aesDataEncryptionKey.getCheckValue()));

            String nonPaddedData = "Non padded data " + new java.util.Date();
            System.out.println("Data src: " + nonPaddedData);
            byte[] cleartextData = nonPaddedData.getBytes(StandardCharsets.UTF_8);
            byte[] encryptedData = r10k.encryptData(aesDataEncryptionKey.getValue(), cleartextData);
            System.out.println("Encrypted data as hex: " + HexUtils.toString(encryptedData));

            byte[] decryptedData = r10k.decryptData(aesDataEncryptionKey.getValue(), encryptedData);
            System.out.println("Cleartext bytes equals decrypted bytes: " + Arrays.equals(cleartextData, decryptedData));
            System.out.println("Cleartext string equals decrypted string: " + nonPaddedData.equals(new String(decryptedData, StandardCharsets.UTF_8)));
            System.out.println("Data decrypted: " + new String(decryptedData, StandardCharsets.UTF_8));

            String hardwareSerial = r10k.getHardwareSerial();
            String masterKeyKCV = r10k.getMasterKeyCheckValue();
            System.out.println("Hardware data " + hardwareSerial + " LMK " + r10k.getLMKKB() + " KCV " + masterKeyKCV);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
