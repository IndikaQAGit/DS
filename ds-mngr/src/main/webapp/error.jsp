<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isErrorPage = "true"%>
<html>
<head>
    <title>Server general error</title>
</head>
<body>
<div align="center">
    Sorry, server encountered system failure, if this repeats contact administrator
    <br/><br/>
    <a href="<%=request.getContextPath() %>/main/main.html">Continue to main page</a>
    <%
    	if (pageContext != null && pageContext.getErrorData()!=null)
    	{
    		org.slf4j.Logger log=org.slf4j.LoggerFactory.getLogger("com.modirum.ds.mngr.Errors");
    		log.error("DS Manager app error ", pageContext.getErrorData().getThrowable());
    	
    	}
    %>
</div>
</body>
</html>
