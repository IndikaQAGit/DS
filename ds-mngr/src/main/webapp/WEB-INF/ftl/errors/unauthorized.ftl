<#import "../common/global.ftl" as global/>
<@global.page>
    <h2><@global.text key="text.unauthorized"/></h2>
    <#if error?exists><span style=" color: red;">${(error!"")?html}</span><br/>
    <#elseif errorm?exists><span style=" color: red;">${(errorm!"")?html}</span></#if>
</@global.page>