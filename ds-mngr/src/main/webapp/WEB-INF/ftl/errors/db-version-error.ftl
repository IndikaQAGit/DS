<#-- @ftlvariable name="ex" type="Exception" -->
<#import "/common/global.ftl" as global/>
<@global.page hideLogout=true>
    <span class="error" style="text-align:center;">
    Error. Database schema needs update.
</span>
    <p>
        Current database schema version is <b>${(currentDbVersion!"")?html}</b><br/>
        Required database schema version is <b>${(requiredDbVersion!"")?html}</b><br/>
        Please run the following schema upgrade scripts bundled with this distribution and appropriate to Your
        database<br/>
        <b>${(dbSchemaUpgradeScript!"")?html}</b>
    </p>
</@global.page>
