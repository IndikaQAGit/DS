<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.home'/></a>
        &gt;
        <span class="selected"><@global.text key='text.keyAdmin'/></span>
    </h1>
    <h2><@global.text key='text.keyAdmin'/></h2>
    <br/>
    <@global.security.authorize access="hasRole('keyManage')">
        <a href="../hsmdevices/hsmDeviceList.html"><@global.text key='text.HSMDevices'/></a>
    </@global.security.authorize>
</@global.page>