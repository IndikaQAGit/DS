<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt;
        <span class="selected"><@global.text key='text.localization'/></span></h1>

    <h2><@global.text key='text.localization'/></h2>
    <#if msg?exists>
        <span class="msg">${msg?html}</span><br/>
    </#if>
    <@global.security.authorize access="hasAnyRole('textEdit')">
        <a href="textEdit.html?x=${(.now?time)?html}">+ <@global.text key='text.addNew'/></a>
    </@global.security.authorize>

    <h2><@global.text key='text.search.texts'/></h2>
    <form id="textSearchForm" action="textsList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.keyword'/>:</span>
    <@global.spring.formInput path="s.keyword" attributes='size="30" maxlength="60"'/><br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.language'/>:</span>
    <@global.spring.formSingleSelect path="s.locale" options= supportedLocales />
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="s.order" attributes='style="width: 120px;"'
    options={"":"","id":"Id", "key":"Key", "message":"Message", "locale", "Language"} />
    <#assign textAsc = (loca.getText("text.order.asc", rc.locale)!"")/>
    <#assign textDesc = (loca.getText("text.order.desc", rc.locale)!"")/>
    <@global.spring.formSingleSelect path="s.orderDirection" options={"asc":"${textAsc?html}","desc":"${textDesc?html}"} />
    <br/>
    <input id="limit" type="hidden" name="limit" value="50"/>
    <input type="hidden" name="cmd" value="search"/>
    <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>

    <#if textslist?exists && searcher?exists && searcher.total &gt; 0 >
        <h2><@global.text key='text.found.total'/> ${(searcher.total!"")?html}
            <@global.text key='text.texts'/>, <@global.text key='text.showing'/> ${((searcher.start+1)!"")?html}
            <@global.text key='text.to'/>
            <#if searcher.total &gt; (searcher.start+searcher.limit)>
                ${(searcher.start+searcher.limit!"")?html}
            <#else>
                ${(searcher.total!"")?html}
            </#if>
        </h2>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeader" width="80">Id</td>
                <td class="tdHeader" width="60"><@global.text key='text.language'/></td>
                <td class="tdHeader" width="300"><@global.text key='text.key'/></td>
                <td class="tdHeader"><@global.text key='text.message'/></td>
            </tr>
            <#list textslist as text>
                <tr
                        <#if text_index%2==0>
                            class="tr_even"
                        <#else>
                            class="tr_odd"
                        </#if>
                >
                    <td><a href="textEdit.html?tid=${(text.getId()!"")?html}">${(text.getId()!"")?html}</a></td>
                    <td>${(text.getLocale()!"")?html}</td>
                    <td><a href="textEdit.html?tid=${text.getId()}">${(text.getKey()!"")?html}</a></td>
                    <td style="white-space: pre-wrap;">${(text.getMessage()!"")?html}</td>
                </tr>
            </#list>
            <@global.pagingButtons searcher=searcher colspan=4/>
        </table>
        </td>
        </tr>
        </table>
        </form>
    <#else>
        <#if textslist?exists>
            <h2>
                <@global.text key='text.no.matches.found'/>
            </h2>
        </#if>
    </#if>
</@global.page>
