<#macro merchantSelection>
    <p>Merchant selection here</p>
    <input type="text" name="filter" placeholder="Filter" value="" style="width: 265px;"
           onkeyup="filterValue(this.value);"/>
    <br/>
    <select name="merchantsAvailable" id="merchantsAvailable" multiple="true" size="15"
            style=" width: 270px; height: 267px;">
        <#list merchantsMap?keys as merchantId >
            <#if merchantId?exists && merchantId?length &gt; 0>
                <option value="${merchantId}">${(merchantsMap[(merchantId!"")?string]!"")?html}</option>
            </#if>
        </#list>
    </select>

    <script type="text/javascript">
        var masterProcessor = <#if masterProcessor?exists && masterProcessor=='true'>true<#else>false</#if>;
        var backup = new Array();
        var prevValue = null;
        var prevProc = null;
        var selectEl = document.getElementById("merchantsAvailable");
        if (selectEl != null) {
            for (i = 0; i < selectEl.options.length; i++) {
                var opx = selectEl.options[i];
                backup.push(opx);
            }
        }

        var procEl = document.getElementById("processorId");
        if (procEl != null && procEl.options != null) {
            filterProcessor(procEl.options[procEl.selectedIndex].text);
        }

        function filterValue(containsValue) {
            if (prevValue != null && prevValue == containsValue)
                return;

            prevValue = containsValue;

            var selectEl = document.getElementById("merchantsAvailable");
            if (selectEl != null) {
                for (i = selectEl.options.length - 1; i >= 0; i--) {
                    selectEl.remove(i);
                }


                var curProc = "";
                if (procEl != null && procEl.options != null) {
                    curProc = procEl.options[procEl.selectedIndex].text;
                    curProc = curProc.replace(" [Inactive]", "");
                }

                for (i = 0; i < backup.length; i++) {
                    var opx = backup[i];
                    if ((!masterProcessor || curProc.length == 0 || endsWith(opx.text, curProc)) &&
                        (containsValue.length == 0 || new String(opx.text).toLowerCase().indexOf(containsValue.toLowerCase()) > -1)) {
                        selectEl.add(opx);
                    }
                }
            }

        }

        function filterProcessor(procName) {
            if (!masterProcessor)
                return;

            if (prevProc != null && prevProc == procName)
                return;

            prevProc = procName;

            var selectEl = document.getElementById("merchantsAvailable");
            if (selectEl != null) {
                for (i = selectEl.options.length - 1; i >= 0; i--) {
                    selectEl.remove(i);
                }

                //procName=procName.replace(" [Inactive]","");
                //alert(procName+" "+new String(procName).length);
                for (i = 0; i < backup.length; i++) {
                    var opx = backup[i];
                    if (procName.length == 0 || endsWith(opx.text, procName)) {
                        selectEl.add(opx);
                    }
                }
            }


        }

        function endsWith(a, b) {
            if (a != null && b != null) {
                var ix = a.lastIndexOf(b);
                if (ix > -1 && ix + b.length == a.length)
                    return true;
            }

            return false;
        }

        function addMer() {
            var srcSelectEl = document.getElementById("merchantsAvailable");
            var dstSelectEl = document.getElementById("merchants");
            if (srcSelectEl != null && dstSelectEl != null) {
                for (i = 0; i < srcSelectEl.options.length; i++) {
                    if (srcSelectEl.options[i].selected) {
                        var exists = false;
                        for (j = 0; j < dstSelectEl.options.length; j++) {
                            if (dstSelectEl.options[j].value == srcSelectEl.options[i].value) {
                                exists = true;
                                break;
                            }
                        }

                        if (!exists) {
                            dstSelectEl.add(new Option(srcSelectEl.options[i].text, srcSelectEl.options[i].value));
                        }

                    }
                }
            }

        }

        function removeMer() {
            var dstSelectEl = document.getElementById("merchants");
            if (dstSelectEl != null) {
                for (i = dstSelectEl.options.length - 1; i >= 0; i--) {
                    if (dstSelectEl.options[i].selected) {
                        dstSelectEl.remove(i);
                    }
                }
            }
        }

        function selectMerchants() {
            var dstSelectEl = document.getElementById("merchants");
            if (dstSelectEl != null) {
                for (i = dstSelectEl.options.length - 1; i >= 0; i--) {
                    if (!dstSelectEl.options[i].selected) {
                        dstSelectEl.options[i].selected = true;
                    }
                }
            }
        }


    </script>
</#macro>

<#-- Macro used for selecting a merchant with dynamic merchant name and assigned ID filtering from the server.
This is appropriate to use, if operating with a whole big merchant map becomes too slow.
-->
<#macro merchantSearchBox path options attributes processorId=-1 multiselection=false changeevt="">

    <#if options?size < merchantBoxMaxSize!500>
        <@global.spring.formSingleSelect path=path options=options attributes=attributes/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
    <#else>
        <@global.spring.bind path/>
        <#assign statusx = springMacroRequestContext.getBindStatus(path) />
        <#assign inputid=statusx.expression />
        <#assign selmname="" />
        <#if statusx.value?exists && options?exists>
            <#assign selmname=options[statusx.value] />
        </#if>
        <table style="border: 0px; display: inline-table; border-spacing: 0px;">
            <tr>
                <#if multiselection>
                    <td style="border-bottom: 0px; padding: 0px;">
                        <select name="auxMerchantRoles" id="merchants" multiple="true" size="16"
                                style=" width: 270px; height: 294px;" ${attributes!""?html} >
                            <#if allMerchantCandidates?exists>
                                <#list allMerchantCandidates as merchantId >
                                    <option value="${merchantId}"
                                            selected>${(merchantsMap[(merchantId!"")?string]!"")?html}</option>
                                </#list>
                            </#if>
                        </select>
                        <@global.spring.bind path="user.merchantId"/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        <br/>
                    </td>

                    <td style="border-bottom: 0px; padding: 0px;">
                        <input type="button" name="addMerchants" value=" << Add " style="width: 65px;"
                               onclick="addMer(); ${changeevt!""?html}"/>
                        <br/>
                        <input type="button" name="remMerchants" value=" Remove " style="width: 65px;"
                               onclick="removeMer(); ${changeevt!""?html}"/>
                    </td>
                <#else>
                    <@global.spring.formInput path=path attributes=attributes fieldType="hidden"/>
                </#if>
                <td style="border: 0px; padding: 0px; vertical-align: initial; ">
                    <input type="text" id="merchantfeedinput" name="filter" placeholder="Find merchant"
                           value="${(selmname!"")?html}" style="width: 265px;"
                            <#if !multiselection>
                           onkeyup="merchantFeed(this, event); focusbox(event);" onfocus="merchantFeed(this, event);"/>
                    <#else>
                        onkeyup="merchantFeed(this, event);" />
                    </#if>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    <br/>
                    <#--
                    </td>
                    <td style="border: 0px; padding: 0px;">
                    -->
                    <select name="merchantsfeeding" id="merchantsfeeding"
                            <#if !multiselection>
                                onclick="populateTextFieldFromSelection('merchantsfeeding', 'merchantfeedinput', '${(inputid!"")?html}'); xhidePopup('merchantsfeeding');"
                                onkeydown="handlekeys(event);" onkeyup="handlekeys(event);"
                            </#if>
                            multiple="${multiselection?c}" size="15"
                            <#if multiselection>
                            style="width: 270px; height: 267px;">
                        <#else>
                            style="width: 270px; height: 267px; visibility: hidden; display: none;">
                        </#if>
                        <!-- must have end tag seprately -->
                    </select>
                </td>
            </tr>
        </table>

        <script type="text/javascript">
            <#-- Common scripts used by both single and multi merchant selection. -->

            var prev = null;
            var xmlDoc = null;
            var delayTimer = null;

            function merchantFeed(input, e) {
                var str = input.value;

                // key up
                if (getKeycode(e) == 13) {
                    e.cancelBubble = true;
                    e.returnValue = false;
                    e.stopPropagation();
                    e.preventDefault();

                    <#if !multiselection>
                    xhidePopup('merchantsfeeding');
                    </#if>
                    return false;
                } else {
                    <#if !multiselection>
                    showPopupNat('merchantsfeeding');
                    </#if>
                }

                // Do not send a request if given string is not a reasonable candidate for matching.
                if (!str || str.length < 3) {
                    prev = str;
                    // need to clear input as well
                    var frominput = document.getElementById("${(inputid!"")?html}");
                    if (frominput != null && frominput.value != null && frominput.value.length > 0) {
                        frominput.value = "";
                        if (frominput.onchange) {
                            frominput.onchange();
                        }
                    }

                    var dstSelect = document.getElementById("merchantsfeeding");
                    clearAllFromSelection(dstSelect);

                    return;
                }

                if (prev != null && prev == str && xmlDoc != null) {
                    return;
                }

                prev = str;

                if (delayTimer != null) {
                    clearTimeout(delayTimer);
                    delayTimer = null;
                }

                var stre = str.replace('"', '\\"');
                delayTimer = setTimeout("merchantFeedLoad(\"" + stre + "\");", 400);
            }


            var optionCahe = new Array();

            function merchantFeedLoad(strv) {
                var co = optionCahe[strv];
                if (co != null) {
                    var dstSelect = document.getElementById("merchantsfeeding");
                    clearAllFromSelection(dstSelect);
                    for (i = 0; i < co.length; i++) {
                        dstSelect.add(co[i]);
                    }

                    return;
                }

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        xmlDoc = xmlhttp.responseXML;
                        if (xmlDoc != null) {
                            var entries = xmlDoc.getElementsByTagName("entry");
                            var dstSelect = document.getElementById("merchantsfeeding");
                            clearAllFromSelection(dstSelect);

                            var optCopy = new Array();
                            for (i = 0; i < entries.length; i++) {
                                var el = entries[i].getElementsByTagName("key");
                                if (el != null && el.length > 0 && el[0].firstChild != null) {
                                    var mkey = entries[i].getElementsByTagName("key")[0].firstChild.nodeValue;
                                    var mname = entries[i].getElementsByTagName("value")[0].firstChild.nodeValue;
                                    dstSelect.add(new Option(mname, mkey));
                                    optCopy[i] = new Option(mname, mkey);
                                }
                            }
                            optionCahe[strv] = optCopy;
                        }
                    }
                }
                <#if processorId != -1>
                xmlhttp.open("GET", "../merchants/merchantsfeed.html?searchString=" + encodeURIComponent(strv) + "&processorId=" + encodeURIComponent(${(processorId!"")?html}), true);
                <#else>
                xmlhttp.open("GET", "../merchants/merchantsfeed.html?searchString=" + encodeURIComponent(strv), true);
                </#if>
                xmlhttp.send();
            }

            function clearAllFromSelection(selectionElement) {
                for (i = selectionElement.options.length - 1; i >= 0; i--) {
                    selectionElement.remove(i);
                }
            }

            function getKeycode(e) {
                var keynum = -1;
                if (window.event && e != null && e.hasOwnProperty("keyCode")) { // IE
                    keynum = e.keyCode;
                } else if (e != null && e.which) { // Netscape/Firefox/Opera
                    keynum = e.which;
                }
                return keynum;
            }


            <#-- Scripts intended only for multi merchant selection -->
            <#if multiselection>

            function addMer() {
                var srcSelectEl = document.getElementById("merchantsfeeding");
                var dstSelectEl = document.getElementById("merchants");
                if (srcSelectEl != null && dstSelectEl != null) {
                    for (i = 0; i < srcSelectEl.options.length; i++) {
                        if (srcSelectEl.options[i].selected) {
                            var exists = false;
                            for (j = 0; j < dstSelectEl.options.length; j++) {
                                if (dstSelectEl.options[j].value == srcSelectEl.options[i].value) {
                                    exists = true;
                                    break;
                                }
                            }

                            if (!exists) {
                                dstSelectEl.add(new Option(srcSelectEl.options[i].text, srcSelectEl.options[i].value));
                            }

                        }
                    }
                }

            }

            function removeMer() {
                var dstSelectEl = document.getElementById("merchants");
                if (dstSelectEl != null) {
                    for (i = dstSelectEl.options.length - 1; i >= 0; i--) {
                        if (dstSelectEl.options[i].selected) {
                            dstSelectEl.remove(i);
                        }
                    }
                }
            }

            <#else>
            <#-- Scripts intended only for multi merchant selection -->

            //xshowPopupAt(refId, x, y)

            function evtSource(e) {
                var elem = null;
                if (e != null && e.srcElement) {
                    elem = e.srcElement;
                } else if (e != null && e.target) {
                    elem = e.target;
                }
                return elem != null ? elem.id : null;
            }

            var prev = null;
            var xmlDoc = null;
            var delayTimer = null;
            document.addEventListener('focus', function (e) {
                unfcusboxes(e);
            }, true);

            function unfcusboxes(e) {
                var evid = evtSource(e);
                if (evid != null &&
                    !(evid == "merchantsfeeding" || evid == "merchantfeedinput")
                ) {
                    xhidePopup('merchantsfeeding');
                    return false;
                }
                return true;
            }

            function focusbox(e) {
                // down
                if (getKeycode(e) == 40 && e.type == "keyup") {
                    document.getElementById("merchantsfeeding").focus();
                }
            }

            function handlekeys(e) {
                var kn = getKeycode(e);

                // key up
                if (kn == 38 && e.type == "keyup") {
                    var dropdown = document.getElementById("merchantsfeeding");
                    if (dropdown != null &&
                        (dropdown.length < 1 || dropdown.selectedIndex == 0)
                    ) {
                        document.getElementById("merchantfeedinput").focus();
                    }
                    return true;
                }

                // return
                if (kn == 13) {
                    if (e.type == "keyup") {
                        populateTextFieldFromSelection('merchantsfeeding', 'merchantfeedinput', '${(inputid!"")?html}')
                        document.getElementById("merchantfeedinput").focus();
                        xhidePopup('merchantsfeeding');
                    }
                    e.cancelBubble = true;
                    e.returnValue = false;
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }

                return true;
            }


            function populateTextFieldFromSelection(selection, textfield, fromInpId) {
                var textbox = document.getElementById(textfield);
                var frominput = document.getElementById(fromInpId);
                var dropdown = document.getElementById(selection);
                if (dropdown != null && frominput != null && textbox != null &&
                    dropdown.length > 0 && dropdown.selectedIndex >= 0 &&
                    dropdown.options[dropdown.selectedIndex].value != null &&
                    dropdown.options[dropdown.selectedIndex].value.length > 0) {
                    textbox.value = dropdown.options[dropdown.selectedIndex].text;
                    frominput.value = dropdown.options[dropdown.selectedIndex].value;
                    if (frominput.onchange) {
                        frominput.onchange();
                    }
                }
            }


            </#if>
        </script>
    </#if>
</#macro>