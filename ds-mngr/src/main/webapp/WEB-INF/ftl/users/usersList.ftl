<#-- @ftlvariable name="filter" type="com.modirum.ds.db.model.UserSearchFilter" -->
<#-- @ftlvariable name="userList" type="java.util.List<com.modirum.ds.db.model.User>" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <span class="selected"><@global.text key='text.users'/></span>
    </h1>
    <#if usersCountsMap??>
        <h2><@global.text key='text.users.summary'/></h2>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr class="tr_odd">
                <td class="tdHeader" width="20%"><@global.text key='text.active'/></td>
                <td>
                <a href="usersList.html?search=y&status=A">${usersCountsMap["A"]!""}</a>

                </td>
            </tr>
            <tr class="tr_even">
                <td class="tdHeader" width="20%"><@global.text key='text.template'/></td>
                <td ><a href="usersList.html?search=y&status=T">${usersCountsMap["T"]!""}</a></td>
            </tr>
            <tr class="tr_odd">
                <td class="tdHeader" width="20%"><@global.text key='text.blocked'/></td>
                <td ><a href="usersList.html?search=y&status=B">${usersCountsMap["B"]!""}</a></td>
            </tr>
            <tr class="tr_even">
                <td class="tdHeader" width="20%"><@global.text key='text.temp.locked'/></td>
                <td ><a href="usersList.html?search=y&status=F">${usersCountsMap["L"]!""}</a></td>
            </tr>
            <tr>
                <td class="tdHeader" width="20%"><@global.text key='text.total'/></td>
                <td class="tdHeader"><#if userCountsTotal?exists>${userCountsTotal}</#if></td>
            </tr>
            </tbody>
        </table>
    </#if>
<@global.security.authorize access="hasAnyRole('userEdit')">
    <a href="../users/userEdit.html"><@global.text key='text.addNewUser'/></a>
</@global.security.authorize>

<@global.security.authorize access="hasAnyRole('userView')">
<h2><@global.text key='text.search.users'/></h2>
<form id="uSearchForm" action="usersList.html" method="post">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<span style="width: 200px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
<@global.spring.formSingleSelect path="filter.paymentSystemId" options=paymentSystemDropDown/>
<br/>
<span style="width: 200px; display: inline-block;"><@global.text key='text.user.loginname'/>:</span>
	<@global.spring.formInput path="filter.loginname" attributes='size="25" maxlength="50"'/>
<br/>
<span style="width: 200px; display: inline-block;"><@global.text key='text.user.firstName'/>:</span>
	<@global.spring.formInput path="filter.firstName" attributes='size="25" maxlength="50"'/>
<br/>
<span style="width: 200px; display: inline-block;"><@global.text key='text.user.lastName'/>:</span>
	<@global.spring.formInput path="filter.lastName" attributes='size="25" maxlength="50"'/>
<br/>

<span style="width: 200px; display: inline-block;"><@global.text key='text.user.status'/>:</span>
<#assign statuses = {"":"", "A":"${loca.getText('text.active', locale)}", "B":"${loca.getText('text.blocked', locale)}",
		"T":"${loca.getText('text.template', locale)}", "L":"${loca.getText('text.temp.locked', locale)}" } />
<@global.spring.formSingleSelect path="filter.status" options=statuses />
<br/>
<span style="width: 200px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
<@global.spring.formSingleSelect path="filter.order" options=userOderMap />
<@global.spring.formSingleSelect path="filter.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
<span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
<@global.spring.formSingleSelect path="filter.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
<br/>
<input type="hidden" name="search" value="Search"/>
<input type="submit" name="submitbtn" value="Search"/>
<br/>
<#if userList?exists>
    <#if filter?exists && filter.total &gt; 0 >
        <h2>
            <@global.text key='text.found.total'/> ${filter.total!""?html} <@global.text key='text.users'/>, <@global.text key='text.showing'/> ${(filter.start+1)!""?html} <@global.text key='text.to'/>
            <#if filter.total &gt; (filter.start+filter.limit)>
                ${(filter.start+filter.limit)!""?html}
            <#else>
                ${(filter.total)!""?html}
            </#if>
        </h2>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<@userTableTitle/>
			<#list userList as userx>
                <tr <#if userx_index%2==0>class="tr_even"<#else>class="tr_odd"</#if>>
                <@userDetails user=userx />
                </tr>
			</#list>
            <@global.pagingButtons searcher=filter colspan=3/>
        </table>
</form>
<#else>
    <@global.text key='text.no.matches.found'/>
</#if>
</#if>
</@global.security.authorize>
</@global.page>

<#macro userTableTitle>
<tr>
	<td class="tdHeader">Id</td>
	<td class="tdHeader">Login Name</td>
	<td class="tdHeader">Name</td>
	<td class="tdHeader">Email</td>
	<td align="center" class="tdHeader">Status</td>
	<td align="center" class="tdHeader">Roles</td>
	<td class="tdHeader"><@global.text key='general.paymentSystem'/></td>
</tr>
</#macro>

<#macro userDetails user>
	<td>
		<a href="../users/userView.html?id=${(user.id!"")?html}">${(user.id!"")?html}</a>
	</td>
	<td>
		<a href="../users/userView.html?id=${(user.id!"")?html}">${(user.loginname!"")?html}</a>
	</td>
	<td>
		<a href="../users/userView.html?id=${(user.id!"")?html}">${(user.firstName!"")?html} ${(user.lastName!"")?html}</a>
	</td>
	<td>${(user.email!'')?html}</td>
	<td align="center">
		<@global.displayStatus user.getStatus()!""/>
	</td>
	<td>
	<#if allroles?exists && user.roles?exists>
		<#list allroles?keys as role>
			<#if user.roles?seq_contains(role)>
				${(allroles[role]!"")?html}<br/>
			</#if>
		</#list>
	</#if>
	</td>
    <td>
        <#if user.paymentSystemId?exists>
            ${(user.paymentSystemName!'')?html}
        <#else>
            <@global.text key='general.unlimited'/>
        </#if>
    </td>
</#macro>
