<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.home'/></a>
        &gt;
        <span class="selected"><@global.text key='text.HSMDevices'/></span>
    </h1>
    <@global.security.authorize access="hasRole('keyManage')">
        <a href="hsmDeviceEdit.html?x=${(.now?time)?html}"><@global.text key='text.addHSMDevice'/></a><br/>
    </@global.security.authorize>
    <h2><@global.text key='text.HSMDevices'/></h2>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="15%" class="tdHeader"><@global.text key='text.HSMDevice.name'/></td>
            <td width="20%" class="tdHeader"><@global.text key='text.HSMDevice.className'/></td>
            <td width="20%" class="tdHeader"><@global.text key='text.HSMDeviceConf.config'/></td>
            <td width="10%" class="tdHeader"><@global.text key='text.status'/></td>
            <td width="10%"></td>
        </tr>
        <#list hsmDeviceList as hsmDevice>
        <tr <#if hsmDevice_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
            <td width="15%">
                <a href="hsmDeviceView.html?id=${(hsmDevice.id!"")?html}">${(hsmDevice.name!"")?html}</a>
            </td>
            <td width="20%">${(hsmDevice.className!"")?html}</td>
            <td width="20%">${(hsmDeviceConfMap[hsmDevice.id?string]!"")?html}</td>
            <td width="20%"><@global.displayStatus hsmDevice.status!""/></td>
            <td width="20%"></td>
        </tr>
        </#list>
    </table>
</@global.page>