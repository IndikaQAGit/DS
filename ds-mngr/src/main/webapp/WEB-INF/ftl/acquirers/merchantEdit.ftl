<#-- @ftlvariable name="merchant" type="com.modirum.ds.db.model.Merchant" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <a href="merchantList.html?x=${(.now?time)?html}"><@global.text key='text.merchants'/></a> &gt;
        <#if merchant?? && merchant.id??>
            <a href="merchantView.html?id=${(merchant.getId()!"")?html}"><@global.text key='text.al.details'/>:${(merchant.name!"")?html}</a>&gt;
        </#if>
        <span class="selected">
            <#if merchant?? && merchant.id??>
                <@global.text key='button.edit'/>
            <#else>
                <@global.text key='text.add.new'/>
            </#if>
        </span>
    </h1>
    <h2><@global.text key='text.merchant'/></h2>
    <#if merchant?exists>
        <form id="uForm" action="merchantSave.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="cmd" value=""/>
            <@global.spring.formHiddenInput path='merchant.id'/>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.name'/>:*</td>
                    <td>
                        <@global.spring.formInput path="merchant.name" attributes='size="50" maxlength="100"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                    <td>
                        <#if merchant?? && merchant.id??>
                            ${(paymentSystemName!"")?html}
                            <@global.spring.formHiddenInput path='merchant.paymentSystemId'/>
                        <#else>
                            <@global.spring.formSingleSelect path="merchant.paymentSystemId" options=paymentSystemComponentMap attributes='onchange="submitFormWithCmd(null,\'uForm\' , \'refreshPaymentSystem\');"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </#if>
                    </td>
                    <td></td>
                </tr>
                <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
                    <tr>
                        <td class="tdHeaderVertical"><@global.text key='text.3ds.requestorId'/>:</td>
                        <td>
                            <@global.spring.formInput path="merchant.requestorID" attributes='size="32" maxlength="35"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td></td>
                    </tr>
                </#if>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.merchant.acquirerMerchantID'/>:</td>
                    <td>
                        <@global.spring.formInput path="merchant.identifier" attributes='size="25" maxlength="24"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>Acquirer-assigned Merchant identifier (required if Acquirer set)</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.acquirer'/>:</td>
                    <td><@global.spring.formSingleSelect path="merchant.acquirerId" options=acquirersMap attributes='style="width: 220px;"' />
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.tdsserverprofile'/>:*</td>
                    <td><@global.spring.formSingleSelect path="merchant.tdsServerProfileId" options=tdsServerMap attributes='style="width: 220px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.email'/>:</td>
                    <td>
                        <@global.spring.formInput path="merchant.email" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.phone'/>:</td>
                    <td>
                        <@global.spring.formInput path="merchant.phone" attributes='size="26" maxlength="30"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.url'/>:</td>
                    <td>
                        <@global.spring.formInput path="merchant.URL" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.country'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="merchant.country" options=countryMapA2 attributes='style="width: 220px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.ext.options'/>:</td>
                    <td>
                        <@global.spring.formInput path="merchant.options" attributes='size="30" maxlength="32"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.status'/>:*</td>
                    <td>
                        <@global.spring.formSingleSelect path="merchant.status" options=merchantStatuses/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                <tr>
                    <td colspan="3">
                        <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                        <#if merchant.id??>
                            <input type="submit" name="saveAsNew" value="Save as new"/>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'merchantView.html?id=${merchant.id}'; return false;"/>
                        <#else>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'merchantList.html'; return false;"/>
                        </#if>
                    </td>
                </tr>
            </table>
        </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
