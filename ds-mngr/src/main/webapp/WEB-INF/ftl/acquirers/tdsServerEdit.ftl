<#-- @ftlvariable name="tdsServerProfile" type="com.modirum.ds.db.model.TDSServerProfile" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <a href="tdsServerList.html?x=${(.now?time)?html}"><@global.text key='text.tdsserverprofiles'/></a> &gt;
        <#if tdsServerProfile?? && tdsServerProfile.id??>
            <a href="tdsServerView.html?id=${(tdsServerProfile.getId()!"")?html}"><@global.text key='text.details'/>: ${(tdsServerProfile.name!"")?html}</a>&gt;
        </#if>
        <span class="selected">
            <#if tdsServerProfile?? && tdsServerProfile.id??>
                <@global.text key='text.edit'/><#else><@global.text key='text.add.new'/>
            </#if>
        </span>
    </h1>
    <h2><@global.text key='text.tdsserverprofile'/></h2>
    <#if tdsServerProfile??>
        <form id="uForm" action="tdsServerSave.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="cmd" value=""/>
            <@global.spring.formHiddenInput path='tdsServerProfile.id'/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="120" class="tdHeaderVertical"><@global.text key='text.name'/>:*</td>
                    <td width="405">
                        <@global.spring.formInput path="tdsServerProfile.name" attributes='size="50" maxlength="100"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                    <td>
                        <#if tdsServerProfile?? && tdsServerProfile.id??>
                            ${(paymentSystemName!"")?html}
                            <@global.spring.formHiddenInput path='tdsServerProfile.paymentSystemId'/>
                        <#else>
                            <@global.spring.formSingleSelect path="tdsServerProfile.paymentSystemId" options=paymentSystemComponentMap attributes='onchange="submitFormWithCmd(null,\'uForm\' , \'refreshPaymentSystem\');"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </#if>

                    </td>
                    <td></td>
                </tr>
                <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
                <#else>
                    <tr>
                        <td width="120" class="tdHeaderVertical"><@global.text key='text.3ds.requestorId'/>:</td>
                        <td width="405">
                            <@global.spring.formInput path="tdsServerProfile.requestorID" attributes='size="32" maxlength="35"'/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </td>
                        <td></td>
                    </tr>
                </#if>
                <tr>
                    <td width="120" class="tdHeaderVertical"><@global.text key='text.3ds.operatorId'/>:</td>
                    <td width="405">
                        <@global.spring.formInput path="tdsServerProfile.operatorID" attributes='size="32" maxlength="35"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.3ds.referenceNumberList'/>:</td>
                    <td>
                        <@global.spring.formTextarea path="tdsServerProfile.tdsReferenceNumberList" attributes='rows="3" style="margin: 0px; width: 350px; height: 50px;" maxlength="255"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>Comma delimited list of 3DS Server Reference Numbers</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.tdsserver.url'/>:</td>
                    <td>
                        <@global.spring.formInput path="tdsServerProfile.URL" attributes='size="50" maxlength="200"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>3DS Server URL (optional, informal only)</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">
                        <@global.text key='text.tdsserverprofile.in.clientcertid'/>
                        :<#if sslCertsManagedExternally?? && sslCertsManagedExternally><#else>*</#if>
                    </td>
                    <td>
                        <@global.spring.formSingleSelect path="tdsServerProfile.inClientCert" options=inCertificateList attributes='style="width: 220px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>Incoming SSL client auth certificate (required if app managed certificates)</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.tdsserverprofile.out.clientcertid'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="tdsServerProfile.outClientCert" options=outCertificateList attributes='style="width: 220px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>OutgoingSSL client auth certificate (if empty default is used)</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.status'/>:*</td>
                    <td>
                        <@global.spring.formSingleSelect path="tdsServerProfile.status" options=tdsServerStatuses/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                <tr>
                    <td colspan="3">
                        <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                        <#if tdsServerProfile.id??>
                            <input type="submit" name="saveAsNew" value="<@global.text key='button.saveAsNew'/>">
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'tdsServerView.html?id=${tdsServerProfile.id}'; return false;"/>
                        <#else>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'tdsServerList.html'; return false;"/>
                        </#if>
                    </td>
                </tr>
            </table>
        </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
