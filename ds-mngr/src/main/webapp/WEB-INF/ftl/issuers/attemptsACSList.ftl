<#-- @ftlvariable name="searcher" type="com.modirum.ds.model.IssuerSearcher" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span
                class="selected"><@global.text key='text.attempts.acs.profiles'/></span></h1>

    <@global.security.authorize access="hasAnyRole('issuerView')">
        <a href="../issuers/attemptsACSEdit.html?x=${(.now?time)?html}"><@global.text key='text.addNewAACS'/></a>
    </@global.security.authorize>
    <h2><@global.text key='text.search.aacs'/></h2>
    <form id="uSearchForm" action="attemptsACSList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <span style="width: 200px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
    <@global.spring.formSingleSelect path="searcher.paymentSystemId" options=paymentSystemSearchMap/>
    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.acsprofile.name'/>:</span>
    <@global.spring.formInput path="searcher.name" attributes='size="25" maxlength="50"'/>
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.acsprofile.status'/>:</span>
    <@global.spring.formSingleSelect path="searcher.status" options=acpStatuses />
    <br/>
    <span style="width: 200px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="searcher.order" options={"id":"Id", "status":"Status", "name":"Name"} />
    <@global.spring.formSingleSelect path="searcher.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
    <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="searcher.limit" options={"10":"10","25":"25","50":"50", "100":"100", "250":"250" } />
    <br/>
    <input type="submit" name="submitbtn" value="Search"/>
    <br/>

    <#if foundAacs??>
        <#if searcher?? && searcher.total &gt; 0 >
            <h2><@global.text key='text.found.total'/> ${searcher.total!""?html} <@global.text key='text.attempts.acs.profiles'/>
                , <@global.text key='text.showing'/> ${(searcher.start+1)!""?html} <@global.text key='text.to'/>
                <#if searcher.total &gt; (searcher.start+searcher.limit)>
                    ${(searcher.start+searcher.limit)!""?html}
                <#else>
                    ${(searcher.total)!""?html}
                </#if>
            </h2>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" class="tdHeader">Id</td>
                    <td width="15%" class="tdHeader"><@global.text key='text.acsprofile.name'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.acsprofile.refno'/></td>
                    <td width="10%" class="tdHeader"><@global.text key='general.paymentSystem'/></td>
                    <td align="center" class="tdHeader"><@global.text key='text.acsprofile.status'/></td>
                </tr>
                <#list foundAacs as aacs>
                    <tr <#if aacs_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                        <td width="5%"><a href="../issuers/attemptsACSView.html?id=${(aacs.id!"")?html}">${(aacs.id!"")?html}</a></td>
                        <td width="15%"><a href="../issuers/attemptsACSView.html?id=${(aacs.id!"")?html}">${(aacs.name!"")?html}</a></td>
                        <td width="25%">${(aacs.refNo!"")?html}</td>
                        <td>
                            <#if aacs.paymentSystemId??>
                                ${((paymentSystemComponentMap[aacs.paymentSystemId?string])!aacs.paymentSystemId)?html}
                            </#if>
                        </td>
                        <td width="15%" align="center"><@global.objstatus acpStatuses aacs.status!""/></td>
                    </tr>
                </#list>
                <@global.pagingButtons searcher=searcher colspan=6/>
            </table>
            </form>
        <#else>
            <@global.text key='text.no.matches.found'/>
        </#if>
    <#else>
        <@global.spring.formHiddenInput path="searcher.start"/>
        </form>
    </#if>
</@global.page>