<#macro input val type>
    <#if type == 'textarea' >
        <span class="settings-value">${val}</span>
        <textarea name="value" class="settings-value-input" style="display:none;"
                  cols="50"></textarea>
    </#if>
    <#if type == 'input' >
        <span class="settings-value">${val}</span>
        <input type="text" name="value" class="settings-value-input" style="display:none;" size="50"/>
    </#if>
    <#if type == 'checkbox' >
        <#if val == 'true'>
            <input type="checkbox" disabled class="settings-value" checked value="true"/>
        <#else>
            <input type="checkbox" disabled class="settings-value" value="false"/>
        </#if>
        <input type="checkbox" name="value" class="settings-value-input" style="display:none;"
               onchange="onCheckboxChange(this);"/>
    </#if>
    <#if type == 'display_only' >
        <span class="settings-value">${val}</span>
    </#if>
</#macro>