<#global loginPage = "true">
<#import "../common/global.ftl" as global/>
<@global.page hideLogout=true>
    <script type="text/javascript">
        window.onload = function() {
            document.getElementById('initAdmin').focus();
        };
    </script>
    <div id="main">
        <div id="content">
            <div id="content-login">
                <h2><@global.text key='text.initial.admin.header'/></h2>
                <P>
                    ${(initialLoginInfo!"")?html}
                </P>
                <#if (initError??)>
                    <div class="error">${(initError!"")?html}</div>
                </#if>
                <form id="uForm" action="../login/login.html" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <#if errorMessage??>
                            <div class="error">${(errorMessage!"")?html}</div>
                        </#if>
                        <tr>
                            <td><@global.text key='text.user.loginname'/>:*</td>
                            <td><input type="text" name="initAdmin" id="initAdmin"/></td>
                        </tr>
                        <tr>
                            <td><@global.text key='user.password'/>:*</td>
                            <td><input type="password" name="initAdminPwd" id="initAdminPwd"/></td>
                        </tr>
                        <tr>
                            <td><@global.text key='text.user.newPasswordRepeat'/>:*</td>
                            <td><input type="password" name="initAdminVerifyPwd" id="initAdminVerifyPwd"/></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="submit" name="loginButton" id="loginButton" value="Login"/></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="content-footer">
                <img src="../img/powered-by-modirum.svg"/>
                <div id="footer-right"></div>
            </div>
        </div>
    </div>
</@global.page>