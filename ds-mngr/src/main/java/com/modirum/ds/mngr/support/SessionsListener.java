package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AuditLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 29.10.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */

public class SessionsListener implements HttpSessionListener {

    static Logger log = LoggerFactory.getLogger(SessionsListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent arg0) {
        User ux = (User) arg0.getSession().getAttribute("User");
        if (ux != null) {
            log.info("New session crated for " + ux.getLoginname() + "(" + ux.getId() + ") max inactivity " +
                     arg0.getSession().getMaxInactiveInterval());
        }

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent arg0) {
        if (arg0 != null && arg0.getSession() != null) {
            User ux = (User) arg0.getSession().getAttribute("User");
            String loggedOut = (String) arg0.getSession().getAttribute("Logout");
            if (ux != null && loggedOut == null) {
                log.info("Inactive session ends, logging auto logout event " + ux.getLoginname() + "(" + ux.getId() +
                         ") ");

                AuditLogService auditLogService = (AuditLogService) arg0.getSession().getServletContext().getAttribute(
                        "auditLogService");

                if (auditLogService != null) {
                    auditLogService.logAction(ux, ux, DSModel.AuditLog.Action.LOGOFF, "User '" + ux.getLoginname() +
                                                                                      "', user has auto logged off due session inacitvity");

                }
            }
        }

    }

}
