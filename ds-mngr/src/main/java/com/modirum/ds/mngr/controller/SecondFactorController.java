package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.mngr.support.ApplicationControl;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.support.CustomUsernamePasswordAuthenticationFilter;
import com.modirum.ds.mngr.support.Helpers;
import com.modirum.ds.mngr.support.ModirumIDASClientService;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.User;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.UserService;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.Message;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

@Controller
@RequestMapping("/secondFactor")
public class SecondFactorController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(SecondFactorController.class);

    @Autowired
    CustomUsernamePasswordAuthenticationFilter customUsernamePasswordAuthenticationFilter;

    ModirumIDASClientService mdidService;

    private final UserService usersService = ServiceLocator.getInstance().getUserService();
    private final PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();


    ModirumIDASClientService getModirumIDASClientService() {
        if (mdidService == null) {
            mdidService = new ModirumIDASClientService();
        }
        return mdidService;
    }


    @RequestMapping(value = "/secondFactor.html")
    public ModelAndView secondFactor(HttpServletRequest request, HttpServletResponse response, @ModelAttribute(value = "user") User userform, BindingResult result) throws Exception {
        User authUser = AuthorityUtil.getUser();
        Date now = new Date(System.currentTimeMillis());
        Date sfStart = (Date) getSessionAttribute("sfStartSess");
        if (sfStart == null) {
            sfStart = now;
            setSessionAttribute("sfStartSess", sfStart);
        }

        String msg = "";
        String err = null;
        ModelAndView mav = new ModelAndView("secondFactor/secondFactor");
        mav.addObject("authmethod", authUser.getFactor2Authmethod());
        mav.addObject("sfStart", sfStart);
        // when sf shall be completed or auto cancel second factor page
        mav.addObject("sfLimit", sfStart.getTime() + 6 * 60000 - now.getTime());

        WebContext wctx = this.getWebContext();

        if (request.getParameter("cancelBtn") != null || "cancel".equals(request.getParameter("cmd"))) {
            try {
                Message m = (Message) getSessionAttribute("mdidMessage");
                if (m != null) {
                    boolean mres = getModirumIDASClientService().rejectMessage(authUser, m, wctx);
                    log.info("MDID message " + m.getMessageId() + " reject sent " + authUser.getLoginname() + " is " +
                             mres);
                    setSessionAttribute("mdidMessage", null);
                }

            } catch (Exception e) {
                log.error("Error validating MDID message approval status ", e);
            }
            return super.redirect("../logout");
        }

        if ("verify".equals(request.getParameter("cmd"))) {
            try {

                Integer sfAttempts = (Integer) getSessionAttribute("sfAttempts");
                if (sfAttempts == null) {
                    sfAttempts = 1;
                    setSessionAttribute("sfAttempts", sfAttempts);
                } else {
                    setSessionAttribute("sfAttempts", (sfAttempts + 1));
                }

                String password = userform.getPassword();


                boolean authresult = false;
                if ("smsotp".equals(authUser.getFactor2Authmethod())) {
                    if (Misc.isNullOrEmpty(password)) {
                        result.addError(new FieldError("user", "password", password, false, null, null,
                                                       localizationService.getText("err.required", "password",
                                                                                   locale())));

                        msg = this.localizationService.getText("msg.mdidotpenter",
                                                               new Object[]{ApplicationControl.getAppName(), authUser.getLoginname()},
                                                               locale());

                        String otpDate = (String) getSessionAttribute("smsotpDate");
                        //String otpDate = com.modirum.ds.utils.DateUtil.formatDate(otpDateD!=null ? otpDateD : now, "dd/MM/yy HH:mm:ss", false);
                        msg = this.localizationService.getText("msg.otp", new Object[]{otpDate}, locale());
                        mav.addObject("msg", msg);
                        return mav;
                    }

                    String otp = (String) getSessionAttribute("smsotp");
                    if (password.equals(otp)) {
                        authresult = true;
                    } else {
                        String otpDate = (String) getSessionAttribute("smsotpDate");
                        msg = this.localizationService.getText("msg.otp", new Object[]{otpDate}, locale());
                    }
                } else if ("mdidotp".equals(authUser.getFactor2Authmethod())) {
                    boolean valid = Boolean.TRUE.equals(getSessionAttribute("mdidMessageApproved"));
                    if (!valid) {
                        try {
                            Message m = (Message) getSessionAttribute("mdidMessage");
                            if (m != null) {
                                log.info("As not MDID approved check results available yet for " + m.getMessageId() +
                                         " " + authUser.getLoginname() +
                                         " lets make last ettempt to check if user accpeted, before requiring otp");
                                String mres = getModirumIDASClientService().checkMessage(authUser, m, wctx);
                                log.info("MDID message approval status for " + authUser.getLoginname() + " is " + mres);
                                if (ModirumIDASClientService.MessageStatus.cApproved.equals(mres)) {
                                    setSessionAttribute("mdidMessageApproved", Boolean.TRUE);
                                    valid = Boolean.TRUE.equals(getSessionAttribute("mdidMessageApproved"));
                                }
                            }

                        } catch (Exception e) {
                            log.error("Error last attempt validating MDID message approval status ", e);
                        }
                        // still not valid must reuire otp
                        if (!valid) {
                            if (Misc.isNullOrEmpty(password)) {
                                result.addError(new FieldError("user", "password", password, false, null, null,
                                                               localizationService.getText("err.required", "password",
                                                                                           locale())));

                                msg = this.localizationService.getText("msg.mdidotpenter",
                                                                       new Object[]{ApplicationControl.getAppName(), authUser.getLoginname()},
                                                                       locale());
                                mav.addObject("msg", msg);
                                return mav;
                            }

                            valid = getModirumIDASClientService().verifyOtp(authUser, password, getWebContext());
                            if (valid) {
                                Message m = (Message) getSessionAttribute("mdidMessage");
                                if (m != null) {
                                    try {
                                        log.info("MDID rejecting message " + m.getMessageId() + " for " +
                                                 authUser.getLoginname() + " because validated otp instead");
                                        getModirumIDASClientService().rejectMessage(authUser, m, wctx);

                                    } catch (Exception e) {
                                        log.error("Error rejecting no longer needed MDID message " + m.getMessageId() +
                                                  " for " + authUser.getLoginname(), e);
                                    }
                                    setSessionAttribute("mdidMessage", null);
                                }
                            } // reject
                        } // valid 2
                    } // valid 1

                    if (valid) {
                        authresult = true;
                    } else {
                        msg = this.localizationService.getText("msg.mdidotpenter",
                                                               new Object[]{ApplicationControl.getAppName(), authUser.getLoginname()},
                                                               locale());
                    }
                } else if ("secureid".equals(authUser.getFactor2Authmethod())) {

                    //if (password.equals("777777"))
                    // TODO: demo to real
                    if (Misc.isNumber(password) && password.length() == 7) {
                        authresult = true;
                    } else {
                        msg = this.localizationService.getText("msg.securid", locale());
                    }

                } else if ("pincalc".equals(authUser.getFactor2Authmethod())) {
                    //if (password.equals("777777"))
                    // TODO: demo to real
                    if (Misc.isNumber(password) && password.length() == 7) {
                        authresult = true;
                    } else {
                        msg = this.localizationService.getText("msg.pincalc", locale());
                    }

                }

                if (authresult) {
                    authUser.setFactor2Passed(true);
                    request.setAttribute("authmethod", "password");

                    if ("mdidotp".equals(authUser.getFactor2Authmethod())) {
                        if (Boolean.TRUE.equals(getSessionAttribute("mdidMessageApproved"))) {
                            request.setAttribute("authmethod2", authUser.getFactor2Authmethod() + "+approval");
                        } else {
                            request.setAttribute("authmethod2", authUser.getFactor2Authmethod() + "+otp");
                        }
                    } else {
                        request.setAttribute("authmethod2", authUser.getFactor2Authmethod());
                    }
                    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(authUser, "",
                                                                                                        AuthorityUtil.getAuthorities(
                                                                                                                authUser));
                    customUsernamePasswordAuthenticationFilter.successfulAuthentication(request, response, null, token);
                    if (response.isCommitted()) {
                        return null;
                    }

                    return super.redirect("/main/main.html");
                }

                result.addError(new FieldError("user", "password", password, false, null, null,
                                               localizationService.getText("err.secondFactor.mismatch", locale())));

                // every 3 otp mistakes makes one failed authentication
                if (sfAttempts % 3 == 0) {
                    User afterFail = unsuccessfulAuthentication(request, response, authUser);
                    if (afterFail != null && !DSModel.User.Status.ACTIVE.equals(afterFail.getStatus())) {
                        HttpSession s = request.getSession(false);
                        if (s != null) {
                            try {
                                s.invalidate();
                            } catch (Exception dc) {
                            }
                        }
                        return super.redirect("/login/login.html?error=1");
                    }
                }

            } catch (Throwable t) {
                String eId = Context.getUnique();
                log.error("2nd factor Verification error id=" + eId, t);
                err = this.localizationService.getText("err.verify.secondfactor", new Object[]{eId}, locale());
                msg = null;
            }

        } else if ("checkapproval".equals(request.getParameter("cmd")) &&
                   "mdidotp".equals(authUser.getFactor2Authmethod())) {
            try {
                Message m = (Message) getSessionAttribute("mdidMessage");
                if (m != null) {
                    String mres = getModirumIDASClientService().checkMessage(authUser, m, wctx);
                    log.info("MDID message approval status for " + authUser.getLoginname() + " is " + mres);
                    if (ModirumIDASClientService.MessageStatus.cApproved.equals(mres)) {
                        setSessionAttribute("mdidMessageApproved", Boolean.TRUE);
                    }
                    response.getWriter().println(
                            "<!doctype html>\n<html><head><title>Result</title></head><body>" + mres +
                            "</body></html>");
                    return null;
                }

            } catch (Exception e) {
                log.error("Error validating MDID message approval status ", e);
            }

            response.getWriter().println(
                    "<!doctype html>\n<html><head><title>Result</title></head><body>error</body></html>");
            return null;
        } else {
            msg = this.localizationService.getText("err.invalid.secondfactor.config", locale());
            if ("smsotp".equals(authUser.getFactor2Authmethod())) {
                if (Helpers.isPhone(authUser.getFactor2AuthDeviceId())) {
                    try {
                        String sentOtp = (String) getSessionAttribute("smsotp");
                        String sentOtpDate = (String) getSessionAttribute("smsotpDate");
                        Integer sentOtpCount = (Integer) getSessionAttribute("smsotpCount");
                        if (sentOtpCount == null) {
                            sentOtpCount = 0;
                        }

                        if (sentOtp == null || sentOtpDate == null ||
                            (sentOtpCount < 3 && "true".equals(request.getParameter("newotp")))) {
                            String otp = Helpers.genRndNumPassword(7);
                            setSessionAttribute("smsotp", otp);
                            String otpDate = DateUtil.formatDate(now, "dd/MM/yy HH:mm:ss", false);
                            setSessionAttribute("smsotpDate", otpDate);

                            msg = this.localizationService.getText("msg.otp", new Object[]{otpDate}, locale());
                            String otpMsg = this.localizationService.getText("sms.otp", new Object[]{otp, otpDate},
                                                                             Locale.ENGLISH);
                            Context ctx = new Context();
                            ctx.setSettingService(ServiceLocator.getInstance().getConfigService());
                            boolean ok = ServiceLocator.getInstance().getSMSService().sendSMS(
                                    authUser.getFactor2AuthDeviceId(), null, otpMsg, 5,
                                    ServiceLocator.getInstance().getConfigService());
                            if (ok) {
                                sentOtpCount++;
                            } else {
                                err = this.localizationService.getText("err.sending.otp", locale());
                            }

                            setSessionAttribute("smsotpCount", sentOtpCount);
                        } else {
                            msg = this.localizationService.getText("msg.otp", new Object[]{sentOtpDate}, locale());
                        }
                    } catch (Exception e) {
                        log.error("Otp error", e);
                        err = this.localizationService.getText("err.sending.otp", locale());
                    }
                } else {
                    err = this.localizationService.getText("err.invalid.otp.no.phone", locale());
                }
            } else if ("mdidotp".equals(authUser.getFactor2Authmethod())) {
                String regId = (String) getSessionAttribute(request, "mdidotpactivationCode");
                Message mdidmsg = (Message) getSessionAttribute(request, "mdidMessage");
                // enrolled
                if (Misc.isNotNullOrEmpty(authUser.getFactor2Data2()) && regId == null) {
                    msg = this.localizationService.getText("msg.mdidapprorotpenter",
                                                           new Object[]{ApplicationControl.getAppName(), authUser.getLoginname()},
                                                           locale());
                    try {
                        if (mdidmsg == null &&
                            !"true".equals(wctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_OTP_ONLY.getKey()))) {
                            String msgMobile = this.localizationService.getText("msg.mdidapprmobile",
                                                                                new Object[]{Misc.merge(
                                                                                        ApplicationControl.getAppName(),
                                                                                        request.getHeader(
                                                                                                "host")), authUser.getLoginname(), DateUtil.formatDate(
                                                                                        new java.util.Date(),
                                                                                        "MMM dd HH:mm")}, locale());

                            mdidmsg = getModirumIDASClientService().sendMessage(authUser, msgMobile,
                                                                                request.getHeader("host"), wctx);
                            setSessionAttribute("mdidMessage", mdidmsg);
                            mav.addObject("checkapproval", 7500);
                        } else // otp only
                        {
                            msg = this.localizationService.getText("msg.mdidotpenter",
                                                                   new Object[]{ApplicationControl.getAppName(), request.getHeader(
                                                                           "host"), authUser.getLoginname()}, locale());
                        }

                    } catch (Exception e) {
                        String eId = Context.getUnique();
                        log.error("Modirum ID sendMsg error id=" + eId + " (shall opt to otp)", e);
                        msg = this.localizationService.getText("msg.mdidotpenter",
                                                               new Object[]{ApplicationControl.getAppName(), request.getHeader(
                                                                       "host"), authUser.getLoginname()}, locale());
                    }
                } else // new registration
                {
                    try {
                        if (regId == null) {

                            regId = this.getModirumIDASClientService().getNewRegistrationCode(authUser,
                                                                                              request.getHeader("host"),
                                                                                              wctx);
                            setSessionAttribute("mdidotpactivationStart", new java.util.Date());
                            setSessionAttribute("mdidotpactivationPing", new java.util.Date());
                            setSessionAttribute("mdidotpactivationCode", regId);
                            mav.addObject("refresh", 15000);
                            msg = this.localizationService.getText("msg.mdidotpactivate", new Object[]{regId},
                                                                   locale());
                        } else {
                            msg = this.localizationService.getText("msg.mdidotpactivate", new Object[]{regId},
                                                                   locale());
                            java.util.Date actPing = (java.util.Date) getSessionAttribute(request,
                                                                                          "mdidotpactivationPing");
                            if (actPing != null && actPing.getTime() < System.currentTimeMillis() - 10000) {
                                setSessionAttribute("mdidotpactivationPing", new java.util.Date());
                                boolean ready = this.getModirumIDASClientService().getIsEnrolledStatus(authUser, wctx);
                                if (ready) {
                                    log.info("MDID activation successful store user " + authUser.getLoginname() +
                                             " remote id as " + authUser.getFactor2Data2());
                                    ServiceLocator.getInstance().getPersistenceService().saveOrUpdate(authUser);
                                    setSessionAttribute("mdidotpactivationCode", null);
                                    setSessionAttribute("mdidotpactivationPing", null);
                                    msg = this.localizationService.getText("msg.mdidotpenter",
                                                                           new Object[]{ApplicationControl.getAppName(), authUser.getLoginname()},
                                                                           locale());
                                    mav.addObject("refresh", null);
                                } else {
                                    log.info("MDID activation for " + authUser.getLoginname() +
                                             " not complete, recheck in 10 s");
                                    mav.addObject("refresh", 7000);
                                }
                            }
                        }

                    } catch (Exception e) {
                        String eId = Context.getUnique();
                        log.error("Modirum ID reg error id=" + eId, e);
                        err = this.localizationService.getText("err.mdidotp.activation", new Object[]{eId}, locale());
                        msg = null;
                    }
                }
            } else if ("secureid".equals(authUser.getFactor2Authmethod())) {
                msg = this.localizationService.getText("msg.securid", locale());
            } else if ("pincalc".equals(authUser.getFactor2Authmethod())) {
                msg = this.localizationService.getText("msg.pincalc", locale());

            }
        }

        mav.addObject("msg", msg);
        mav.addObject("err", err);
        //mav.addObject("smsotpCount", getSessionAttribute("smsotpCount"));

        return mav;
    }

    public User unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, User failed) throws IOException, ServletException {

        User user = null;
        try {
            String username = failed != null ? failed.getLoginname() : null;
            log.info("Second factor Login failed for " + username, failed);
            user = usersService.getUserByLoginname(username);
            if (user != null) {
                Integer c = user.getLoginAttempts();
                user.setLoginAttempts((c == null ? 1 : c + 1));
                if (user.getLoginAttempts() >= customUsernamePasswordAuthenticationFilter.getMaxAttempts()) {
                    user.setLockedDate(new Date());
                    user.setStatus(DSModel.User.Status.TEMP_LOCKED);
                    log.warn("Locked user " + user.getLoginname() + " becuse of too many bad attempts");

                }
                persistenceService.saveOrUpdate(user);
            }

            StringBuffer details = new StringBuffer();
            details.append(" IP: " + request.getRemoteAddr());
            details.append(" User-Agent: " + request.getHeader("User-Agent"));

            if (user != null) {
                auditLogService.logAction(user, user, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username +
                                          "', invalid secont factor password, details: " + details);
            } else {
                auditLogService.logAction(user != null ? user : new User(), null, DSModel.AuditLog.Action.LOGINFAIL,
                                          "Login fail user '" + username + "', invalid user, details: " + details);
            }

        } catch (Exception e) {
            log.error("Login Error ", e);
            new ServletException(e);
        }

        return user;
    }


}
