package com.modirum.ds.mngr.controller;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.User;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.model.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/paymentsystems")
public class PaymentSystemController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentSystemController.class);

    @RequestMapping(value = "/paymentsystemsList.html")
    public ModelAndView paymentsystemList(HttpServletRequest request) {
        if (!AuthorityUtil.hasAnyRole(Roles.paymentsystemView, Roles.adminSetup)) {
            return unauthorized(request);
        }

        User currentUser = currentUser();
        if (!paymentSystemsService.hasAccessToAllPaymentSystems(currentUser)) {
            // Single PS user case
            return super.redirect("paymentsystemView.html?id=" + currentUser.getPaymentSystemId());
        }

        ModelAndView mav = new ModelAndView("paymentsystems/paymentsystemsList");
        List<PaymentSystem> paymentSystems = paymentSystemsService.getPaymentSystems();
        Collections.sort(paymentSystems);
        mav.addObject("paymentsystemsList", paymentSystems);
        mav.addObject("hasAccessToAllPaymentSystems", Boolean.TRUE);
        return mav;
    }

    @RequestMapping("/paymentsystemView.html")
    public ModelAndView paymentsystemView(HttpServletRequest request, @RequestParam(value = "id") Integer id) {
        PaymentSystem paymentSystem = paymentSystemsService.get(id);
        if (!AuthorityUtil.isAccessAllowed(paymentSystem, Roles.paymentsystemView) &&
            !AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("paymentsystems/paymentsystemView");
        mav.addObject("paymentSystem", paymentSystem);
        mav.addObject("hasAccessToAllPaymentSystems", paymentSystemsService.hasAccessToAllPaymentSystems(currentUser()));

        auditLogService.logAction(currentUser(), paymentSystem, DSModel.AuditLog.Action.VIEW,
                                           "View payment system " + paymentSystem.getId());

        return mav;
    }

    @RequestMapping("/paymentsystemEdit.html")
    public ModelAndView getEditPage(HttpServletRequest request, @RequestParam(value = "id", required = false) String id) {
        boolean isUpdateMode = StringUtils.isNotEmpty(id);
        if (!AuthorityUtil.hasRole(Roles.adminSetup) ||
            (!isUpdateMode && !paymentSystemsService.hasAccessToAllPaymentSystems(currentUser()))) {
            return unauthorized(request);
        }

        PaymentSystem paymentSystem = null;
        if (isUpdateMode) {
            paymentSystem = paymentSystemsService.get(Integer.parseInt(id));
        }
        if (paymentSystem == null) {
            paymentSystem = new PaymentSystem();
        }

        ModelAndView mav = new ModelAndView("paymentsystems/paymentsystemEdit");
        mav.addObject("paymentSystem", paymentSystem);
        return mav;
    }

    @RequestMapping("/paymentsystemUpdate.html")
    public ModelAndView savePaymentSystem(HttpServletRequest request, @ModelAttribute("paymentSystem") PaymentSystem paymentSystem, BindingResult result) throws Exception {
        Integer paymentSystemId = paymentSystem.getId();
        Short port = paymentSystem.getPort();
        boolean isUpdateMode = paymentSystemId != null;

        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }

        // validations
        if (StringUtils.isEmpty(paymentSystem.getName())) {
            result.addError(
                new FieldError("paymentSystem", "name", null, false, null, null,
                    getLocalizedMessage("err.required", "text.paymentsystem.name")));
        }
        if (port == null) {
            result.addError(
                new FieldError("paymentSystem", "port", null, false, null, null,
                    getLocalizedMessage("err.required", "text.paymentsystem.port")));
        } else {
            Optional<Integer> existingPaymentSystemId = persistenceService.getPaymentSystemIdByPort(port);
            if (existingPaymentSystemId.isPresent() && (!isUpdateMode || !existingPaymentSystemId.get().equals(paymentSystemId))) {
                result.addError(new FieldError("paymentSystem", "port", port, false, null, null,
                        getLocalizedMessage("err.duplicate.value", "text.paymentsystem.port")));
            }
        }
        if (result.hasErrors()) {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("paymentsystems/paymentsystemEdit");
            mav.addObject("id", paymentSystemId);
            return mav;
        }

        // save payment system
        persistenceService.saveOrUpdate(paymentSystem);
        if (isUpdateMode) {
            auditLogService.logUpdate(AuthorityUtil.getUser(), paymentSystem,"Updated payment system " + paymentSystemId);
        } else {
            paymentSystemId = paymentSystem.getId();
            auditLogService.logInsert(AuthorityUtil.getUser(), paymentSystem, "Added payment system " + paymentSystemId);
        }

        return super.redirect("paymentsystemView.html?id=" + paymentSystemId);
    }
}
