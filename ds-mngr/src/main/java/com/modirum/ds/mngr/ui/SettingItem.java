package com.modirum.ds.mngr.ui;

import java.util.Date;

public class SettingItem {
    private String key;
    private String type;
    private String value;
    private String comment;
    private String placeholder;
    private Boolean isEditable = Boolean.TRUE;
    private Date lastModified;
    private String lastModifiedBy;

    public String getKey() {
        return key;
    }

    public SettingItem setKey(String key) {
        this.key = key;
        return this;
    }

    public String getType() {
        return type;
    }

    public SettingItem setType(String type) {
        this.type = type;
        return this;
    }

    public String getValue() {
        return value;
    }

    public SettingItem setValue(String value) {
        this.value = value;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public SettingItem setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public SettingItem setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
        return this;
    }

    public boolean getIsEditable() {
        return isEditable;
    }

    public SettingItem setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
        return this;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public SettingItem setLastModified(Date lastModified) {
        this.lastModified = lastModified;
        return this;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public SettingItem setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }
}
