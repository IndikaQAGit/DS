package com.modirum.ds.mngr.ui;

import com.modirum.ds.enums.DSId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@Builder
public class PaymentSystemConfigUIForm {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");

    private Integer dsId;
    private Integer paymentSystemId;
    private String key;
    private String inputType;
    private String comment;
    private String value;
    private Boolean editable;
    private String lastModifiedBy;
    private LocalDateTime lastModifiedDate;

    public String getLastModified(){
        if(lastModifiedDate == null)
            return "";

        String formatedLastModified = "";
        formatedLastModified = lastModifiedDate.format(formatter);
        return String.format("%s    %s", formatedLastModified, lastModifiedBy);
    }

    public Integer getDsId(){
        if(dsId == null)
            return DSId.DEFAULT;
        return dsId;
    }
}
