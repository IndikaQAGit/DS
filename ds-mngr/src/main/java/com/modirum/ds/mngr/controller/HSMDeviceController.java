package com.modirum.ds.mngr.controller;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceConfService;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceService;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping({"/hsmdevices"})
public class HSMDeviceController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(HSMDeviceController.class);

    @Autowired
    protected HsmDeviceService hsmDeviceService;

    @Autowired
    protected HsmDeviceConfService hsmDeviceConfService;

    @RequestMapping("/hsmDeviceList")
    public ModelAndView getListPage(HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.keyManage)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("hsmdevices/hsmDeviceList");
        List<HsmDevice> hsmDeviceList = hsmDeviceService.getActiveHSMDevices();
        Map<String, String> hsmDeviceConfMap = getHsmDeviceConfMap(hsmDeviceList);
        mav.addObject("hsmDeviceConfMap", hsmDeviceConfMap);
        mav.addObject("hsmDeviceList", hsmDeviceList);
        return mav;
    }

    @RequestMapping("/hsmDeviceView")
    public ModelAndView getViewPage(HttpServletRequest request, @RequestParam(value = "id") Long id) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.keyManage)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("hsmdevices/hsmDeviceView");
        HsmDevice hsmDevice = hsmDeviceService.get(id);
        mav.addObject("hsmDevice", hsmDevice);
        HsmDeviceConf hsmDeviceConf = hsmDeviceConfService.get(id);
        if (hsmDeviceConf != null) {
            mav.addObject("hsmDeviceConfig", hsmDeviceConf.getConfig());
        }
        mav.addObject("deleteConfirmMsg", getLocalizedMessage("text.wrn.are.you.sure.delete", getLocalizedMessage("text.HSMDevice")));
        return mav;
    }

    @RequestMapping("/hsmDeviceEdit")
    public ModelAndView getEditPage(HttpServletRequest request, @RequestParam(value = "id", required = false) Long id) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.keyManage)) {
            return unauthorized(request);
        }

        boolean isUpdateMode = Misc.isNotNullOrEmpty(id);
        HsmDevice hsmDevice = null;
        HsmDeviceConf hsmDeviceConf = null;
        if (isUpdateMode) {
            hsmDevice = hsmDeviceService.get(id);
            hsmDeviceConf = hsmDeviceConfService.get(id);
        }
        if (hsmDevice == null) {
            hsmDevice = new HsmDevice();
        }
        if (hsmDeviceConf == null) {
            hsmDeviceConf = new HsmDeviceConf();
        }

        ModelAndView mav = getEditPageTemplate();
        mav.addObject("hsmDevice", hsmDevice);
        mav.addObject("hsmDeviceConf", hsmDeviceConf);
        return mav;
    }

    @RequestMapping("/hsmDeviceDelete")
    public ModelAndView deleteHsmDevice(HttpServletRequest request, @RequestParam(value = "id", required = false) Long id) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.keyManage)) {
            return unauthorized(request);
        }

        HsmDevice hsmDevice = hsmDeviceService.get(id);
        hsmDevice.setStatus(DSModel.HsmDevice.Status.DISABLED);
        hsmDeviceService.update(hsmDevice);
        auditLogJdbcService.logUpdate(hsmDevice, currentUser());
        return redirect("hsmDeviceList.html");
    }

    @RequestMapping("/hsmDeviceUpdate")
    public ModelAndView saveHsmDevice(HttpServletRequest request,
                                       @ModelAttribute("hsmDevice") HsmDevice hsmDevice,
                                       BindingResult serviceResult,
                                       @ModelAttribute("hsmDeviceConf") HsmDeviceConf hsmDeviceConf,
                                       BindingResult configResult) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.keyManage)) {
            return unauthorized(request);
        }

        Long hsmDeviceId = hsmDevice.getId();
        boolean isUpdateMode = hsmDeviceId != null;

        trimHsmDeviceFields(hsmDevice);
        trimHsmDeviceConfFields(hsmDeviceConf);

        // validations
        if (Misc.isNullOrEmpty(hsmDevice.getName())) {
            serviceResult.addError(
                new FieldError("hsmDevice", "name", getLocalizedMessage("err.required", "text.HSMDevice.name")));
        } else if (hsmDeviceService.isExistingActiveHSMDeviceName(hsmDevice.getName(), hsmDeviceId)) {
            serviceResult.addError(
                new FieldError("hsmDevice", "name", hsmDevice.getName(), false, null, null, getLocalizedMessage("err.duplicate.value")));
        }
        if (Misc.isNullOrEmpty(hsmDevice.getClassName())) {
            serviceResult.addError(
                new FieldError("hsmDevice", "className", getLocalizedMessage("err.required", "text.HSMDevice.className")));
        }
        if (Misc.isNotNullOrEmpty(hsmDeviceConf.getConfig()) &&
            !hsmDeviceConf.getConfig().matches("([^=;]+=[^=;]+(;(?!$)|$))+")) {
            configResult.addError(
                new FieldError("hsmDeviceConf", "config", hsmDeviceConf.getConfig(), false, null, null,
                    getLocalizedMessage("err.hsmdeviceconf.config.invalid.format", "text.HSMDeviceConf.config")));
        }
        if (serviceResult.hasErrors() || configResult.hasErrors()) {
            ModelAndView mav = getEditPageTemplate();
            mav.addObject("hsmDevice", hsmDevice);
            mav.addObject("hsmDeviceConf", hsmDeviceConf);
            return mav;
        }

        // save hsm device
        hsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
        hsmDeviceService.saveOrUpdate(hsmDevice);
        if (isUpdateMode) {
            auditLogJdbcService.logUpdate(hsmDevice, currentUser());
        } else {
            hsmDeviceId = hsmDevice.getId();
            auditLogJdbcService.logInsert(hsmDevice, currentUser());
        }

        // save hsm device conf
        if (StringUtils.isNotEmpty(hsmDeviceConf.getConfig()) || isUpdateMode) {
            if (hsmDeviceConf.getHsmDeviceId() == null) {
                hsmDeviceConf.setHsmDeviceId(hsmDeviceId);
                insertHsmDeviceConf(hsmDeviceConf);
            } else {
                updateHsmDeviceConf(hsmDeviceConf);
            }
        }

        return super.redirect("hsmDeviceView.html?id=" + hsmDeviceId);
    }

    private void updateHsmDeviceConf(HsmDeviceConf hsmDeviceConf) {
        if (StringUtils.isEmpty(hsmDeviceConf.getConfig())) {
            hsmDeviceConfService.delete(hsmDeviceConf);
            auditLogJdbcService.logDelete(hsmDeviceConf, currentUser());
        } else {
            hsmDeviceConfService.update(hsmDeviceConf);
            auditLogJdbcService.logUpdate(hsmDeviceConf, currentUser());
        }
    }

    private void insertHsmDeviceConf(HsmDeviceConf hsmDeviceConf) {
        hsmDeviceConfService.insert(hsmDeviceConf);
        auditLogJdbcService.logInsert(hsmDeviceConf, currentUser());
    }

    /**
     * Get map of HSM Device Configurations given list of HSM Devices
     * @param hsmDeviceList        List of HSM Devices
     * @return hsmDeviceConfMap    Map of HSM Device Configurations, where key is hsmDeviceId and value is hsmDeviceConfig
     * @throws Exception
     */
    private Map<String, String> getHsmDeviceConfMap(List<HsmDevice> hsmDeviceList) {
        Map<String, String> hsmDeviceConfMap = new LinkedHashMap<>();
        hsmDeviceList.forEach(hsmDevice -> {
            Long hsmDeviceId = hsmDevice.getId();
            try {
                String config = hsmDeviceConfService.get(hsmDeviceId).getConfig();
                hsmDeviceConfMap.put(String.valueOf(hsmDevice.getId()), config);
            } catch (Exception e) {
                log.debug("Error encountered when retrieving config record for HSM Device ID {}. Error: {}", hsmDeviceId, e.getMessage());
            }
        });
        return hsmDeviceConfMap;
    }

    private void trimHsmDeviceFields(HsmDevice hsmDevice) {
        hsmDevice.setName(StringUtils.trim(hsmDevice.getName()));
        hsmDevice.setClassName(StringUtils.trim(hsmDevice.getClassName()));
    }

    private void trimHsmDeviceConfFields(HsmDeviceConf hsmDeviceConf) {
        hsmDeviceConf.setConfig(StringUtils.trim(hsmDeviceConf.getConfig()));
    }

    private ModelAndView getEditPageTemplate() {
        ModelAndView mav = new ModelAndView("hsmdevices/hsmDeviceEdit");
        mav.addObject("helpText", getLocalizedMessage("general.help"));
        mav.addObject("configTooltip", getLocalizedMessage("text.tooltip.hsmDeviceEdit.config"));
        mav.addObject("deleteConfirmMsg", getLocalizedMessage("text.wrn.are.you.sure.delete", getLocalizedMessage("text.HSMDevice")));
        return mav;
    }
}