package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.Text;
import com.modirum.ds.db.model.TextSearcher;
import com.modirum.ds.services.ConfigService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Andri Kruus
 * @version 1.0, 2.02.2012
 */
@Controller
@RequestMapping("/localization")
public class LocalizationController extends BaseController {

    private final transient Logger log = LoggerFactory.getLogger(LocalizationController.class);

    protected ConfigService configService = ServiceLocator.getInstance().getConfigService();

    @ModelAttribute("supportedLocales")
    public java.util.Map<String, String> getSupportedLocales() {
        java.util.Map<String, String> am = new java.util.LinkedHashMap<String, String>();
        am.put("", "");
        try {
            List<java.util.Locale> sl = this.getWebContext().getSupportedLocaleList();
            for (int i = 0; i < sl.size(); i++) {
                java.util.Locale ct = sl.get(i);
                if (ct.getCountry() != null && am.get(ct.getLanguage()) == null) {
                    am.put(ct.getLanguage(), localizationService.getText("locale." + ct.getLanguage(), locale()));
                }
                am.put(ct.toString(), localizationService.getText("locale." + ct.toString(), locale()));
            }
        } catch (Exception e) {
            log.error("Error getting locales list", e);
        }
        return am;
    }


    @RequestMapping("/textEdit.html")
    public ModelAndView textEdit(HttpServletRequest request, @RequestParam(value = "tid", required = false) Integer tid) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.textEdit)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("localization/textEdit");
        try {

            if (tid != null) {
                Text t = persistenceService.getPersistableById(tid, Text.class);
                if (t == null) {
                    modelAndView.addObject("error", "Text by id not found");
                    modelAndView.addObject("text", new Text());
                } else {
                    modelAndView.addObject("text", t);
                }
            } else {
                modelAndView.addObject("text", new Text());
            }
        } catch (Exception e) {
            log.error("Error edit text", e);
            throw e;
        }

        return modelAndView;
    }

    @RequestMapping(value = "/textSave.html", method = RequestMethod.POST)
    public ModelAndView textSave(HttpServletRequest request, @RequestParam(value = "tid", required = false) Integer tid, @RequestParam(value = "cmd", required = false) String cmd, @Valid Text text, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.textEdit)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("localization/textEdit");
        try {

            if ("save".equals(cmd) || Misc.isNullOrEmpty(cmd)) {

                // validation
                if (Misc.isNullOrEmpty(text.getKey())) {
                    result.addError(new FieldError("text", "key", text.getKey(), false, null, null,
                                                   "Text key can not be empty."));
                }

                if (Misc.isNullOrEmpty(text.getLocale())) {
                    result.addError(new FieldError("text", "locale", text.getLocale(), false, null, null,
                                                   "Language code is required (2 letter code)"));
                } else if (text.getLocale().length() != 2 && text.getLocale().length() != 5) {
                    result.addError(new FieldError("text", "locale", text.getLocale(), false, null, null,
                                                   "Language code must be iso 639-1 2 letter code or combination with country en_US"));
                }

                if (Misc.isNullOrEmpty(text.getMessage())) {
                    result.addError(new FieldError("text", "message", text.getMessage(), false, null, null,
                                                   "Text message can not be empty."));
                } else if (text.getMessage().length() > 8000) {
                    result.addError(new FieldError("text", "message", text.getMessage(), false, null, null,
                                                   "Text message maximum length allowed is 8000 chars."));
                }

                if (!result.hasErrors()) {
                    TextSearcher searcher = new TextSearcher();
                    searcher.setKey(text.getKey());
                    searcher.setLocale(text.getLocale());
                    java.util.List<Text> duplicates = localizationService.findTexts(searcher);
                    for (int i = 0; duplicates != null && i < duplicates.size(); i++) {
                        Text duptext = duplicates.get(i);
                        if (text.getId() == null || text.getId() != null && !duptext.getId().equals(text.getId())) {
                            result.addError(new FieldError("text", "key", text.getKey(), false, null, null,
                                                           "Duplicate text, text with same key and language already exists."));
                        }
                    }
                }

                if (!result.hasErrors()) {
                    persistenceService.saveOrUpdate(text);
                    // update running service
                    localizationService.putText(text);

                    //com.modirum.mdpay.vpos.resources.ResourceHelperDb.resetTextCache();
                    // tell vpos to reload its texts
                    Setting textReset = configService.getSettingByKey(DsSetting.RESET_TEXT_CACHE.getKey());
                    if (textReset != null) {
                        textReset.setValue("true");
                        persistenceService.saveOrUpdate(textReset);
                    }
                    LocalizationService.resetTextCache();
                    resetMaps();
                    return super.redirect("textsList.html?save=true");
                }
            } else if ("delete".equals(cmd)) {
                if (text != null && text.getId() != null) {
                    persistenceService.delete(text);
                    LocalizationService.resetTextCache();
                    resetMaps();
                }
                return super.redirect("textsList.html?del=true");
            }
        } catch (Exception e) {
            log.error("Error getting/saving text", e);
            throw e;
        }

        return modelAndView;
    }

    @RequestMapping("/textsList.html")
    public ModelAndView textsList(HttpServletRequest request, @RequestParam(value = "cmd", required = false) String cmd, @ModelAttribute(value = "s") TextSearcher searcher, BindingResult result) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.textView)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("localization/textsList");
        if ("true".equals(request.getParameter("save"))) {
            modelAndView.addObject("msg", localizationService.getText("text.saved.ok", locale()));
        } else if ("true".equals(request.getParameter("del"))) {
            modelAndView.addObject("msg", localizationService.getText("text.deleted.ok", locale()));
        }

        try {

            if (searcher != null && "search".equals(cmd)) {
                if (searcher.getStart() == null) {
                    searcher.setStart(0);
                }
                if (searcher.getLimit() == null) {
                    searcher.setLimit(50);
                }

                modelAndView.addObject("searcher", searcher);
                modelAndView.addObject("textslist", localizationService.findTexts(searcher));
            }
        } catch (Exception e) {
            log.error("Error getting texts  list", e);
            throw e;
        }

        return modelAndView;
    }

}
