package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.hsm.KeyServiceBase;
import com.modirum.ds.mngr.model.CSR;
import com.modirum.ds.mngr.model.CertificateDataSession;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.CertificateData;
import com.modirum.ds.model.CertificateSearcher;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.KeyData.KeyStatus;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.KeyService.KeyAlias;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceService;
import com.modirum.ds.util.CertUtil;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.ObjectDiff;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.pkcs.bc.BcPKCS10CertificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import sun.security.x509.X500Name;

import javax.security.auth.x500.X500Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Controller
@RequestMapping("/ca")
public class CAController extends BaseController {
    public static final String SIGNED_CERTIFICATES_MAP_SESSION_KEY = "signedCertificates";
    private final transient Logger log = LoggerFactory.getLogger(CAController.class);

    @Autowired
    private KeyService keyService;

    @Autowired
    private HsmDeviceService hsmDeviceService;

    @ModelAttribute("countryMapA2")
    public Map<String, String> countryMap() {
        return super.countriesMapA2();
    }

    @ModelAttribute("certificateStatuses")
    public Map<String, String> certificateStatusesMap() {
        return statusMap(DSModel.CertificateData.Status.class.getName());
    }

    @ModelAttribute("MIStatuses")
    public Map<String, String> MIStatusesMap() {
        return statusMap(DSModel.TDSServerProfile.Status.class.getName());
    }

    @ModelAttribute("merchantStatuses")
    public Map<String, String> MerchantStatusesMap() {
        return statusMap(DSModel.Merchant.Status.class.getName());
    }

    @RequestMapping("/certificateList.html")
    public ModelAndView certificateList(HttpServletRequest request, @ModelAttribute(value = "searcher") CertificateSearcher searcher) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();

        ModelAndView mav = new ModelAndView("ca/certificateList");
        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {
            if (searcher.getStart() == null) {
                searcher.setStart(0);
            }
            if (searcher.getLimit() == null) {
                searcher.setLimit(25);
            }

            if (searcher.getExpiresTo() != null) {
                searcher.setExpiresTo(DateUtil.normalizeDateDayEnd(searcher.getExpiresTo()));
            }
            if (searcher.getIssuedTo() != null) {
                searcher.setIssuedTo(DateUtil.normalizeDateDayEnd(searcher.getIssuedTo()));
            }

            java.util.Map<String, Object> query = new java.util.HashMap<String, Object>();
            if (searcher.getExpiresFrom() != null) {
                query.put("expires>=", searcher.getExpiresFrom());
            }
            if (searcher.getExpiresTo() != null) {
                query.put("expires<=", searcher.getExpiresTo());
            }

            if (searcher.getIssuedFrom() != null) {
                query.put("issued>=", searcher.getIssuedFrom());
            }
            if (searcher.getIssuedTo() != null) {
                query.put("issued<=", searcher.getIssuedTo());
            }

            if (Misc.isNotNullOrEmpty(searcher.getName())) {
                query.put("name", searcher.getName() + "%");
            }
            if (Misc.isNotNullOrEmpty(searcher.getIssuerDN())) {
                query.put("issuerDN", searcher.getIssuerDN() + "%");
            }
            if (Misc.isNotNullOrEmpty(searcher.getSubjectDN())) {
                query.put("subjectDN", searcher.getSubjectDN() + "%");
            }
            if (Misc.isNotNullOrEmpty(searcher.getStatus())) {
                query.put("status", searcher.getStatus());
            }

            List<CertificateData> found = persistenceService.getPersitableList(CertificateData.class,
                                                                               searcher.getOrder(),
                                                                               searcher.getOrderDirection(), true,
                                                                               searcher.getStart(), searcher.getLimit(),
                                                                               query);

            searcher.setTotal(Integer.valueOf(((Long) query.get("total")).intValue()));

            mav.addObject("found", found);
            mav.addObject("searcher", searcher);

            if (log.isDebugEnabled()) {
                log.debug("Certificate list done in " + (System.currentTimeMillis() - start) + " ms");
            }

            if ("true".equals(cnfs.getStringSetting(DsSetting.AUDIT_LOG_SEARCHES.getKey()))) {
                String detail = "" + ObjectDiff.properties(searcher, true);
                auditLogService.logAction(AuthorityUtil.getUser(), new CertificateData(),
                                          DSModel.AuditLog.Action.SEARCH, detail);
            }
        } else {
            if (Misc.isNullOrEmpty(searcher.getLimit())) {
                searcher.setLimit(25);
            }
            if (Misc.isNullOrEmpty(searcher.getOrder())) {
                searcher.setOrder("id");
                searcher.setOrderDirection("desc");
            }
        }

        return mav;
    }

    @RequestMapping("/certificateView.html")
    public ModelAndView viewCertificate(@RequestParam("id") Long id, HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("ca/certificateView");
        if ("true".equals(request.getParameter("revoke"))) {
            mav.addObject("msg", localizationService.getText("certificate.revoked", locale()));
        }

        CertificateData is = persistenceService.getPersistableById(id, CertificateData.class);

        mav.addObject("certificate", is);
        if (is != null) {
            if (is.getId() != null) {
                java.util.Map<String, Object> query = new java.util.HashMap<String, Object>();
                query.put("inClientCert", is.getId());
                persistenceService.getPersitableList(TDSServerProfile.class, null, null, true, 0, 0, query);
                mav.addObject("MICount", query.get("total"));
                query.remove("total");

                persistenceService.getPersitableList(ACSProfile.class, null, null, true, 0, 0, query);
                mav.addObject("ACSCount", query.get("total"));
            }
            this.auditLogService.logAction(AuthorityUtil.getUser(), is, DSModel.AuditLog.Action.VIEW,
                                           "View certificate " + is.getId());
        }

        return mav;
    }

    @RequestMapping(value = "/certificateRevoke.html", method = RequestMethod.POST)
    public ModelAndView revokeCertificate(@RequestParam("id") Long id, HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caEdit)) {
            return unauthorized(request);
        }

        CertificateData is = persistenceService.getPersistableById(id, CertificateData.class);
        if (is != null) {
            is.setStatus(DSModel.CertificateData.Status.cRevoked);
            persistenceService.saveOrUpdate(is);
            this.auditLogService.logAction(AuthorityUtil.getUser(), is, DSModel.AuditLog.Action.UPDATE,
                                           "Revoke certificate " + is.getId());
            return super.redirect("certificateView.html?id=" + is.getId() + "&revoke=true");
        }

        return super.redirect("certificateList.html?revoke=fail");
    }

    private Map<String, String> caKeyMap() throws Exception {
        @SuppressWarnings("unchecked")
        Map<String, String> map = (Map<String, String>) getContextAttribute("caMap_cached" + locale());
        Date caMapDate = (Date) getContextAttribute("caMapDate" + locale());

        List<KeyData> kdlt = persistenceService.getKeyDatasByAlias("scheme" + "%", KeyStatus.working, "keyDate", "desc",
                                                                   0, 1, null);
        java.util.Date lastKeyDate = null;
        if (kdlt.size() > 0) {
            lastKeyDate = kdlt.get(0).getKeyDate();
        }


        if (map == null || caMapDate != null && (caMapDate.getTime() < System.currentTimeMillis() - DateUtil.DT24H ||
                                                 lastKeyDate != null && lastKeyDate.after(caMapDate))) {
            map = new LinkedHashMap<>();

            List<KeyData> kdl = persistenceService.getKeyDatasByAlias(KeyAlias.schemeIntermedCert + "%",
                                                                      KeyStatus.working, "keyAlias", null, null, null,
                                                                      null);
            List<KeyData> kdl2 = persistenceService.getKeyDatasByAlias(KeyAlias.schemeRootCert + "%", KeyStatus.working,
                                                                       "keyAlias", null, null, null, null);
            kdl.addAll(kdl2);

            for (KeyData kx : kdl) {
                // only roots or intermediates accompanied with private key
                boolean hasPrivateKey = KeyServiceBase.parsePemPrivateKey(kx.getKeyData()) != null
                        || KeyServiceBase.parsePemEncryptedPrivateKey(kx.getKeyData()) != null;

                String x509certificatePem = KeyService.parsePEMCert(kx.getKeyData());

                if (hasPrivateKey && x509certificatePem != null) {
                    byte[] cerPlain = Base64.decode(x509certificatePem);
                    X509Certificate[] certs = KeyService.parseX509Certificates(cerPlain, "X509");
                    X509Certificate cert = certs[0];
                    if (cert.getNotAfter().getTime() < System.currentTimeMillis()) {
                        continue;
                    }

                    String commonName = "CN=" + ((X500Name) cert.getSubjectDN()).getCommonName();
                    String sdn = commonName + "," + cert.getSubjectX500Principal().getName(X500Principal.RFC2253);
                    sdn = sdn.replace("," + commonName, "");
                    if (sdn.length() > 50) {
                        sdn = Misc.cut(sdn, 48) + "..";
                    }

                    map.put(String.valueOf(kx.getId()),
                            Misc.replace(kx.getKeyAlias(), "scheme", "") + "[" + sdn + "] " +
                            DateUtil.formatDate(cert.getNotBefore(), this.getDateFormat()) + " - " +
                            DateUtil.formatDate(cert.getNotAfter(), this.getDateFormat()));
                }
            }
            setContextAttribute("caMap_cached" + locale(), map);
            setContextAttribute("caMapDate" + locale(), new java.util.Date());
        }

        return map;
    }

    private Map<String, String> sigAlgs() throws Exception {
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("", "");
        map.put(KeyService.sigAlgRSASHA256,
                localizationService.getText("sigalg." + KeyService.sigAlgRSASHA256, locale()));
        map.put(KeyService.sigAlgRSASHA384,
                localizationService.getText("sigalg." + KeyService.sigAlgRSASHA384, locale()));
        map.put(KeyService.sigAlgRSASHA512,
                localizationService.getText("sigalg." + KeyService.sigAlgRSASHA512, locale()));
        return map;

    }

    private Map<String, String> extUses() throws Exception {
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("2", "Client Authentication");
        map.put("3", "Server Authentication");
        map.put("12", "Client & Server Auth.");
        return map;
    }


    @RequestMapping("/signCSR.html")
    public ModelAndView signCSR(HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caEdit)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("ca/signCSR");
        CSR csr = new CSR();
        csr.setExpires(DateUtil.normalizeDateDayEnd(DateUtil.addMonths(new java.util.Date(), 12)));
        csr.setSigalg(KeyService.sigAlgRSASHA256);
        mav.addObject("csr", csr);
        Map<String, String> caKeyMap = caKeyMap();
        mav.addObject("caKeyMap", caKeyMap);
        mav.addObject("sigAlgMap", sigAlgs());
        mav.addObject("extUsesMap", extUses());

        if (caKeyMap.size() < 1) {
            mav.addObject("msg", localizationService.getText("text.msg.nocafunc.noschemeroot.privatekeys", locale()));
        }

        return mav;
    }

    @RequestMapping(value = "/signCSR2.html", method = RequestMethod.POST)
    public ModelAndView signCSR2(@ModelAttribute(value = "csr") CSR csr, HttpServletRequest request, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caEdit)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("ca/signCSR");
        Map<String, String> caKeyMap = caKeyMap();
        mav.addObject("caKeyMap", caKeyMap);
        mav.addObject("sigAlgMap", sigAlgs());
        mav.addObject("extUsesMap", extUses());
        mav.addObject("csr", csr);
        int forServer = Misc.parseInt(csr.getExtUse());

        if (caKeyMap.size() < 1) {
            mav.addObject("msg", localizationService.getText("text.msg.nocafunc.noschemeroot.privatekeys", locale()));
            return mav;
        }

        BcPKCS10CertificationRequest certReq = null;
        X500Principal subject = null;

        if("true".equals(request.getParameter("fileSelect"))) {
            if (csr.getUploadFile() == null || csr.getUploadFile().getSize() < 400) {
                result.addError(new FieldError("uploadFile", "uploadFile", null, false, null, null,
                                               localizationService.getText("err.required", localizationService.getText(
                                                       "text.uploadfile", locale()), locale())));
            } else {
                try {

                    KeyService.getBouncyCastleProvider();
                    new BcPKCS10CertificationRequest(KeyService.parsePEMCSR(csr.getUploadFile().getBytes()));
                    csr.setPemData(new String(csr.getUploadFile().getBytes(), StandardCharsets.US_ASCII));

                } catch (Exception e) {
                    log.error("Failed to parse certificate request file", e);
                    result.addError(new FieldError("csr", "uploadFile", null, false, null, null,
                                                   localizationService.getText("err.invalid",
                                                                               localizationService.getText(
                                                                                       "err.must.be.valid.pkcs10",
                                                                                       locale()), locale())));
                }
            }
        } else if (request.getParameter("signBtn") != null) {
            if (Misc.isNotNullOrEmpty(csr.getPemData())) {
                try {
                    KeyService.getBouncyCastleProvider();
                    certReq = new BcPKCS10CertificationRequest(KeyService.parseCSR(csr.getPemData()));
                    subject = new X500Principal(certReq.getSubject().getEncoded());
                    csr.setSubjectDN(subject.getName(X500Principal.RFC2253));
                    csr.setSubjAltNames(Misc.listToCSV(CertUtil.getSubjectAltNames(certReq)));
                } catch (Exception e) {
                    log.error("Failed to parse certificate request", e);
                    result.addError(new FieldError("csr", "pemData", csr.getPemData(), false, null, null,
                                                   localizationService.getText("err.invalid", localizationService.getText(
                                                           "err.must.be.valid.pkcs10", locale()), locale())));
                }
            }

            validateCsr(csr, result);

            if (!result.hasErrors()) {
                mav.addObject("errors", "0");

                X509Certificate certificate = signCsrAndGenerateCert(csr, forServer, certReq, subject);

                StringBuilder certificateStringBuilder = new StringBuilder();
                certificateStringBuilder.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
                certificateStringBuilder.append(Base64.encode(certificate.getEncoded(), 76));
                certificateStringBuilder.append("\r\n" + KeyService.END_CERTIFICATE);

                log.info("Signed certificate issuer " + certificate.getIssuerDN() + " subject " +
                         certificate.getSubjectDN());
                log.info("\n" + certificateStringBuilder);

                CertificateData certData = new CertificateData();
                certData.setExpires(certificate.getNotAfter());
                certData.setIssued(certificate.getNotBefore());
                certData.setIssuerDN(certificate.getIssuerX500Principal().getName(X500Principal.RFC2253));
                certData.setSubjectDN(certificate.getSubjectX500Principal().getName(X500Principal.RFC2253));
                certData.setStatus(CertificateData.Status.cValid);
                certData.setX509data(certificateStringBuilder.toString());
                certData.setName(csr.getName());

                X509Certificate caCert = keyService.loadCertificate(csr.getCaKeyId(), null);

                certificateStringBuilder.append("\r\n");
                certificateStringBuilder.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
                certificateStringBuilder.append(Base64.encode(caCert.getEncoded(), 76));
                certificateStringBuilder.append("\r\n" + KeyService.END_CERTIFICATE);

                if (!KeyService.isRootCert(caCert)) {
                    List<X509Certificate> schemeRoots = keyService.getSchemeRoots();
                    // find correct root...
                    for (X509Certificate rootx : schemeRoots) {
                        if (rootx.getSubjectX500Principal().getName(X500Principal.RFC2253).equals(
                                caCert.getIssuerX500Principal().getName(X500Principal.RFC2253))) {
                            certificateStringBuilder.append("\r\n");
                            certificateStringBuilder.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
                            certificateStringBuilder.append(Base64.encode(rootx.getEncoded(), 76));
                            certificateStringBuilder.append("\r\n" + KeyService.END_CERTIFICATE);
                            break;
                        }
                    }
                }
                csr.setChain(certificateStringBuilder.toString());

                saveTemporaryCertificateDataInSession(request, csr, certData);

                CertificateData existingCertificateData = persistenceService.getCertificateDataBySDN(subject.getName(X500Principal.RFC2253));
                if (existingCertificateData != null) {
                    mav.addObject("duplicate", "true");
                }

            }

        } else if (request.getParameter("uploadBtn") != null) {

            Map<String, CertificateDataSession> certMap = (Map<String, CertificateDataSession>)request.getSession().getAttribute(SIGNED_CERTIFICATES_MAP_SESSION_KEY);
            CertificateDataSession certificateDataSession = certMap.get(csr.getPemData());

            CertificateData certDataFromSession = certificateDataSession.getCertificateData();

            // needed also here for safety of we dont know what may have happened in other windows sessions meanwhile
            CertificateData existingCertData = persistenceService.getCertificateDataBySDN(certDataFromSession.getSubjectDN());
            boolean isNewCert = (existingCertData == null);

            //check if upload endpoint is triggered but replace checkbox is not ticked
            if (!isNewCert && !csr.isReplace()) {
                //reload the upload page using the the computed CSR data model from session
                mav.getModelMap().addAttribute("csr", certificateDataSession.getCsr());
                mav.addObject("duplicate", "true");
                return mav;
            }

            saveCertificate(certDataFromSession, isNewCert);

            //remove this certificate from session since already stored
            certMap.remove(csr.getPemData());
            clearContextAttributeStartsWith(new String[]{"inCertificateList_cached"});

            mav.addObject("chain", certificateDataSession.getCsr().getChain());
            mav.addObject("subjAltNames", certificateDataSession.getCsr().getSubjAltNames());
            mav.addObject("certificate", certDataFromSession);

        }

        return mav;
    }

    private void saveCertificate(CertificateData certDataFromSession, boolean isNewCert) throws Exception {
        CertificateData existingCertData = persistenceService.getCertificateDataBySDN(certDataFromSession.getSubjectDN());
        if (isNewCert) {
            persistenceService.save(certDataFromSession);
        } else {
            certDataFromSession.setId(existingCertData.getId());
            persistenceService.saveOrUpdate(certDataFromSession);
        }

        log.info((isNewCert ? "New" : "Re-issue (overwrite)") + " certificate " + certDataFromSession.getSubjectDN() +
                 " saved to store with id " + certDataFromSession.getId() + " and name " + certDataFromSession.getName());

        this.auditLogService.logAction(AuthorityUtil.getUser(), certDataFromSession,
                                       isNewCert ? DSModel.AuditLog.Action.INSERT : DSModel.AuditLog.Action.UPDATE,
                                       isNewCert ? "New certificate " + certDataFromSession.getId() + " issued" :
                                       "Re-issue certificate " + certDataFromSession.getId());
    }

    /**
     * Using temporary session map for signed certificates before uploading(saving in database). This is safer
     * compare to putting the values on hidden fields on html forms, which can be hacked without backend validations.
     * Backend validations will just repeat the same process, so might as well store the already signed certificate in
     * session. Once user is logged out, these session data will be cleared anyway.
     * @param request
     * @param csr
     * @param cd
     */
    private void saveTemporaryCertificateDataInSession(HttpServletRequest request, CSR csr, CertificateData cd) {
        Map<String, CertificateDataSession> inSessionSignedCertMap = (Map<String, CertificateDataSession>) request.getSession().getAttribute(SIGNED_CERTIFICATES_MAP_SESSION_KEY);
        if (inSessionSignedCertMap == null) {
            inSessionSignedCertMap = new HashMap<>();
            request.getSession().setAttribute(SIGNED_CERTIFICATES_MAP_SESSION_KEY, inSessionSignedCertMap);
        }
        CertificateDataSession certificateDataSession = new CertificateDataSession();
        certificateDataSession.setCertificateData(cd);
        certificateDataSession.setCsr(csr);
        inSessionSignedCertMap.put(csr.getPemData(), certificateDataSession);
    }

    private X509Certificate signCsrAndGenerateCert(CSR csr, int forServer, BcPKCS10CertificationRequest certReq, X500Principal subject) throws Exception {
        X509Certificate caCert = keyService.loadCertificate(csr.getCaKeyId(), null);
        csr.setIssuerDN(caCert.getSubjectX500Principal().getName(X500Principal.RFC2253));
        Date exp = DateUtil.normalizeDateDayEnd(csr.getExpires());
        csr.setExpires(exp);
        String crlUrl = getWebContext().getStringSetting(DsSetting.DSCA_CRL_URL + "." + csr.getCaKeyId());
        if (crlUrl == null) {
            crlUrl = getWebContext().getStringSetting(DsSetting.DSCA_CRL_URL.getKey());
        }
        String aiaUrl = getWebContext().getStringSetting(DsSetting.DSCA_AIA_URL + "." + csr.getCaKeyId());

        if (caCert != null && exp.after(caCert.getNotAfter())) {
            log.warn("Certifcate max expiration " + exp + " can not exceed signer certificate expiration " +
                     caCert.getNotAfter() + " adjusting..");
            exp = caCert.getNotAfter();
        }

        KeyData sigKeyData = keyService.getKey(csr.getCaKeyId());
        byte[] signingPrivateKeyBytes = cryptoService.unwrapPrivateKey(sigKeyData);

        List<KeyPurposeId> extendedKeyUsageList = toExtendedKeyUsageList(forServer);
        X509v3CertificateBuilder certificateBuilder =
                CertUtil.prepareTLSCertBuilder(subject, certReq.getSubjectPublicKeyInfo(), caCert, exp, crlUrl,
                                               aiaUrl, extendedKeyUsageList, CertUtil.getSubjectAltNames(certReq));

        // HSM device used for signing must correspond to _issuer (CA) private key_ HSM device
        HSMDevice signingKeyHsmDevice = hsmDeviceService.getInitializedHSMDevice(sigKeyData.getHsmDeviceId());
        X509Certificate certificate = cryptoService.signX509certificate(certificateBuilder,
                                                                             signingKeyHsmDevice, signingPrivateKeyBytes, caCert.getPublicKey(), KeyService.sigAlgRSASHA256);

        byte[] issuerCertSubject = caCert.getSubjectX500Principal().getEncoded();
        byte[] subjectCertIssuer = certificate.getIssuerX500Principal().getEncoded();
        if (!Arrays.equals(issuerCertSubject, subjectCertIssuer)) {
            throw new RuntimeException("Generated certifcate Issuer DN " + HexUtils.toString(subjectCertIssuer) +
                                       " mismatch with supplied sig cert subject DN " + HexUtils.toString(issuerCertSubject));
        }
        return certificate;
    }

    private void validateCsr(CSR csr, BindingResult result) {
        if (Misc.isNullOrEmpty(csr.getPemData())) {
            result.addError(new FieldError("csr", "pemData", csr.getPemData(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.csrPem",
                                                                                                   locale()),
                                                                       locale())));
        }
        if (csr.getCaKeyId() == null) {
            result.addError(new FieldError("csr", "caKeyId", csr.getCaKeyId(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.cakey",
                                                                                                   locale()),
                                                                       locale())));
        }
        if (Misc.isNullOrEmpty(csr.getSigalg())) {
            result.addError(new FieldError("csr", "sigalg", csr.getSigalg(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.sigalg",
                                                                                                   locale()),
                                                                       locale())));
        }

        if (Misc.isNullOrEmpty(csr.getName())) {
            result.addError(new FieldError("csr", "name", csr.getName(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.name",
                                                                                                   locale()),
                                                                       locale())));
        }

        java.util.Date maxDate = null;
        int maxDays = KeyService.cMaxCryptoperiod;
        int maxDaysSett = super.getWebContext().getIntSetting(DsSetting.MAX_SSL_CERT_DAYS.getKey());
        if (maxDaysSett < maxDays && maxDaysSett > 0) {
            maxDays = maxDaysSett;
        }

        if (csr.getExpires() == null) {
            result.addError(new FieldError("csr", "expires", null, false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.expires",
                                                                                                   locale()),
                                                                       locale())));
        } else if (csr.getExpires().before(new java.util.Date())) {
            result.addError(new FieldError("csr", "expires", csr.getExpires(), false, null, null,
                                           localizationService.getText("err.invalid.value",
                                                                       localizationService.getText("err.must.be.future",
                                                                                                   locale()),
                                                                       locale())));
        } else if (csr.getExpires().after(
                maxDate = new java.util.Date(System.currentTimeMillis() + maxDays * KeyService.day))) {
            result.addError(new FieldError("csr", "expires", csr.getExpires(), false, null, null,
                                           localizationService.getText("err.invalid.value",
                                                                       localizationService.getText("err.max.allowed",
                                                                                                   DateUtil.formatDate(
                                                                                                           maxDate,
                                                                                                           this.getDateFormat()),
                                                                                                   locale()),
                                                                       locale())));
        }
    }

    @RequestMapping("/uploadCertificate.html")
    public ModelAndView uploadCertificate(@ModelAttribute(value = "uploadCert") CSR uploadData, HttpServletRequest request, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caEdit)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("ca/uploadCertificate");
        mav.addObject("uploadCert", uploadData);
        CertificateData cd = null;
        boolean replace = "true".equals(request.getParameter("replace"));
        X509Certificate root = null;

        if (request.getParameter("uploadBtn") != null) {
            X509Certificate[] certificates = null;
            if (uploadData.getUploadData() == null &&
                (uploadData.getUploadFile() == null || uploadData.getUploadFile().getSize() < 400)) {
                result.addError(new FieldError("uploadCert", "uploadFile", null, false, null, null,
                                               localizationService.getText("err.required", localizationService.getText(
                                                       "text.uploadfile", locale()), locale())));
            } else {
                try {
                    byte[] data = uploadData.getUploadData() != null ? Base64.decode(
                            uploadData.getUploadData()) : uploadData.getUploadFile().getBytes();


                    certificates = KeyService.parseX509PemHeadersSafeCertificates(data, null);
                    if (certificates.length != 1) {
                        result.addError(new FieldError("uploadCert", "uploadFile", null, false, null, null,
                                                       localizationService.getText("err.invalid",
                                                                                   localizationService.getText(
                                                                                           "err.must.contain.one.certificate",
                                                                                           locale()), locale())));
                    }
                } catch (Exception e) {
                    log.error("Failed to parse certificate data", e);
                    result.addError(new FieldError("uploadCert", "uploadFile", null, false, null, null,
                                                   localizationService.getText("err.invalid",
                                                                               localizationService.getText(
                                                                                       "err.must.be.valid.x509",
                                                                                       locale()), locale())));
                }
            }

            if (Misc.isNullOrEmpty(uploadData.getName())) {
                result.addError(new FieldError("uploadCert", "name", uploadData.getName(), false, null, null,
                                               localizationService.getText("err.required",
                                                                           localizationService.getText("text.name",
                                                                                                       locale()),
                                                                           locale())));
            }

            // validate if signed by scheme roots or intermediates
            if (!result.hasErrors() && !"false".equalsIgnoreCase("certUpload.validateSchemeRootSigned")) {
                X509Certificate cx = certificates[0];
                boolean rootfound = false;
                List<X509Certificate> rootsAndImed = new java.util.LinkedList<X509Certificate>();
                rootsAndImed.addAll(keyService.getSchemeRoots());
                rootsAndImed.addAll(keyService.getSchemeIntermediates());

                for (X509Certificate scrx : rootsAndImed) {
                    if (scrx.getSubjectX500Principal().getName(X500Principal.RFC2253).
                            equals(cx.getIssuerX500Principal().getName(X500Principal.RFC2253))) {
                        rootfound = true;
                        root = scrx;
                        try {
                            cx.verify(scrx.getPublicKey());
                            break;
                        } catch (SignatureException se) {
                            log.error(
                                    "Failed to validate certifcate with schemeroot or intermed " + scrx.getSubjectDN() +
                                    " invalid signature", se);
                            result.addError(new FieldError("uploadCert", "uploadFile", null, false, null, null,
                                                           localizationService.getText("err.invalid",
                                                                                       localizationService.getText(
                                                                                               "err.signature.mismatch",
                                                                                               locale()), locale())));

                        } catch (Exception e) {
                            log.error(
                                    "Failed to validate certifcate with schemeroot or intermed " + scrx.getSubjectDN() +
                                    " invalid signature", e);
                            result.addError(new FieldError("uploadCert", "uploadFile", null, false, null, null,
                                                           localizationService.getText("err.signature.validation.error",
                                                                                       locale())));

                        }
                    }
                } // for
                if (!rootfound) {
                    log.error("Failed to validate certifcate with schemeroot or intermed " + cx.getIssuerDN() +
                              " no such root or intermed found");
                    result.addError(new FieldError("uploadCert", "uploadFile", null, false, null, null,
                                                   localizationService.getText("err.signature.validation.failed.noroot",
                                                                               cx.getIssuerDN(), locale())));
                }


                if (!result.hasErrors()) {
                    cd = persistenceService.getCertificateDataBySDN(
                            cx.getSubjectX500Principal().getName(X500Principal.RFC2253));
                    if (cd != null && !replace) {
                        uploadData.setSubjectDN(cx.getSubjectX500Principal().getName(X500Principal.RFC2253));
                        if (uploadData.getUploadFile() != null) {
                            uploadData.setUploadData(Base64.encode(uploadData.getUploadFile().getBytes(), 10000));
                            uploadData.setUploadFileName(uploadData.getUploadFile().getOriginalFilename());
                        }
                        mav.addObject("duplicate", "true");
                        return mav;
                    }
                }
            }

            if (!result.hasErrors()) {
                X509Certificate cx = certificates[0];
                boolean newcd = false;
                if (cd == null) {
                    cd = new CertificateData();
                    newcd = true;
                }

                cd.setExpires(cx.getNotAfter());
                cd.setName(uploadData.getName());
                cd.setIssued(cx.getNotBefore());
                cd.setStatus(DSModel.CertificateData.Status.cValid);
                cd.setIssuerDN(cx.getIssuerX500Principal().getName(X500Principal.RFC2253));
                cd.setSubjectDN(cx.getSubjectX500Principal().getName(X500Principal.RFC2253));
                StringBuilder buf = new StringBuilder();
                buf.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
                buf.append(Base64.encode(cx.getEncoded(), 76));
                buf.append("\r\n" + KeyService.END_CERTIFICATE);
                cd.setX509data(buf.toString());
                persistenceService.saveOrUpdate(cd);
                log.info((newcd ? "New" : "Overwrite existing") + " certificate " + cd.getSubjectDN() +
                         " saved to store with id " + cd.getId() + " and name " + cd.getName());
                this.auditLogService.logAction(AuthorityUtil.getUser(), cd,
                                               newcd ? DSModel.AuditLog.Action.INSERT : DSModel.AuditLog.Action.UPDATE,
                                               (newcd ? "New" : "Overwrite existing") + " certificate " +
                                               cd.getSubjectDN() + " uploaded " + cd.getId());
                mav.addObject("certificate", cd);
                mav.addObject("msg", localizationService.getText("certificate.upload.success", locale()));

                if (root != null) {
                    buf.append("\r\n");
                    buf.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
                    buf.append(Base64.encode(root.getEncoded(), 76));
                    buf.append("\r\n" + KeyService.END_CERTIFICATE);
                    mav.addObject("chain", buf.toString());
                }
                clearContextAttributeStartsWith(new String[]{"inCertificateList_cached"});

            }
        }

        return mav;
    }

    @RequestMapping("/certificateDownload.html")
    public ModelAndView dnlCertificate(@RequestParam("id") Long id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.caView)) {
            return unauthorized(request);
        }

        boolean chain = "true".equals(request.getParameter("chain"));
        boolean p7 = "true".equals(request.getParameter("p7"));
        CertificateData cert = persistenceService.getPersistableById(id, CertificateData.class);

        if (cert != null) {
            // get root
            if (chain) {
                byte[] cdraw = Base64.decode(KeyService.parsePEMCert(cert.getX509data(), false));
                X509Certificate x509cert = KeyService.parseX509Certificates(cdraw, null)[0];

                List<X509Certificate> cchain = new java.util.ArrayList<X509Certificate>(2);
                cchain.add(x509cert);

                List<X509Certificate> schemeImed = keyService.getSchemeIntermediates();
                // find correct imed

                X509Certificate imed = null;
                for (X509Certificate imedx : schemeImed) {
                    if (imedx.getSubjectX500Principal().getName(X500Principal.RFC2253).equals(
                            x509cert.getIssuerX500Principal().getName(X500Principal.RFC2253))) {
                        imed = imedx;
                        cchain.add(imed);
                        break;
                    }
                }

                List<X509Certificate> schemeRoots = keyService.getSchemeRoots();
                // find correct root...
                for (X509Certificate rootx : schemeRoots) {
                    if (rootx.getSubjectX500Principal().getName(X500Principal.RFC2253).equals(
                            imed != null ? imed.getIssuerX500Principal().getName(
                                    X500Principal.RFC2253) : x509cert.getIssuerX500Principal().getName(
                                    X500Principal.RFC2253))) {
                        cchain.add(rootx);
                        break;
                    }
                }

                if (p7) {
                    response.setContentType("application/x-pkcs7-certificates");
                    String fileName = cert.getName() + "-chain.p7b";
                    response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                    byte[] data = KeyService.createP7B(cchain);
                    response.getOutputStream().write(data);
                } else {
                    String fileName = cert.getName() + "-chain.crt";
                    response.setContentType("application/pkix-cert");
                    response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                    //response.getWriter().write(cert.getX509data());
                    int count = 0;
                    for (X509Certificate certx : cchain) {
                        count++;
                        if (count > 1) {
                            response.getWriter().write("\n");
                        }
                        response.getWriter().write(KeyService.BEGIN_CERTIFICATE);
                        response.getWriter().write("\n");
                        response.getWriter().write(Base64.encode(certx.getEncoded()));
                        response.getWriter().write("\n");
                        response.getWriter().write(KeyService.END_CERTIFICATE);
                    }
                }
            } else {
                String fileName = cert.getName() + ".crt";
                response.setContentType("application/pkix-cert");
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                response.getWriter().write(cert.getX509data());
            }

            this.auditLogService.logAction(AuthorityUtil.getUser(), cert, DSModel.AuditLog.Action.VIEW,
                                           "Download certificate " + cert.getId());
            return null;
        }

        response.sendError(HttpServletResponse.SC_NOT_FOUND, "Certificate was not found");
        return null;
    }

    /**
     * Converts DS UI-specific extendedKeyUsageIndicator to Bouncy-castle readable.
     */
    private static final List<KeyPurposeId> toExtendedKeyUsageList(int forServer) {
        List<KeyPurposeId> extendedKeyUsageList = new ArrayList<>();
        if (forServer > 0) {
            if (forServer % 2 == 0) {
                extendedKeyUsageList.add(KeyPurposeId.id_kp_clientAuth);
            }
            if (forServer % 3 == 0) {
                extendedKeyUsageList.add(KeyPurposeId.id_kp_serverAuth);
            }
        }
        return extendedKeyUsageList;
    }

}
