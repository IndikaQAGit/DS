package com.modirum.ds.mngr.model.ui.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportsFilter {
    private String type;
    private Integer paymentSystemId;
    private String locale;
    private String timezone;
    private Date startDate;
    private String startDateHour;
    private String startDateMinute;
    private Date endDate;
}
