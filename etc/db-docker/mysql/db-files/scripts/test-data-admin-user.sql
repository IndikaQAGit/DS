
-- Add an administrator user
-- username: admin
-- password: pass1234
INSERT INTO ds_users (id, loginname, password, oldpasswords, lastPassChangeDate, loginAttempts, lockedDate, firstName, lastName, status, phone, email, locale, factor2Authmethod, factor2AuthDeviceId, factor2Data1, factor2Data2, factor2Data3, factor2Data4)
VALUES (10000,'admin','+t+dUx5cl1rGyIWLlzEoD7CGJzos25Cp+uFr9mBKbiA=','rbSmdWVjAJbfqyMltpLCDwz+2T3pgAHJLl0HcF0pjl0=+t+dUx5cl1rGyIWLlzEoD7CGJzos25Cp+uFr9mBKbiA=','2030-01-01 06:56:05.827000',0,NULL,'Admin','Admin','A','','test@modirum.com','',NULL,NULL,NULL,NULL,NULL,NULL);

-- Add all roles to admin user
INSERT INTO ds_userroles (user_id, role)
VALUES (10000,'acquirerEdit'),
  (10000,'acquirerView'),
  (10000,'adminSetup'),
  (10000,'auditLogView'),
  (10000,'caEdit'),
  (10000,'caView'),
  (10000,'issuerEdit'),
  (10000,'issuerView'),
  (10000,'merchantsEdit'),
  (10000,'merchantsView'),
  (10000,'panView'),
  (10000,'recordsView'),
  (10000,'textEdit'),
  (10000,'textView'),
  (10000,'userEdit'),
  (10000,'userView')
  (10000,'keyManage');